/*****************************************************************************************************************
Description: This trigger has a lot of processes. When an opportunity is before insert we check the account related to
             the Opportunities Bill To Only value to make sure an opportunity can be created. And then we set the owner percentage
             on the Opportunity. These field is used to copy and set the owner percentage when an Opportunity changes owners

             Before update: Check the Bill To Only value on the related Account. Update the Owners Commission Percentage on the
             Opportunity. This will either just update the new percentage with the current owner or if a change of owner is happening
             Then the new owner will get the old owners percentage, which is stored in the Opportuntiy Owner Percentage value
             Then we update the Opportunity Commission Percentage roll up value

             After Update:  Update the Opportunity Owner Percentage with the current owner percentage. Update the sales team
             members commission amount if the opportunity amount has changed. Update the Account Roll up values if Amount or NEP has changed

             After Insert:  Insert the owner of the Opportunity as a Sales Team Member. And update the Account Roll Up Revenue values with
             the values of the Opportunities Amounts and NEP

             After Delete: Remove the Opportunities Amount and NEP values from the Account Roll Up values.

Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   Created Trigger
Version 2:   Added new call in before update to restrict users from updating Closed Opportunities
Version 3:   Added new call in after update to update goals of Opportunity Team members.
Version 4:   Added new call in before insert to generate source key for Opportunity if one DOES NOT exist
Version 5:   Added new call in before insert to update opportunity owner same as account owner if the current user role is AA/AC/AD/PC
Version 6:   Added new call in before udpate to update Primary Product for SSD and Digital opps
Version 7:   Jagan: Added new call in before udpate and before insert to update territory hierarchy from opp owner
Version 8:   Seth: Added new call in before update and before insert to check if user is apart of Account Team
Version 9:   Stephen: Added new call in before update and before insert to update opportunity stage from integration status
Version 10:  Stephen: Added new call in before update and before insert to set opportunity fiscal year from matching Calendar
********************************************************************************************************************/

trigger OpportunityTrigger on Opportunity (after update, after insert, before update, before insert, after delete) {
    /***********************************************************************
    Trigger is used for BEFORE the Opportunity object has been through a process
    ************************************************************************/
    if(Trigger.isBefore){
        /***********************************************************************
        Trigger is used for BEFORE an Opportunity object has been INSERTED
        ************************************************************************/
        if(Trigger.isInsert){
            

            //method used to update Opportunity's related Calendar and Calendar Fiscal Year
            OpportunityFiscalYearCalculator.calculateFiscalYearCalendar(Trigger.New);
            //Method to update Stage from integration Status
            OpportunityTriggerHelper.beforeUpsertSetStageFromStatus(Trigger.New, null, null);
            
            //Method to check some validation rules on Account
            OpportunityTriggerHelper.beforeInsertUpdateAccountValidation(Trigger.New, TRUE);

            OpportunityTriggerHelper.beforeInsertSourceKeyGenerate(Trigger.New);
            

            //Method called to insert owner commission percentage for the opportunity
            OpportunityTriggerHelper.beforeInsertOwnerPercentage(Trigger.New);

            OpportunityTriggerHelper.beforeInsertInstoreFSIFields(Trigger.New);
            
            //associate instore parent opportunities.
            //OpportunityTriggerHelper.associateISOPChildOpportunities(Trigger.New,null);
            
            //Update opportunity owner same as account owner if the current user role is AA/AC/AD/PC
            OpportunityTriggerHelper.beforeInsertModifyOppOwner(Trigger.new);
            
            //Copy territory fields from owner to the opportunity
            OpportunityTriggerHelper.beforeinsertupdateCopyTerritoryFields(Trigger.new,null,null);
            
            ParentOpportunityCreator.createParentOpportunity(Trigger.New);
            
            //Method called to make Source key as a null field, before opprtunity gets inserted.
            //OpportunityTriggerHelper.beforeInsertSourceKeyNull(Trigger.New);

        }
        /***********************************************************************
        Trigger is used for BEFORE an Opportunity object has been UPDATED
        ************************************************************************/
        else if(Trigger.isUpdate){
            //method used to update Opportunity's related Calendar and Calendar Fiscal Year
            OpportunityFiscalYearCalculator.calculateFiscalYearCalendar(Trigger.New);
            //Method to update Stage from integration Status
            OpportunityTriggerHelper.beforeUpsertSetStageFromStatus(null, Trigger.newMap, Trigger.oldMap);
            
            OpportunityTriggerHelper.beforeInsertUpdateAccountValidation(Trigger.New, FALSE);
            
            //OpportunityTriggerHelper.beforeInsertBillToOnlyCheck(Trigger.New);

            //method used to update owner commission percentage for the opportunity
            OpportunityTriggerHelper.beforeUpdateOwnerPercentage(Trigger.New, Trigger.oldMap);

            //OpportunityTriggerHelper.afterUpdateOpportunityOwner(Trigger.New, Trigger.oldMap);

            //Method used to update opportunity commission percentage
            OpportunityTriggerHelper.beforeUpdateCommissionPercentage(Trigger.New);

            OpportunityTriggerHelper.beforeUpdateInstoreFSIFields(Trigger.New, Trigger.oldMap);
            
            //Method to restrict updating opportunity after it is closed
            OpportunityTriggerHelper.beforeUpdateLockfields(Trigger.NewMap, Trigger.OldMap);
            // associate instore parent opportunities.
            // OpportunityTriggerHelper.associateISOPChildOpportunities(Trigger.New,Trigger.OldMap);
            //Update primary Category
            OpportunityTriggerHelper.beforeUpdatePrimaryProduct(trigger.new,trigger.oldmap);
            
            //Copy territory fields from owner to the opportunity
            OpportunityTriggerHelper.beforeinsertupdateCopyTerritoryFields(trigger.new,Trigger.newmap,Trigger.oldmap);
        }

    }
    /***********************************************************************
    Trigger is used for AFTER the Opportunity Credit object has been through a process
    ************************************************************************/
    if(Trigger.isAfter) {
        /***********************************************************************
        Trigger is used for AFTER an Opportunity object has been UPDATED
        ************************************************************************/
        if(Trigger.isUpdate){
            //method used to update the Opportunity Owner
            OpportunityTriggerHelper.afterUpdateOpportunityOwner(Trigger.New, Trigger.oldMap);

            //Method used to update Sales Team Commission Amount if Opportunity Amount has changed
            OpportunityTriggerHelper.afterUpdateOpptyTeamMemberAmount(Trigger.New,Trigger.oldMap);

            //Method used to update the Roll Up Revenue for the Accounts related to Opportuntiy
            OpportunityTriggerAccountRollUpRevenue.afterUpdateRollUpRevenue(Trigger.New, Trigger.oldMap);
            //Method to update Paced_Production_Revenue__c and Paced_Space_Revenue__c in Goals related to SalesTeam
            //OpportunityTriggerHelper.updateGoalsafterUpdate(Trigger.NewMap, Trigger.OldMap);
        }
        /***********************************************************************
        Trigger is used for AFTER an Opportunity object has been INSERTED
        ************************************************************************/
        if(Trigger.isInsert){
            //Method used to update Roll Up Revenue for the Accounts related to Opportunity
            OpportunityTriggerAccountRollUpRevenue.afterUpdateRollUpRevenue(Trigger.New, Trigger.oldMap);
            
            //Method used to insert the Opportunity Owner as a sales team member
            OpportunityTeamMemberTriggerHelper.isopportunityCreate = TRUE;
            OpportunityTriggerHelper.isOpportunityInStoreCreate = TRUE;
            OpportunityTriggerHelper.afterInsertOpportunityOwner(Trigger.New);
        }
        /***********************************************************************
        Trigger is used for AFTER an Opportunity object has been DELETED
        ************************************************************************/
        if(Trigger.isDelete){
            //Method used to remove old Opportunity Amount from Roll Up Revenue for related Accounts
            OpportunityTriggerAccountRollUpRevenue.afterDeleteRollUpRevenue(Trigger.old);
            
        }
    }
}