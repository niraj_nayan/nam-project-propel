/*****************************************************************************************************************
Description: This trigger is used before an Account is updated. This trigger will update the roll up revenue
             and the NEP roll up values for Division Accounts and Corp Accounts
             
Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   1
********************************************************************************************************************/

trigger AccountTrigger on Account (before update, before insert,after update) {
    /***********************************************************************
    Trigger is used for before an account is updated and inserted
    ************************************************************************/
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            AccountTriggerHelper.beforeInsertSourceKeyGenerate(Trigger.New);
            AccountTriggerHelper.beforeinsertupdateCopyTerritoryFields(trigger.new,null,null);
        }
        else if(Trigger.isUpdate){
            
            //This trigger method is used to Roll Up the revenue from Opportunities to Division Accounts and then Roll Up
            //the revenue from Division Accounts to the Corp Account the Division accounts are related to
            AccountTriggerHelper.beforeUpdateRollUpRevenue(Trigger.New, Trigger.oldMap);
            AccountTriggerHelper.beforeinsertupdateCopyTerritoryFields(trigger.new,Trigger.newmap,Trigger.oldmap);
        }
    }
    else if(trigger.isAfter && trigger.isUpdate)
        AccountTriggerHelper.afterUpdateChangeCorpOwner(Trigger.New, Trigger.oldMap); 
    
}