/*****************************************************************************************************************
Description: Assign user to territory when user is updated with primary territory
             
Created on:  23nd October 2015
Created by:  Jagan
Version#     Description
----------------------------
1:           Created trigger which calls the UserTriggerHelper class
2:           Added calls for fedid check and source key generate
3:           Added call for userTerritory2Association adjustment
********************************************************************************************************************/

trigger UserTrigger on User (before insert, before update, after update) {

    if(Trigger.isBefore){
        if(Trigger.isInsert){
            userTriggerHelper.beforeInsertFederationIdentifierValidation(Trigger.New);
            userTriggerHelper.beforeInsertSourceKeyGenerate(Trigger.New);
            userTriggerHelper.beforeUpsertUpdateUserAssociations(Trigger.New);
        }
        if(Trigger.isUpdate){
            userTriggerHelper.beforeInsertFederationIdentifierValidation(Trigger.New);
            userTriggerHelper.beforeInsertSourceKeyGenerate(Trigger.New);
            //Check userTerritory2Association before deactivation
            userTriggerHelper.adjustUserTerritoryBeforeUpdate(trigger.newmap,trigger.oldmap);
            userTriggerHelper.beforeUpsertUpdateUserAssociations(Trigger.New);
        }
    }
    if(Trigger.isAfter){
        if(trigger.isUpdate){
            //Check userTerritory2Association before deactivation
            userTriggerHelper.adjustUserTerritoryAfterUpdate(trigger.newmap,trigger.oldmap);
        }
    }

    

}