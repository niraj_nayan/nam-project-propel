/***
Version 1: SFIT- 719 :- Share the Form Header with the Primary territory hierarchy of the Owner
**/
trigger FormHeaderTrigger on Form_Header__c (after insert, after update) {
    
     if(Trigger.isAfter){
        if(trigger.isInsert){
            FormHeaderTriggerHelper.shareFormHeader(trigger.newMap,null);
        }
        if(trigger.isUpdate){
            FormHeaderTriggerHelper.shareFormHeader(trigger.newMap,trigger.oldmap);
        }
    }
    
}