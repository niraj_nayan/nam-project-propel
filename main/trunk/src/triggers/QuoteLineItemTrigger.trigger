/********************************************************************************************************************************************
Description: This trigger is called after a Quote record is created. It will copy the custom fields from product line item to Quote line item
             
Created on:  11/10/2015
Created by:  Jagan Gorre
Version 1:   Created trigger and helper class
********************************************************************************************************************/

trigger QuoteLineItemTrigger on QuoteLineItem (after insert) {

    QuoteLineItemTriggerHelper.CopyOppLineItemCustomFields(trigger.newmap);
    
}