/* CycleTrigger
 * Trigger to cause update on opportunities related to cycles for fiscal year calculation.
 * Author: Stephen Lane
 */
trigger CycleTrigger on Cycle__c (after insert, after update, before delete) {
    if(Trigger.isInsert || Trigger.isUpdate){
        CycleTriggerHelper.updateRelatedOpportunities(Trigger.new);
    }
    else if(Trigger.isDelete){
        CycleTriggerHelper.updateRelatedOpportunities(Trigger.old);
    }
}