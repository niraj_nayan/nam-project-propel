/* CalendarTrigger
 * Trigger to cause update on opportunities related to calendars for fiscal year calculation.
 * Author: Stephen Lane
 */
trigger CalendarTrigger on Calendar__c (after insert, after update, after delete) {
    if(Trigger.isInsert || Trigger.isUpdate){
        CalendarTriggerHelper.updateRelatedOpportunities(Trigger.new);
    }
    else if(Trigger.isDelete){
        CalendarTriggerHelper.updateRelatedOpportunities(Trigger.old);
    }  
}