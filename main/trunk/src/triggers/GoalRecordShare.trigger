/*****************************************************************************************************************
Description: Whenever a goal team record is inserted/update, get the goal team memeber 
and share the goal with goal team.

Created Date: 9/15/2016
Created by:  Kiran Madadi
Version 1:   Created trigger to share the goal record.

********************************************************************************************************************/

trigger GoalRecordShare on Goal_Team__c (After insert,After Update) {
    
    GoalRecordShareHelper ObjGoal = new GoalRecordShareHelper();
    if(Trigger.isAfter){
        if(trigger.isInsert){
            // calling the method to share the goal with new goal team
            objGoal.shareGoals (trigger.newMap,null);
        }
        if(trigger.isUpdate){
            //calling the method to share the goal with updated goal team
            objGoal.shareGoals (trigger.newMap,trigger.oldmap);
        }
    }
}