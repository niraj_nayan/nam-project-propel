/*****************************************************************************************************************
Description: Calls afterDeleteProdJunc method of  productJunctionTriggerHelper Helper class  to ipdates the 
             Product_Junction_Created_Updated_Time__c field of Related product when the junction record is deleted.
Created on:  11/24/2015
Created by:  Jagan
Version 1:   Created Product Junction Trigger
********************************************************************************************************************/
trigger ProductJunctionTrigger on Product_Junction__c (after delete) {

    productJunctionTriggerHelper.afterDeleteProdJunc(trigger.old);
    
}