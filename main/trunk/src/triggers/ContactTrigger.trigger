/*****************************************************************************************************************
Description: Trigger for Contact records. Trigger will do different actions based on when a contact
			 is inserted, updated, or deleted.
             
Created on:  4th November 2015
Created by:  Seth Martin
Version 1:   Created trigger which calls ContactTriggerHelper on before insert
********************************************************************************************************************/

trigger ContactTrigger on Contact (before insert, before update) {
	if(Trigger.isBefore){
		if(Trigger.isInsert){
			ContactTriggerHelper.beforeInsertSourceKeyGenerate(Trigger.New);
		}
		//if(trigger.isUpdate){
		//	ContactTriggerHelper.beforeInsertSourceKeyGenerate(Trigger.New);
		//}
	}
}