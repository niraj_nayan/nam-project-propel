/*****************************************************************************************************************
Description: Whenever a goal record is inserted/update, get the territories of the goal owner (?) and their parent territories
             and share the goal with all the users belonging to those territories. //THIS IS ON HOLD. NEEDS MORE DISCUSSION
             
Created on:  22nd October 2015
Created by:  Kavitha
Version 1:   Created trigger to share the goal record.
Version 2:   Jagan: dded new call for before insert/update to map assinged to field to Owner
Version 3:   Jagan: added new call for before insert/update to copy territory fields from owner
********************************************************************************************************************/


trigger Goal_Record_share on Goal__c (after insert, after update, before update, before insert) {
 
    goal_record_share_helper objGoal = new goal_record_share_helper();
    
    if(Trigger.isBefore){
        //map assinged to field to Owner
        //objGoal.beforeinsertupdategoalowner(Trigger.New);
        
        if(Trigger.isInsert){
            objGoal.beforeInsertSourceKeyGenerate(Trigger.New);
            objGoal.beforeinsertupdateCopyTerritoryFields(Trigger.new,null,null);
        }
        else{
            objGoal.beforeinsertupdateCopyTerritoryFields(Trigger.new,trigger.newmap,trigger.oldmap);
        }
    }

    if(Trigger.isAfter){
        if(trigger.isInsert){
            objGoal.shareGoals (trigger.newMap,null);
        }
        if(trigger.isUpdate){
            objGoal.shareGoals (trigger.newMap,trigger.oldmap);
        }
    }

    //if(Trigger.isBefore){
    //    if(Trigger.isUpdate){
    //        objGoal.beforeInsertSourceKeyGenerate(Trigger.New);
    //    }
    //}
}