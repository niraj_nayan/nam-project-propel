trigger ProductTrigger on Product2 (before insert) {
	if(Trigger.isBefore){
		if(Trigger.isInsert){
			ProductTriggerHelper.beforeInsertSourceKeyGenerate(Trigger.New);
		}
	}
}