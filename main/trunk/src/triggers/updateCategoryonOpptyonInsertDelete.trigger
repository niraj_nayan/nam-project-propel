/*****************************************************************************************************************
Description: Opportunity Category trigger to update "Category" field on opportunity (overrides with most recent value) whenever a new Opportunity category 
             is inserted/deleted
             
Created on:  22nd October 2015
Created by:  Jagan
Version 1:   Created trigger which calls the updateCategory method of the class OppCategoryTriggerHelper
********************************************************************************************************************/

trigger updateCategoryonOpptyonInsertDelete on Opportunity_Category__c (after insert,  after delete) {

    if(!trigger.isDelete)
        OppCategoryTriggerHelper.updateCategory(trigger.new);
    else
        OppCategoryTriggerHelper.updateCategory(trigger.old);
        
}