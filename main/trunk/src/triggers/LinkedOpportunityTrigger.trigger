/*****************************************************************************************************************
Description: This trigger is called after a Linked Opportunity is created. For insert we create another linked opportunity
             to link the in the other direction of main to link. Example: Link from A to B insert link from B to A
             We update the links when the opportunity is changed so they are changed on both link directions
             On delete we will remove the other link direction
             
Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   1
********************************************************************************************************************/

trigger LinkedOpportunityTrigger on Linked_Opportunity__c (after insert, after update, after delete) {

    if(trigger.isAfter) {
        if(trigger.isInsert){
            LinkedOpportunityTriggerHelper.afterInsertLinkedOpportunity(Trigger.New);
        }
        else if(trigger.isUpdate){
            LinkedOpportunityTriggerHelper.afterUpdateLinkedOpportunity(Trigger.New,Trigger.oldMap);
        }
        else if(trigger.isDelete){
            LinkedOpportunityTriggerHelper.afterDeleteLinkedOpportunity(Trigger.oldMap);
        }
    }

}