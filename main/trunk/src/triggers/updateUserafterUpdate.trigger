/*****************************************************************************************************************
Description: Update User's territory hierarchy fields
             
Created on:  26th October 2015
Created by:  Jagan
Version 1:   Created trigger which calls the UserTriggerHelper class
Version 2:   Added new action and call to insert users to Account Team
********************************************************************************************************************/

trigger updateUserafterUpdate on UserTerritory2Association (after insert,after update,after delete) {

    if(trigger.isInsert || trigger.isUpdate){
        userAssociationTriggerHelper.assignUsertoTerritory(trigger.new); 
        userAssociationTriggerHelper.addUsertoAccountTeam(trigger.new);
    }
    else{
        userAssociationTriggerHelper.assignUsertoTerritory(trigger.old);
        userAssociationTriggerHelper.removeUserfromAccountTeam(trigger.old);
    }
}