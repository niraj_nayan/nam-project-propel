/*****************************************************************************************************************
Description: This trigger has mulitple processes. Before update and before insert is used to update the team members
             commission values. After update is used to update the opportunity commission percentage
             After Insert is used update the opportunity commission percentage
             
Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   Created the trigger
Version 2:   Jagan: Added new call to update goals when a team member is deleted
********************************************************************************************************************/

trigger OpportunityTeamMemberTrigger on OpportunityTeamMember (before insert, before update, after update, after insert) {
    /***********************************************************************
    Trigger is used for before the Opportunity Team Member(Sales Team Member) object has been through a process
    ************************************************************************/
    if(Trigger.isBefore) {
        /***********************************************************************
        Trigger is used for before an Opportunity Team Member(Sales Team Member) object has been updated
        ************************************************************************/
        if(Trigger.isUpdate){
            //This method is used to update the Commission Value on the calculate 
            //the sales team member commission value based on commission percentage
            OpportunityTeamMemberTriggerHelper.beforeUpdateCommissionValue(Trigger.New);
            OpportunityTeamMemberTriggerHelper.beforeinsertUpdateHierarchy(Trigger.New);
        }
        /***********************************************************************
        Trigger is used for before an Opportunity Team Member(Sales Team Member) object has been inserted
        ************************************************************************/
        else if(Trigger.isInsert){
            //This method is used to update the Commission Value on the calculate 
            //the sales team member commission value based on commission percentage
            OpportunityTeamMemberTriggerHelper.beforeUpdateCommissionValue(Trigger.New);
            //Update territory hierarchy for the opportunity team member
            OpportunityTeamMemberTriggerHelper.beforeinsertUpdateHierarchy(Trigger.New);
        }
    }  

    /***********************************************************************
    Trigger is used for after the Opportunity Team Member(Sales Team Member) object has been through a process
    ************************************************************************/
    if(Trigger.isAfter){
        /***********************************************************************
        Trigger is used for after an Opportunity Team Member(Sales Team Member) object has been updated
        ************************************************************************/
        if(Trigger.isUpdate){
            //This method is used to update the Commission percentage on the related Opportunity
            //which is the sum of all of the Sales Team Members commission percentage ONLY ON CHANGED PERCENTAGES
            OpportunityTeamMemberTriggerHelper.afterUpdateOpportunityCommissionPercentage(Trigger.New, Trigger.oldMap);
            //Method to update goals related to opportunity team member when commission percentage is changed.
            //COMMENTED BASED ON SFIT-971
            //OpportunityTeamMemberTriggerHelper.afterupdateGoalsUpdate(Trigger.New, Trigger.oldMap);
            
        }
        /***********************************************************************
        Trigger is used for after an Opportunity Team Member(Sales Team Member) object has been inserted
        ************************************************************************/
        else if(Trigger.isInsert){
            //This method is used to update the Commission percentage on the related Opportunity
            //which is the sum of all of the Sales Team Members commission percentage

            OpportunityTeamMemberTriggerHelper.afterOpportunityCommissionPercentage(Trigger.New);
        }
        //else if(Trigger.isDelete){
         // OpportunityTeamMemberTriggerHelper.afterDeleteUpdateGoals(Trigger.old);
        //}

    }
}