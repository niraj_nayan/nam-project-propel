/*****************************************************************************************************************
Description: This trigger is used to run a process on a Rate Card before update, after insert, and after update
             
Created on:  10/23/2015
Created by:  Pavan Kumar
Version 1:   
********************************************************************************************************************/

trigger RateCardTrigger on Rate_Card__c (before update,after insert,after update) {
   if(Trigger.isBefore) {
       RateCardTriggerHelper.beforeUpdateActions(Trigger.New,Trigger.OldMap);    
   }
   else {
       if(Trigger.isInsert)
       		RateCardTriggerHelper.afterInsertActions(Trigger.New);
       else 
            RateCardTriggerHelper.afterUpdateActions(Trigger.New,Trigger.OldMap);		
   }
}