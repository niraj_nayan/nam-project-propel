/*****************************************************************************************************************
Description: This trigger is used after a record is inserted. The trigger is used to call the approval process 
			 for linked opportunities
             
Created on:  
Created by:  Seth Martin
Version 1:   
********************************************************************************************************************/

trigger OpportunityCreditTrigger on Opportunity_Credit__c (after insert) {
	/***********************************************************************
	Trigger is used for after the Opportunity Credit object has been through a process
	************************************************************************/
	if(Trigger.isAfter) {
		/***********************************************************************
		Trigger is used for after an Opportunity Credit object has been inserted
		************************************************************************/
		if(Trigger.isInsert){
			//This method is used to start the Approval process for the Opportunity Credit
			//The approval process should only start when an Opportunity Credit record is created
			OpportunityCreditTriggerHelper.beforeInsertApproval(Trigger.New);
		}
	}

}