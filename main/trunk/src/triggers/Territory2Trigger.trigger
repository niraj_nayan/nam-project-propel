/* Territory2Trigger
 * Trigger for preventing Territory2 names from being more than 30 characters long.
 * Author: Stephen Lane
 */

trigger Territory2Trigger on Territory2 (before insert, before update) {
	Territory2TriggerHelper.checkNameLength(Trigger.new);
}