/*****************************************************************************************************************
Description: This trigger is called after a record has been updated. When a record is update we need to update the Opportunity
             that the line item belongs to
             
Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   Created trigger
Version 2:   Added Before insert,before update actions and updating Charge Type Category text with with Charge Type Category formula field.
Version 3:   Jagan: Added new call to recalculate the goal when opportunity product is deleted
********************************************************************************************************************/

trigger OpportunityLineItemTrigger on OpportunityLineItem (after update, after insert, before update, before insert) {

    /***********************************************************************
    Trigger is used for after the Opportunity Line Item(Opportunity Product) object has been through a process
    either insert, update, or delete
    ************************************************************************/
    if(Trigger.isAfter){
        /***********************************************************************
        Trigger is used for after an Opportunity Line Item(Opportunity Product) object has been inserted
        ************************************************************************/
        if(Trigger.isInsert){
            OpportunityProductTriggerHelper.afterUpsertCloneForISOPParent(Trigger.New);
        }
        /***********************************************************************
        Trigger is used for after an Opportunity Line Item(Opportunity Product) object has been updated
        ************************************************************************/
        if(Trigger.isUpdate){
            OpportunityProductTriggerHelper.afterProductOpportunityUpdate(Trigger.New, Trigger.oldMap);
            OpportunityProductTriggerHelper.afterUpsertCloneForISOPParent(Trigger.New);
        }
        /***********************************************************************
        Re-calculate goals for the deleted opportunity products
        //COMMENTED BASED ON SFIT-971
        ************************************************************************/
        //if(trigger.isDelete){
         //   OpportunityProductTriggerHelper.afterDeleteUpdateGoals(Trigger.Old);
        //}
    }
    else {
        for(OpportunityLineItem oli:trigger.new){
            oli.Charge_Type_Category_text__c = oli.Charge_Type_Categorty__c;
        }
    }
}