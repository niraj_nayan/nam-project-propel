<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Agency_Account_Name</fullName>
        <description>Field Update action to update the Trade Account record Name based on the Trade/Agency Account and Division Account</description>
        <field>Name</field>
        <formula>Account_Division__r.Name + &apos; - &apos; +  Agency_Account__r.Name</formula>
        <name>Update Agency Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Agency Account Name</fullName>
        <actions>
            <name>Update_Agency_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow Rule to Update the Agency Account Record Name</description>
        <formula>OR(ISNEW(),ISCHANGED(Name),ISCHANGED( Account_Division__c ),ISCHANGED( Agency_Account__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
