<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FinApps_Account_Rejected</fullName>
        <description>FinApps Account Rejected</description>
        <protected>false</protected>
        <recipients>
            <recipient>adixit.consultant@newsamerica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/FinApps_Account_Rejected</template>
    </alerts>
    <alerts>
        <fullName>New_Account_in_FinApps_for_Review</fullName>
        <description>New Account in FinApps for Review</description>
        <protected>false</protected>
        <recipients>
            <recipient>adixit.consultant@newsamerica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/New_Account_in_FinApps_for_Review</template>
    </alerts>
    <alerts>
        <fullName>Prospect_Account_Approved_by_Billing_Management</fullName>
        <description>Prospect Account Approved by Billing Management</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Prospect_Account_Approved_by_Billing_Management</template>
    </alerts>
    <alerts>
        <fullName>Prospect_Account_Approved_by_Sales_Ops</fullName>
        <description>Prospect Account Approved by Sales Ops</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/New_Prospect_Account_Approval_Notification_Sales_Ops</template>
    </alerts>
    <alerts>
        <fullName>Prospect_Account_Rejected_by_Billing</fullName>
        <description>Prospect Account Rejected by Billing</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Prospect_Account_Rejected_by_Billing</template>
    </alerts>
    <alerts>
        <fullName>Prospect_Account_Rejected_by_Sales_Ops</fullName>
        <description>Prospect Account Rejected by Sales Ops</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/New_Prospect_Account_Rejection_Notification_Sales_Ops</template>
    </alerts>
    <alerts>
        <fullName>Prospect_Account_Submitted_for_FinApps</fullName>
        <description>Prospect Account Submitted for FinApps</description>
        <protected>false</protected>
        <recipients>
            <recipient>adixit.consultant@newsamerica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Prospect_Account_Submitted_for_FinApps</template>
    </alerts>
    <alerts>
        <fullName>Prospect_Account_Submitted_for_Review_by_Credit_Management</fullName>
        <description>Prospect Account Submitted for Review by Credit Management</description>
        <protected>false</protected>
        <recipients>
            <recipient>adixit.consultant@newsamerica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Prospect_Pending_Approval_by_Credit_Management</template>
    </alerts>
    <alerts>
        <fullName>Prospect_Account_Submitted_to_Credit_Team</fullName>
        <description>Prospect Account Submitted to Credit Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>adixit.consultant@newsamerica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Prospect_Account_Submitted_to_Credit</template>
    </alerts>
    <alerts>
        <fullName>Rejected_by_Billing</fullName>
        <description>Rejected by Billing</description>
        <protected>false</protected>
        <recipients>
            <recipient>adixit.consultant@newsamerica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/FinApps_Account_Not_Created</template>
    </alerts>
    <alerts>
        <fullName>Rejected_by_Credit_Team</fullName>
        <description>Rejected by Credit Team</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Prospect_Rejected_by_Credit</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Status_Effective_Date_Update</fullName>
        <description>When the Status field is populated/updated we will populate/update the Status Effective Date field</description>
        <field>Status_Effective_Date__c</field>
        <formula>TODAY()</formula>
        <name>Account Status Effective Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Status_to_Approved_by_Sales_Ops</fullName>
        <field>Status__c</field>
        <literalValue>Approved by Sales Ops</literalValue>
        <name>Account Status to Approved by Sales Ops</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Status_to_Rejected_by_Sales_Ops</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by Sales Ops</literalValue>
        <name>Account Status to Rejected by Sales Ops</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_by_Billing_Management</fullName>
        <field>Status__c</field>
        <literalValue>Approved by Billing Management</literalValue>
        <name>Approved by Billing Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_by_Credit_Management</fullName>
        <description>Field update when Credit Management team approves a Prospect Account</description>
        <field>Status__c</field>
        <literalValue>Approved by Credit Mgt</literalValue>
        <name>Approved by Credit Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Billing_Account_Creation</fullName>
        <field>Status__c</field>
        <literalValue>Pending Billing Account Creation</literalValue>
        <name>Pending Billing Account Creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Billing_Management</fullName>
        <description>Field Update Status to &quot;Billing Account Created&quot;</description>
        <field>Status__c</field>
        <literalValue>Pending Billing Management</literalValue>
        <name>Pending Billing Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Credit</fullName>
        <field>Status__c</field>
        <literalValue>Pending Credit</literalValue>
        <name>Pending Credit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Senior_Credit_Management</fullName>
        <description>Field update when Credit Management team approves a Prospect Account</description>
        <field>Status__c</field>
        <literalValue>Pending Senior Credit Management</literalValue>
        <name>Pending Senior Credit Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_to_Credit_Management</fullName>
        <description>Field update when Credit team approves a Prospect Account</description>
        <field>Status__c</field>
        <literalValue>Pending Credit Management</literalValue>
        <name>Pending to Credit Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_By_Billing</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by Billing</literalValue>
        <name>Rejected By Billing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_by_Billing_Management</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by Billing Management</literalValue>
        <name>Rejected by Billing Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_by_Credit</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by Credit</literalValue>
        <name>Rejected by Credit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_by_Credit_Management</fullName>
        <description>Field update when Prospect Account rejected by Credit Management</description>
        <field>Status__c</field>
        <literalValue>Rejected by Credit Management</literalValue>
        <name>Rejected by Credit Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_by_Senior_Credit_Management</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by Senior Credit Management</literalValue>
        <name>Rejected by Senior Credit Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bill_To</fullName>
        <description>When Account.Type=&quot;AOR&quot; then Bill To set to true</description>
        <field>Bill_To_Only__c</field>
        <literalValue>1</literalValue>
        <name>Update Bill To</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_PendingSalesOps</fullName>
        <description>Used when a new Prospect Account is created in the system</description>
        <field>Status__c</field>
        <literalValue>Pending Sales Ops</literalValue>
        <name>Update Status to &quot;Pending Sales Ops&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Status Update</fullName>
        <actions>
            <name>Account_Status_Effective_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Status field is populated/updated we will populate/update the Status Effective Date field</description>
        <formula>NOT(ISBLANK(TEXT(Status__c))) &amp;&amp; (ISNEW() || ISCHANGED(Status__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Checked Bill To for AOR</fullName>
        <actions>
            <name>Update_Bill_To</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>AOR</value>
        </criteriaItems>
        <description>Check Bill to checkbox when account is created for Division agency and type is AOR</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
