<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Digital_Quote_Request_Notification</fullName>
        <description>Digital Quote Request Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Digital_Production_Team</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Digital_Opportunity_Quote_Request_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_for_Late_Buy_Opportunities</fullName>
        <description>Notification for Late Buy Opportunities</description>
        <protected>false</protected>
        <recipients>
            <recipient>Core-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Specialty-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Support-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Temp Access</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Finance</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Late_Buy_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Extended_Execution_Date_on_Opportunity_to_Opportunity_Team_Membe</fullName>
        <description>Notification of Extended Execution Date on Opportunity to Opportunity Team Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>Core-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Specialty-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Support-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Temp Access</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Execution_Date_Extended</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Open_Opportunity_Nearing_Execution_Date</fullName>
        <description>Notification of Open Opportunity Nearing Execution Date</description>
        <protected>false</protected>
        <recipients>
            <recipient>Core-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Specialty-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Support-Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Temp Access</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Open_Opportunity_Nearing_Execution_Date</template>
    </alerts>
    <alerts>
        <fullName>Notify_Merch_Operations_of_Signed_LOC</fullName>
        <ccEmails>forms-merch-loc@newsamerica.com.dev1</ccEmails>
        <description>Notify Merch Operations of Signed LOC</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Signed_Merchandising_LOC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Quote_Provided</fullName>
        <description>Quote Provided</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/SSD_Opportunity_Quote_Provided_Notification</template>
    </alerts>
    <alerts>
        <fullName>SSD_Quote_Request_Notification</fullName>
        <description>SSD Quote Request Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>SSD_Production_Team</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/SSD_Opportunity_Quote_Request_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Digital</fullName>
        <description>Change record type to digital from digital canada when opportunity currency is USD</description>
        <field>RecordTypeId</field>
        <lookupValue>Digital</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Digital</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Digital_Canda</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Digital_Canada</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Digital Canda</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_when_Closed_won_is_set</fullName>
        <description>Change Status to Sold when Opportunity Stage is set to Closed won</description>
        <field>Status__c</field>
        <literalValue>Sold</literalValue>
        <name>Change Status when Closed won is set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Oppty_Status_Effective_Date</fullName>
        <description>Field update to populate the status effective date on an Opportunity based on the order status change</description>
        <field>Status_Effective_Date__c</field>
        <formula>TODAY()</formula>
        <name>Populate Oppty Status Effective Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Probability_Modified_Date_Update</fullName>
        <description>When the Probability field is populated/updated we will populate/update the Probability Date field.</description>
        <field>Probability_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Probability Modified Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_Started_On</fullName>
        <description>Sets the date for Stage Started On field</description>
        <field>Stage_Started_On__c</field>
        <formula>Today()</formula>
        <name>Set Stage Started On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Type_for_InStore_US_Opps_to_Tactical</fullName>
        <description>Sets Type field for InStore US Opportunities to &quot;Tactical&quot;</description>
        <field>Type</field>
        <literalValue>Tactical</literalValue>
        <name>Set Type for InStore US Opps to Tactical</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Default_Probability_from_Picklist</fullName>
        <field>Probability</field>
        <formula>IF(
  ISPICKVAL( Probability__c ,&quot;0&quot;), 
  0.0,
If(
  ISPICKVAL( Probability__c ,&quot;25&quot;),
  0.25,
If(
  ISPICKVAL( Probability__c ,&quot;75&quot;), 
  0.75,
If(
  ISPICKVAL( Probability__c ,&quot;100&quot;), 
  1.00,NULL
)
)
)
)</formula>
        <name>Update Default Probability from Picklist</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Status_on_Cancelled_Non_OM</fullName>
        <description>Update Order Status to Cancelled Sold when Closed Won - Cancelled is set in the Opportunity Stage field</description>
        <field>Status__c</field>
        <literalValue>Cancelled Sold</literalValue>
        <name>Update Order Status on Cancelled Non-OM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Status_on_Closed_Lost_Oppor</fullName>
        <description>Update Order Status to Cancelled Reserved on Closed Lost Opportunities</description>
        <field>Status__c</field>
        <literalValue>Cancelled Reserved</literalValue>
        <name>Update Order Status on Closed Lost Oppor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Status_when_Closed_Won_set</fullName>
        <description>Update Order Status to Sold when the Order Status is set to Closed Won</description>
        <field>Status__c</field>
        <literalValue>Sold</literalValue>
        <name>Update Order Status when  Closed Won set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Status_with_Early_Stage_set</fullName>
        <description>Update Order Status to Reserved - RS1 when Opportunity Stage is Prospecting, Client Engagement, Recommendation and Proposal and/or Contract</description>
        <field>Status__c</field>
        <literalValue>Reserved - RS1</literalValue>
        <name>Update Order Status with Early Stage set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Prob_Value_to_0_for_Lost_Opps</fullName>
        <field>Probability__c</field>
        <literalValue>0</literalValue>
        <name>Update Prob. Value to 0 for Lost Opps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Prob_Value_to_100_for_Won_Opps</fullName>
        <field>Probability__c</field>
        <literalValue>100</literalValue>
        <name>Update Prob. Value to 100 for Won Opps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ready_for_Control_Desk_to_False</fullName>
        <field>Ready_for_Control_Desk__c</field>
        <literalValue>0</literalValue>
        <name>Update Ready for Control Desk to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_SSD_CAN_to_SSD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>SmartSource_Direct</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type SSD CAN to SSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_SSD_to_SSD_CAN</fullName>
        <field>RecordTypeId</field>
        <lookupValue>SmartSource_Direct_Canada</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type SSD to SSD CAN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Notify_FSIOM_Interface</fullName>
        <apiVersion>35.0</apiVersion>
        <description>Outbound message to notify NAM FSIOM interface</description>
        <endpointUrl>http://192.168.0.1:8081</endpointUrl>
        <fields>Id</fields>
        <fields>Late_Buy__c</fields>
        <fields>NEP__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>pravuri@nam.com</integrationUser>
        <name>Notify FSIOM Interface</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Digital Canada to Digital when oppty currency %3D USD</fullName>
        <actions>
            <name>Change_Record_Type_to_Digital</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Digital- Canada</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CurrencyIsoCode</field>
            <operation>equals</operation>
            <value>USD</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Digital Opportunity Quote Request</fullName>
        <actions>
            <name>Digital_Quote_Request_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pricing_Request__c</field>
            <operation>equals</operation>
            <value>Pricing Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Digital,Digital- Canada</value>
        </criteriaItems>
        <description>Sends an email to the Digital Production Team when a user indicates that pricing is requested.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Digital to Digital Canada when Oppty Currency%3D CAD</fullName>
        <actions>
            <name>Change_Record_Type_to_Digital_Canda</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Digital</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CurrencyIsoCode</field>
            <operation>equals</operation>
            <value>CAD</value>
        </criteriaItems>
        <description>Changes the record type from Digital to Digital Canada when the opportunity currency is CAD</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Merch Production Team when LOC is Signed</fullName>
        <actions>
            <name>Notify_Merch_Operations_of_Signed_LOC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Merchandising</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Ready_for_Merch_Ops__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends an email notification to the Merchandising Operations team when a Merchandising Opportunity has had the &quot;Ready for Merch Ops&quot; field checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Quote Provided</fullName>
        <actions>
            <name>Quote_Provided</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pricing_Request__c</field>
            <operation>equals</operation>
            <value>Pricing Provided</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>SmartSource Direct,SmartSource Direct- Canada</value>
        </criteriaItems>
        <description>Sends an email to the Opportunity Owner when the appropriate Production team provides pricing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Status Effective Date</fullName>
        <actions>
            <name>Populate_Oppty_Status_Effective_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to populate the opportunity effective date</description>
        <formula>OR(AND(ISNEW(),NOT(ISBLANK(TEXT(Status__c)))),ISCHANGED(Status__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Probability Modified Date</fullName>
        <actions>
            <name>Probability_Modified_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to populate the Probability Modified Date</description>
        <formula>OR(ISNEW(),ISCHANGED( Probability__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SSD CAN to SSD when Oppty currency%3DUSD</fullName>
        <actions>
            <name>Update_Record_Type_SSD_CAN_to_SSD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>SmartSource Direct- Canada</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CurrencyIsoCode</field>
            <operation>equals</operation>
            <value>USD</value>
        </criteriaItems>
        <description>Changes the record type of an opportunity to SSD from SSD Canada if the account and opportunity have a currency of USD</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SSD Opportunity Quote Request</fullName>
        <actions>
            <name>SSD_Quote_Request_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pricing_Request__c</field>
            <operation>equals</operation>
            <value>Pricing Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>SmartSource Direct,SmartSource Direct- Canada</value>
        </criteriaItems>
        <description>Sends an email to the SSD Production Team when a user indicates that pricing is requested.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSD Oppty to SSD CAN when Currentcy%3D CAN</fullName>
        <actions>
            <name>Update_Record_Type_SSD_to_SSD_CAN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>SmartSource Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Currency_Code__c</field>
            <operation>equals</operation>
            <value>CAD</value>
        </criteriaItems>
        <description>Changes the record type from SSD to SSD-Canada when the opportunity has a currency of CAN based on the Account&apos;s currency.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Ready for Control Desk to False</fullName>
        <actions>
            <name>Update_Ready_for_Control_Desk_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>Sets &quot;Ready for Control Desk&quot; to false when an opportunity is set to Closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Stage Started On</fullName>
        <actions>
            <name>Set_Stage_Started_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Stage Started On based on the update to the Stage field.</description>
        <formula>OR(  ISNEW(),  AND(    NOT(ISBLANK(TEXT(StageName)))),    ISCHANGED(StageName))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Default Probability %25 from Custom Probability %25 Field</fullName>
        <actions>
            <name>Update_Default_Probability_from_Picklist</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to ensure that the Default Probability % field has the value from the Custom Probability % field</description>
        <formula>OR(ISChanged( StageName ), ISChanged(  Probability__c ), ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Status on Cancelled Non-OM Opportunities</fullName>
        <actions>
            <name>Update_Order_Status_on_Cancelled_Non_OM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Merchandising,SmartSource Direct,SSMG,Digital,SmartSource Direct- Canada,Digital- Canada</value>
        </criteriaItems>
        <description>Update Order Status on Closed Won - Cancelled Non-OM Opportunities  based upon Opportunity Stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Status on Closed Lost Opportunities</fullName>
        <actions>
            <name>Update_Order_Status_on_Closed_Lost_Oppor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Merchandising,SmartSource Direct,SSMG,Digital,SmartSource Direct- Canada,Digital- Canada</value>
        </criteriaItems>
        <description>Update Order Status to Cancelled reserved on Closed Lost Opportunity Stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Status on Closed Won Opportunities</fullName>
        <actions>
            <name>Change_Status_when_Closed_won_is_set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Merchandising,SmartSource Direct,SSMG,Digital,SmartSource Direct- Canada,Digital- Canada</value>
        </criteriaItems>
        <description>Update Order Status on Closed Won Opportunity Stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Status on early stage Non-OM Opportunities</fullName>
        <actions>
            <name>Update_Order_Status_with_Early_Stage_set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Prospecting,Client Engagement,Recommendation and Proposal,Contract</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Merchandising,SmartSource Direct,SSMG,Digital,SmartSource Direct- Canada,Digital- Canada</value>
        </criteriaItems>
        <description>Update Order Status on early stage Non-OM Opportunities  based upon Opportunity Stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Probability to 0%25 for Lost Opps</fullName>
        <actions>
            <name>Update_Prob_Value_to_0_for_Lost_Opps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won Cancelled</value>
        </criteriaItems>
        <description>Update Probability to 0% for Lost Opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Probability to 100%25 for Won Opps</fullName>
        <actions>
            <name>Update_Prob_Value_to_100_for_Won_Opps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Update Probability to 100% for Won Opps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Type for InStore US Opps</fullName>
        <actions>
            <name>Set_Type_for_InStore_US_Opps_to_Tactical</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>InStore</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CurrencyIsoCode</field>
            <operation>equals</operation>
            <value>USD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <description>Used to update the Type field to Tactical for InStore Opportunities that have a value of &quot;Standard&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
