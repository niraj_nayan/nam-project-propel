<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Update_Approval</fullName>
        <description>Account Update Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Account_Update_Approval</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Rejected</fullName>
        <description>Account Update Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Account_Update_Rejected</template>
    </alerts>
    <alerts>
        <fullName>FinApps_Account_Rejected_Form_Header</fullName>
        <description>FinApps Account Rejected_Form Header</description>
        <protected>false</protected>
        <recipients>
            <recipient>adixit.consultant@newsamerica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/FinApps_Account_Rejected_Form_Header</template>
    </alerts>
    <alerts>
        <fullName>InStore_Production_Quote_Approved_by_InStore_Production_Team</fullName>
        <description>InStore Production Quote Approved by InStore Production Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/InStore_Production_Quote_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>InStore_Production_Quote_Rejected_by_InStore_Production_Team</fullName>
        <description>InStore Production Quote Rejected by InStore Production Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/InStore_Production_Quote_Approval_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Pricing_Exception_Approval</fullName>
        <description>Pricing Exception Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Pricing_Exception_Approval</template>
    </alerts>
    <alerts>
        <fullName>Pricing_Exception_Rejected</fullName>
        <description>Pricing Exception Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Pricing_Exception_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Rejected_by_Billing_Form_Header</fullName>
        <description>Rejected by Billing-Form Header</description>
        <protected>false</protected>
        <recipients>
            <recipient>adixit.consultant@newsamerica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/FinApps_Account_Not_Created_Form_Header</template>
    </alerts>
    <alerts>
        <fullName>SSMG_Quote_Approval_Notification</fullName>
        <description>SSMG Quote Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/SSMG_Quote_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>SSMG_Quote_Rejection_Notification</fullName>
        <description>SSMG_Quote_Rejection_Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/SSMG_Quote_Rejection_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Owner_to_Finance_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Finance_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner to Finance Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_Assigned</fullName>
        <description>Upon submission of the SSMG Custom Production Quote form, change status to Assigned</description>
        <field>Status__c</field>
        <literalValue>Assigned</literalValue>
        <name>Change Status to Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_Closed</fullName>
        <description>When SSMG forms are approved, update the Status field to Closed.</description>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Change Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Change status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Combine_Name_field</fullName>
        <field>Name</field>
        <formula>Request_Number__c &amp;&quot; - &quot;&amp; Account__r.Name &amp; &quot; - &quot; &amp; RecordType.Name</formula>
        <name>Combine Name field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Form_Header_Rejected</fullName>
        <description>Used when a Form Header has been rejected.</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Form Header Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PE_Approval_Accepted</fullName>
        <description>Used to update the Status field on the Form Header Object when a PE form has received final approval.</description>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>PE Approval Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PE_Approval_Submission</fullName>
        <description>Used to update the Status field on the Form Header when a PE form is submitted for approval.</description>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>PE Approval Submission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Form_Header_Status_for_Finance</fullName>
        <description>Used to update the Form Header Status to pending Finance Approval. Used to determine what items require Finance Approval</description>
        <field>Status__c</field>
        <literalValue>Pending Finance Approval</literalValue>
        <name>Update Form Header Status for Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Closed</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Update Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Combine Name field on Form Header</fullName>
        <actions>
            <name>Combine_Name_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Form_Header__c.Name</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Form_Header__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default Shared Revenue form to Submitted</fullName>
        <actions>
            <name>Change_status_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Form_Header__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Shared Revenue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Form_Header__c.Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When a Shared Revenue form is created, default the Status to Submitted</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
