<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Product_Family_Status_Eff_Date</fullName>
        <description>Field update to populate the Status Effective date on the product family record</description>
        <field>Status_Effective_Date__c</field>
        <formula>TODAY()</formula>
        <name>Populate Product Family Status Eff Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Product Family - Status Effective Date</fullName>
        <actions>
            <name>Populate_Product_Family_Status_Eff_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to populate the status effective date on Product family record</description>
        <formula>OR(AND(ISNEW(),NOT(IsActive__c)),ISCHANGED(IsActive__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
