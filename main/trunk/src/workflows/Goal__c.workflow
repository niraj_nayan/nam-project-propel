<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_batch_apex_context</fullName>
        <field>BatchApexContext__c</field>
        <literalValue>0</literalValue>
        <name>Update batch apex context</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update batch apex context</fullName>
        <actions>
            <name>Update_batch_apex_context</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Goal__c.BatchApexContext__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>To update the batch apex context field so that the validation rule will rue will not fire</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
