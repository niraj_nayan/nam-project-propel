<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Opportunity_Credit_Approved</fullName>
        <description>Opportunity Credit Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Opportunity_Credit_Approval</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Credit_Rejected</fullName>
        <description>Opportunity Credit Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Opportunity_Credit_Rejection</template>
    </alerts>
</Workflow>
