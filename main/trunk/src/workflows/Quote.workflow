<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Merchandise_LOC_Approval</fullName>
        <description>Merchandise LOC Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Merchandise_LOC_Approval</template>
    </alerts>
    <alerts>
        <fullName>Merchandise_LOC_Notification</fullName>
        <ccEmails>Merchandisingplan@newsamerica.com.dev1</ccEmails>
        <description>Merchandise LOC Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Merchandise_LOC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Merchandise_LOC_Rejection</fullName>
        <description>Merchandise LOC Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Notification_Folder/Merchandise_LOC_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_LOC_Status_to_Approved</fullName>
        <description>Updates LOC Status to Approved after being approved by the Merchandising Operations Team.</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update LOC Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LOC_Status_to_Rejected</fullName>
        <description>Updates LOC Status to Rejected after being rejected by the Merchandising Operations Team</description>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update LOC Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
