<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Weekly_Redemption_Rev_Rollup</fullName>
        <field>Weekly_Redemption_Rev_Rollup__c</field>
        <formula>Weekly_Redemption_Revenue__c</formula>
        <name>Update Weekly Redemption Rev Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Weekly Redemption Revenue Rule</fullName>
        <actions>
            <name>Update_Weekly_Redemption_Rev_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign_Execution_Week__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
