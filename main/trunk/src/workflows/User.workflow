<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Status_Active_Date_c</fullName>
        <field>Status_Active_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Status_Active_Date__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set User Status_Active_Date%5F%5Fc</fullName>
        <actions>
            <name>Set_Status_Active_Date_c</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule sets Status_Active_Date__c to accurately reflect when a User became Active.</description>
        <formula>IF(AND(ISCHANGED(IsActive),IsActive),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
