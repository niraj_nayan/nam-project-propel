@isTest
private class OpptyCloneTest {
	
	@isTest static void OpptyCloneSaveWithoutProducts() {
		// Implement test code
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
	    user1.FederationIdentifier = 'tLastName';
	    insert user1;

	    User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
	    secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
	    user1.FederationIdentifier = 'secondUser2';
	    insert secondUser;

		System.runAs(secondUser){

			Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
			insert corp1;

			//Create Division Account record
			Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
			insert div1;

			//then an opportunity with lookup to the account
			Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
			insert testOppty;

			OpportunityTeamMember testMember = TestDataUtils.createTeamMember(user1.Id, 'Manager', testOppty.Id, 10);
			insert testMember;

			Product2 prod = TestDataUtils.createProduct2('Test Prod');
			insert prod;

			//CREATE PRICEBOOK ENTRY RECORD
			PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
			insert priceEntry;

			//ADD OPPORTUNITY LINE ITEM
			OpportunityLineItem testProduct = TestDataUtils.createOpportunityLineItem('Test ProdLine', 'Test Pricing Detail', 150, 10, testOppty.Id, priceEntry.Id);
			insert testProduct;

			ApexPages.currentPage().getParameters().put('id', testOppty.Id);
			ApexPages.currentPage().getParameters().put('cloneWithProd', 'false');
			OpptyClone controller = new OpptyClone();
			controller.cloneTypeWithProduct = false;

			controller.saveButton();
			Integer totalopps = [select count() from Opportunity where name='Test Oppty'];
			system.assert(totalopps > 1);
		}
	}
	
	@isTest static void OpptyCloneSaveWithProducts() {
		// Implement test code
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		insert user1;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		insert secondUser;

		System.runAs(secondUser){

			Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
			insert corp1;

			//Create Division Account record
			Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
			insert div1;

			//then an opportunity with lookup to the account
			Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
			insert testOppty;

			//OpportunityTeamMember testMember = TestDataUtils.createTeamMember(user1.Id, 'Manager', testOppty.Id, 10);
			//insert testMember;

			Product2 prod = TestDataUtils.createProduct2('Test Prod');
			insert prod;

			//CREATE PRICEBOOK ENTRY RECORD
			PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
			insert priceEntry;

			//ADD OPPORTUNITY LINE ITEM
			OpportunityLineItem testProduct = TestDataUtils.createOpportunityLineItem('Test ProdLine', 'Test Pricing Detail', 150, 10, testOppty.Id, priceEntry.Id);
			insert testProduct;

			ApexPages.currentPage().getParameters().put('id', testOppty.Id);
			ApexPages.currentPage().getParameters().put('cloneWithProd', 'true');
			OpptyClone controller = new OpptyClone();

			controller.saveButton();

			Integer totalopps = [select count() from Opportunity where name='Test Oppty'];
			system.assert(totalopps == 2);

			Integer totalprods = [select count() from OpportunityLineItem where Opportunity.name='Test Oppty'];
			system.assert(totalprods == 2);
		}

	}
	
}