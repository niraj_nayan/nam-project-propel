/*****************************************************************************************************************
Description: This is a controller for AccountNewOpportunity Page. It will check if the Account is Bill To Only. If yes
             display error on page that opportunities cannot be created for "Bill To" accounts

Created on:  9/28/2015
Created by:  Seth Martin
Version 1:   Created the visualforce page and controller.
********************************************************************************************************************/

public with sharing class AccountNewOpportunity {

    public string AccID = ApexPages.currentpage().getparameters().get('addTo');

    public Account currentAccount {get;set;}
    public Boolean billToOnlyError {get;set;}

    public AccountNewOpportunity() {
        currentAccount = [Select Id, Name, Bill_To_Only__c From Account Where Id =:AccID];
        System.debug('Account: ' + currentAccount);
        billToOnlyError = false;

    }

    public PageReference redirectOnLoad(){
        if(currentAccount.Bill_To_Only__c != true){
            //redirect to new opportunity screen
            PageReference newOpptyPage = new PageReference('/setup/ui/recordtypeselect.jsp?ent=Opportunity&retURL=%2F' + AccID + '&save_new_url=%2F006%2Fe%3FretURL%3D%252F' +AccId+'%26accid%3D'+AccId );
            return newOpptyPage;
        }
        else{
            //PageReference accountPage = new PageReference('/' + AccID);
            //System.debug('redirect page: ' + accountPage);
            billToOnlyError = false;
            System.debug('bill to boolean: ' + billToOnlyError);
            return null;
        }
    }

    public PageReference redirectFromErrorPage(){
        PageReference accountPage = new PageReference('/' + AccID);
        System.debug('redirect page: ' + accountPage);
        return accountPage;
    }
}