@isTest
private class FinnAppsAccountSyncTest {

	private class MockHttpResponseGenerator implements HttpCalloutMock {
		public String body {get;set;}

        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //String testEndpoint = Label.Credit_Profile_End_Point+'101813-19226';
            //System.assertEquals(true, req.getEndpoint().startsWith(testEndpoint));
            //System.assertEquals('POST', req.getMethod());
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String jsonString = body;
            res.setBody(jsonString);
            res.setStatusCode(200);
            return res;
        }
    }

	@isTest static void testAccountUpdateSuccess() {
		MockHttpResponseGenerator testMock = new MockHttpResponseGenerator();
		testMock.body = '[{"CLASS_CODE": "Division-QSR","TYPE": "Customer","LOC": "163515-98221","ADDRESS": "33 Westport Road  ","STATE": "CT","ACCOUNT_NAME": "tbj229-Second Division","SITE_USE_CODE": "SHIP_TO","ZIP": "06897","STATUS": "Active","CITY": "Wilton"}]';
		Test.setMock(HttpCalloutMock.class, testMock);
		Account testAccount = new Account();
		testAccount.Name = 'AAA';
		insert testAccount;
		CreditProfileCredentials__c cred = new CreditProfileCredentials__c();
		cred.Name = 'FinApps';
		cred.UserName__c = 'NAMCreditProf123';
		cred.Password__c = 'NewsAmerica123$$';
		insert cred;
		FinnAppsAccountSyncProfiles__c finn = new FinnAppsAccountSyncProfiles__c(Name = 'System Administrator');
		insert finn;
		PageReference pageRef = new ApexPages.StandardController(testAccount).view();
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('acctId', testAccount.Id);
		Test.startTest();
		FinnAppsAccountSync controller = new FinnAppsAccountSync();
		controller.selectedLOCValue = '163515-98221';
		controller.saveButton();
		Test.stopTest();
		testAccount = [SELECT Id, Name, Site, LOC__c FROM Account WHERE Name = 'AAA'];
		System.assertEquals('163515-98221', testAccount.LOC__c);
	}

	@isTest static void testAccountUpdateHasSiteAuthFailure() {
		MockHttpResponseGenerator testMock = new MockHttpResponseGenerator();
		testMock.body = 'UserName or Password or both did not Match';
		Test.setMock(HttpCalloutMock.class, testMock);
		Account testAccount = new Account();
		testAccount.Name = 'AAA';
		testAccount.Site = 'test';
		insert testAccount;
		CreditProfileCredentials__c cred = new CreditProfileCredentials__c();
		cred.Name = 'FinApps';
		cred.UserName__c = 'NAMCreditProf123';
		cred.Password__c = 'NewsAmerica123$$';
		insert cred;
		FinnAppsAccountSyncProfiles__c finn = new FinnAppsAccountSyncProfiles__c(Name = 'System Administrator');
		insert finn;
		PageReference pageRef = new ApexPages.StandardController(testAccount).view();
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('acctId', testAccount.Id);
		Test.startTest();
		FinnAppsAccountSync controller = new FinnAppsAccountSync();
		controller.selectedLOCValue = '163515-98221';
		controller.saveButton();
		Test.stopTest();
		testAccount = [SELECT Id, Name, Site, LOC__c FROM Account WHERE Name = 'AAA'];
		System.assertEquals('test', testAccount.LOC__c);
	}

	@isTest static void testAccountUpdateEmptySyncInfo() {
		MockHttpResponseGenerator testMock = new MockHttpResponseGenerator();
		testMock.body = '';
		Test.setMock(HttpCalloutMock.class, testMock);
		Account testAccount = new Account();
		testAccount.Name = 'AAA';
		insert testAccount;
		CreditProfileCredentials__c cred = new CreditProfileCredentials__c();
		cred.Name = 'FinApps';
		cred.UserName__c = 'NAMCreditProf123';
		cred.Password__c = 'NewsAmerica123$$';
		insert cred;
		FinnAppsAccountSyncProfiles__c finn = new FinnAppsAccountSyncProfiles__c(Name = 'System Administrator');
		insert finn;
		PageReference pageRef = new ApexPages.StandardController(testAccount).view();
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('acctId', testAccount.Id);
		Test.startTest();
		FinnAppsAccountSync controller = new FinnAppsAccountSync();
		controller.selectedLOCValue = '163515-98221';
		controller.saveButton();
		Test.stopTest();
		testAccount = [SELECT Id, Name, Site, LOC__c FROM Account WHERE Name = 'AAA'];
		System.assertEquals(null, testAccount.LOC__c);
	}

	@isTest static void testAccountUpdateSyncError() {
		MockHttpResponseGenerator testMock = new MockHttpResponseGenerator();
		testMock.body = 'StatusCode=500';
		Test.setMock(HttpCalloutMock.class, testMock);
		Account testAccount = new Account();
		testAccount.Name = 'AAA';
		insert testAccount;
		CreditProfileCredentials__c cred = new CreditProfileCredentials__c();
		cred.Name = 'FinApps';
		cred.UserName__c = 'NAMCreditProf123';
		cred.Password__c = 'NewsAmerica123$$';
		insert cred;
		FinnAppsAccountSyncProfiles__c finn = new FinnAppsAccountSyncProfiles__c(Name = 'System Administrator');
		insert finn;
		PageReference pageRef = new ApexPages.StandardController(testAccount).view();
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('acctId', testAccount.Id);
		Test.startTest();
		FinnAppsAccountSync controller = new FinnAppsAccountSync();
		controller.selectedLOCValue = '163515-98221';
		controller.saveButton();
		Test.stopTest();
		testAccount = [SELECT Id, Name, Site, LOC__c FROM Account WHERE Name = 'AAA'];
		System.assertEquals(null, testAccount.LOC__c);
	}

	@isTest static void testAccountUpdateSEmptyLocValueRedirect() {
		MockHttpResponseGenerator testMock = new MockHttpResponseGenerator();
		testMock.body = 'StatusCode=500';
		Test.setMock(HttpCalloutMock.class, testMock);
		Account testAccount = new Account();
		testAccount.Name = 'AAA';
		insert testAccount;
		CreditProfileCredentials__c cred = new CreditProfileCredentials__c();
		cred.Name = 'FinApps';
		cred.UserName__c = 'NAMCreditProf123';
		cred.Password__c = 'NewsAmerica123$$';
		insert cred;
		FinnAppsAccountSyncProfiles__c finn = new FinnAppsAccountSyncProfiles__c(Name = 'System Administrator');
		insert finn;
		PageReference pageRef = new ApexPages.StandardController(testAccount).view();
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('acctId', testAccount.Id);
		Test.startTest();
		FinnAppsAccountSync controller = new FinnAppsAccountSync();
		controller.selectedLOCValue = '';
		controller.saveButton();
		Test.stopTest();
		testAccount = [SELECT Id, Name, Site, LOC__c FROM Account WHERE Name = 'AAA'];
		System.assertEquals(null, testAccount.LOC__c);
		controller.redirectFromPage();
	}
}