/**********************************************************************************************************************************************
Description: Helper class for user trigger
             
Created on:  23nd October 2015
Created by:  Jagan
Version 1:   Created Class to assing user to territory if primary territory is updated.
version 2:   Hierarchy assumed earlier was wrong. Hence, updated class to restructure according to correct hierarchy model
Version 3:   Removed the code which is earlier used for Territory Management. That code is now moved to User association trigger helper class
*********************************************************************************************************************************************/

public class userTriggerHelper{
    
    static boolean adjustUserTerritorybeforeUpdateRuncheck = FALSE;
    static boolean adjustUserTerritoryAfterUpdateRuncheck = FALSE;
    static boolean beforeUpsertUpdateUserAssociationscheck = FALSE;
    
    public static void beforeInsertSourceKeyGenerate(List<User> lstUser){

        integer counter = 0;
        for(User insertUser: lstUser){
          if(insertUser.Source_Key__c == null){
            DateTime current = System.now();
            counter += 10;
            insertUser.Source_Key__c = sourceKeyGenerationUtility.generateSourceKey('U', current.getTime() + counter);
          }
        }
    }

    //This method is used before a user is inserted. The method validates that the user
    //has a federation identifier
    public static void beforeInsertFederationIdentifierValidation(List<User> lstUser){

        for(User insertUser: lstUser){
            if(String.isEmpty(insertUser.FederationIdentifier) == true){//the federation identifier is empty
                System.debug('User Profile: ' + insertUser.Profile);
                insertUser.addError(Label.UserFederationIDValidation);
            }
        }
    }
    
    //If a user is getting deactivated, check if there are any userterritory association, if yes then it should be inserted after 
    //the user is deactivated
    Static List<UserTerritory2Association> lstUt2a = new List<UserTerritory2Association>();
    public static void adjustUserTerritorybeforeUpdate(Map<Id,User> triggernewmap, Map<Id,User> triggeroldmap){
        if(!adjustUserTerritorybeforeUpdateRuncheck){
            adjustUserTerritorybeforeUpdateRuncheck = TRUE;   
            Set<Id> useridset = new Set<Id>();
            
            for(Id uid:triggernewmap.keyset()){
                if(!triggernewmap.get(uid).isActive && triggeroldmap.get(uid).isActive)
                    useridset.add(uid);
            }
            //collect all the user territory2 association records and put in a list
            if(useridset.size()>0){
                lstUt2a = [select id,userid,territory2id,RoleInTerritory2 from UserTerritory2Association where UserId in :useridset];            
            }
        }
    }
    //Insert User Territory Association Records after user is deactivated
    public static void adjustUserTerritoryafterUpdate(Map<Id,User> triggernewmap, Map<Id,User> triggeroldmap){
        if(!adjustUserTerritoryafterUpdateRuncheck){
            adjustUserTerritoryafterUpdateRuncheck = TRUE;
            Set<Id> useridset = new Set<Id>();
            
            for(Id uid:triggernewmap.keyset()){
                if(!triggernewmap.get(uid).isActive && triggeroldmap.get(uid).isActive)
                    useridset.add(uid);
            }
            system.debug('USERS DEACTIVATED- AFTER UPDATE....'+useridset);
            system.debug('USER Territory2Association size....'+lstut2a.size());
            //use the list collected earlier and call a future method to insert those record back
            if(useridset.size()>0){
                List<String> userterrinsertlst = new List<String>();
                for(UserTerritory2Association u2a : lstut2a){
                   userterrinsertlst.add(JSON.serialize(u2a));
                }
                insertUserTerritoryRecords(userterrinsertlst);
            }
        }
    }
    
    //Insert the user territory association records in future method
    @future
    public static void insertUserTerritoryRecords(List<String> userterrinsertlst){
        
        system.debug('INSIDE Future method...USERS DEACTIVATED....'+userterrinsertlst.size());
        
        if(userterrinsertlst!= null && userterrinsertlst.size()>0){
            
            List<UserTerritory2Association> listut2a = new List<UserTerritory2Association>();
            List<UserTerritory2Association> insertlstut2a = new List<UserTerritory2Association>();
                
            for(String str : userterrinsertlst)
                listut2a.add((UserTerritory2Association) JSON.deserialize(str, UserTerritory2Association.class));
               
            for(UserTerritory2Association ut2a: listut2a){
                
                UserTerritory2Association temp = new UserTerritory2Association();
                temp = ut2a.clone(false,true);
                insertlstut2a.add(temp);
                
            }
            
            if(insertlstut2a.size() > 0){
                Database.SaveResult[] lsr = database.insert(insertlstut2a,false);
                for(Database.SaveResult sr : lsr)
                    if (!sr.isSuccess()) system.debug('Error occured during insert of userterritory2association after deactivation of user ....'+sr);     
            }
        }
           
    }
    
    //Update a user's managers and territory info when they're updated
    public static void beforeUpsertUpdateUserAssociations(List<User> lstUser){
        
        if(!beforeUpsertUpdateUserAssociationscheck){
            beforeUpsertUpdateUserAssociationscheck  = TRUE;
            Map<String, User> usersByUsername = new Map<String, User>();
            Set<id> useridset = new Set<id>();
            for(User usr : lstUser){
                useridset.add(usr.id);
            }
            List<user> updatedUsers = userAssociationTriggerHelper.updateUsers(useridset);
            for(User usr : updatedUsers){
                usersByUsername.put(usr.Username, usr);
            }
            for(User usr : lstUser){
                if(usersByUsername.get(usr.Username) != null){
                    usr.Assigned_Primary_TerritoryId__c = usersByUsername.get(usr.Username).Assigned_Primary_TerritoryId__c;
                    usr.Assigned_Primary_Territory_Name__c = usersByUsername.get(usr.Username).Assigned_Primary_Territory_Name__c;
                    
                    usr.Division_Name__c = usersByUsername.get(usr.Username).Division_Name__c;
                    usr.Division_Manager__c = usersByUsername.get(usr.Username).Division_Manager__c;
                    usr.Division_Manager_Name__c = usersByUsername.get(usr.Username).Division_Manager_Name__c;
                    usr.Division_Territory_Id__c = usersByUsername.get(usr.Username).Division_Territory_Id__c;
                    
                    usr.Region_Name__c = usersByUsername.get(usr.Username).Region_Name__c;
                    usr.Region_Manager__c = usersByUsername.get(usr.Username).Region_Manager__c;
                    usr.Region_Manager_Name__c = usersByUsername.get(usr.Username).Region_Manager_Name__c;
                    usr.Region_Territory_Id__c = usersByUsername.get(usr.Username).Region_Territory_Id__c;
                   
                    usr.Team_Name__c = usersByUsername.get(usr.Username).Team_Name__c;
                    usr.Team_Manager__c = usersByUsername.get(usr.Username).Team_Manager__c;
                    usr.Team_Manager_Name__c = usersByUsername.get(usr.Username).Team_Manager_Name__c;
                    usr.Team_Territory_Id__c = usersByUsername.get(usr.Username).Team_Territory_Id__c;
                     
                }
            }
        }
    }
}