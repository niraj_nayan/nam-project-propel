/****************************************************************************************************************************
Description: Helper class for GoalRecordShare Trigger. 
Whenever a goal Assignment object record is inserted/update, get the Assigned user for the goal and share the goal with the users. 
Created Date: 9/15/2016             
Created by:  Kiran Madadi
Version 1:   Created class to share the goal record with goal team.
JIRA No : PP-79
*******************************************************************************************************************************/

public with sharing class GoalRecordShareHelper {
    public Set<Id> useridset = new Set<Id>();
    public Set<Id> updateduseridset = new Set<Id>();
    Public List<Goal__Share> goalShares = new List<Goal__Share>();
    Map<id,id> Goalids = new Map<id,id>();
    Map<id,id> Goalteamids = new Map<id,id>();
    
    public void shareGoals(Map<Id,Goal_Team__c> TriggerNewMap,Map<Id,Goal_Team__c> TriggerOldMap ){
        
        for(id g:TriggerNewMap.keyset()){
            if(TriggerOldMap == null){
                useridset.add(TriggerNewMap.get(g).Team_Member__c);
            }
            else{
                //if the Assigned user is changed for a goal
                if(TriggerNewMap.get(g).Team_Member__c <> TriggerOldMap.get(g).Team_Member__c)
                    updateduseridset.add(TriggerNewMap.get(g).Team_Member__c);
                //updateduseridset1.add(TriggerNewMap.get(g).Goal_Name__c);
            }
        }
        
        //share goals for new members
        if(useridset.size()>0){
            shareGoals(useridset,TriggerNewMap);
        }
        //share the goal for updated users
        else if(updateduseridset.size()>0){
            
            //  clearSharerecords(updateduseridset1);
            shareGoals(updateduseridset,TriggerNewMap);
        }
    }
    
    
    
    //code to delete previous shares
    /*  public void clearSharerecords(Set<Id> deletegoalsset){
System.debug('-------'+deletegoalsset);
List<Goal__share> lstshare = new LIst<Goal__share>();
if(!deletegoalsset.isempty()){
lstshare = [select id,parentid,RowCause from Goal__share where parentid in: deletegoalsset];
System.debug('-------'+lstshare);
if(!lstshare.isempty()){
database.delete(lstshare,false);
}
}
}
*/
    //Method for sharing the goal to new and updated users
    public void shareGoals(Set<id> useridset,Map<Id,Goal_Team__c> TriggerNewMap){
        
        if(useridset.size()>0){
            
            for(Goal_Team__c g: TriggerNewMap.values()){
                Goalids.put(g.id,g.Goal_Name__c);
                Goalteamids.put(g.id,g.Team_Member__c);
            }
            
            for(Goal_Team__c g: TriggerNewMap.values()){
                Goal__Share gshare = new Goal__Share();
                gshare.ParentId = Goalids.get(g.id);
                gshare.UserOrGroupId =Goalteamids.get(g.id);
                gshare.AccessLevel = 'read';
                gshare.RowCause =Schema.Goal__Share.RowCause.Goal_Record_Share_to_GoalTeam__c;
                goalShares.add(gshare);
                
            }
            
        }
        
        system.debug('Goals tobe shared..'+goalShares);
        if(goalShares.size()>0){
            Database.SaveResult[] lsr = database.Insert(goalShares,false);
            for(Database.SaveResult sr : lsr){
                if (!sr.isSuccess()){
                    system.debug('Error occured during Form header share records insert....'+sr);
                }        
            }
        }
    }
}