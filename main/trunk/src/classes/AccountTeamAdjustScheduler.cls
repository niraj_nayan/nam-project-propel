/***************************************************************************************************************************
Description: Scheduler Class for AccountTeamAdjustBatchClass
Created on:  3 Feb 2016
Created by:  Jagan

Version 1:   Created the class
********************************************************************************************************************************/
Global class AccountTeamAdjustScheduler implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        //Execute the territory hierarchy batch class
        AccountTeamAdjustBatchClass batch = new AccountTeamAdjustBatchClass();
        database.executebatch(batch);
    }

}