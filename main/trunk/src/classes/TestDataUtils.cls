public class TestDataUtils {
    //METHOD TO CREATE A CORPORATE ACCOUNT
    public static Account createCorpAccount(String accName){
        Account newAccount = new Account();
        newAccount.Name = accName;
        newAccount.Type = 'Corporate';
        newAccount.RecordTypeId = retrieveCorpAccountRecordTypeId();

        return newAccount;
    }

    //METHOD TO CREATE DIVISION ACCOUNT
    public static Account createDivAccount(String accName, Id parentAcctID){
        Account newAccount = new Account();
        newAccount.Name = accName;
        newAccount.Type = 'Division';
        newAccount.ParentId = parentAcctID;
        newAccount.RecordTypeId = retrieveDivisionAccountRecordTypeId();

        return newAccount;
    }
    
    //METHOD TO CREATE A PROSPECT ACCOUNT
    public static Account createProsAccount(String accName){
        Account newAccount = new Account();
        newAccount.Name = accName;
        newAccount.Type = 'Corporate';
        newAccount.Status__c = 'New';
        newAccount.RecordTypeId = retrieveProspectAccountRecordTypeId();

        return newAccount;
    }

    public static Opportunity createOppty(String opptyName, String stageName, Id accId, String opptyType){
        Opportunity newOppty = new Opportunity();
        newOppty.Name = opptyName;
        System.debug('Account Id: ' + accId);
        newOppty.AccountId = accId;
        newOppty.StageName = stageName;
        newOppty.CloseDate = Date.newInstance(2020, 1, 1);
        newOppty.RecordTypeId = retrieveOpportunityRecordTypeId(opptyType);

        return newOppty;
    }

    public static OpportunityTeamMember createTeamMember(Id user, String teamRole, Id opptyId, Double commissionPercent){
        return new OpportunityTeamMember(
            UserId = user,
            TeamMemberRole = teamRole,
            Commission_Percentage__c = commissionPercent,
            OpportunityId = opptyId);
    }

    public static Product2 createProduct2(String prodName){
        return new Product2(
            Name = prodName);
    }

    public static Product_Line__c createProductLine(String prodLineName){
        return new Product_Line__c(
            Name = prodLineName);
    }

    public static PricebookEntry createPriceBookEntry(Double priceValue, Id prodID){
        //need to create pricebook
        Id pricebookId = Test.getStandardPricebookId();

        return new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = prodID,
            UnitPrice = priceValue, 
            IsActive = true,
            CurrencyIsoCode = 'USD');
    }

    public static Product_Junction__c createProductJunction(Id prodID, Id prodLineId){
        return new Product_Junction__c(
            Product__c = prodId,
            Product_Line__c = prodLineId);
    }

    public static OpportunityLineItem createOpportunityLineItem(String prodLine, String pricingDetail, Double salesPrice, Double quanNumber, Id opptyId, Id priceEntryID){

        Id pricebookId = test.getStandardPricebookId();

        return new OpportunityLineItem(
            //Product_Line__c = prodLine,
            Pricing_Detail__c = pricingDetail,
            Sales_Price__c = salesPrice,
            Quantity = quanNumber,
            TotalPrice = salesPrice * quanNumber,
            OpportunityId = opptyId,
            PricebookEntryId = priceEntryID);

    }

    public static User createUser(String lastName, String email, String phone) {
        Profile a = new Profile();
        return new User(
            FirstName = 'Test',
            LastName = lastName,
            Email = email,
            Phone = phone,
            FederationIdentifier = lastName + '1',

            //required user fields
            Username = email, 
            Alias = 'test', 
            CommunityNickname = '', 
            TimeZoneSidKey = 'America/Los_Angeles', 
            LocaleSidKey = 'en_US', 
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = userinfo.getProfileId(), 
            LanguageLocaleKey = 'en_US'
        );
    }

    //Return the record type for Corporate Accounts
    public static Id retrieveCorpAccountRecordTypeId(){
        //return [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Account' and Name = 'Corporate'].Id;
        return Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.AccRecordTypeCorp).getRecordTypeId();
    }

    //Return the record type for Division Accounts
    public static Id retrieveDivisionAccountRecordTypeId(){
        //return [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Account' and Name = 'Division Account'].Id;
        return Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.AccRecordTypeDiv).getRecordTypeId();
    }
    
    //Return the record type for Prospect Accounts
    public static Id retrieveProspectAccountRecordTypeId(){
        //return [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Account' and Name = 'Prospect'].Id;
        return Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.AccRecordTypeProspect).getRecordTypeId();
    }

    //Return the record type for Opportunity
    public static Id retrieveOpportunityRecordTypeId(String recordTypeName){
        return [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name =:recordTypeName].Id;
        
    }
}