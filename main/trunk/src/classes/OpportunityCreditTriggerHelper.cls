/*****************************************************************************************************************
Description: This class is used to start the approval process for the Opportunity Credit when an Opportunity Credit 
			 is created. 
Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   Created helper class
********************************************************************************************************************/

Public class OpportunityCreditTriggerHelper {
	/*This method is used to create the approval process for the Opportunity Credit
	so the Opportunity Credit record is locked down.
	*/
	public static void beforeInsertApproval(List<Opportunity_Credit__c> opptyCredit){
		for(Opportunity_Credit__c submittedOpptyCredit: opptyCredit){
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();

			req.setComments('Submitted for approval. Please approve.');
			req.setObjectId(submittedOpptyCredit.Id);

			Approval.ProcessResult result = Approval.process(req);

			System.debug('Submitted for approval successfully: ' + result.isSuccess());
		}
	}
}