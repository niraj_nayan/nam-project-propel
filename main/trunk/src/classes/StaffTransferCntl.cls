/*********************************************************************************************************************************************
Description: Ability provided to Sales Ops to transfer Accounts/Contacts/Opportunities/Tasks/Goals to the new Owners          
Created on:  1 October 2015
Created by:  Kavitha

Version 1:   Controller class for Staff Transfer VF.This controller makes a call to a Batch class
Version 2:   Users should be able to submit request for Past and Today's date. 
Changes Made: ValidateEffectiveDate() JS is commented out and respective changes are made on Command buttons
Version 3:   Finance Users should be able to submit Goal Transfers. Calculations needed on Paced Production, Paced Space Revenue fields, Goal Amounts,
             Production Revenue and Space Revenue fields
Changes Made: 1. Created 2 components: one for Finance Users and one for Sales Ops (Non Finance Users)
              2. Included math calculations on populating the above Revenue fields
Version 4:   Leads section is replaced with Prospect type Accounts
Version 5:   Non Finance Users do not access Transfers and Sales Ops will do Account/Prospect, Goal Transfers. Also no math caluclations needed on Revenue fields.
**************************************************************************************************************************************************/
public with sharing class StaffTransferCntl {

    // temporary variables to hold the Owner information
    public Account fromUser{get;set;}
    public Account toUser{get;set;}
    public list<AccountInfo> accounts {get;set;}
    public list<ProspectInfo> prospects {get;set;}
    public list<GoalInfo> goals {get;set;}
    
    // below are the properties for review screen
    public list<AccountInfo> accountsSelected {get;set;}
    public list<ProspectInfo> prospectsSelected {get;set;}
    public list<GoalInfo> goalsSelected {get;set;}
    public list<AccountInfo> accountsRemained {get;set;}
    public list<ProspectInfo> prospectsRemained {get;set;}
    public list<GoalInfo> goalsRemained {get;set;}
    
    // boolean properties to capture select all event
    public boolean selectAllAccount{get;set;}
    public boolean selectAllProspects{get;set;}
    public boolean selectAllGoals {get;set;}
    public integer accountsRemainingAssignment{get;set;}
    public integer prospectsRemainingAssignment{get;set;}
    public integer goalsRemainingAssignment {get;set;}
    list<Staff_Transfer_Request__c>  currentReqs = new list<Staff_Transfer_Request__c>();
    public integer stepCount {get;set;} 
    
    public final static string OWNERID = 'OwnerId';
    public final static string PROSPECT_RT_NAME = 'Prospect'; //KR: check
    
    /******************************************************************************
    Wrapper class for Account Information
    *******************************************************************************/
    public class AccountInfo {
        public Account acct{get;set;}
        public boolean isSelected{get;set;}
        // constructor
        public AccountInfo(Account acct, boolean isSelected) {
          this.isSelected = isSelected;
          this.acct = acct;
        }
    }
    /******************************************************************************
    Wrapper class for Prospect Information
    *******************************************************************************/
    public class prospectInfo {
        public Account prospect{get;set;}
        public boolean isSelected{get;set;}
        // constructor
        public prospectInfo(Account prospect, boolean isSelected) {
          this.isSelected = isSelected;
          this.prospect = prospect;
        }
    }
    /******************************************************************************
    Wrapper Class for Goal Information
    *******************************************************************************/
        public class GoalInfo{
            public Goal__c goal{get;set;}
            public boolean isSelected {get;set;}
            public GoalInfo(Goal__c goal, boolean isSelected){
                this.isSelected = isSelected;
                this.goal = goal;
            }
        }
    
    /***********************************************************************
    Constructor
    ************************************************************************/
    public StaffTransferCntl() {
       init();
    }
    /**************************************************************************
    Method to initialize collections
    ***************************************************************************/
    private void init() {
       fromUser = new Account();
       toUser = new Account();
       accounts = new list<AccountInfo>();
       prospects = new list<ProspectInfo>();
       goals = new list<GoalInfo>();
       accountsSelected = new list<AccountInfo>();
       prospectsSelected = new list<ProspectInfo>();
       goalsSelected = new list<goalInfo>();
       stepCount = accountsRemainingAssignment = prospectsRemainingAssignment = goalsRemainingAssignment = 0;
       accountsRemained = new list<AccountInfo>();
       prospectsRemained = new list<ProspectInfo>();
       goalsRemained = new list<GoalInfo>();
      
    }
    /**************************************************************************
    Method to clear collection variables
    ***************************************************************************/
    public void clearCollections() {
       //fromUser = new Account();
       //toUser = new Account();
       accounts.clear();
       prospects.clear();
       goals.clear();
       accountsSelected.clear();
       prospectsSelected.clear();
       goalsSelected.clear();
       stepCount = 0;
    }
    
    /**************************************************************************
    Method to increment the counter
    ***************************************************************************/
    public void incrementCounter() {
       stepCount++;
       utilRetrieveData();
       
    }
    /**************************************************************************
    Method to select all Accounts
    ****************************************************************************/
    public void selectAllAccounts() {
        for(AccountInfo inf : accounts) {
            if(selectAllAccount)
                inf.isSelected = true;
            else
                inf.isSelected = false;
        }
    }
    /**************************************************************************
    Method to select all prospects
    ****************************************************************************/
    public void selectAllProspects() {
        for(ProspectInfo inf : prospects) {
            if(selectAllProspects)
                inf.isSelected = true;
            else
                inf.isSelected = false;
        }
    }
    /**************************************************************************
     Method to select all Goals
     ****************************************************************************/
     public void selectAllGoals() {
        for(GoalInfo inf : goals) {
            if(selectAllGoals)
                inf.isSelected = true;
            else
                inf.isSelected = false;
        }
      }
    
    /**************************************************************************
    Method to decrease the counter
    ***************************************************************************/
    public void decreaseCounter() {
       stepCount--;
       System.debug('Step Count: ' + stepCount);
       utilRetrieveData();
    }
    /*****************************************************************************
    Utility method to retrieve data
    ******************************************************************************/
    private void utilRetrieveData() {
       if(stepCount == 0){
          clearCollections();
       }

       if(stepCount == 1 && currentReqs.size() == 0)
            currentReqs.addAll(getCurrentRequests());
       
       if(stepCount == 1 && prospects.size() == 0)
            retrieveProspectsOwned();
       else if(stepCount == 2 && accounts.size() == 0)
            retrieveAccountsOwned();
        else if(stepCount == 3 && goals.size() == 0)
            retrieveGoalsOwned();
       else if(stepCount == 4)
           filterSelections();   
    }
    /***********************************************************************
    Method to identify the records selected
    ************************************************************************/
    public void filterSelections() {
       accountsSelected.clear();
       prospectsSelected.clear();
       goalsSelected.clear();
       accountsRemained.clear();
       prospectsRemained.clear();
       goalsRemained.clear();
       
       // prospects
       for(ProspectInfo inf : prospects) {
          if(inf.isSelected) {
              prospectsSelected.add(inf);
          }
          else
              prospectsRemained.add(inf);
       } 
       // accounts
       for(AccountInfo inf : accounts) {
          if(inf.isSelected) {
              accountsSelected.add(inf);
          }
          else
              accountsRemained.add(inf);
       }
       //goals
               for(GoalInfo g:goals){
                if(g.isSelected){
                   goalsSelected.add(g); 
                }
                else
                    goalsRemained.add(g);
            }
     
    }
    /**********************************************************************************
    Method to retrieve Staff transfer Requests for the current user
    ***********************************************************************************/
    private list<Staff_Transfer_Request__c> getCurrentRequests() {
        return [SELECT Account__c, Prospect__c, Goal__c FROM Staff_Transfer_Request__c WHERE Current_Owner__c = :fromUser.OwnerId AND Effective_Date__c > =TODAY AND Is_Processed__c = False];  
    }
    /********************************************************************************************************
    Method to retrieve Prospects Owned by the user
    *********************************************************************************************************/
    public void retrieveProspectsOwned() {
       if(fromUser.OwnerId != Null) {
             final string STAFF_TRANSFER_FIELDS = 'Staff_Transfer_Fields';
             final string PROSPECT_OBJ = 'Account';
             string query = getfieldSetInfo(STAFF_TRANSFER_FIELDS,PROSPECT_OBJ);
             // make sure there is no entry for the account record in the Staff Transfer request object
             set<string> prospectIdsToExclude = new set<string>();
             for(Staff_Transfer_Request__c req : currentReqs)  
                prospectIdsToExclude.add(req.prospect__c);
             
              
             if(!String.isEmpty(query)) {
                 // see if the query has Ownerid
                 query = queryHelper(query,OWNERID);
                 query = 'SELECT ' + query + ' FROM Account WHERE id NOT IN :prospectIdsToExclude AND RecordType.Name =\''+PROSPECT_RT_NAME+ '\'';
                
                 //although the id field will never have special characters just being cautious
                 query = query + ' AND OwnerId = \'' + String.escapeSingleQuotes(fromUser.OwnerId) + '\''; 
                 System.debug('@@@@@KR:InsideRetrieveQuery::'+query);
                 for(Account pro : database.query(query)) {
                    prospectInfo  info = new prospectInfo(pro,false);
                    prospects.add(info);
                 }  
             }
         }
    }
    /***********************************************************************
    Method to retrieve Accounts Owned by a user
    ************************************************************************/
    public void retrieveAccountsOwned() {
       if(fromUser.OwnerId != Null) {
             final string STAFF_TRANSFER_FIELDS = 'Staff_Transfer_Fields';
             final string ACCOUNT_OBJ = 'Account';
             
             string query = getfieldSetInfo(STAFF_TRANSFER_FIELDS,ACCOUNT_OBJ);
             // make sure there is no entry for the account record in the Staff Transfer request object
             set<string> accountIdsToExclude = new set<string>();
             for(Staff_Transfer_Request__c req : currentReqs)  
                accountIdsToExclude.add(req.Account__c);
             
              
             if(!String.isEmpty(query)) {
                 // see if the query has Ownerid
                 query = queryHelper(query,OWNERID);
                 query = 'SELECT ' + query + ' FROM Account WHERE id NOT IN :accountIdsToExclude AND RecordType.Name !=\''+PROSPECT_RT_NAME+ '\'';
                
                 //although the id field will never have special characters just being cautious
                 query = query + ' AND (OwnerId = \'' + String.escapeSingleQuotes(fromUser.OwnerId) + '\''; 
                 // get the accountIds where the user is part of Account teams
                
                 query = query + ') limit 1000';
                 for(Account acc : database.query(query)) {
                      AccountInfo  info = new AccountInfo(acc,false);
                      accounts.add(info);
                 }  
             }
         
         }
    }
    /********************************************************************************************************
    Method to retrieve Goals Owned by the user
    *********************************************************************************************************/
    public void retrieveGoalsOwned(){
            if(fromUser.OwnerId != Null){
                final string GOAL_TRANSFER_FIELDSET = 'Goal_Transfer_Fields';
                final string GOAL_OBJ = 'Goal__c';
                
                string query = getfieldSetInfo(GOAL_TRANSFER_FIELDSET, GOAL_OBJ);
                set<String> goalsToExclude = new set<String>();
                for(Staff_Transfer_Request__c str: currentReqs){
                  goalsToExclude.add(str.goal__c);
                }
                
                if(!String.isEmpty(query)){
                  system.debug('Excluded goals....'+goalsToExclude);
                  query = 'SELECT ' + query + ' , Product_Line_Name__c, Cycle_Begin_Date__c FROM Goal__c WHERE id NOT IN :goalsToExclude';
                  query = query + ' AND (Assigned_To__c = \'' + String.escapeSingleQuotes(fromUser.OwnerId) + '\''; 
                  query = query + ')';
                  System.debug('@@@@@@@KR::Query::'+query);  
                  system.debug('plan effective start date..'+toUser.Plan_Effective_Start_Date__c);
                    for(Goal__c g : database.query(query)) {
                        if(g.Cycle_Begin_Date__c<>null && (g.Cycle_Begin_Date__c>= toUser.Plan_Effective_Start_Date__c)){
                            GoalInfo  info = new GoalInfo(g,false);
                            goals.add(info);
                        }
                        else if(g.Effective_Date__c!= null && (g.Effective_Date__c >= toUser.Plan_Effective_Start_Date__c)) {
                            GoalInfo  info = new GoalInfo(g,false);
                            goals.add(info);
                        }
                   }
                   system.debug('addded goals...'+goals);
               } 
           }
         }   
    /****************************************************************************************
    Method to build a dynamic query
    *****************************************************************************************/
    public string getfieldSetInfo(string fldsetName,string objectName) {
        string query;
        if(!string.isEmpty(fldsetName) && !string.isEmpty(objectName)) {
            map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe(); 
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
            if(SObjectTypeObj != Null) {
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fldsetName);
                
                if(fieldSetObj != Null) {
                    // retrieve the fieldset information
                     for(Schema.FieldSetMember fldMember : fieldSetObj.getFields()) {
                            if(String.isEmpty(query))
                                query = fldMember.getFieldPath();
                            else
                                query = query + ',' + fldMember.getFieldPath();
                      }
                }
            }
        }
       return query;
    }
    
    /******************************************************************************
    Method to retrieve Accounts Where the Given User is a member of the AccountTeam
    *******************************************************************************/
    private set<string> getAccountsSharedByTeam(string userId) {
       set<string> acctIds = new set<string>();
       if(!String.isEmpty(userId)) {
          for(AccountTeamMember member : [SELECT AccountId FROM AccountTeamMember WHERE UserId = :userId])
              acctIds.add(member.AccountId);
       }
       return acctIds; 
    }
    
    /***********************************************************************
    Util Method to help build the dynamic query
    ************************************************************************/
    private string queryHelper(string query, string fld) {
      // see if the query has Ownerid
      if(!query.containsIgnoreCase(fld))
          query = query + ',' + fld;
      return query;    
    }
    
    /********************************************************************************************************
    Method to submit the Staff Transfer Request
    *********************************************************************************************************/
    public void submitTransferRequest() {
        stepCount++;
        list<Staff_Transfer_Request__c> req = new list<Staff_Transfer_Request__c>();
        list<AccountTeamMember> accountTeams = new list<AccountTeamMember>();
        list<AccountShare> accountShares = new list<AccountShare>();
        map<string,string> accountAccess = new map<string,string>();    
        
        
        // process selected Accounts
        for(AccountInfo act : accountsSelected) {
            Staff_Transfer_Request__c r = new Staff_Transfer_Request__c(Account__c = act.acct.Id,Current_Owner__c = fromUser.OwnerId,New_Owner__c = toUser.OwnerId,Effective_Date__c = toUser.Plan_Effective_Start_Date__c);
            req.add(r);
            AccountTeamMember mem = StaffTransferBatchClass.createAccountTeamMember(act.acct.Id,toUser.OwnerId);
            mem.TeamMemberRole = Label.Temp_Access_Label;  
            accountTeams.add(mem);
        }
        // process prospect records
        for(ProspectInfo inf : prospectsSelected) {
            Staff_Transfer_Request__c r = new Staff_Transfer_Request__c(Prospect__c = inf.prospect.Id,Current_Owner__c = fromUser.OwnerId,New_Owner__c = toUser.OwnerId,Effective_Date__c = toUser.Plan_Effective_Start_Date__c, Is_Processed__c = false);
            req.add(r);
            AccountTeamMember mem = StaffTransferBatchClass.createAccountTeamMember(inf.prospect.Id,toUser.OwnerId);
            mem.TeamMemberRole = Label.Temp_Access_Label;  
            accountTeams.add(mem);
        }
            // process selected Goals
        for(GoalInfo goalSel : goalsSelected) {
            Staff_Transfer_Request__c r = new Staff_Transfer_Request__c(Goal__c = goalSel.goal.Id,Current_Owner__c = fromUser.OwnerId, New_Owner__c = toUser.OwnerId, Effective_Date__c = toUser.Plan_Effective_Start_Date__c);
            req.add(r);
        }
        
        // insert staff transfer request
        if(req.size() > 0)
            insert req;
        //insert Account teams  
        integer counter = 0;
        for(database.saveresult r : database.insert(accountTeams,false)) {
            if(r.isSuccess()) {
                string access = Label.Staff_Transfer_Account_Access_Level;
                if(!String.isEmpty(access)) {
                     AccountShare shareRec = new AccountShare(UserOrGroupId = accountTeams[counter].UserId,AccountId = accountTeams[counter].AccountId,AccountAccessLevel = access,OpportunityAccessLevel = Label.Account_Team_Oppty_Access);
                     accountShares.add(shareRec);
                }
            }
            counter++;
        }
        
        //Insert AccountShares
        if(accountShares.size() > 0)
            database.insert(accountShares,true);
            //clearCollections();
        accountsRemainingAssignment = accounts.size() - accountsSelected.size();
        prospectsRemainingAssignment = prospects.size()  - prospectsSelected.size();
        
        // Call Staff Transfer Batch Class when Effective Date <= Today
        
        If(toUser.Plan_Effective_Start_Date__c <= System.Today()){
        System.debug('@@@@@@@@KR::EffectiveDate::'+toUser.Plan_Effective_Start_Date__c+':TodayDate::'+System.Today());
        string todaysDate = System.Now().format('yyyy-MM-dd');   
            string query = 'SELECT Access_through_Account_Team__c,Account__c, Prospect__c, Goal__c,  Current_Owner__c,New_Owner__c,Opportunity__c,Opportunity_Access_through_Sales_Team__c, CreatedDate, Is_Processed__c FROM Staff_Transfer_Request__c WHERE Effective_Date__c <= ' + todaysDate + ' AND  Is_Processed__c ='+false;
            System.debug('Query: ' + query);
            Database.executeBatch(new StaffTransferBatchClass(query), 200); 
        } 
    
    }
    /**************************************************************************************************************************
    Method to reset counters during reassignment
    **************************************************************************************************************************/
    public void resetCounter() {
       stepCount = 0;
       accounts.clear();
       prospects.clear();
       goals.clear();
       // populate the collections
       accounts.addAll(accountsRemained);
       prospects.addAll(prospectsRemained);
       goals.addAll(goalsRemained);
       
       
    }

}