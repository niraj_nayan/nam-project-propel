/****************************************************************************************************************************************************
Description: To populate CustomerCredit information on Account by consuming JSON string            
Created on:  16th September 2015
Created by:  Kavitha
Version 1:   Controller for CustomerProfileInline VF page to parse the JSON feed. 
             
******************************************************************************************************************************************************/

public with sharing class AcctCreditProfileExtn {

    public CreditProfileUtility.profileDetails profDetail {get;set;}
    String sampleString;
    String creditProfileInfo;
    public Account acct {get;set;}
    public Map<String, Decimal> dateAgingValues = new Map<String, Decimal>();
    public Decimal currentAmount {get;set;}
    public Decimal firstPastDueAmount {get;set;}
    public Decimal secondPastDueAmount {get;set;}
    public Decimal thirdPastDueAmount {get;set;}
    public boolean isRendable {get;set;}
    
    public AcctCreditProfileExtn(ApexPages.StandardController con){ 
      // get the Account site id

      
      acct = (Account) con.getRecord();
      acct = [SELECT Site,Collector_Name__c, OwnerId FROM Account WHERE Id = :acct.Id];
      isRendable = canViewFinancialData(acct);
      
      sampleString =Label.CreditProfileSampleString1+Label.CreditProfileSampleString2+Label.CreditProfileSampleString3;
      if(!String.isEmpty(acct.Site)) {
      
          String userName = CreditProfileCredentials__c.getInstance('CreditProfile').UserName__c;//'NAMCreditProfile';
          String pwd=CreditProfileCredentials__c.getInstance('CreditProfile').Password__c;//'NAMCreditProfilePwd';    
          
          //Get the encoded crypto key from custom label and decode it
          Blob decodedKey = EncodingUtil.base64decode(Label.encodedKey);
          //System.debug(decodedKey);
          Blob un = Blob.valueOf(userName);
          Blob pd = Blob.valueOf(pwd);
          
          Blob encryptedUn = Crypto.encryptWithManagedIV('AES128', decodedKey, un);
          Blob encryptedPd = Crypto.encryptWithManagedIV('AES128', decodedKey, pd);
          String encryptedUnF = EncodingUtil.base64encode(encryptedUn);
          String encryptedPdF = EncodingUtil.base64encode(encryptedPd);
          encryptedUnF = EncodingUtil.urlEncode(encryptedUnF,'UTF-8');
          encryptedPdF = EncodingUtil.urlEncode(encryptedPdF,'UTF-8');
          //String encryptedUn = EncodingUtil.base64encode(un);
          //String encryptedPd = EncodingUtil.base64encode(pd);
          
          //below code is for decrpytion
          //Blob decryptedValue = Crypto.decryptWithmanagedIV('AES256', decodedKey, encryptedUn);
          //System.debug(decryptedValue.toString());
          system.debug('&userName='+encryptedUnF+'&pwd='+encryptedPdF);
          string url = Label.Credit_Profile_End_Point + acct.Site +'&userName='+encryptedUnF+'&pwd='+encryptedPdF;
          // Instantiate a new http object
          Http h = new Http();
          // Instantiate a new HTTP request, specify the method (POST) as well as the endpoint
          Continuation cont = new Continuation(40);
          cont.continuationMethod='processResponse';
          
          HttpRequest req = new HttpRequest();
          req.setEndpoint(url);
          req.setMethod('POST');
          req.setTimeout(20000);
          req.setBody(sampleString);
          // Send the request, and return a response
          String requestLabel;
          requestLabel = cont.addHttpRequest(req);
          
          //HttpResponse res = Continuation.getResponse(requestLabel);
          HttpResponse res = h.send(req);
          if(res.getStatusCode() != 200){
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,Label.CreditProfileError);
              ApexPages.addMessage(myMsg);
          }
          else{
              creditProfileInfo = res.getBody();
              System.debug('creditProfileInfo:'+creditProfileInfo);
          }
        }     
        if(creditProfileInfo != Null && creditProfileInfo.contains('UserName or Password or both did not Match')){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Authentication Failed. Please contact your system administrator');
             ApexPages.addMessage(myMsg);    
        }     
        else {  
            If(creditProfileInfo !=Null && !creditProfileInfo.contains('StatusCode=500') && !creditProfileInfo.contains('Response code 503')){
                profDetail =  CreditProfileUtility.parseProfileFeed(creditProfileInfo);          
                System.debug('#######KR:profDetail' +profDetail);
                if(profDetail<>null && profDetail.results<>null && profDetail.results.size()>0){
                  dateAgingValues = profDetail.results[0].dateAgingBuckets;      
                  If(profDetail.results[0].dateAgingBuckets != Null){
                       processDateAging(dateAgingValues); 
                  }
                }
            }
            else {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,Label.CreditProfileError);
                 ApexPages.addMessage(myMsg);
            }      
        }      
    }
    
    public void processDateAging(Map<String, Decimal> dateAgingVal){
        Date currentDateVal = Date.ValueOf(System.Now()); 
        currentAmount= firstPastDueAmount=secondPastDueAmount=thirdPastDueAmount=0.0;
          
        for(String d: dateAgingVal.keySet()){
            
            if(Date.parse(d)>=currentDateVal)               
                currentAmount +=dateAgingVal.get(d);
            else if(Date.parse(d)>=currentDateVal.addDays(-30) && Date.parse(d)<currentDateVal)
                firstPastDueAmount +=dateAgingVal.get(d);
            else if(Date.parse(d)>=currentDateVal.addDays(-60) && Date.parse(d)<currentDateVal.addDays(-31))
                secondPastDueAmount +=dateAgingVal.get(d);
            else if(date.parse(d)<=currentDateVal.addDays(-61))
                thirdPastDueAmount +=dateAgingVal.get(d);
        }
    }

    public boolean canViewFinancialData(Account acct) {

        Set<String> profiles = new Set<String> ();
        for(Account_Financial_Data_View_Profile__c profile : Account_Financial_Data_View_Profile__c.getAll().values()) {
            profiles.add(profile.Name);
        }
        
        User currentUser=[Select Id, Name, Profile.Name From User Where Id=:UserInfo.getUserId()];
        
        if(profiles.contains(currentUser.Profile.Name)){
            return true;
        }
        else{
            if(acct.OwnerId == currentUser.Id){
              return true;
            }
            else{
              Boolean isAccountTeamMember = false;
              List<AccountTeamMember> acctTeamList = [Select Id, AccountId, UserId From AccountTeamMember
                                                      Where AccountId =:acct.Id];
              for(AccountTeamMember member: acctTeamList){
                if(member.UserId == currentUser.Id && isAccountTeamMember == false){
                  isAccountTeamMember = true;
                }
              }
              return isAccountTeamMember;
            }
        }   


    }
    

}