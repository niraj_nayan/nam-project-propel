/********************************************************************************************************************************************
Description: Helper class for quote line item. It will copy the custom fields from product line item to Quote line item
             
Created on:  11/10/2015
Created by:  Jagan Gorre
Version 1:   Created helper class for trigger
********************************************************************************************************************/

public class QuoteLineItemTriggerHelper{
    
    public static void CopyOppLineItemCustomFields (Map<Id,QuoteLineItem> triggernewmap){
        
        Set<Id> oppidset = new Set<Id>();
        Set<Id> quoteidset = new Set<Id>();
        
        List<OpportunityLineItem> lstOli = new List<OpportunityLineItem>();
        Map<string,QuoteAndProductLineItemFieldMap__c> fieldList = QuoteAndProductLineItemFieldMap__c.getAll();  
        map<string,string> mapQuoteOppty=new map<string,string>();
        string JSONContent=Json.Serialize(Trigger.New);
        JSONParser parser =JSON.createParser(JSONContent);
        list<string> OpptyLineId=new list<string>();
        list<string> QuoteLineId=new list<string>();
        
        while (parser.nextToken() != null) 
        {
            system.debug(parser.getCurrentName()+'......'+parser.getText());
            if(parser.getCurrentToken()==JSONToken.VALUE_STRING && parser.getCurrentName()=='OpportunityLineItemId')
                OpptyLineId.add(parser.getText());
            if(parser.getCurrentToken()==JSONToken.VALUE_STRING && parser.getCurrentName()=='Id')
                QuoteLineId.add(parser.getText());
                
            parser.nextToken();
            
            if(parser.getCurrentToken()==JSONToken.VALUE_STRING && parser.getCurrentName()=='OpportunityLineItemId')
                OpptyLineId.add(parser.getText());
            if(parser.getCurrentToken()==JSONToken.VALUE_STRING && parser.getCurrentName()=='Id')
                QuoteLineId.add(parser.getText());
        }

        integer iCount=0;
        for(string strOppLineId : OpptyLineId)
        {
            string qlid=QuoteLineId[iCount];
            mapQuoteOppty.put(qlid,strOppLineId);
            iCount++;
        }
        
        for(Id qlid:triggernewmap.keyset())
            quoteidset.add(triggernewmap.get(qlid).quoteid);
             
        for(Quote q:[select id,opportunityid from Quote where id in: quoteidset])
            oppidset.add(q.opportunityid);
        
        String commaSepratedFields='';
        String quotecommaSepratedFields='';
        for(String qfl:fieldList.keySet()){
            
            if(commaSepratedFields == null || commaSepratedFields == '')
                commaSepratedFields = fieldList.get(qfl).Product_Line_Item_Field_Name__c;
            else
                commaSepratedFields+=','+fieldList.get(qfl).Product_Line_Item_Field_Name__c;
            
            if(quotecommaSepratedFields == null || quotecommaSepratedFields == '')
                quotecommaSepratedFields = qfl;
            else
                quotecommaSepratedFields +=','+qfl;
        }
        
        Map<Id,OpportunityLineItem> MapOLI=new Map<Id,OpportunityLineItem>((List<OpportunityLineItem>)database.Query('select ' + commaSepratedFields + ' from OpportunityLineItem where Opportunityid in :oppidset'));
        Map<Id,QuoteLineItem > MapQLI=new map<Id,QuoteLineItem>((List<QuoteLineItem>)database.Query('select ' + quotecommaSepratedFields + ' from QuoteLineItem where QuoteId in :quoteidset'));
        
        
        list<QuoteLineItem> updateQuoteLineItem =new list<QuoteLineItem >();
        for(QuoteLineItem qli:MapQLI.values())
        {
           
           if(mapQuoteOppty.get(qli.id)!=null)
           {
              String OppID = mapQuoteOppty.get(qli.id);
              OpportunityLineItem OLI = MapOLI.get(OppID);
               
              for(String qfl:fieldList.keySet()){
                  qli.put(qfl,oli.get(fieldList.get(qfl).Product_Line_Item_Field_Name__c));
              }
              
              updateQuoteLineItem.add(qli);
                
           }
        }
        database.update(updateQuoteLineItem,false);
    }
    
    

}