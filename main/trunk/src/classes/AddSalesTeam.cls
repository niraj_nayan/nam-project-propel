/****************************************************************************************************************************************************
Description: Controller to add sales team for opportunity        
Created on:  8/24/2015
Created by:  Seth
Version 1:   Controller for Add_Sales_Team page
******************************************************************************************************************************************************/


public class AddSalesTeam {

    public string OpportId = ApexPages.currentpage().getparameters().get('addTo');
    public String saveMoreButtonClick {get;set;}
    public String opptyName {get;set;}
    public List<SelectOption> teamRoles{get;set;}
    public List<SelectOption> opptyAccessLevel{get;set;}

    public List<SalesTeam> opptyTeamMember{get;set;}

    public User currentUser{get;set;}
    public Boolean standardUser{get;set;}
    public String duplicateUserNames{get;set;}


    public AddSalesTeam() {//check the user and see what profile they have
        init();
    }

    private void init(){
        saveMoreButtonClick = null;
        opptyTeamMember = new List<SalesTeam>();
        //check the current user
        currentUser = new User();

        teamRoles = getTeamMemberRoleOptions();
        opptyAccessLevel = getOpptyAccessOptions();

        for(Integer a = 0; a < 5; a++){
            SalesTeam temp = new SalesTeam();
            opptyTeamMember.add(temp);
        }
        System.debug('standardUser value: ' + standardUser);
    }

    public List<SelectOption> getTeamMemberRoleOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(Label.Select_Option_None, Label.Select_Option_None));
        Schema.DescribeFieldResult fieldResult = OpportunityTeamMember.TeamMemberRole.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple){
            options.add(new SelectOption(p.getValue(), p.getValue()));
        }       
        return options;
    }

    public List<SelectOption> getOpptyAccessOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = OpportunityTeamMember.OpportunityAccessLevel.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple){
            if((p.getValue() != 'None') && (p.getValue() != 'All')){
                if(p.getValue() == Label.Add_Sales_Team_Oppty_Access){
                    options.add(new SelectOption(p.getValue(), p.getValue()));
                }
                else{
                    options.add(new SelectOption(p.getValue(), p.getLabel()));
                }
            }
        }        
        return options;
    }

    public PageReference saveSalesTeam(){
        duplicateUserNames = null;
        System.debug('Save and More Button: ' + saveMoreButtonClick);
        List<OpportunityTeamMember> membersToInsert = new List<OpportunityTeamMember>();

        Boolean savePass = saveAndValidate(duplicateUserNames, membersToInsert);
        if(savePass == false){
            return null;
        }
       
        if(membersToInsert.size() > 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm, membersToInsert.size() + ' Sales Team member(s) has been added');
            ApexPages.addmessage(myMsg);
        }
        if(String.isNotBlank(duplicateUserNames)){
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, 'Sales Team Member(s) cannot be saved due to ' + duplicateUserNames + ' already being a Sales Team Member');
            ApexPages.addmessage(myMsg2);
            init();
            return null;
        }
        if(membersToInsert.size() == 0){
            return null;
        }
        PageReference opptyPage = new PageReference('/' + OpportId);
        return opptyPage;
    }

    public PageReference saveSalesTeamAndMore(){
        duplicateUserNames = null;
        System.debug('Save and More Button: ' + saveMoreButtonClick);
        List<OpportunityTeamMember> membersToInsert = new List<OpportunityTeamMember>();
        
        Boolean savePass = saveAndValidate(duplicateUserNames, membersToInsert);
        if(savePass == false){
            return null;
        }

        //Which save button they clicked on
        if(membersToInsert.size() > 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm, membersToInsert.size() + ' Sales Team member(s) has been added');
            ApexPages.addmessage(myMsg);
        }
        if(String.isNotBlank(duplicateUserNames)){
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, 'Sales Team Member(s) cannot be saved due to ' + duplicateUserNames + ' already being a Sales Team Member');
            ApexPages.addmessage(myMsg2);
        }
        if(membersToInsert.size() == 0){
            return null;
        }
        init();
        return null;
    }

    public Boolean saveAndValidate(String duplicateUserNames, List<OpportunityTeamMember> membersToInsert){
        Map<Id, SalesTeam> sTeamMap= new Map<Id, SalesTeam>();
        Double totalRevenue = [Select Id, Amount, Commission_Roll_Up__c From Opportunity Where Id =:OpportId].Commission_Roll_Up__c;

        Integer noRoleSelected = 0;

        for(SalesTeam sTeam: opptyTeamMember){
            if((sTeam.selectedTeamRole != Label.Select_Option_None) || (sTeam.teamMember.UserId != null)){                
                sTeamMap.put(sTeam.teamMember.UserId,sTeam);
            }
            if(sTeam.selectedTeamRole == Label.Select_Option_None){
                noRoleSelected = noRoleSelected + 1;
            }

            if(sTeam.teamMember.Commission_Percentage__c < 0){
                ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, 'Sales Team Member(s) percentage needs to be greater than 0 ');
                ApexPages.addmessage(myMsg2);
                //init();
                return false;
            }
        }

        //validation check to see if at least one user was added to the opportunity
        if(sTeamMap.isEmpty()){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'There must be at least one user selected to be added to the Sales Team');
            ApexPages.addmessage(myMsg);
            return false;
        }

        System.debug('No Roles Selected: ' + noRoleSelected);
        if(noRoleSelected == 5){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Must select a Team Role for the Sales Team member');
            ApexPages.addmessage(myMsg);
            return false;
        } 

        //have to check to make sure the user is not already inserted
        Map<Id, OpportunityTeamMember> checkTeamMemberMap = new Map<Id, OpportunityTeamMember>();
        List<OpportunityTeamMember> checkTeamMember = new List<OpportunityTeamMember>([Select Id, UserId From OpportunityTeamMember Where OpportunityId =:OpportId AND UserId =:sTeamMap.keySet()]);
        List<OpportunityTeamMember> allTeamMembers = new List<OpportunityTeamMember>([Select Id, UserId, User.Name From OpportunityTeamMember Where OpportunityId =:OpportId]);
        
        for(OpportunityTeamMember otm : checkTeamMember){
            checkTeamMemberMap.put(otm.UserId, otm);
        }
        System.debug('Sales Team members: ' + sTeamMap);
        System.debug('check: ' + checkTeamMemberMap);
        for(SalesTeam sTeam : sTeamMap.values()){    
            if(!checkTeamMemberMap.containsKey(sTeam.teamMember.UserId)){//the user has not been inserted
                //Set the opportunity team member values
                sTeam.teamMember.TeamMemberRole = sTeam.selectedTeamRole;
                sTeam.teamMember.OpportunityId = OpportId;
                sTeam.TeamMember.Commission_Amount__c = (sTeam.teamMember.Commission_Percentage__c *.01) * (totalRevenue);

                if(sTeam.teamMember.Commission_Percentage__c < 0){
                    ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, 'Sales Team Member(s) percentage needs to be greater than 0 ');
                    ApexPages.addmessage(myMsg2);
                    init();
                    return false;
                }

                membersToInsert.add(sTeam.teamMember); //KR included
            }
            else{
                for(OpportunityTeamMember member: allTeamMembers){
                    if(member.UserId == sTeam.teamMember.UserId){
                        if(String.isEmpty(duplicateUserNames)){
                            duplicateUserNames = member.User.Name + ' ';
                        }
                        else{
                            duplicateUserNames = duplicateUserNames +  ', ' + member.User.Name;
                        }
                        //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Cannot be saved due to ' + member.User.Name + ' already being a Sales Team Member');
                        //ApexPages.addmessage(myMsg);
                        //return null;
                    }
                }
                //memberstoUpdate

            }
            System.debug('dup: ' + duplicateUserNames);
        }

        if(String.isNotBlank(duplicateUserNames)){
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, 'Sales Team Member(s) cannot be saved due to ' + duplicateUserNames + ' already being a Sales Team Member');
            ApexPages.addmessage(myMsg2);
        }

        if(membersToInsert.size() > 0){
            System.debug('Member to insert: ' + membersToInsert);
            try{
                insert membersToInsert;
            
                //may turn back list. dont know why
                //this will fix that problem right away
                List<OpportunityShare> updateShare = new List<OpportunityShare>();
                List<OpportunityShare> share =[Select Id, OpportunityAccessLevel, OpportunityId, RowCause, UserOrGroupId From OpportunityShare
                                                 Where OpportunityId =:OpportId AND RowCause = 'Team' AND UserOrGroupId =:sTeamMap.keySet()];
                            
                for(SalesTeam sTeam : sTeamMap.values()){ 
                    for(OpportunityShare opptyAccess: share){
                        if(sTeam.teamMember.UserId == opptyAccess.UserOrGroupId){
                            if(sTeam.selectedOpptyAccess == 'All'){
                                opptyAccess.OpportunityAccessLevel = 'Edit';
                            }
                            else{
                                opptyAccess.OpportunityAccessLevel = sTeam.selectedOpptyAccess;
                            }
                            if(opptyAccess.RowCause == 'Team'){//only update share if rowcause is TEAM
                                updateShare.add(opptyAccess);
                            }
                        }
                    }
                }
                update updateShare;
            }
            catch(DmlException e){
                System.debug('Failed to delete Opportunity Products: ' + e.getDmlType(0));
                System.debug('Failed to delete Opportunity Products: ' + e.getCause());
                System.debug('Error: ' + e.getMessage());
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, + 'You do not have access to add Sales Teams Members to this Opportunity. Please contact a System Administrator');
                ApexPages.addmessage(myMsg);
                return false;
            }
            catch(Exception e){
                System.debug('Failed to delete Opportunity Products: ' + e.getDmlType(0));
                System.debug('Failed to delete Opportunity Products: ' + e.getCause());
                System.debug('Error: ' + e.getMessage());
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, + 'You do not have access to add Sales Teams Members to this Opportunity. Please contact a System Administrator');
                ApexPages.addmessage(myMsg);
                return false;
            }
        }

        if(membersToInsert.size() > 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm, membersToInsert.size() + ' Sales Team member(s) has been added');
            ApexPages.addmessage(myMsg);
            return true;
        }
        else{
            return false;
        }
    }

    public PageReference redirectOnLoad(){
        currentUser = new User();
        currentUser=[Select Id, Name, Profile.Name From User Where Id=:UserInfo.getUserId()];
        //create a set from list of custom setting SalesTeamsProfiles
        Set<String> profilesAccess = new Set<String>();
        for(Sales_Teams_Profiles__c profile: Sales_Teams_Profiles__c.getAll().Values()){
                profilesAccess.add(profile.Name);
        }
        
        if(profilesAccess.contains(currentUser.Profile.Name)){
            opptyName = [Select Id, Name From Opportunity Where Id =:OpportId].Name;
            standardUser = false;
            return null;
        }
        else{
            opptyName = [Select Id, Name From Opportunity Where Id =:OpportId].Name;
            //PageReference opptyPage = new PageReference('/ui/opportunity/team/MultiOpportunityTeamMemberEdit?oppId=' + OpportId + '&retURL=' + OpportId);
            standardUser = true;
            return null;
        }
    }

    public PageReference cancelButton(){
        PageReference opptyPage = new PageReference('/' + OpportId);
        return opptyPage;
    }

    public class SalesTeam{

        public OpportunityTeamMember teamMember {get;set;}
        public String selectedTeamRole {get;set;}
        public String selectedOpptyAccess {get;set;}

        public SalesTeam(){
            teamMember = new OpportunityTeamMember();
            teamMember.Commission_Percentage__c = 0;
        }
    }
}