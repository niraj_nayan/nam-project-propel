/***************************************************************************************************************************
Description: Controller for Oppty_Sales_Team - Inline VF page for Opportunity
Created on:  8/26/2015
Created by:  Seth

Version 1:   Controller for Oppty_Sales_Team to display the team members inside opportunity layout
********************************************************************************************************************************/
public with sharing class OpptySalesTeamList {

    Id currentId;
    public Integer currentPage{get;set;}
    public String redirectURL{get;set;}
    public Boolean shouldRedirect {get;set;}
    public List<TeamMember> teamMembers{get;set;}
    public List<TeamMember> totalTeamMembers{get;set;}
    public Map<Id,OpportunityTeamMember> mapTeamMember{get;set;}
    public Boolean onFirstPage{get;set;}
    public Boolean nextPageNeeded{get;set;}
    public Boolean isUserHaveEditAccess {get;set;}

    /***********************************************************************
    Constructor
    ************************************************************************/
    public OpptySalesTeamList(ApexPages.StandardController stdcon) {
        teamMembers = new List<TeamMember>();
        totalTeamMembers = new List<TeamMember>();
        mapTeamMember = new Map<Id,OpportunityTeamMember>();
        currentId = stdcon.getId();
        isUserHaveEditAccess = [Select HasEditAccess, RecordId From UserRecordAccess Where UserId =:UserInfo.getUserId() AND RecordId =:currentId].HasEditAccess && checkEditAccess();
        shouldRedirect = false;
        currentPage = 1;
        onFirstPage = true;
        nextPageNeeded = false;

        retrieveTeamMembers();
        for(TeamMember currentTeam: teamMembers){
            mapTeamMember.put(currentTeam.teamMem.Id, currentTeam.teamMem);
        }
    }

    /***********************************************************************
    Method to retieve the team members related to the opportunity
    ************************************************************************/
    public void retrieveTeamMembers(){
        //get list of team members then set them to class of team members
        List<OpportunityTeamMember> tempList = new List<OpportunityTeamMember>();
        tempList = [Select Id, UserId, User.Name, TeamMemberRole, Commission_Percentage__c, Commission_Amount__c From OpportunityTeamMember Where OpportunityId =:currentId];
        for(OpportunityTeamMember temp: tempList){
            TeamMember members = new TeamMember(temp);
            totalTeamMembers.add(members);
            //System.debug('Size of TeamMembers: ' + teamMembers.size());
        }

        Integer tempCount = 0;
        if(totalTeamMembers.size() > 5){
            for(TeamMember members: totalTeamMembers){
                System.debug('Temp Count: ' + tempCount);
                if(tempCount < 5){
                    teamMembers.add(members);
                }
                tempCount = tempCount + 1;
                nextPageNeeded = true;
            }
        }
        else{
                teamMembers = totalTeamMembers;
        }

        System.debug('Size of TeamMembers: ' + totalTeamMembers.size());
    }

    public void nextPage(){
        onFirstPage = false;

        teamMembers.clear();
        currentPage = currentPage + 1;

        Integer recordCountEnd = currentPage * 5;
        Integer recordCountBeginning = recordCountEnd - 5;

        if(totalTeamMembers.size() <= recordCountEnd){
            nextPageNeeded = false;
            recordCountEnd = totalTeamMembers.size();
        }

        System.debug('Beginning count: ' + recordCountBeginning);
        System.debug('End Count: ' + recordCountEnd);

        Integer num = 0;
        for(num = recordCountBeginning; num < recordCountEnd; num++){
            teamMembers.add(totalTeamMembers.get(num));
        }

    }

    public void previousPage(){
        onFirstPage = false;
        nextPageNeeded = true;

        teamMembers.clear();
        currentPage = currentPage - 1;

        if(currentPage == 1){
            onFirstPage = true;
        }

        System.debug('Current Page: ' + currentPage);
        System.debug('On First Page:' + onFirstPage);
        System.debug('Next Button Needed: ' + nextPageNeeded);
        Integer recordCountEnd = currentPage * 5;
        Integer recordCountBeginning = recordCountEnd - 5;

        //if(totalTeamMembers.size() <= recordCountEnd){
        //  nextPageNeeded = false;
        //  recordCountEnd = totalTeamMembers.size();
        //}

        System.debug('Beginning count: ' + recordCountBeginning);
        System.debug('End Count: ' + recordCountEnd);

        Integer num = 0;
        for(num = recordCountBeginning; num < recordCountEnd; num++){
            teamMembers.add(totalTeamMembers.get(num));
        }
    }

    /***********************************************************************
    Method to redirect to the Add Sales Team page
    ************************************************************************/
    public void addTeamMember(){
        //PageReference addTeamPage = new PageReference('/apex/Add_Sales_Team?addTo=' + currentId);
        //addTeamPage.setRedirect(true);
        //return addTeamPage;
        redirectURL = '/apex/Add_Sales_Team?addTo=' + currentId;
        shouldRedirect = true;
    }

    /***********************************************************************
    Method to Delete the team member from the opportunity
    ************************************************************************/
    public void DeleteRecord(){
        String recordID = Apexpages.currentPage().getParameters().get('recordID');
        redirectURL = '/' + currentId;
        shouldRedirect = true;

        delete mapTeamMember.get(recordId);
    }

    /***********************************************************************
    Wrapper class for team member so we can set if the user can view
    commission information
    ************************************************************************/
    public class TeamMember{
        public OpportunityTeamMember teamMem {get;set;}
        public Boolean sameUser {get;set;}
        public Boolean isAllowedToEdit {get;set;}

        /***********************************************************************
        Constructor for wrapper class
        ************************************************************************/
        public TeamMember(OpportunityTeamMember setTeamMem){
            teamMem = setTeamMem;
            sameUser = false;
            if(teamMem.UserId == UserInfo.getUserId()){
                sameUser = true;
            }
            else{
                sameUser = false;
            }
            isAllowedToEdit = false;
            viewAccess();
        }

        /***********************************************************************
        Method to see if current user can edit/view the commission information
        ************************************************************************/
        public void viewAccess(){
            //create a set from list of custom setting SalesTeamsProfiles
            Set<String> profilesAccess = new Set<String>();
            User currentUser=[Select Id, Name, Profile.Name From User Where Id=:UserInfo.getUserId()];
            //Users that can edit and view
            for(Sales_Teams_Profiles__c profile: Sales_Teams_Profiles__c.getAll().Values()){
                profilesAccess.add(profile.Name);
            }
            //only users that can view
            for(Sales_Team_View_Profile__c profile: Sales_Team_View_Profile__c.getAll().Values()){
                profilesAccess.add(profile.Name);
            }

            if(profilesAccess.contains(currentUser.Profile.Name)){
                sameUser = true;
            }
            System.debug('Same User Boolean: ' + sameUser);
        }
    }
    
    public Boolean checkEditAccess(){
        Set<String> profilesAccess = new Set<String>();
        User currentUser=[Select Id, Name, Profile.Name From User Where Id=:UserInfo.getUserId()];
        for(Sales_Teams_Profiles__c profile: Sales_Teams_Profiles__c.getAll().Values()){
            profilesAccess.add(profile.Name);
        }
        if(profilesAccess.contains(currentUser.Profile.Name)){
            return true;
        }
        else{
            return false;
        }
    }

}