/*****************************************************************************************************************
Description: This class is used so that when a product is added or updated to an Opportunity that we update the Opportunity
             The Opportunity will need to be updated so that all of the roll up values can be updated correctly
             as well as the sales team members commission amount can be correctly evaluated.
             
Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   created the helper class
Version 2:   Jagan: Added new method to delete opportunity goal record if product is deleted
Version 3:   Stephen: Added new method to clone product items for parents of ISOP opportunities
********************************************************************************************************************/

Public class OpportunityProductTriggerHelper {
    
    /*******************************************************************************************
    Method to update the Opportunities that is related to the Line Item.
    Update the Opportunity cause we need to update the Sales Team Members Commission Amount and the Opportunity Commission Amount
    ********************************************************************************************/
    public static void afterProductOpportunityUpdate(List<OpportunityLineItem> opptyItem, Map<Id,OpportunityLineItem> oldMap){
        list<Opportunity> opptys = new list<Opportunity>();
        //<string> filteredOppties = new set<string>();
        //check to see if the old totalprice is changed
        //create a set of opportunity ids
        for(OpportunityLineItem oppLine : opptyItem) {
            if(oppLine.TotalPrice != oldMap.get(oppLine.Id).TotalPrice){
                Opportunity updateOppty = new Opportunity();
                updateOppty.Id = oppLine.OpportunityId;
                opptys.add(updateOppty);
                //filteredOppties.add(oppLine.OpportunityId);
            }
        }
        Set<Opportunity> oppSet = new Set<Opportunity>();
        List<Opportunity> oppList = new List<Opportunity>();
        oppSet.addAll(opptys);
        oppList.addAll(oppSet);


        //opptys = [Select Id, Name from Opportunity Where Id in:filteredOppties];
        Database.update(oppList, false);
    }
     /*******************************************************************************************
    Method to update the goals related to opportunity team when the opportunity line item is deleted.
    COMMENTED BASED ON SFIT-971
    ********************************************************************************************/
    /*public static void afterDeleteUpdateGoals(List<OpportunityLineItem> lstoli){
        
        Set<Id> oppidset = new Set<Id>();
        set<id> prodidset = new set<Id>();
        List<goal__c> lstgoals = new List<goal__c>();
        for(OpportunityLineItem oli: lstoli){
            oppidset.add(oli.opportunityid);
            prodidset.add(oli.product2id);                
        }
        if(oppidset.size()>0){
            List<Opportunity_Goals__c> deloppgoals = new List<Opportunity_Goals__c>();
                List<Opportunity_Goals__c> lstoppgoal = new List<Opportunity_Goals__c>();
                //get all opportuntity goal records related to current opportunities
                lstoppgoal = [select id,opportunity__c,Other_Revenue__c,Participation_Revenue__c,Production_Revenue__c,Space_Revenue__c, goal__c,goal__r.Product__c,goal__r.ownerid,
                                goal__r.Paced_Space_Revenue__c, goal__r.Paced_Participation_Revenue__c,goal__r.Paced_Other_Revenue__c,goal__r.Paced_Production_Revenue__c 
                                from Opportunity_Goals__c where opportunity__c in:oppidset];
                
                for(Opportunity_Goals__c og:lstoppgoal){
                    if(prodidset.contains(og.goal__r.Product__c))
                        deloppgoals.add(og);
                }

                if(deloppgoals.size()>0)
                    Database.delete(deloppgoals,false);

        }

    }*/
    /*******************************************************************************************
    Method to create a clone of products that link to the parent opportunities of the ISOPs they're linked to.
    Will only create a clone for the parent if the parent doesn't already have a product.
    ********************************************************************************************/
    public static void afterUpsertCloneForISOPParent(List<OpportunityLineItem> opptyItem){
        List<Id> productOpptyIds = new List<Id>();
        Map<Id, Id> oppRecordTypeMapID = new Map<Id, Id>();
        //Get IDs of child isops
        for(OpportunityLineItem oli : opptyItem){
            productOpptyIds.add(oli.OpportunityId);
        }
        //Select child isops
        Map<Id, Id> parentOpptysByChild = new Map<Id, Id>();
        List<Opportunity> productOpptys = [SELECT Id, Parent_Opportunity__c, Legacy_Source__c, RecordTypeId, CurrencyIsoCode, Type
                                           FROM Opportunity
                                           WHERE Id IN :productOpptyIds];
        for(Opportunity oppor : productOpptys){
            oppRecordTypeMapID.put(oppor.Id, oppor.RecordTypeId);
        }
        //Query for all of the record types and create a map of record type Id and record Type name
        Map<Id,Schema.RecordTypeInfo> rtMapByID = Schema.Sobjecttype.Opportunity.getRecordTypeInfosById();
        //Map child ids to parent ids
        for(Opportunity oppor : productOpptys){
            if(oppor.Legacy_Source__c == null && rtMapByID.get(oppor.RecordTypeId).getName() == Label.OppRecordTypeInStore && !(oppor.CurrencyIsoCode == 'CAD' && oppor.Type == 'Standard') && !String.isBlank(oppor.Parent_Opportunity__c)){
                parentOpptysByChild.put(oppor.Id, oppor.Parent_Opportunity__c);
            }
        }
        List<Id> parentOpptyIds = parentOpptysByChild.values();
        //Get line items of parent opps
        List<OpportunityLineItem> parentOpptyItems = [SELECT Id, OpportunityId
                                 FROM OpportunityLineItem
                                 WHERE OpportunityId IN :parentOpptyIds];
        Map<Id, OpportunityLineItem> parentOpptyItemsByParent = new Map<Id, OpportunityLineItem>();
        //Map parent line items to parents
        for(OpportunityLineItem parentItem : parentOpptyItems){
            parentOpptyItemsByParent.put(parentItem.OpportunityId, parentItem);
        }
        //If parent opp has no line items, put null spot in map
        for(Id parentId : parentOpptyIds){
            if(!parentOpptyItemsByParent.containsKey(parentId)){
                parentOpptyItemsByParent.put(parentId, null);
            }
        }
        //Clone the new products; if the parent of each product's ISOP doesn't have its own product,
        //set the cloned product's opportunity to that parent
        List<OpportunityLineItem> newOpptyItems = opptyItem.clone();
        List<OpportunityLineItem> newOpptyItemsToInsert = new List<OpportunityLineItem>();
        OpportunityLineItem tempProduct = new OpportunityLineItem();
        for(OpportunityLineItem oli : newOpptyItems){
            if(parentOpptysByChild.containsKey(oli.OpportunityId)){
                if(parentOpptyItemsByParent.containsKey(parentOpptysByChild.get(oli.OpportunityId))){
                    if(parentOpptyItemsByParent.get(parentOpptysByChild.get(oli.OpportunityId)) == null){
                        tempProduct.Quantity = oli.Quantity;
                        tempProduct.Pricing_Detail__c = oli.Pricing_Detail__c;
                        tempProduct.TotalPrice = oli.TotalPrice;
                        tempProduct.Sales_Price__c = oli.Sales_Price__c;
                        tempProduct.ServiceDate = oli.ServiceDate;
                        tempProduct.PricebookEntryId = oli.PricebookEntryId;
                        tempProduct.OpportunityId = parentOpptysByChild.get(oli.OpportunityId);
                        //tempProduct.Product_Line__c = product.Product_Line__c;
                        tempProduct.Commissionable__c = oli.Commissionable__c;
                        tempProduct.Charge_Type_Category_text__c = oli.Charge_Type_Category_text__c;
                        tempProduct.Charge_Type__c = oli.Charge_Type__c;
                        tempProduct.Circulation__c = oli.Circulation__c;
                        tempProduct.Component__c = oli.Component__c;
                        tempProduct.Beginning_Range__c = oli.Beginning_Range__c;
                        tempProduct.End_Range__c = oli.End_Range__c;
                        newOpptyItemsToInsert.add(tempProduct);
                        tempProduct = new OpportunityLineItem();
                    }
                }
            }
        }
        System.debug(newOpptyItemsToInsert);
        try{
            insert newOpptyItemsToInsert;
        }
        catch (DMLException e){
            System.debug(e);
        }
    }
}