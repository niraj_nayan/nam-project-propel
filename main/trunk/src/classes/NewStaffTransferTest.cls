@isTest
private class NewStaffTransferTest {

	@isTest static void TestAccountTransferSuccess() {
		User user1 = TestDataUtils.createUser('first user', 'no-reply@email.com', '1231231234');
		user1.FederationIdentifier = 'firstuser';
		User user2 = TestDataUtils.createUser('second user', 'no-reply2@email.com', '1231231235');
		user2.FederationIdentifier = 'seconduser';
		insert user1;
		insert user2;
		System.debug(user1);
		System.debug(user2);
		Account account1 = new Account(Name='TestAccount', OwnerId = user1.Id);
		insert account1;
		Account account2 = new Account(Name='TestAccount2', OwnerId = user1.Id);
		insert account2;
		AccountTeamMember accmember1 = new AccountTeamMember(AccountId = account2.Id, UserId=user1.Id, TeamMemberRole='Support');
		insert accmember1;
		//user territory association
		//territory
		//^ above two only for staff transfer
		Goal__c goal1 = new Goal__c(OwnerId = user1.Id, Effective_Date__c = Date.today()+60);
		insert goal1;
		//goals
		//opportunities
		Opportunity opp1 = new Opportunity(StageName = 'Contract', CloseDate = Date.today()+90, AccountId = account2.Id, Name='TestOpp', OwnerId = user1.Id, Insert_Date__c = Date.today()+60);
		System.runAs(user1){
			insert opp1;
		}
		//opp team members
		OpportunityTeamMember oppmember1 = new OpportunityTeamMember(OpportunityId=opp1.Id, UserId=user1.Id);
		insert oppmember1;
		NewStaffTransfer controller = new NewStaffTransfer();
		controller.isStaffTransfer = false;
		controller.transferRequest.Current_Owner__c = user1.Id;
		controller.transferRequest.New_Owner__c = user2.Id;
		controller.transferRequest.Effective_Date__c = Date.today()+30;
		Test.startTest();
		controller.incrementCounter();
		controller.incrementCounter();
		controller.accountTableValues[0].isSelected = true;
		controller.incrementCounter();
		controller.incrementCounter();
		controller.opptyTableValues[0].isSelected = true;
		controller.incrementCounter();
		controller.incrementCounter();
		controller.goalTableValues[0].updateOwner = true;
		controller.incrementCounter();
		controller.incrementCounter();
		//final review
		controller.incrementCounter();
		controller.stepCounter -= 1;
		controller.decreaseCounter();
		controller.decreaseCounter();
		controller.decreaseCounter();
		controller.decreaseCounter();
		controller.decreaseCounter();
		controller.decreaseCounter();
		controller.decreaseCounter();
		controller.decreaseCounter();
		controller.decreaseCounter();
		Test.stopTest();
		opp1 = [SELECT Name, OwnerId FROM Opportunity][0];
		System.assertNotEquals(null, opp1);
	}

	@isTest static void TestStaffTransferSuccess() {
	    Id divisiontypeid;
    	Id regiontypeid;
    	Id teamtypeid;

    	User user1 = TestDataUtils.createUser('first user', 'no-reply@email.com', '1231231234');
		user1.FederationIdentifier = 'firstuser';
		User user2 = TestDataUtils.createUser('second user', 'no-reply2@email.com', '1231231235');
		user2.FederationIdentifier = 'seconduser';
		insert user1;
		insert user2;

   		List<Territory2Type> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerrTypeTestData');
	    //[SELECT id, DeveloperName,MasterLabel from Territory2Type]
	    for (Territory2Type te: listTerritoryType){
	        if(te.MasterLabel == 'TestDivision')
	            divisiontypeid = te.id;
	        else if(te.MasterLabel == 'TestRegion')
	            Regiontypeid = te.id;
	        else if(te.MasterLabel == 'TestTeam')
	            Teamtypeid = te.id;
	    }
	    
	    //id modelid = [select id from Territory2Model where name='NAM Sales Hierarchy'].id;
	    Territory2Model terrModel = new Territory2Model();
	    terrModel .DeveloperName='TestNAMModelName'; // required field
	    terrModel.Name = 'Name'; // required field
	    insert terrModel ;

	    Territory2 terr = new Territory2();
	    terr.DeveloperName = 'TestDivision';
	    terr.Name ='TestDivision';
	    terr.Territory2TypeId = divisiontypeid;
	    terr.Territory2ModelId = terrModel.id;
	    insert terr;
	    
	    Territory2 terr2 = new Territory2();
	    terr2.DeveloperName = 'TestRegional';
	    terr2.Name ='TestRegional';
	    terr2.Territory2TypeId = Regiontypeid;
	    terr2.Territory2ModelId = terrModel.id;
	    terr2.ParentTerritory2Id = terr.id;
	    insert terr2;
	    
	    Territory2 terr3 = new Territory2();
	    terr3.DeveloperName = 'TestTeam';
	    terr3.Name ='TestTeam';
	    terr3.Territory2TypeId = Teamtypeid;
	    terr3.Territory2ModelId = terrModel.id;
	    terr3.ParentTerritory2Id = terr2.id;
	    insert terr3;
	    UserTerritory2Association u2a = new UserTerritory2Association();
	    u2a.userid = user1.id;
	    u2a.territory2id = terr.id;
	    u2a.RoleInTerritory2 = 'Primary';
	    insert u2a;
	    
	    UserTerritory2Association u2a2 = new UserTerritory2Association();
	    u2a2.userid = user2.id;
	    u2a2.territory2id = terr2.id;
		u2a2.RoleInTerritory2 = 'Primary';
	    insert u2a2;

		System.runAs(user1){
			Account account2 = new Account(Name='TestAccount2', OwnerId = user1.Id);
			insert account2;
			AccountTeamMember accmember1 = new AccountTeamMember(AccountId = account2.Id, UserId=user1.Id, TeamMemberRole='Support');
			insert accmember1;
			Goal__c goal1 = new Goal__c(OwnerId = user1.Id, Effective_Date__c = Date.today()-60);
			insert goal1;
			//goals
			//opportunities
			Opportunity opp1 = new Opportunity(StageName = 'Contract', CloseDate = Date.today()+90, AccountId = account2.Id, Name='TestOpp', OwnerId = user1.Id, Insert_Date__c = Date.today()+60);
			insert opp1;
			//opp team members
			OpportunityTeamMember oppmember1 = new OpportunityTeamMember(OpportunityId=opp1.Id, UserId=user1.Id);
			insert oppmember1;
			Territory_Membership_History__c history1 = new Territory_Membership_History__c();
			history1.Territory_Node__c = terr.Id;
			history1.User__c = user1.Id;
			insert history1;
			Test.startTest();
			NewStaffTransfer controller = new NewStaffTransfer();
			controller.isStaffTransfer = true;
			controller.transferRequest.Current_Owner__c = user1.Id;
			controller.transferRequest.New_Owner__c = user2.Id;
			controller.transferRequest.Effective_Date__c = Date.today()+30;
			controller.transferRequestGoals.Current_Owner__c = user1.Id;
			controller.transferRequestGoals.New_Owner__c = user2.Id;
			controller.transferRequestGoals.Effective_Date__c = Date.today()+30;
			controller.incrementCounter();
			controller.incrementCounter();
			controller.selectedFromTerritoryId = terr.Id;
			controller.selectedTerritoryId = terr2.Id;
			controller.incrementCounter();
			controller.accountTableValues[0].isSelected = true;
			controller.incrementCounter();
			controller.incrementCounter();
			controller.goalTableValues[0].updateOwner = true;
			controller.goalTableValues[0].freezeHierarchy = true;
			controller.incrementCounter();
			controller.incrementCounter();
			controller.goalTableValues[0].updateOwner = true;
			controller.incrementCounter();
			controller.incrementCounter();
			controller.incrementCounter();
			controller.stepCounter -= 1;
			controller.decreaseCounter();
			controller.decreaseCounter();
			controller.decreaseCounter();
			controller.decreaseCounter();
			controller.decreaseCounter();
			controller.decreaseCounter();
			controller.decreaseCounter();
			controller.decreaseCounter();
			controller.decreaseCounter();
			controller.decreaseCounter();
			Test.stopTest();
			account2 = [SELECT Name, Id, OwnerId FROM Account][0];
			System.assertNotEquals(null, account2);
		}
	}
}