@isTest
private class userAssociationTriggerHelperTest {
  
  static testMethod void UpdatePrimaryCategory() {
    Id divisiontypeid;
    Id regiontypeid;
    id teamtypeid;
    
    List<Territory2Type> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerrTypeTestData');
    //[SELECT id, DeveloperName,MasterLabel from Territory2Type]
    for (Territory2Type te: listTerritoryType){
        if(te.MasterLabel == 'TestDivision')
            divisiontypeid = te.id;
        else if(te.MasterLabel == 'TestRegion')
            Regiontypeid = te.id;
        else if(te.MasterLabel == 'TestTeam')
            Teamtypeid = te.id;
    }
    
    //id modelid = [select id from Territory2Model where name='NAM Sales Hierarchy'].id;
    Territory2Model terrModel = new Territory2Model();
    terrModel .DeveloperName='TestNAMModelName'; // required field
    terrModel.Name = 'Name'; // required field
    insert terrModel ;

    Territory2 terr = new Territory2();
    terr.DeveloperName = 'Division';
    terr.Name ='Division';
    terr.Territory2TypeId = divisiontypeid;
    terr.Territory2ModelId = terrModel.id;
    insert terr;
    
    Territory2 terr2 = new Territory2();
    terr2.DeveloperName = 'Regional';
    terr2.Name ='Regional';
    terr2.Territory2TypeId = Regiontypeid;
    terr2.Territory2ModelId = terrModel.id;
    terr2.ParentTerritory2Id =terr.id;
    insert terr2;
    
    Territory2 terr3 = new Territory2();
    terr3.DeveloperName = 'Team';
    terr3.Name ='Team';
    terr3.Territory2TypeId = Teamtypeid;
    terr3.Territory2ModelId = terrModel.id;
    terr3.ParentTerritory2Id = terr2.id;
    insert terr3;
    
    User newAddUser = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
    User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
    User thirdUser = TestDataUtils.createUser('third User', 'no-reply@gmail.com', '123123456');
    
    insert newAddUser;
    insert seconduser;
    insert thirduser;
    
    UserTerritory2Association u2a = new UserTerritory2Association();
    u2a.userid = newAddUser.id;
    u2a.territory2id = terr.id;
    insert u2a;
    
    u2a.RoleInTerritory2 = Label.userAssociationTriggerDivisionManager;
    update u2a;
    
    UserTerritory2Association u2a2 = new UserTerritory2Association();
    u2a2.userid = seconduser.id;
    u2a2.territory2id = terr2.id;
    insert u2a2;
    
    u2a2.RoleInTerritory2 = Label.userAssociationTriggerRegionalManager;
    update u2a2;
    
    UserTerritory2Association u2a3 = new UserTerritory2Association();
    u2a3.userid = thirduser.id;
    u2a3.territory2id = terr3.id;
    insert u2a3;
    
    u2a3.RoleInTerritory2 = Label.userAssociationTriggerPrimaryRole;
    update u2a3;
    
    thirduser = [select id,Division_Manager__c,Region_Manager__c from User where id=:thirduser.id];
    System.assertEquals(newAddUser.id, thirduser.Division_Manager__c);
    delete u2a3;

  }
  
  
}