/*****************************************************************************************************************
Description: Helper class for Product Junction trigger. Updates the Product_Junction_Created_Updated_Time__c field of
             related product when the junction record is deleted.
             
Created on:  11/24/2015
Created by:  Jagan
Version 1:   Created helper class for Product Junction Trigger
********************************************************************************************************************/
public class productJunctionTriggerHelper{

    public static void afterDeleteProdJunc(List<Product_Junction__c> triggerold){
        
           set<id> prodidset = new set<id>();
            List<Product2> ListupdProd = new list<product2>();
            for(Product_Junction__c p:triggerold){
                if(!prodidset.contains(p.product__c)){
                    prodidset.add(p.Product__c);
                    Product2 prod = new Product2(id=p.Product__c);
                    prod.Product_Junction_Created_Updated_Time__c = system.now();
                    ListupdProd.add(prod);
                }
            }
            
            if(ListupdProd.size()>0)
                database.update(ListupdProd,false);
    }
    
}