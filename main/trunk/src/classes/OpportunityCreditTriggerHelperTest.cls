@isTest
private class OpportunityCreditTriggerHelperTest {
	
	@isTest static void OpportunityCreditTriggerStartApprovalProcessTest() {

		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		Id profileID = [Select Id, Name From Profile Where Name like 'Sales Management'].Id;
		System.debug('Sales Management ID: ' + profileID);
		user1.ProfileID = profileID;
		insert user1;

		User user2 = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		user2.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		insert user2;

		System.runAs(user2){

			// Implement test code
			Account corp1 = TestDataUtils.createCorpAccount('Test Corp1');
			insert corp1;

			Account div1 = TestDataUtils.createDivAccount('Test Div', corp1.Id);
			insert div1;

			Opportunity opp1 = TestDataUtils.createOppty('Test Oppty', 'Client Engagement', div1.Id, 'FSI');
			insert opp1;

			Id oppCreditRecordID = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity_Credit__c' and Name like 'Account Credit'].Id;

			Opportunity_Credit__c oppCredit = new Opportunity_Credit__c(
				Credit_Type__c = 'Compliance',
				Opportunity__c = opp1.Id,
				Amount__c = 100,
				RecordTypeId = oppCreditRecordID,
				Sales_Mgmt_Approval_User__c = user1.Id);
			insert oppCredit;

			Opportunity_Credit__c oppCredit2 = [select id,createddate from Opportunity_Credit__c where id=:oppCredit.id];
			ProcessInstance pi = [SELECT TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :oppCredit2.Id];
			System.assertEquals(oppCredit2.CreatedDate,pi.CreatedDate);

		}

	}
	
}