@isTest
private class UserTerritoryHierarchyBatchClassTest{

    static testMethod void batchTest(){
        
        Id divisiontypeid;
        Id regiontypeid;
        id teamtypeid;
        
        User newAddUser = TestDataUtils.createUser('first user', 'no-reply@email.com', '1231231234');
        newAddUser.FederationIdentifier = 'firstUser';
        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.FederationIdentifier = 'secondUser';
        User ThirdUser = TestDataUtils.createUser('third User', 'no-reply@gmail.com', '123123456');
        ThirdUser.FederationIdentifier = 'thirdUser';
        
        insert newAddUser;
        insert seconduser;
        insert thirduser;
        
        system.runas(newAddUser){
            List<Territory2Type> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerrTypeTestData');
            //[SELECT id, DeveloperName,MasterLabel from Territory2Type]
            for (Territory2Type te: listTerritoryType){
                if(te.MasterLabel == 'TestDivision')
                    divisiontypeid = te.id;
                else if(te.MasterLabel == 'TestRegion')
                    Regiontypeid = te.id;
                else if(te.MasterLabel == 'TestTeam')
                    Teamtypeid = te.id;
            }
            
            
            Territory2Model terrModel = new Territory2Model();
            terrModel .DeveloperName='TestNAMModelName'; // required field
            terrModel.Name = 'Name'; // required field
            insert terrModel ;
        
            Territory2 terr = new Territory2();
            terr.DeveloperName = 'Division';
            terr.Name ='Division';
            terr.Territory2TypeId = divisiontypeid;
            terr.Territory2ModelId = terrModel.id;
            insert terr;
            
            Territory2 terr2 = new Territory2();
            terr2.DeveloperName = 'Regional';
            terr2.Name ='Regional';
            terr2.Territory2TypeId = Regiontypeid;
            terr2.Territory2ModelId = terrModel.id;
            terr2.ParentTerritory2Id =terr.id;
            insert terr2;
            
            Territory2 terr3 = new Territory2();
            terr3.DeveloperName = 'Team';
            terr3.Name ='Team';
            terr3.Territory2TypeId = Teamtypeid;
            terr3.Territory2ModelId = terrModel.id;
            terr3.ParentTerritory2Id = terr2.id;
            insert terr3;
        
            UserTerritory2Association u2a = new UserTerritory2Association();
            u2a.userid = newAddUser.id;
            u2a.territory2id = terr.id;
            u2a.RoleInTerritory2 = Label.userAssociationTriggerPrimaryRole;
            insert u2a;
            
                    
            UserTerritory2Association u2a2 = new UserTerritory2Association();
            u2a2.userid = seconduser.id;
            u2a2.territory2id = terr2.id;
            u2a2.RoleInTerritory2 = Label.userAssociationTriggerPrimaryRole;
            insert u2a2;
            
            UserTerritory2Association u2a3 = new UserTerritory2Association();
            u2a3.userid = thirduser.id;
            u2a3.territory2id = terr3.id;
            u2a3.RoleInTerritory2 = Label.userAssociationTriggerPrimaryRole;
            insert u2a3;
            
            thirduser = [select Division_Manager__c,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,Region_Name__c,Region_Manager__c,Team_Manager__c,Team_Name__c from User where id=:thirduser.id];
            system.assert(thirduser.Region_Manager__c == seconduser.id);
            
            
            u2a2.RoleInTerritory2 = 'Test Dummy Role';
            update u2a2;
        
        }
        
        
        system.runas(thirduser){
            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div1;
            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, Label.OppRecordTypeMerchandising);
            testOppty.Amount = 100;
            testOppty.Insert_Date__c = system.today()+1;
            testOppty.ownerid=thirduser.id;
            insert testOppty;
            
            testOppty =  [select id,Insert_date__c,Division_Manager__c,Ownerid,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,Region_Name__c,Region_Manager__c,Team_Manager__c,Team_Name__c,Primary_Sales_Rep__c from Opportunity where id =:testOppty.id];
            System.assert(testOppty.Region_Manager__c == seconduser.id);   
            
            Goal__c objG = new Goal__c();
            objG.Assigned_To__c = thirduser.id;
            //objG.Fiscal_Year__c = 2015;
            objG.Goal_Amount__c = 12000;
            objg.effective_date__c = system.today()+1;
            objG.Account__c = corp1.id;
            insert objG;
                
            String CRON_EXP = '0 0 0 3 11 ? 2022';
            String jobID = System.schedule('UserTerritoryHierarchySchedular', CRON_EXP, new UserTerritoryHierarchySchedular());
            UserTerritoryHierarchyBatchClass batch = new UserTerritoryHierarchyBatchClass();
            Database.executeBatch(batch);
            
            //testOppty =  [select id,Insert_date__c,Division_Manager__c,Ownerid,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,Region_Name__c,Region_Manager__c,Region_Manager__r.name,Team_Manager__c,Team_Name__c,Primary_Sales_Rep__c from Opportunity where id =:testOppty.id];
            //System.debug('Region Manager after batch class....'+testOppty.Region_Manager__c);
              
        
        }
    }

}