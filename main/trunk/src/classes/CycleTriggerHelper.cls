/* CycleTriggerHelper
 * Class to update opportunities related to cycles after update for fiscal year calculation.
 * Author: Stephen Lane
 */
public class CycleTriggerHelper {
    Static boolean updateRelatedOpportunitiesCheck = FALSE;
    
    public static void updateRelatedOpportunities(List<Cycle__c> cycles){
        if(!updateRelatedOpportunitiesCheck){
            updateRelatedOpportunitiesCheck = TRUE;
            List<Id> cycleIds = new List<Id>();
            for(Cycle__c cycle: cycles){
                cycleIds.add(cycle.Id);
            }
            if(cycleIds <> null){
                List<Opportunity> relatedOpps = [SELECT Id
                                                 FROM Opportunity
                                                 WHERE InStore_Cycle__c IN :cycleIds];
                update relatedOpps;
            }
        }
    }
}