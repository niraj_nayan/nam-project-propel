public class Territory2TriggerHelper {  
    static boolean checkNameLengthCheck = FALSE;
    
    public static void checkNameLength(List<Territory2> territories){
        if(!checkNameLengthCheck){
            checkNameLengthCheck = TRUE;
            for(Territory2 territory: territories){
                if(territory.Name.length()>30){
                    territory.addError(label.Territory_Name_Too_Long);
                }
            }
        }
    }
}