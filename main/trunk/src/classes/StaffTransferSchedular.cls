/***************************************************************************************************************************
Description: Scheduler Class for Stanff Transfer Batch
Created on:  8/29/2015 
Created by:  Pavan

Version 1:   Created the class
********************************************************************************************************************************/
global class StaffTransferSchedular implements Schedulable {
    global void execute(SchedulableContext ctx) {
        string todaysDate = System.Now().format('yyyy-MM-dd');
        string query = 'SELECT '+Label.StaffTransferQueryColumns+' FROM Staff_Transfer_Request__c WHERE Effective_Date__c = ' + todaysDate + 'AND Is_Processed__c ='+false;
        StaffTransferBatchClass batch = new StaffTransferBatchClass(query);
        // set the batch size to 1
        database.executebatch(batch,100);
          // this section of code will abort the current schedule job. We will use the below statement if the batch executes continuously
        //system.abortJob(ctx.getTriggerId());
    }

}