@isTest
private class Goal_record_share_helperTest {
  
  static testMethod void CreateAndUpdateGoals() {
    Id divisiontypeid;
    Id regiontypeid;
    id teamtypeid;
    
   
    List<Territory2Type> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerrTypeTestData');
    //[SELECT id, DeveloperName,MasterLabel from Territory2Type]
    for (Territory2Type te: listTerritoryType){
        if(te.MasterLabel == 'TestDivision')
            divisiontypeid = te.id;
        else if(te.MasterLabel == 'TestRegion')
            Regiontypeid = te.id;
        else if(te.MasterLabel == 'TestTeam')
            Teamtypeid = te.id;
    }
    
    //id modelid = [select id from Territory2Model where name='NAM Sales Hierarchy'].id;
    Territory2Model terrModel = new Territory2Model();
    terrModel .DeveloperName='TestNAMModelName'; // required field
    terrModel.Name = 'Name'; // required field
    insert terrModel ;

    Territory2 terr = new Territory2();
    terr.DeveloperName = 'TestDivision';
    terr.Name ='TestDivision';
    terr.Territory2TypeId = divisiontypeid;
    terr.Territory2ModelId = terrModel.id;
    insert terr;
    
    Territory2 terr2 = new Territory2();
    terr2.DeveloperName = 'TestRegional';
    terr2.Name ='TestRegional';
    terr2.Territory2TypeId = Regiontypeid;
    terr2.Territory2ModelId = terrModel.id;
    terr2.ParentTerritory2Id = terr.id;
    insert terr2;
    
    Territory2 terr3 = new Territory2();
    terr3.DeveloperName = 'TestTeam';
    terr3.Name ='TestTeam';
    terr3.Territory2TypeId = Teamtypeid;
    terr3.Territory2ModelId = terrModel.id;
    terr3.ParentTerritory2Id = terr2.id;
    insert terr3;
    Set<Id> terridset = new Set<Id>();
    terridset.add(terr.id);
    terridset.add(terr2.id);
    terridset.add(terr3.id);

    //List<Group> lstgroup = [Select Id, RelatedId from Group where (Type='Territory' OR Type='TerritoryAndSubordinates') AND RelatedId IN : terridset];
    //system.debug('lstgroup.....'+lstgroup);
    User newAddUser = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
    User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
    User ThirdUser = TestDataUtils.createUser('third User', 'no-reply@gmail.com', '123123456');
    
    //String profid = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
    
    //secondUser.ProfileId = profid;
    //newAddUser.profileId = profid;
    
    
    // newAddUser.primary_territory__c ='Division';
    insert newAddUser;
    
    //seconduser.primary_territory__c = 'Regional';
    insert seconduser;
    insert thirduser;
    
    UserTerritory2Association u2a = new UserTerritory2Association();
    u2a.userid = newAddUser.id;
    u2a.territory2id = terr.id;
    u2a.RoleInTerritory2 = Label.userAssociationTriggerDivisionManager;
    insert u2a;
    
    UserTerritory2Association u2a2 = new UserTerritory2Association();
    u2a2.userid = seconduser.id;
    u2a2.territory2id = terr2.id;
    u2a2.RoleInTerritory2 = Label.userAssociationTriggerRegionalManager;
    insert u2a2;
    
    UserTerritory2Association u2a3 = new UserTerritory2Association();
    u2a3.userid = thirduser.id;
    u2a3.territory2id = terr3.id;
    u2a3.RoleInTerritory2 = Label.PrimaryRoletoShareGoal;
    insert u2a3;
    
    system.runAs(ThirdUser){
    
    
    
    Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
    insert corp1;
    
    Goal__c objG = new Goal__c();
    objG.Assigned_To__c = thirduser.id;
    //objG.Fiscal_Year__c = 2015;
    objG.Goal_Amount__c = 12000;
    objG.Account__c = corp1.id;
    insert objG;
    
    system.debug('Territory one id..'+terr.id);
    system.debug('Territory one id..'+terr2.id);
    system.debug('Territory one id..'+terr3.id);

    Integer goalsharecnt = [select count() from Goal__Share where parentid=:objG.id];
    system.debug('Count of goal share...'+goalsharecnt);
    system.assert(goalsharecnt >= 1);

    objG.ownerid = seconduser.id;
    update objG; 
    }

  }

  static testMethod void CreateAndUpdateGoalsSourceKey() {
    Id divisiontypeid;
    Id regiontypeid;
    id teamtypeid;
    
   
    List<Territory2Type> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerrTypeTestData');
    //[SELECT id, DeveloperName,MasterLabel from Territory2Type]
    for (Territory2Type te: listTerritoryType){
        if(te.MasterLabel == 'TestDivision')
            divisiontypeid = te.id;
        else if(te.MasterLabel == 'TestRegion')
            Regiontypeid = te.id;
        else if(te.MasterLabel == 'TestTeam')
            Teamtypeid = te.id;
    }
    
    //id modelid = [select id from Territory2Model where name='NAM Sales Hierarchy'].id;
    Territory2Model terrModel = new Territory2Model();
    terrModel .DeveloperName='TestNAMModelName'; // required field
    terrModel.Name = 'Name'; // required field
    insert terrModel ;

    Territory2 terr = new Territory2();
    terr.DeveloperName = 'TestDivision';
    terr.Name ='TestDivision';
    terr.Territory2TypeId = divisiontypeid;
    terr.Territory2ModelId = terrModel.id;
    insert terr;
    
    Territory2 terr2 = new Territory2();
    terr2.DeveloperName = 'TestRegional';
    terr2.Name ='TestRegional';
    terr2.Territory2TypeId = Regiontypeid;
    terr2.Territory2ModelId = terrModel.id;
    terr2.ParentTerritory2Id = terr.id;
    insert terr2;
    
    Territory2 terr3 = new Territory2();
    terr3.DeveloperName = 'TestTeam';
    terr3.Name ='TestTeam';
    terr3.Territory2TypeId = Teamtypeid;
    terr3.Territory2ModelId = terrModel.id;
    terr3.ParentTerritory2Id = terr2.id;
    insert terr3;
    Set<Id> terridset = new Set<Id>();
    terridset.add(terr.id);
    terridset.add(terr2.id);
    terridset.add(terr3.id);

    //List<Group> lstgroup = [Select Id, RelatedId from Group where (Type='Territory' OR Type='TerritoryAndSubordinates') AND RelatedId IN : terridset];
    //system.debug('lstgroup.....'+lstgroup);
    User newAddUser = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
    User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
    User ThirdUser = TestDataUtils.createUser('third User', 'no-reply@gmail.com', '123123456');
    
    //String profid = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
    
    //secondUser.ProfileId = profid;
    //newAddUser.profileId = profid;
    
    
    // newAddUser.primary_territory__c ='Division';
    insert newAddUser;
    
    //seconduser.primary_territory__c = 'Regional';
    insert seconduser;
    insert thirduser;
    
    UserTerritory2Association u2a = new UserTerritory2Association();
    u2a.userid = newAddUser.id;
    u2a.territory2id = terr.id;
    u2a.RoleInTerritory2 = Label.userAssociationTriggerDivisionManager;
    insert u2a;
    
    UserTerritory2Association u2a2 = new UserTerritory2Association();
    u2a2.userid = seconduser.id;
    u2a2.territory2id = terr2.id;
    u2a2.RoleInTerritory2 = Label.userAssociationTriggerRegionalManager;
    insert u2a2;
    
    UserTerritory2Association u2a3 = new UserTerritory2Association();
    u2a3.userid = thirduser.id;
    u2a3.territory2id = terr3.id;
    u2a3.RoleInTerritory2 = Label.PrimaryRoletoShareGoal;
    insert u2a3;
    
    system.runAs(ThirdUser){
    
    
    
    Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
    insert corp1;
    
    Goal__c objG = new Goal__c();
    objG.Assigned_To__c = thirduser.id;
    //objG.Fiscal_Year__c = 2015;
    objG.Goal_Amount__c = 12000;
    objG.Account__c = corp1.id;
    objG.ownerid = thirduser.id;
    objG.Legacy_Source__c = 'A0';
    objG.Division_Manager__c = 'A011111111';
    objG.Team_Manager__c = 'A011111111';
    objG.Region_Manager__c = 'A011111111';
    objG.Primary_Sales_Rep__c = 'A011111111';
    insert objG;
    
    system.debug('Territory one id..'+terr.id);
    system.debug('Territory one id..'+terr2.id);
    system.debug('Territory one id..'+terr3.id);

    Integer goalsharecnt = [select count() from Goal__Share where parentid=:objG.id];
    system.debug('Count of goal share...'+goalsharecnt);
    system.assert(goalsharecnt >= 1);

    objG.ownerid = seconduser.id;
    update objG; 
    }

  }
  
  
}