/***************************************************************************************************************************
Description: Helper class for product trigger
Created on:  11/6/2015
Created by:  Seth

Version 1:   Created the helper class to generate source key
********************************************************************************************************************************/
public class ProductTriggerHelper {

	public static void beforeInsertSourceKeyGenerate(List<Product2> prod){

		integer counter = 0;
        for(Product2 pro: prod){
            if(pro.Source_Key__c == null){
                DateTime current = System.now();
                counter += 10;
                pro.Source_Key__c = sourceKeyGenerationUtility.generateSourceKey('P', current.getTime() + counter);
            }
        }
    }
}