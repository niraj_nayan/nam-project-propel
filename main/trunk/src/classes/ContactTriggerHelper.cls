/*****************************************************************************************************************
Description: Trigger helper class for contact. This helper class will be where all of the contact trigger actions
			 will call to run a method, to do a certain task
             
Created on:  4th November 2015
Created by:  Seth Martin
Version 1:   Created class and method beforeInsertSourceKeyGenerate to generate the source key for the contact
			 record if the key is null
********************************************************************************************************************/

public class ContactTriggerHelper {
	
	public static void beforeInsertSourceKeyGenerate(List<Contact> lstContact){
		integer counter = 0;
		for(Contact insertContact: lstContact){
			if(insertContact.Source_Key__c == null){
				DateTime current = System.now();
				counter += 10;
            	insertContact.Source_Key__c = sourceKeyGenerationUtility.generateSourceKey('C', current.getTime() + counter);
			}
		}
	}
}