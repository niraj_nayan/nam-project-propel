/**********************************************************************************************************************************************
Description: Helper class for trigger for Opportunity Category trigger (updateCategoryonOpponInsertUpdateDelete)
             
Created on:  22nd October 2015
Created by:  Jagan
Version 1:   Created Class to update "Category" field on opportunity (overrides with most recent value) whenever a new Opportunity category 
             is inserted/deleted
*********************************************************************************************************************************************/

public class OppCategoryTriggerHelper{

    public static void updateCategory(List<Opportunity_Category__c> listOppCatg){
        
        //Variable declarations
        Set<Id> oppids = new Set<Id>();
        Set<Id> tempoppids = new Set<Id>();
        List<Opportunity> updateOppLst = new List<Opportunity>();
        Map<Id,List<Opportunity_Category__c>> oppCatgmap = new Map<Id,List<Opportunity_Category__c>>();
        
        //Collect opportunity ids from the "Opportunity Category" records that are being updated/inserted/deleted
        for(Opportunity_Category__c oppCatg: listOppCatg){
            oppids.add(oppCatg.Opportunity__c);
        }
        
        //Query all Opportunity Categories corresponding to the opportunity ids order by the Category name ascending
        List<Opportunity_Category__c> lstOppCatg = [select id, Opportunity__c,Category__c,Opportunity__r.Category__c, Category__r.name from Opportunity_Category__c where Opportunity__c in: oppids order by Category__r.name asc];
        
        //Loop over the list and frame a map to collect Opportunity id and related "Opportunity Category" records
        //Here, some opps may contain category records and some may not. we can use this map to determine and update opportunities accordingly
        for(Opportunity_Category__c opcatg: lstOppCatg){
            if(oppCatgmap.containsKey(opcatg.opportunity__c))
                oppCatgmap.get(opcatg.opportunity__c).add(opcatg);
            else
                oppCatgmap.put(opcatg.opportunity__c,new List<Opportunity_Category__c>{opcatg});
        }
        
        System.debug('Opportunity Category Map....'+oppCatgmap);
        
        //Lopp over the original set of opportunities and gather the ones that should be updated (Category field).
        for(Id oppid:oppids){
            //condition to check duplicate opportunity id in the update list. So, once the opportuniy is first set with a category, eventhough it has more opportunity category records,
            //those will be discarded.
            if(!tempoppids.contains(oppid)){
                
                Opportunity opp = new Opportunity(id=oppid);
                //condition to determine if the opportunity has to be updated with a value or with out a value
                if(oppCatgmap.containsKey(oppid) && oppCatgmap.get(oppid).size()>0) {
                    opp.Category__c = oppCatgmap.get(oppid)[0].Category__r.name;
                    // [Pavan 11/22/2015] included the below statement to populate category id for reporting purposes.
                    opp.Reporting_Category_Id__c = oppCatgmap.get(oppid)[0].Category__c; 
                }
                else if(!oppCatgmap.containsKey(oppid) || (oppCatgmap.containsKey(oppid) && oppCatgmap.get(oppid).size() ==0 )) {
                    opp.Category__c = null;
                    // [Pavan 11/22/2015] included the below statement to populate category id for reporting purposes.
                    opp.Reporting_Category_Id__c = null;
                }
                
                updateOppLst.add(opp);
                tempoppids.add(oppid);
            }
        }
        
        System.debug('Opportunity Update list...'+updateOppLst);
        
        //update the list and clear the variables.
        if(updateOppLst.size()>0){
            database.update(updateOppLst,false);
            updateOppLst.clear();
            tempoppids.clear();
            oppids.clear();
            oppCatgmap.clear();
        }
        
        
    }
    
}