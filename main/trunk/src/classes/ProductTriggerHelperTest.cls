@isTest
private class ProductTriggerHelperTest {
	
	@isTest static void beforeInsertSourceKeyTest() {
		// Implement test code
		Product2 prod = TestDataUtils.createProduct2('Test Product');
		insert prod;

		Product2 testProd = [Select Id, Name From Product2 Where Id =:prod.Id];
		System.assertEquals('Test Product', testProd.Name);
	}
	
}