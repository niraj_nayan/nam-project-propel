@isTest
private class OppCategoryTriggerHelperTest {
  
  static testMethod void insertOppCategory() {
    // Implement test code
    User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
    user1.FederationIdentifier = 'tLastName';
    insert user1;

    User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
    secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
    user1.FederationIdentifier = 'secondUser2';
    insert secondUser;
    
    
    
    System.runAs(secondUser){

      List<Opportunity_category__c> lstcatg = new List<Opportunity_Category__c>();
      
      Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
      insert corp1;

      //Create Division Account record
      Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
      insert div1;

      //then an opportunity with lookup to the account
      Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
      testOppty.Category__c = '';
      insert testOppty;
     
      Opportunity testOppty2 = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
      testOppty2.Category__c = '';
      insert testOppty2;
       
      Category__c ca = new category__c();
      ca.name ='testc1';
      insert ca;
      
      Category__c ca2 = new category__c();
      ca2.name ='atestc1';
      insert ca2;
      
      Opportunity_category__c objOppCat = new opportunity_category__c();
      objoppcat.opportunity__c = testOppty2.id;
      
      objoppcat.category__c = ca.id;
      
      Opportunity_category__c objOppCat2 = new opportunity_category__c();
      objoppcat2.opportunity__c = testOppty.id;
      objoppcat2.category__c = ca2.id;
      
      Opportunity_category__c objOppCat3 = new opportunity_category__c();
      objoppcat3.opportunity__c = testOppty.id;
      objoppcat3.category__c = ca.id;
      
      lstcatg.add(objoppcat);
      lstcatg.add(objoppcat2);
      lstcatg.add(objoppcat3);
      
      insert lstcatg;
      
      String testoppty2cate = [select Category__c from Opportunity where id=:testOppty2.id].Category__c;
      String testopptycate = [select Category__c from Opportunity where id=:testOppty.id].Category__c;
      
      system.assert(testoppty2cate=='testc1');
      system.assert(testopptycate=='atestc1');
      
      delete objoppcat3;
      delete objoppcat;
      delete objoppcat2;
      
    }


  }
  
   
  
  
}