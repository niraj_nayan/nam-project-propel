@isTest
private class OpportunityProductTriggerHelperTest {
	
	@isTest static void UpdateOpportunityProductLineItem() {
		// Implement test code
		User newAddUser = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		insert newAddUser;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		insert secondUser;

		System.runAs(secondUser){

			Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
			insert corp1;

			//Create Division Account record
			Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
			insert div1;

			//then an opportunity with lookup to the account
			Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
			insert testOppty;

			Product2 prod = TestDataUtils.createProduct2('Test Prod');
			insert prod;

			//CREATE PRICEBOOK ENTRY RECORD
			PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
			insert priceEntry;

			//ADD OPPORTUNITY LINE ITEM
			OpportunityLineItem testProduct = TestDataUtils.createOpportunityLineItem('Test ProdLine', 'Test Pricing Detail', 150, 10, testOppty.Id, priceEntry.Id);
			insert testProduct;

			testProduct.Sales_Price__c = 10000;
			testProduct.Quantity = 1000;
			testProduct.TotalPrice = 10000 * 1000;
			testProduct.Commissionable__c = true;

			update testProduct;

			Opportunity testAssertOpp = [Select Id, Commission_Roll_Up__c From Opportunity
											Where Id =:testOppty.Id];
			System.assertEquals(10000000, testAssertOpp.Commission_Roll_Up__c);
		}


	}
	
	
}