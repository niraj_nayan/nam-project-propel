@isTest
private class RateCardTriggerHelperTest {
	
	@isTest static void RateCardInsertTest() {
		// Implement test code
		//Create NAM Standard Rate Card
		Id rateCardNAMRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'NAM Rate Card'].Id;
		Id rateCardClientRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'Client Rate Card'].Id;

		//CREATE USERS SO CORP ACCOUNT HAS SALES TEAM MEMBERS
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		insert user1;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		insert secondUser;
		//---------------------------------------

		//NEED TO CREATE ACCOUNTS FOR CLIENT RATE CARDS
		Account corp1 = TestDataUtils.createCorpAccount('Test Corp');
		insert corp1;

		//NEED TO CREATE DIVISION ACCOUNT FOR CLIENT CARDS
		Account div1 = TestDataUtils.createDivAccount('Test Div', corp1.Id);
		insert div1;

		//CREATE ACCOUNT SALES TEAM MEMBERS
		AccountTeamMember accTeamMember = new AccountTeamMember(
			UserId = user1.Id,
			AccountId = corp1.Id);
		insert accTeamMember;

		//Create product line
		Product_Line__c testLine = new Product_Line__c(
			Name = 'FSI');
		insert testLine;

		//Create product to be used for rate card
		Product2 prod = TestDataUtils.createProduct2('Test Prod');
		insert prod;

		Product2 prod2 = TestDataUtils.createProduct2('Test Prod2');
		insert prod2;

		//CREATE PRICEBOOK ENTRY RECORD
		PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
		insert priceEntry;

		//Create NAM rate card
		Rate_Card__c rateCardNAM = new Rate_Card__c(
			RecordTypeId = rateCardNAMRecordId,
			Product__c = prod.Id,
			Product_Line__c = testLine.Id);
		insert rateCardNAM;

		//Create Client Rate Card rate card
		Rate_Card__c rateCardClient = new Rate_Card__c(
			Account__c = corp1.Id,
			RecordTypeId = rateCardClientRecordId,
			Product__c = prod.Id,
			Product_Line__c = testLine.Id);
		insert rateCardClient;

		Rate_Card__c rateCardAssertTest = [Select Id, Account__r.Name, RecordTypeId, Product__r.Name, Product_Line__r.Name From Rate_Card__c
												Where Account__c != null];
		System.assertEquals(rateCardClientRecordId, rateCardAssertTest.RecordTypeId);

	}


	@isTest static void RateCardUpdateTest() {
		//Create NAM Standard Rate Card
		Id rateCardNAMRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'NAM Rate Card'].Id;

		//CREATE USERS SO CORP ACCOUNT HAS SALES TEAM MEMBERS
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		insert user1;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		insert secondUser;
		//---------------------------------------

		//NEED TO CREATE ACCOUNTS FOR CLIENT RATE CARDS
		Account corp1 = TestDataUtils.createCorpAccount('Test Corp');
		insert corp1;

		//TO UPDATE A RATE CARD WE ARE CHANGING THE CORP ACCOUNT
		//THIS WILL DELETE THE OLD RATECARD
		Account corp2 = TestDataUtils.createCorpAccount('Test Corp2');
		insert corp2;

		//CREATE ACCOUNT SALES TEAM MEMBERS
		AccountTeamMember accTeamMemberCorp1 = new AccountTeamMember(
			UserId = user1.Id,
			AccountId = corp1.Id);
		insert accTeamMemberCorp1;

		//CREATE ACCOUNT SALES TEAM MEMBERS
		AccountTeamMember accTeamMemberCorp2 = new AccountTeamMember(
			UserId = user1.Id,
			AccountId = corp2.Id);
		insert accTeamMemberCorp2;

		//NEED TO CREATE DIVISION ACCOUNT FOR CLIENT CARDS
		Account div1 = TestDataUtils.createDivAccount('Test Div', corp1.Id);
		insert div1;

		//CREATE ACCOUNT SALES TEAM MEMBERS
		AccountTeamMember accTeamMember = new AccountTeamMember(
			UserId = user1.Id,
			AccountId = corp1.Id);
		insert accTeamMember;

		//Create product line
		Product_Line__c testLine = new Product_Line__c(
			Name = 'FSI');
		insert testLine;

		//Create product to be used for rate card
		Product2 prod = TestDataUtils.createProduct2('Test Prod');
		insert prod;

		Product2 prod2 = TestDataUtils.createProduct2('Test Prod2');
		insert prod2;

		//CREATE PRICEBOOK ENTRY RECORD
		PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
		insert priceEntry;

		//Create NAM rate card
		Rate_Card__c rateCardNAM = new Rate_Card__c(
			RecordTypeId = rateCardNAMRecordId,
			Product__c = prod.Id,
			Product_Line__c = testLine.Id);
		insert rateCardNAM;

		//Create Client Rate Card rate card
		Rate_Card__c rateCardClient = new Rate_Card__c(
			Account__c = corp1.Id,
			RecordTypeId = rateCardNAMRecordId,
			Product__c = prod.Id,
			Product_Line__c = testLine.Id);
		insert rateCardClient;

		//rateCardClient.Account__c = corp2.Id;
		update rateCardClient;

		Rate_Card__c rateCardAssertTest = [Select Id, Account__r.Name, RecordTypeId, Product__r.Name, Product_Line__r.Name From Rate_Card__c
												Where Account__c != null];
		System.assertEquals(rateCardNAMRecordId, rateCardAssertTest.RecordTypeId);
	}

}