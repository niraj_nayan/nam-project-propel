public with sharing class GoalAssignmentTriggerHelper {
	//map the assigned to to owner of the goal   
    public void afterinsertupdateGoalOwner(List<Goal_Assignment__c> lstGoalAssignment){
        try{
	        Set<String> goalIds = new set<String>(); 
	        for(Goal_Assignment__c objGoalAssignment: lstGoalAssignment){
	            if(objGoalAssignment.Assigned_To__c != null){
	            	goalIds.add(objGoalAssignment.Goal__c);
	            }
	        }
	        List<Goal__c> goalList = [select ownerid,id from Goal__c where id =: goalIds limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())];
	        if(!goalList.isEmpty()){
	        	List<Goal__c> updateGoalList = new List<Goal__c>();
	        	for(Goal__c goalObj :goalList){
	        		for(Goal_Assignment__c objGoalAssignment: lstGoalAssignment){
			            if(objGoalAssignment.Goal__c == goalObj.id && objGoalAssignment.Assigned_To__c != null && objGoalAssignment.Double_Goal__c == false){
			            	goalObj.ownerid = objGoalAssignment.Assigned_To__c;
			            	updateGoalList.add(goalObj);
			            	break;
			            }		           
			        }
	        	}
	        	System.debug('-->> updateGoalList = ' + updateGoalList);
	        	if(!updateGoalList.isEmpty()){
	        		update updateGoalList;
	        	}
	        }
        }catch(Exception e){
        	System.debug('Exception in goal assignment trigger : message = '+ e.getMessage());
        }
	}
	
	/*  
	    Created Date :15/09/2016  
		Author : Durgapaavan
		Description : This method creates the goal team record after insertion and updation of Goal
		              Assignment Object
		JIRA No : PP-85
	
	public void CreateGoalTeam(List<Goal_Assignment__c> goalAssignmentLst){
		System.debug('\n***********inside CreateGoalTeam');
		Goal_Team__c goalTeam; 
		List<Goal_Team__c> goalTeamList=new List<Goal_Team__c>();
		try{
			
			if(goalAssignmentLst.size()>0){//
				for(Goal_Assignment__c goalAssignment : goalAssignmentLst){
					goalTeam = new Goal_Team__c();
					System.debug('\n***********inside For'+goalAssignment.Assigned_To__c);
					goalTeam.Team_Member__c=goalAssignment.Assigned_To__c;
					System.debug('\n***********inside For'+goalAssignment.Assigned_To__c);
					goalTeam.Goal_Name__c=goalAssignment.Goal__c;
					
					goalTeamList.add(goalTeam);
				}
				insert goalTeamList;
			}
		}catch(Exception e){
			System.debug('Exception @@'+e.getMessage());
			System.debug('Exception occurred at@@'+e.getLineNumber());
		}
	}
	public void updateGoalTeam(List<Goal_Assignment__c> newGoalAssignmentLst,Map<Id,Goal_Assignment__c> oldGoalAssignmentMap){
		Goal_Team__c goalTeam; 
		List<Goal_Team__c> goalTeamList=new List<Goal_Team__c>();
		Map<Id,Goal_Team__c> oldGoalTeam=new Map<Id,Goal_Team__c>([select Id,Team_Member__c,Goal_Name__c, Frozen_Hierarchy__c from Goal_Team__c ]); 
		list<id> goalIds = new list<Id>();
		for(Goal_Assignment__c goalAssignment : newGoalAssignmentLst){
			goalIds.add(goalAssignment.Goal__c);
		}
		List<Goal_Team__c> existGoalTeamList= [select id,Goal_Name__c,Frozen_Hierarchy__c from Goal_Team__c where 	Goal_Name__c in :goalIds ];
		map<Id,list<Goal_Team__c>> goalandGoalTeam = new map<Id,list<Goal_Team__c>>();
		for(Goal_Team__c gTeam: existGoalTeamList){ 
			if(goalandGoalTeam.containsKey(gTeam.Goal_Name__c)){
				goalandGoalTeam.get(gTeam.Goal_Name__c).add(gTeam);
			}else{
				goalandGoalTeam.put(gTeam.Goal_Name__c, new list<Goal_Team__c>{gTeam});
			}
		
		}
		List<Goal_Team__c> updateGoalTeamList=new List<Goal_Team__c>();
		try{
			
			if(newGoalAssignmentLst.size() > 0 && oldGoalAssignmentMap!=null){//
				
				
				for(Goal_Assignment__c goalAssignment : newGoalAssignmentLst){
					if(goalAssignment.Assigned_To__c!=(oldGoalAssignmentMap.get(goalAssignment.Id)).Assigned_To__c){
						goalTeam=new Goal_Team__c(); 
						goalTeam.Team_Member__c=goalAssignment.Assigned_To__c;
						goalTeam.Goal_Name__c=goalAssignment.Goal__c;
						
						goalTeamList.add(goalTeam);
						if(goalandGoalTeam.containsKey(goalAssignment.Goal__c)){
							for(Goal_Team__c gTeam:goalandGoalTeam.get(goalAssignment.Goal__c)){
								gTeam.Frozen_Hierarchy__c = true;
								updateGoalTeamList.add(gTeam);
							}
						}
					}
				}
				insert goalTeamList;
				update updateGoalTeamList;
			}
		}catch(Exception e){
			System.debug('Exception @@'+e.getMessage());
			System.debug('Exception occurred at@@'+e.getLineNumber());
		}
	}*/
}