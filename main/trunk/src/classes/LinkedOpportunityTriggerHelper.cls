/**********************************************************************************************************************************************
Description: Helper Class for Linked Opportunity Trigger          
Created on:  9/1/2015
Created by:  Seth

Version 1:   Created the helper class
***********************************************************************************************************************************************/
Public class LinkedOpportunityTriggerHelper {

	/******************************************************************************************
	Method to insert opportunity association records
	******************************************************************************************/
	public static void afterInsertLinkedOpportunity(list<Linked_Opportunity__c> linkedOppty){
		try{
            insert associateOpportunities(linkedOppty);
        }catch(DmlException e){
            //going to check and see if the error message contains
            System.debug('Failed to insert Linked Opportunity: ' + e.getDmlType(0));
            System.debug('Failed to insert Linked Opportunity:  ' + e.getCause());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, ''+e.getDmlMessage(0));
            ApexPages.addmessage(myMsg);
        }
	}
	/******************************************************************************************
	Method to update opportunity association records 
	******************************************************************************************/
	public static void afterUpdateLinkedOpportunity(list<Linked_Opportunity__c> linkedOppty,map<id,Linked_Opportunity__c> oldMap) {
        list<Linked_Opportunity__c> filteredRecs = new list<Linked_Opportunity__c>();
        map<string,string> previousRelatedOppty = new map<string,string>();
        
        for(Linked_Opportunity__c rec : linkedOppty) {
        	boolean addToCollection = false;
        	if(rec.Related_Opportunity__c != oldMap.get(rec.Id).Related_Opportunity__c && rec.Linked_Opportunity__c == oldMap.get(rec.Id).Linked_Opportunity__c) {
        		previousRelatedOppty.put(rec.Linked_Opportunity__c,oldMap.get(rec.Id).Related_Opportunity__c);
        		addToCollection = true;
        	}
        	else if(rec.Linked_Opportunity__c != oldMap.get(rec.Id).Linked_Opportunity__c && rec.Related_Opportunity__c == oldMap.get(rec.Id).Related_Opportunity__c) {
	            previousRelatedOppty.put(oldMap.get(rec.Id).Linked_Opportunity__c,rec.Related_Opportunity__c);
	            if(!addToCollection)
	            	addToCollection = true;
           }
           else if(rec.Linked_Opportunity__c != oldMap.get(rec.Id).Linked_Opportunity__c && rec.Related_Opportunity__c != oldMap.get(rec.Id).Related_Opportunity__c) {
           	   previousRelatedOppty.put(oldMap.get(rec.Id).Linked_Opportunity__c,oldMap.get(rec.Id).Related_Opportunity__c);
           	   if(!addToCollection)
	            	addToCollection = true;
           }
            if(addToCollection)
        		filteredRecs.add(rec);
        }
        // delete the association records accordingly
        if(previousRelatedOppty.size() > 0)
        	deleteRelatedOpptyAssociations(previousRelatedOppty); 	
        // create new associations accordingly
        if(filteredRecs.size() > 0){
        	try{
                    insert associateOpportunities(filteredRecs);
                }catch(DmlException e){
                    //going to check and see if the error message contains
                    System.debug('Failed to insert Linked Opportunity:  ' + e.getDmlType(0));
                    System.debug('Failed to insert Linked Opportunity:  ' + e.getCause());
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, ''+e.getDmlMessage(0));
                    ApexPages.addmessage(myMsg);
                }
        }
    }
	/******************************************************************************************
	Method to delete opportunity association records
	******************************************************************************************/
	public static void afterDeleteLinkedOpportunity(map<id,Linked_Opportunity__c> oldMap){
		map<string,string> linkedOpptyIds = new map<string,string>();
		for(Linked_Opportunity__c rec: oldMap.values()){
			linkedOpptyIds.put(rec.Linked_Opportunity__c,rec.Related_Opportunity__c);
		}
		deleteRelatedOpptyAssociations(linkedOpptyIds);
		
	}
   /**************************************************************************************************************
   Utility Method to delete Oppty associataions
   ***************************************************************************************************************/
   static void deleteRelatedOpptyAssociations(map<string,string> linkedOpptyIds) {
   	  List<Linked_Opportunity__c> recsTodelete = new List<Linked_Opportunity__c>();
   	  for(Linked_Opportunity__c rec : [SELECT Linked_Opportunity__c,Related_Opportunity__c 
		                                 FROM Linked_Opportunity__c 
		                                 WHERE Linked_Opportunity__c IN :linkedOpptyIds.values() AND Related_Opportunity__c IN :linkedOpptyIds.keyset()]) {
		       if(linkedOpptyIds.get(rec.Related_Opportunity__c) != Null && linkedOpptyIds.get(rec.Related_Opportunity__c) == rec.Linked_Opportunity__c)
		       		recsTodelete.add(rec);
	  }
	  try{
                    delete recsTodelete;
                }catch(DmlException e){
                    //going to check and see if the error message contains
                    System.debug('Failed to insert Linked Opportunity:  ' + e.getDmlType(0));
                    System.debug('Failed to insert Linked Opportunity:  ' + e.getCause());
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, ''+e.getDmlMessage(0));
                    ApexPages.addmessage(myMsg);
                }                         
   }
	/*************************************************************************************************************
	Method to create Linked Opportunity relations
	*************************************************************************************************************/
	public static List<Linked_Opportunity__c> associateOpportunities(list<Linked_Opportunity__c> linkedOppty) {
		list<Linked_Opportunity__c> opptyAssociations = new list<Linked_Opportunity__c>();
		map<string,string> opportunityMap = new map<string,string>();
		for(Linked_Opportunity__c rel : linkedOppty) {
			//have to check to see if it is a related opportunity link
			if(rel.Related_Opportunity__c != null && rel.Linked_Opportunity__c != Null) {
				opportunityMap.put(rel.Linked_Opportunity__c,rel.Related_Opportunity__c);
			}
		}
			
		if(opportunityMap.size() > 0) {	
				// query the related opportunity information if there is a relationship record available
				//Check database if reverse of this linked opportunity is present
				for(Linked_Opportunity__c rec : [SELECT Linked_Opportunity__c, Related_Opportunity__c 
				                                 FROM Linked_Opportunity__c
												 WHERE Linked_Opportunity__c IN :opportunityMap.values() AND Related_Opportunity__c in :opportunityMap.keyset()]) {
				
				system.debug('rec:'+rec);
				if(opportunityMap.get(rec.Related_Opportunity__c) != Null && rec.Linked_Opportunity__c == opportunityMap.get(rec.Related_Opportunity__c)) 
					opportunityMap.remove(rec.Related_Opportunity__c);
			    }
				System.debug('opportunityMap:'+opportunityMap);
				// create relationship records accordingly
				for(string key : opportunityMap.keyset()) {
					Linked_Opportunity__c newLink = new Linked_Opportunity__c();
					newLink.Related_Opportunity__c = key;
					newLink.Linked_Opportunity__c = opportunityMap.get(key);
					opptyAssociations.add(newLink);
				}

		}
		return opptyAssociations;
	}
}