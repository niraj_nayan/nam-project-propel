@isTest
private class StaffTransferCntlTest {
    //public static String CUST_PROSPECT_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
    
   @isTest static void selectAllRecordsForTransfer() {
    
        Date futureDate = Date.today().addDays(2);
        Date futureMonth = Date.today().addMonths(2);
        List<User> usersToInsert = new List<User>();
        
         User u1 = TestDataUtils.createUser('lastName', 'test123@nam.test', '3213213211');
         u1.FederationIdentifier = 'firstUser';
         usersToInsert.add(u1);
         User u2 = TestDataUtils.createUser('lastName2', 'test22@nam.test', '222223211');
         u2.FederationIdentifier = 'secondUser';
         usersToInsert.add(u2);
         User u = TestDataUtils.createUser('lastName3', 'test33@nam.test', '333333211');
         u.FederationIdentifier = 'thirdUser';
        usersToInsert.add(u);   
        insert  usersToInsert;
         
        List<Account> accountsToInsert = new List<Account>();
        Account corpAcct = TestDataUtils.createCorpAccount('testCorp');
        corpAcct.OwnerId = u1.Id;
        accountsToInsert.add(corpAcct);
        
        Account divAcct = TestDataUtils.createDivAccount('testDiv', corpAcct.Id);
        divAcct.OwnerId = u1.Id;
        accountsToInsert.add(divAcct);
        
        Account prosAcct = TestDataUtils.createProsAccount('testProspect');
        prosAcct.OwnerId = u1.Id;
        accountsToInsert.add (prosAcct);
        
        Account toAcct = TestDataUtils.createCorpAccount('toCorp');
        accountsToInsert.add(toAcct);
        insert accountsToInsert;
        
        Product_Line__c pl_Instore = TestDataUtils.createProductLine('InStore');
        insert pl_Instore;
        
        Product_Line__c pl_FSI = TestDataUtils.createProductLine('InStore');
        insert pl_FSI;

        Cycle__c cyc_Instore = new Cycle__c(Begin_Date__c = futureDate, End_Date__c = futureMonth, Source_Key__c='11223344', Week__c = 1);
        insert cyc_Instore;
        
        Goal__c g1 = new Goal__c(Assigned_To__c = u1.Id, Product_Line__c = pl_Instore.Id, Cycle__c= cyc_Instore.Id );
        insert g1;
        
        Goal__c g2 = new Goal__c(Assigned_To__c = u1.Id, Product_Line__c = pl_FSI.Id, Effective_Date__c = futureDate );
        insert g2;        
        
        list<Staff_Transfer_Request__c>  stReqs = new list<Staff_Transfer_Request__c>();
        pageReference TransferMainRef1 = Page.StaffTransfer;
        Test.setCurrentPageReference(TransferMainRef1);
        system.runAs(u){
        Test.startTest();         
        staffTransferCntl cont = new staffTransferCntl();
        System.assert(cont != null);
        cont.fromUser = corpAcct;
        cont.toUser = toAcct;
        cont.toUser.Plan_Effective_Start_Date__c = Date.today();
        
        List<staffTransferCntl.AccountInfo> acctInfoWrapList = new List<staffTransferCntl.AccountInfo>();
            cont.accounts = acctInfoWrapList;
            cont.accountsSelected = acctInfoWrapList;
            cont.accountsRemained = acctInfoWrapList;
            
        List<staffTransferCntl.ProspectInfo> prosInfoWrapList = new List<staffTransferCntl.ProspectInfo>();
            cont.prospects = prosInfoWrapList;
            cont.prospectsSelected = prosInfoWrapList;
            cont.prospectsRemained = prosInfoWrapList;
            
        List<staffTransferCntl.goalInfo> goalInfoWrapList = new List<staffTransferCntl.goalInfo>();
            cont.goals = goalInfoWrapList;
            cont.goalsSelected = goalInfoWrapList;
            cont.goalsRemained = goalInfoWrapList;
            
        cont.stepCount = 0;
        cont.accountsRemainingAssignment  = 0;
        cont.prospectsRemainingAssignment  = 0;
        cont.goalsRemainingAssignment = 0;
        cont.incrementCounter();
         Boolean recordIsSelected = false;
        
        staffTransferCntl.prospectInfo prosWrap = new staffTransferCntl.prospectInfo(prosAcct,recordIsSelected); 
            cont.retrieveProspectsOwned();  
            cont.selectAllProspects = True; 
            system.assert(cont.prospects.size()>0);
            system.assert(cont.prospectsSelected.size()>0);
            cont.incrementCounter();
            cont.decreaseCounter();
            cont.incrementCounter();
            
        staffTransferCntl.AccountInfo acctWrap = new staffTransferCntl.AccountInfo(corpAcct,recordIsSelected);
            cont.retrieveAccountsOwned();
            cont.selectAllAccount = True;
            system.assert(cont.accounts.size()>0);
            system.assert(cont.accountsSelected.size()>0);
            cont.incrementCounter();
            
         staffTransferCntl.goalInfo goalWrap = new staffTransferCntl.goalInfo(g1,recordIsSelected);
            cont.retrieveGoalsOwned();
            System.assert(cont.goals.size()>0);
            cont.selectAllGoals = True;
            cont.selectAllGoals();
            cont.incrementCounter();            
            cont.filterSelections();
           /* System.assert(cont.accountsSelected.size()>0);
            System.assert(cont.prospectsSelected.size()>0);
            System.assert(cont.goalsSelected.size()>0);           
           */
            cont.incrementCounter();
            cont.submitTransferRequest();

            //cont.selectAllAccount();
            //cont.selectAllProspects();

            
            cont.clearCollections();
            
         Test.stopTest();       
       }
    }
    
     @isTest static void doStaffTransferTest () {

        Date futureDate = Date.today().addDays(2);
        Date futureMonth = Date.today().addMonths(2);
        List<User> usersToInsert = new List<User>();
        
         User u1 = TestDataUtils.createUser('lastName', 'test123@nam.test', '3213213211');
         usersToInsert.add(u1);
         User u2 = TestDataUtils.createUser('lastName2', 'test22@nam.test', '222223211');
         usersToInsert.add(u2);
         User u = TestDataUtils.createUser('lastName3', 'test33@nam.test', '333333211');
        usersToInsert.add(u);   
        insert  usersToInsert;
         
        List<Account> accountsToInsert = new List<Account>();
        Account corpAcct = TestDataUtils.createCorpAccount('testCorp');
        corpAcct.OwnerId = u1.Id;
        accountsToInsert.add(corpAcct);
        
        Account divAcct = TestDataUtils.createDivAccount('testDiv', corpAcct.Id);
        divAcct.OwnerId = u1.Id;
        accountsToInsert.add(divAcct);
        
        Account prosAcct = TestDataUtils.createProsAccount('testProspect');
        prosAcct.OwnerId = u1.Id;
        accountsToInsert.add (prosAcct);
        
        Account toAcct = TestDataUtils.createCorpAccount('toCorp');
        accountsToInsert.add(toAcct);
        insert accountsToInsert;
        
        Product_Line__c pl_Instore = TestDataUtils.createProductLine('InStore');
        insert pl_Instore;
        
        Product_Line__c pl_FSI = TestDataUtils.createProductLine('InStore');
        insert pl_FSI;

        Cycle__c cyc_Instore = new Cycle__c(Begin_Date__c = futureDate, End_Date__c = futureMonth, Source_Key__c='11223344', Week__c = 1);
        insert cyc_Instore;
        
        Goal__c g1 = new Goal__c(Assigned_To__c = u1.Id, Product_Line__c = pl_Instore.Id, Cycle__c= cyc_Instore.Id );
        insert g1;
        
        Goal__c g2 = new Goal__c(Assigned_To__c = u2.Id, Product_Line__c = pl_FSI.Id, Effective_Date__c = futureDate );
        insert g2;        
        
        list<Staff_Transfer_Request__c>  stReqs = new list<Staff_Transfer_Request__c>();
        pageReference TransferMainRef1 = Page.StaffTransfer;
        Test.setCurrentPageReference(TransferMainRef1);
        system.runAs(u){
        Test.startTest();         
        staffTransferCntl cont = new staffTransferCntl();
        System.assert(cont != null);
        cont.fromUser = corpAcct;
        cont.toUser = toAcct;
        cont.toUser.Plan_Effective_Start_Date__c = Date.today();
        
        List<staffTransferCntl.AccountInfo> acctInfoWrapList = new List<staffTransferCntl.AccountInfo>();
            cont.accounts = acctInfoWrapList;
            cont.accountsSelected = acctInfoWrapList;
            cont.accountsRemained = acctInfoWrapList;
            
        List<staffTransferCntl.ProspectInfo> prosInfoWrapList = new List<staffTransferCntl.ProspectInfo>();
            cont.prospects = prosInfoWrapList;
            cont.prospectsSelected = prosInfoWrapList;
            cont.prospectsRemained = prosInfoWrapList;
            
        List<staffTransferCntl.goalInfo> goalInfoWrapList = new List<staffTransferCntl.goalInfo>();
            cont.goals = goalInfoWrapList;
            cont.goalsSelected = goalInfoWrapList;
            cont.goalsRemained = goalInfoWrapList;
            
        cont.stepCount = 0;
        cont.accountsRemainingAssignment  = 0;
        cont.prospectsRemainingAssignment  = 0;
        cont.goalsRemainingAssignment = 0;
        cont.incrementCounter();
        Boolean recordIsSelected = true;
        
            staffTransferCntl.prospectInfo prosWrap = new staffTransferCntl.prospectInfo(prosAcct,recordIsSelected); 
            cont.retrieveProspectsOwned();   
            cont.prospectsSelected.add(prosWrap);
            system.assert(cont.prospects.size()>0);
            system.assert(cont.prospectsSelected.size()>0);
            cont.incrementCounter();
            
            staffTransferCntl.AccountInfo acctWrap = new staffTransferCntl.AccountInfo(corpAcct,recordIsSelected);
            cont.retrieveAccountsOwned();
            cont.accountsSelected.add(acctWrap);
            system.assert(cont.accounts.size()>0);
            system.assert(cont.accountsSelected.size()>0);
            cont.incrementCounter();
            
            staffTransferCntl.goalInfo goalWrap = new staffTransferCntl.goalInfo(g1,recordIsSelected);
            cont.retrieveGoalsOwned();
            cont.goalsSelected.add(goalWrap);
            cont.incrementCounter();
            cont.selectAllAccounts();
            cont.selectAllProspects();
            //cont.getAccountsSharedByTeam(userinfo.getuserid());
            cont.filterSelections();
            cont.incrementCounter();
            cont.submitTransferRequest();   

            cont.clearCollections();
            cont.resetCounter();
           /* cont.fromUser = corpAcct;
            cont.toUser = toAcct;
            cont.selectAllAccounts();*/
            
         Test.stopTest();
            
            //System.assert(g.Assigned_To__c!=finance_User1.Id);
           // cont.selectAllGoals();

        }
    }

    static testMethod void batchTest(){
        List<User> usersToInsert = new List<User>();
        
        User u1 = TestDataUtils.createUser('lastName', 'test123@nam.test', '3213213211');
         usersToInsert.add(u1);
         User u2 = TestDataUtils.createUser('lastName2', 'test22@nam.test', '222223211');
         usersToInsert.add(u2);
         User u = TestDataUtils.createUser('lastName3', 'test33@nam.test', '333333211');
        usersToInsert.add(u);   
        insert  usersToInsert;
         
        List<Account> accountsToInsert = new List<Account>();
        Account corpAcct = TestDataUtils.createCorpAccount('testCorp');
        corpAcct.OwnerId = u1.Id;
        accountsToInsert.add(corpAcct);
        
        Account divAcct = TestDataUtils.createDivAccount('testDiv', corpAcct.Id);
        divAcct.OwnerId = u1.Id;
        insert divAcct;

        Contact con = new Contact();
        con.AccountId = divacct.id;
        con.firstname ='test first';
        con.lastname = 'test last';
        con.ownerid = u1.id;
        insert con;

        AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = u1.Id,
                AccountId = divacct.Id,
                TeamMemberRole = 'Merch Sales Rep');
            insert accTeamMember;

        System.runAs(u1){
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', divacct.Id, 'Merchandising');
            testOppty.Amount = 100;
            testOppty.ownerid = u1.id;
            insert testOppty;

            Goal__c objG = new Goal__c();
            objG.Assigned_To__c = u1.id;
            //objG.Fiscal_Year__c = 2015;
            objG.Goal_Amount__c = 12000;
            objG.Account__c = divacct.id;
            insert objG;

            Task tsk = new Task();
            tsk.whatid = divacct.id;
            tsk.subject ='Test sub';
            tsk.description = 'test description';
            tsk.ownerid = u1.id;
            insert tsk;

            Task tsk2 = new Task();
            tsk2.whatid = testOppty.id;
            tsk2.subject ='Test sub';
            tsk2.description = 'test description';
            tsk2.ownerid = u1.id;
            insert tsk2;

            Task tsk3 = new Task();
            tsk3.whoid = con.id;
            tsk3.subject ='Test sub';
            tsk3.description = 'test description';
            tsk3.ownerid = u1.id;

            insert tsk3;

            Account prosAcct = TestDataUtils.createProsAccount('testProspect');
            prosAcct.OwnerId = u1.Id;
            accountsToInsert.add (prosAcct);
            
            Account toAcct = TestDataUtils.createCorpAccount('toCorp');
            accountsToInsert.add(toAcct);
            insert accountsToInsert;
            
            Product_Line__c pl_Instore = TestDataUtils.createProductLine('InStore');
            insert pl_Instore;
            
            Product_Line__c pl_FSI = TestDataUtils.createProductLine('InStore');
            insert pl_FSI;

            Cycle__c cyc_Instore = new Cycle__c(Begin_Date__c = system.today()+2, End_Date__c = system.today()+10, Source_Key__c='11223344', Week__c = 1);
            insert cyc_Instore;
            
            Goal__c g1 = new Goal__c(Assigned_To__c = u1.Id, Product_Line__c = pl_Instore.Id, Cycle__c= cyc_Instore.Id );
            insert g1;
            
            Goal__c g2 = new Goal__c(Assigned_To__c = u2.Id, Product_Line__c = pl_FSI.Id, Effective_Date__c = system.today() );
            insert g2;        
            
            list<Staff_Transfer_Request__c>  req = new list<Staff_Transfer_Request__c>();
            
            Staff_Transfer_Request__c r = new Staff_Transfer_Request__c(Account__c = divAcct.Id,Current_Owner__c = u1.id,New_Owner__c = u2.id,Effective_Date__c = system.today());
            req.add(r);

            //Staff_Transfer_Request__c r = new Staff_Transfer_Request__c(Account__c = divAcct.Id,Current_Owner__c = u1.id,New_Owner__c = u2.id,Effective_Date__c = system.today());
            //req.add(r);

            insert req;
            Account tstacc = [select id,ownerid from Account where id=:divAcct.Id];
            system.assert(tstacc.ownerid ==  u1.id);
                
            String CRON_EXP = '0 0 0 3 11 ? 2022';
            String jobID = System.schedule('StaffTransferSchedular', CRON_EXP, new StaffTransferSchedular());

            string todaysDate = System.Now().format('yyyy-MM-dd');
            system.debug('IN test method before batch process...');
            //WHERE Effective_Date__c = ' + todaysDate + ' AND Is_Processed__c ='+false
            string query = 'SELECT id,Access_through_Account_Team__c,Account__c,Prospect__c,Goal__c,Current_Owner__c,New_Owner__c,Opportunity__c,Opportunity_Access_through_Sales_Team__c FROM Staff_Transfer_Request__c limit 1';
            StaffTransferBatchClass.createAccountTeamMember(divacct.Id, u2.id);
            StaffTransferBatchClass.createOpportunityTeamMember(testoppty.id,u2.id);
            StaffTransferBatchClass batch = new StaffTransferBatchClass(query);

            Database.executeBatch(batch,1);
        }
      

    }
    
}