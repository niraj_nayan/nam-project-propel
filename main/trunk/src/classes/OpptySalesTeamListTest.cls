@isTest
private class OpptySalesTeamListTest {
    
    @isTest static void OpptySalesTeamAddTeamButton() {
        // Implement test code
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            corp1.Roll_Up_Revenue__c = 0;
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            div1.Roll_Up_Revenue__c = 0;
            insert div1;

            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
            insert testOppty;

            testOppty.Name = 'New Name';
            testOppty.OwnerId = user1.Id;
            testOppty.Amount = 10000;
            update testOppty;

            OpportunityTeamMember testMember = TestDataUtils.createTeamMember(user1.Id, 'Manager', testOppty.Id, 10);
            insert testMember;

            OpptySalesTeamList controller = new OpptySalesTeamList(new ApexPages.StandardController(testOppty));
            controller.addTeamMember();
          
            system.assert(controller.redirectURL == '/apex/Add_Sales_Team?addTo='+testOppty.id);
        }
    }
    

    @isTest static void OpptySalesTeamNextPage(){
        // Implement test code
        User user1 = TestDataUtils.createUser('Test Last Name1', 'no-reply@email1.com', '1231231234');
        User user2 = TestDataUtils.createUser('Test Last Name2', 'no-reply@email2.com', '1231231234');
        User user3 = TestDataUtils.createUser('Test Last Name3', 'no-reply@email3.com', '1231231234');
        User user4 = TestDataUtils.createUser('Test Last Name4', 'no-reply@email4.com', '1231231234');
        User user5 = TestDataUtils.createUser('Test Last Name5', 'no-reply@email5.com', '1231231234');
        User user6 = TestDataUtils.createUser('Test Last Name6', 'no-reply@email6.com', '1231231234');
        User user7 = TestDataUtils.createUser('Test Last Name7', 'no-reply@email7.com', '1231231234');
        insert user1;
        insert user2;
        insert user3;
        insert user4;
        insert user5;
        insert user6;
        insert user7;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div1;

            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
            insert testOppty;

            OpportunityTeamMember testMember1 = TestDataUtils.createTeamMember(user1.Id, 'Manager', testOppty.Id, 10);
            OpportunityTeamMember testMember2 = TestDataUtils.createTeamMember(user2.Id, 'Temp Access', testOppty.Id, 10);
            OpportunityTeamMember testMember3 = TestDataUtils.createTeamMember(user3.Id, 'National Lead', testOppty.Id, 10);
            OpportunityTeamMember testMember4 = TestDataUtils.createTeamMember(user4.Id, 'Temp Access', testOppty.Id, 10);
            OpportunityTeamMember testMember5 = TestDataUtils.createTeamMember(user5.Id, 'Temp', testOppty.Id, 10);
            OpportunityTeamMember testMember6 = TestDataUtils.createTeamMember(user6.Id, 'Sales Rep', testOppty.Id, 10);
            OpportunityTeamMember testMember7 = TestDataUtils.createTeamMember(user7.Id, 'Sales Rep', testOppty.Id, 10);
            insert testMember1;
            insert testMember2;
            insert testMember3;
            insert testMember4;
            insert testMember5;
            insert testMember6;
            insert testMember7;

            testMember1.TeamMemberRole = 'Test Role';
            update testMember1;

            //testOppty.Name = 'New Name';
            //testOppty.OwnerId = user7.Id;
            //update testOppty;

            OpptySalesTeamList controller = new OpptySalesTeamList(new ApexPages.StandardController(testOppty));

            controller.nextPage();
            controller.previousPage();
            controller.addTeamMember();
            system.assert(controller.redirectURL == '/apex/Add_Sales_Team?addTo='+testOppty.id);
        }
    }
}