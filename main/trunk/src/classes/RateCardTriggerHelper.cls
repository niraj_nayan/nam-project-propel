/*****************************************************************************************************************
Description: This class is used by the RateCardTrigger. All of these methods are called from that trigger
             
Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   created the helper class
Version 2:   Updated class to prevent nullpointer exeption before looping through Account team members
********************************************************************************************************************/

Public class RateCardTriggerHelper {
    public static string clientSpecificRate = Rate_Card__c.SObjectType.getDescribe().getRecordTypeInfosByName().get(Label.Client_Specific_Rate_Card).getRecordTypeId();
    
    /***********************************************************************************
    Method to process before update actions
    ************************************************************************************/
    public static void beforeUpdateActions(list<Rate_Card__c> rateCards,map<Id,Rate_Card__c> oldMap) {
        deletePreviousShareRecords(isClientSpecificRateCard(rateCards),oldMap);
    }
    
    /***********************************************************************************
    Method to process after insert actions
    ************************************************************************************/
    public static void afterInsertActions(list<Rate_Card__c> rateCards) {
        map<string,string> relatedAccount = getRelatedAccountInfo(rateCards);
        if(relatedAccount.size() > 0)
            createRateCardShareRecords(relatedAccount);
    }
    
    /***********************************************************************************
    Method to process after update actions
    ************************************************************************************/
    public static void afterUpdateActions(list<Rate_Card__c> rateCards,map<Id,Rate_card__c> oldMap) {
        map<string,string> relatedAccount = getRelatedAccountInfo(rateCards);
        if(relatedAccount.size() > 0)
            createRateCardShareRecords(relatedAccount);
    }
    /************************************************************************************
    Helper method to process ClientSpecificRate Card field
    *************************************************************************************/
    static list<Rate_Card__c>  isClientSpecificRateCard(list<Rate_Card__c> rateCards) {
           string RecordTypeId = 'RecordTypeId';
           list<Rate_Card__c> clientSpecificTypes = new list<Rate_Card__c>();
           for(Rate_Card__c rc : rateCards) {
               if(String.valueOf(rc.get(RecordTypeId)) == clientSpecificRate)
                   clientSpecificTypes.add(rc);
           }
           return clientSpecificTypes;
    }
    
    /************************************************************************************
    Helper method to create share records
    *************************************************************************************/
    static void createRateCardShareRecords(map<string,string> relatedAccountMap) {
        set<string> accountIds = new set<string>();
        accountIds.addAll(relatedAccountMap.values());
        list<Rate_Card__Share> shareRecs = new list<Rate_Card__Share>();
        // get the account team members
        if(accountIds.size() > 0) {
            map<string,set<string>> accountTeamMembers = getAccountTeamMembers(accountIds);
            // create share records accordingly
            
                for(string recId : relatedAccountMap.keyset()) {
                    if(accountTeamMembers.containsKey(relatedAccountMap.get(recId))){
                        for(string memId : accountTeamMembers.get(relatedAccountMap.get(recId))) {
                            Rate_Card__Share  shareRec = new Rate_Card__Share();
                            shareRec.ParentId = recId;
                            shareRec.AccessLevel = Label.Sharing_Permission_Read;
                            shareRec.UserOrGroupId = memId;
                            shareRecs.add(shareRec);
                        }
                    }
                }
            
        }
        if(shareRecs.size() > 0) {
            system.debug('shareRecs:'+shareRecs);
            database.insert(shareRecs,false);
        }
        
    }
    /************************************************************************************
    Helper method to get related account information
    *************************************************************************************/
    static map<string,string> getRelatedAccountInfo(list<Rate_Card__c> ratecards) {
       map<string,string> relatedAccount = new map<string,string>();
       for(Rate_Card__c rc : rateCards) {
            if(rc.Account__c != Null) {
                relatedAccount.put(rc.Id,rc.Account__c); 
            }
        }
        return relatedAccount;  
    }
    /************************************************************************************
    Helper method to delete Previous Share records
    *************************************************************************************/
    static void deletePreviousShareRecords(list<Rate_Card__c> rateCards,map<Id,Rate_Card__c> oldMap) {
         set<string> accountIds = new set<string>();
         for(Rate_Card__c rc : rateCards) {
            if(rc.Account__c != oldMap.get(rc.Id).Account__c)
                accountIds.add(oldMap.get(rc.Id).Account__c);
         }
         if(accountIds.size() > 0) {
            
            set<string> accountTeamMembers = retrieveValues(getAccountTeamMembers(accountIds));
            // delete the rate card share records accordingly
            if(accountTeamMembers.size() > 0) {
                list<Rate_Card__Share> sharerecords = [SELECT ParentID FROM Rate_Card__Share WHERE UserOrGroupId in :accountTeamMembers AND ParentID in :oldMap.keyset()]; 
                database.delete(sharerecords,false);
            }  
         }   
          
    }
    /************************************************************************************
    Method to retrieve account teammember information
    *************************************************************************************/
    static map<string,set<string>> getAccountTeamMembers(set<string> accountIds) {
           map<string,set<string>> accountMembers = new map<string,set<string>>();
          // query for account team members
           for(AccountTeamMember acctMember : [SELECT UserId,AccountId,Account.OwnerId FROM AccountTeamMember WHERE AccountId in :accountIds ORDER BY AccountId]) {
                 if(accountMembers.get(acctMember.AccountId) != Null) {
                    accountMembers.get(acctMember.AccountId).add(acctMember.UserId);
                    accountMembers.get(acctMember.AccountId).add(acctMember.Account.OwnerId);
                 }
                 else {
                    set<string> temp = new set<string>();
                    temp.add(acctMember.UserId);
                    temp.add(acctMember.Account.OwnerId);
                    accountMembers.put(acctMember.AccountId,temp);
                 }  
           }
           return accountMembers;       
    }
    /********************************************************************************************
    method to process a nested map collection 
    *********************************************************************************************/
    static set<string> retrieveValues(map<string,set<string>> recs) {
        set<string> retrieved = new set<string>();
        for(string key : recs.keyset())
            retrieved.addAll(recs.get(key));
        return retrieved;
    }

}