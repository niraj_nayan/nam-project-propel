/****************************************************************************************************************************************************
Description: To get the NAM accounts and update site id from the selected Account            
Created on:  21 Jan 2016
Created by:  Seth
Version 1:   Controller for FinnAppsAccountSync VF page to get the NAM Accounts based on Account Name. 
             
******************************************************************************************************************************************************/

public class FinnAppsAccountSync {

    public CreditProfileUtility.profileDetails profDetail {get;set;}
    public string AcctId = ApexPages.currentpage().getparameters().get('acctID');
    String accountSyncInfo;
    public Account currentAccount {get;set;}
    public Boolean isAllowedToPage {get;set;}
    public List<ParsedAccount> finAppAccounts {get;set;}
    public String selectedLOCValue {get;set;}
    
    public String AccountNameSearch {get;set;}
    public String AccountSiteSearch {get;set;}
    
    public FinnAppsAccountSync() {
        currentAccount = [Select Id, Name, ParentId, Parent.Name, Site, LOC__c From Account 
                                        Where Id =:AcctId];
        //first check the current user profile to see if they can view the page.
        isAllowedToPage = userProfileCheck();
        if(isAllowedToPage == true) {
            
        }
    }
    
    public void Search(){
        if( (AccountNameSearch == null || AccountNameSearch == '') && (AccountSiteSearch == null || AccountSiteSearch == '') ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter either Account Name or Account Site to Search!!'));
            return;
        }
        else{
            getAccounts();
            
        }
    }
    
    public void getAccounts(){
    
                if(currentAccount.site<>null){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning,'Please note this Account already have Account Site associated with it.');
                    ApexPages.addMessage(myMsg);
                }
                
                finAppAccounts = new List<ParsedAccount>();

                List<String> stringList = currentAccount.Name.split(' ');
                System.debug('Split String: ' + stringList);
                
                String userName = CreditProfileCredentials__c.getInstance('FinApps').UserName__c;//'NAMCreditProfile';
                String pwd=CreditProfileCredentials__c.getInstance('FinApps').Password__c;//'NAMCreditProfilePwd';    
                Blob un = Blob.valueOf(userName);
                Blob pd = Blob.valueOf(pwd);
                Blob decodedKey = EncodingUtil.base64decode(Label.encodedKey);
                
                Blob encryptedUn = Crypto.encryptWithManagedIV('AES128', decodedKey, un);
                Blob encryptedPd = Crypto.encryptWithManagedIV('AES128', decodedKey, pd);
                String encryptedUnF = EncodingUtil.base64encode(encryptedUn);
                String encryptedPdF = EncodingUtil.base64encode(encryptedPd);
                
                //String accName = currentAccount.Name;
                String accName = AccountNameSearch;
                accName = EncodingUtil.urlEncode(accName,'UTF-8');
                encryptedUnF = EncodingUtil.urlEncode(encryptedUnF,'UTF-8');
                encryptedPdF = EncodingUtil.urlEncode(encryptedPdF,'UTF-8');
                string url = Label.Finapps_End_Point + accName +'&userName='+encryptedUnF+'&pwd='+encryptedPdF+'&site='+AccountSiteSearch;
                
                // Instantiate a new http object
                Http h = new Http();
                // Instantiate a new HTTP request, specify the method (POST) as well as the endpoint
                Continuation cont = new Continuation(40);
                cont.continuationMethod='processResponse';

                HttpRequest req = new HttpRequest();
                req.setEndpoint(url);
                req.setMethod('GET');
                req.setTimeout(30000);
                req.getBody();
                //req.setBody(sampleString);
                // Send the request, and return a response
                String requestLabel;
                requestLabel = cont.addHttpRequest(req);

                //HttpResponse res = Continuation.getResponse(requestLabel);
                HttpResponse res = h.send(req);
                accountSyncInfo = res.getBody();
                System.debug('Account Name: ' + currentAccount.Name);
                System.debug('accountSyncInfo:'+ accountSyncInfo);
                System.debug('The whole response: ' + res);
                System.debug('Body: ' + accountSyncInfo);
                system.debug(accountSyncInfo.contains('UserName or Password or both did not Match'));
                //accountSyncInfo = '[{"PARENT_ACCOUNT_NUMBER": "102072","LOC": "102072-4104","ACCOUNT_NAME": "GREY ADVERTISING-G2 DIRECT & DIGITAL FOR AAA"},{"PARENT_ACCOUNT_NUMBER": "124361","LOC": "124361-35659","ACCOUNT_NAME": "AAA OF WESTERN & CENTRAL NEW YORK-WILLIAMSVILLE."}]';
                if(accountSyncInfo<>null && accountSyncInfo.contains('UserName or Password or both did not Match')){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Authentication Failed. Please contact your system administrator');
                    ApexPages.addMessage(myMsg);
                }
                else{
                    if((!String.isEmpty(accountSyncInfo) && !accountSyncInfo.contains('[]'))&& !accountSyncInfo.contains('StatusCode=500') && !accountSyncInfo.contains('Response code 503')  && !accountSyncInfo.contains('StatusCode=500') ){
                        JSONParser parser = JSON.createParser(accountSyncInfo);
    
                        //JSON will always come as
                        //ARRAY
                        //  START_OBJECT-----REPEAT THE OBJECT
                        //      FIELD NAME = PARENT_ACCOUNT_NUMBER
                        //      VALUE_STRING
                        //      FIELD NAME = LOC
                        //      VALUE_STRING
                        //      FIELD NAME = ACCOUNT NAME
                        //      VALUE_STRING
                        //  END_OBJECT-----REPEAT THE OBJECT
                        String tempParentAccountNumber = null;
                        String tempLOCValue = null;
                        String tempAccountName = null;
                        ParsedAccount tempParsedAccount = new ParsedAccount();
                        while(parser.nextToken() != null){
                            //Start of the object
                            if (parser.getCurrentToken() == JSONToken.START_OBJECT){
                                //take next step to first FIELD_NAME
                                parser.nextToken();
                                //ACCOUNT_NAME,LOC,STATUS,TYPE,ADDRESS,CITY,ZIP,STATE,SITE_USE_CODE,Parent_Account_Number
                                //check to make sure next token is field name
                                if(parser.getCurrentToken() == JSONToken.FIELD_NAME){
                                    //need to step one more time to get the parent account number
                                    parser.nextToken();
                                    tempParsedAccount.classcode=parser.getText();
                                    if(tempParsedAccount.classcode != null && tempParsedAccount.classcode.contains('-'))
                                        tempParsedAccount.type= tempParsedAccount.classcode.split(('-'))[0];
                                        
                                    //two steps forward
                                    parser.nextToken();
                                    parser.nextToken();
                                    //type
                                    //tempParsedAccount.type= parser.getText();
                                    
                                    parser.nextToken();
                                    parser.nextToken();
                                    //loc
                                    tempLOCValue = parser.getText();
                                    
                                    
                                    parser.nextToken();
                                    parser.nextToken();
                                    //address
                                    tempParsedAccount.address= parser.getText();
                                    
                                    parser.nextToken();
                                    parser.nextToken();
                                    //state
                                    tempParsedAccount.state= parser.getText();
                                                                    
                                    parser.nextToken();
                                    parser.nextToken();
                                    //account name
                                    tempAccountName = parser.getText();
                                    
                                    
                                    parser.nextToken();
                                    parser.nextToken();
                                    //siteuse code
                                    tempParsedAccount.locationType= parser.getText();
                                    
                                    
                                    parser.nextToken();
                                    parser.nextToken();
                                    //zip
                                    tempParsedAccount.zip= parser.getText();
                                    
                                    parser.nextToken();
                                    parser.nextToken();
                                    //status
                                    tempParsedAccount.status = parser.getText();
                                    
                                    parser.nextToken();
                                    parser.nextToken();
                                    //city
                                    tempParsedAccount.city= parser.getText();
                                    //tempParentAccountNumber = parser.getText();
                                    
                                    //set all of the parsered values to a temp account
                                    tempParsedAccount.parentAccount = tempParentAccountNumber;
                                    tempParsedAccount.locValue =  tempLOCValue;
                                    tempParsedAccount.accountName = tempAccountName;
    
                                    //add temp account to list of all parsed accounts
                                    if(finAppAccounts.size()<1000)
                                    finAppAccounts.add(tempParsedAccount);
    
                                }
    
                                tempParsedAccount = new ParsedAccount();
                            }
    
                        }
    
                        System.debug('List of Parsed Accounts: ' + finAppAccounts);
                        
                    }
                    else if((String.isEmpty(accountSyncInfo)) || (accountSyncInfo.contains('[]'))){
                        System.debug('HTTP error: ' + res);
                        System.debug('HTTP error body: ' + res.getBody());
                        System.debug('HTTP error status: ' + res.getStatus());
                        System.debug('HTTP error status code: ' + res.getStatusCode());
                        System.debug('HTTP error String Message: ' + res.toString());
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'No Accounts in FinApps that match the given Account Name/Account Site.');
                        ApexPages.addMessage(myMsg);
                    }
                    
                    else{
                        //connection issue to the http web service has happened
                        System.debug('HTTP error: Status code or response code: ' + res);
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'An error has occured. Please contact a System Administrator.');
                        ApexPages.addMessage(myMsg);
                    }
                }
                
        
    }
    public void DoNothing(){

    }

    public PageReference saveButton(){
        //check to make sure a value was selected
        if(String.isEmpty(selectedLOCValue)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'An Account must be selected to save.');
            ApexPages.addMessage(myMsg);
        }

        //we will first check all of accounts to make sure that account site id is not already in the system
        List<Account> siteIDExistAccount = [Select Id, Name, Site, LOC__c From Account Where Site =:selectedLOCValue LIMIT 1];

        if(siteIDExistAccount.size() < 1){//if no other account comes back with the same site id
            try{
                for(ParsedAccount acct: finAppAccounts){
                    if(acct.locValue == selectedLOCValue){//meaning that is the selected account
                        currentAccount.LOC__c = acct.locValue;
                        currentAccount.Site = acct.locValue;
                        //currentAccount.Name = acct.accountName;
                    }
                }

                update currentAccount;

                PageReference acctPage = new PageReference('/' + AcctId);
                return acctPage;
            }
            catch(DmlException e){
                System.debug('Failed to insert Opportunity Products: ' + e.getDmlType(0));
                System.debug('Failed to insert Opportunity Products: ' + e.getCause());
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, ''+e.getDmlMessage(0));
                ApexPages.addmessage(myMsg);
                return null;
            }
            catch(Exception e){
                System.debug('Error: ' + e);
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'An error has occured. Please contact a System Administrator.');
                ApexPages.addMessage(myMsg);
            }
        }
        else{//The site ID is already in the system
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'The Site Id is already assigned to another Account. Please select a different Account.');
            ApexPages.addMessage(myMsg);
        }

        System.debug('selected Site ID: ' + selectedLOCValue);
        return null;
    }

    public class ParsedAccount{
        public Boolean isSelected {get;set;}
        public String parentAccount {get;set;}
        public String locValue {get;set;}
        public String address {get;set;}
        public String status {get;set;}
        public String type {get;set;}
        public String city {get;set;}
        public String zip {get;set;}
        public String state {get;set;}
        public String locationType {get;set;}
        public String accountName {get;set;}
        public String classCode {get;set;}
        
        public ParsedAccount(){
            isSelected = false;
            parentAccount = null;
            locValue = null;
            accountName = null;
        }

        public ParsedAccount(String parentAccountJSON, String locValueJSON, String accountNameJSON){
            isSelected = false;
            parentAccount = parentAccountJSON;
            locValue = locValueJSON;
            accountName = accountNameJSON;
        }
    }

    public static boolean userProfileCheck(){

        //create a set from list of custom setting SalesTeamsProfiles
        Set<String> profilesAccess = new Set<String>();
        User currentUser=[Select Id, Name, Profile.Name From User Where Id=:UserInfo.getUserId()];
        //Users that can edit and view
        for(FinnAppsAccountSyncProfiles__c profile: FinnAppsAccountSyncProfiles__c.getAll().Values()){
            profilesAccess.add(profile.Name);
        }

        //if current user profile is not System Admin, Integration, or Billing/Credit
        if(profilesAccess.contains(currentUser.Profile.Name)){
            //checkIfCurrentUserIsOnAccountTeam(oppty);
            return true;
        }
        else{
            return false;
        }
    }

    public PageReference redirectFromPage(){
        PageReference acctPage = new PageReference('/' + AcctId);
        return acctPage;
    }
}