@isTest
private class OpptyInlineInstoreTest {
    //static OpptyInlineInstoreExtn extn;
    static Id   divRT, corpRt, oppRt;
    static {
        for(RecordType rt : [select Id, Name from RecordType where Name IN ('Division Account', 'Corporate', 'InStore')]){
                if(rt.Name == 'Division Account')
                    divRT = rt.Id;
                else if (rt.Name == 'Corporate')
                    corpRT = rt.Id;
                else if (rt.Name == 'InStore')
                    oppRT = rt.Id;
            }
        }
    private static testMethod void init(){
        Account corpAcc = new Account(Name = 'testAcct', RecordTypeId = corpRT, CurrencyIsoCode = 'USD');
        insert corpAcc;
        
        Account divAcc = new Account(Name = 'testAcct', ParentId = corpAcc.Id, RecordTypeId = divRT, CurrencyIsoCode = 'USD');
        insert divAcc;
        
        Opportunity opp1 = new Opportunity(Name = 'opp1', AccountId = divAcc.Id, StageName = 'Client Engagement', CloseDate = System.today(), Order__c = '11223', RecordTypeId = oppRT, CurrencyIsoCode = 'USD');
        insert opp1;
        
        Opportunity opp2 = new Opportunity(Name = 'opp2', AccountId = divAcc.Id, StageName = 'Client Engagement', CloseDate = System.today(), Parent_Order__c = '11223', RecordTypeId = oppRT, CurrencyIsoCode = 'USD');
        opp2.Id = null;
        insert opp2;

        System.debug('opp1 Id: ' + opp1.Id);
        System.debug('opp2 Id: ' + opp2.Id);
        
        Test.StartTest();
        pageReference oppRef = Page.OpportunityInlineInstoreProgram;
        Test.setCurrentPageReference(oppRef);
        
        ApexPages.CurrentPage().getparameters().put('id', opp1.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp1);
        
        OpptyInlineInstoreExtn extn = new OpptyInlineInstoreExtn(sc);
        System.assert(extn != null);
        Opportunity opp = extn.oppRecord; 
        String ord = extn.orderNumber;
        String qs = extn.queryString;
        List<opportunity> opptyList = extn.childoppList;
        System.debug('###opptyList'+opptyList);
        System.assertEquals(opp.Id, opp1.id); 
        System.assert(qs != null);
        //System.assertEquals(extn.currentRecordId, opp1.id);
        System.debug('oppty size: ' + opptyList.size());
        System.assert(opptyList.size() == 0);
        List<opportunity> oppResults = [Select Id, Name, Order__c from Opportunity where Id IN: opptyList];
        //System.assertEquals(oppResults[0].Name,'opp2');
        //System.assertEquals(oppResults[0].Order__c,opp2.Order__c);
        Test.stopTest();
    }
}