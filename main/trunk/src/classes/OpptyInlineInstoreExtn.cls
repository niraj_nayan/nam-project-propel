/***************************************************************************************************************************
Description: Inline VF page to populate all related InStore Opportunities with same Order # except the record they are on 
Created on:  29 September 2015
Created by:  Kavitha

Version 1:  Controller for OpportunityInlineInstoreProgram to fetch all related Instore Opportunities
Version 2:  Jagan: Updated code to prevent SOQL injection
Version 3:  Jagan: Updated code to include more columns and display a new table for "Category Ownership" type of opps
********************************************************************************************************************************/
public with sharing class OpptyInlineInstoreExtn {
    String currentRecordId;
    //public String oppProductLine {get;set;}
    public String orderNumber {get;set;}
    public String ParentorderNumber {get;set;}
    Public id parentOppId {get;set;}
    public String oppType{get;set;}
    public String queryString {get;set;}
    public Opportunity oppRecord {get;set;}
    public transient List<opportunity> childoppList {get;set;}
    public transient List<opportunity> childorderoppList {get;set;}
    public Map<String,Decimal> childspacerevmap {get;set;}
    public Map<String,Decimal> childprodrevmap {get;set;}
    
    public Map<String,Decimal> childcommrevmap {get;set;}
    public Map<String,Decimal> childnoncommrevmap {get;set;}
    
    public OpptyInlineInstoreExtn(ApexPages.StandardController con){  
        currentRecordId = ApexPages.currentPage().getParameters().get('id');
        childoppList = new List<opportunity>();
        childorderoppList = new List<opportunity>();
        childspacerevmap = new Map<String,Decimal>();
        childprodrevmap = new Map<String,Decimal>();
        childnoncommrevmap  = new Map<String,Decimal>();
        childcommrevmap  = new Map<String,Decimal>();
        oppRecord = [select Id, Type,Name,Parent_Opportunity__c, Parent_Order__c,Order__c, RecordType.Name from Opportunity where Id =:currentRecordId Limit 1];
        //oppProductLine = oppRecord.RecordType.Name;
       
        oppType = oppRecord.Type;
        
        if(oppRecord.Parent_Opportunity__c!= Null)
            parentOppId = oppRecord.Parent_Opportunity__c;
        
        if(oppRecord.Parent_Order__c != Null)
            ParentorderNumber = oppRecord.Parent_Order__c;
        if(oppRecord.Order__c != Null)
            orderNumber = oppRecord.Order__c; 
        if(currentRecordId != null || parentOppId <> null)
            getChildandSameOrderOpps();    
        if(ParentorderNumber<> null || orderNumber<>null )
            getChildOrderOpps();
    }
    public String getqueryString(String filterVal){
        String queryString;// = 'select ';//,Name
        for(Schema.SobjectField sof:Opportunity.sObjectType.getDescribe().fields.getMap().values()){
                if(queryString ==null)
                    queryString = 'Select Parent_Opportunity__r.name,Instore_Cycle__r.name,Parent_Opportunity__r.Source_Key__c,'+sof.getDescribe().getName();
                else
                queryString += ','+sof.getDescribe().getName();
        }
        queryString += ' from Opportunity'+ filterVal;
        System.debug('Query String.....'+queryString);
        return queryString ;
    }
    
    //For Tactical :- Bring the child opps or the sibling depending on the current opportunity
    public void getChildandSameOrderOpps(){
        system.debug('Current opp....'+currentRecordId+'....parent opp...'+parentOppId);
        //String filterVal =' where Parent_Opportunity__c=\''+String.escapeSingleQuotes(currentRecordId)+'\' and Order__c=\''+String.escapeSingleQuotes(orderNumber)+'\'';
        String filterVal;
        //if the current opportunity is child then bring the siblings else bring the children
        if(parentOppId != null)
            filterVal =' where Parent_Opportunity__c=\''+String.escapeSingleQuotes(parentOppId)+'\'';
        else
            filterVal =' where Parent_Opportunity__c=\''+String.escapeSingleQuotes(currentRecordId)+'\'';
        queryString = getqueryString(filterVal);
        childoppList = Database.query(queryString);
        //return childoppList;
    }
    
    //For Category Ownership and Subscription :- opportunities that have parent order as current opportunity
    public void getChildOrderOpps(){
        //String filterVal =' where Parent_Order__c =\''+String.escapeSingleQuotes(orderNumber)+'\'';
        String filterVal;
        if(orderNumber != null)
            filterVal =' where Parent_Order__c =\''+String.escapeSingleQuotes(orderNumber)+'\'';
        if(ParentorderNumber != null)
            filterVal =' where Parent_Order__c =\''+String.escapeSingleQuotes(ParentorderNumber)+'\'';
        queryString = getqueryString(filterVal);
        childorderoppList = Database.query(queryString);
        set<Id> oppidset = new set<Id>();
        
        for(opportunity opp:childorderopplist){
            oppidset.add(opp.id);
        }
        if(oppidset.size()>0){
            List<Opportunity> lstChildoppsofChildorders = new List<Opportunity>();
            lstChildoppsofChildorders = [select id,Parent_Opportunity__c, Source_Key__c,Order__c,Parent_order__c,Space_Revenue__c,Production_Revenue__c,Other_Commissionable__c,Other_NonCommissionable__c 
                                            from Opportunity where parent_opportunity__c in :oppidset];
            for(Opportunity opp: lstChildoppsofChildorders)
            {
                
                if(!childspacerevmap.containsKey(opp.parent_opportunity__c ))
                    childspacerevmap.put(opp.parent_opportunity__c ,opp.Space_Revenue__c);
                else
                    childspacerevmap.put(opp.parent_opportunity__c , childspacerevmap.get(opp.parent_opportunity__c )+opp.Space_Revenue__c);
                    
                if(!childprodrevmap.containsKey(opp.parent_opportunity__c ))
                    childprodrevmap.put(opp.parent_opportunity__c ,opp.Production_Revenue__c);
                else
                    childprodrevmap.put(opp.parent_opportunity__c, childprodrevmap.get(opp.parent_opportunity__c )+opp.Production_Revenue__c);
                
                if(!childcommrevmap.containsKey(opp.parent_opportunity__c ))
                    childcommrevmap.put(opp.parent_opportunity__c ,opp.Other_Commissionable__c);
                else
                    childcommrevmap.put(opp.parent_opportunity__c , childcommrevmap.get(opp.parent_opportunity__c )+opp.Other_Commissionable__c);
                
                
                if(!childnoncommrevmap.containsKey(opp.parent_opportunity__c ))
                    childnoncommrevmap.put(opp.parent_opportunity__c ,opp.Other_NonCommissionable__c );
                else
                    childnoncommrevmap.put(opp.parent_opportunity__c , childnoncommrevmap.get(opp.parent_opportunity__c )+opp.Other_NonCommissionable__c );
                
            }
            
            for(opportunity opp:childorderopplist){
                
                    
                if(!childspacerevmap.containsKey(opp.id))
                    childspacerevmap.put(opp.id ,opp.Space_Revenue__c);
                else
                    childspacerevmap.put(opp.id, childspacerevmap.get(opp.id)+opp.Space_Revenue__c);
                    
                if(!childprodrevmap.containsKey(opp.id))
                    childprodrevmap.put(opp.id,opp.Production_Revenue__c);
                else
                    childprodrevmap.put(opp.id, childprodrevmap.get(opp.id)+opp.Production_Revenue__c);
                
                if(!childcommrevmap.containsKey(opp.id))
                    childcommrevmap.put(opp.id,opp.Other_Commissionable__c);
                else
                    childcommrevmap.put(opp.id, childcommrevmap.get(opp.id)+opp.Other_Commissionable__c);
                
                
                if(!childnoncommrevmap.containsKey(opp.id))
                    childnoncommrevmap.put(opp.id,opp.Other_NonCommissionable__c );
                else
                    childnoncommrevmap.put(opp.id, childnoncommrevmap.get(opp.id)+opp.Other_NonCommissionable__c );
                    
                }
            
        }
        
    }
}