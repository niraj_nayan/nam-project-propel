public with sharing class OverrideOppHierarchyCntrl {
    
    public opportunity opp {get;set;}
    public opportunity originalopp = new Opportunity();
    public OverrideOppHierarchyCntrl(ApexPages.StandardController controller) {
        opp = (Opportunity) controller.getRecord();
        opp = [select id,name,Frozen_Hierarchy__c,Account_Sales_Office_Text__c,Primary_Reps_Territory__c,Primary_Sales_Rep__c,Team_Name__c,Team_Manager__c,Region_Name__c,
                Division_Territory_Id__c,Primary_Rep_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,
                Region_Manager__c,Division_Name__c,Division_Manager__c from Opportunity where id=:opp.id];
        //To check for differences
        originalopp = [select id,name,Frozen_Hierarchy__c,Account_Sales_Office_Text__c,Primary_Reps_Territory__c,Primary_Sales_Rep__c,Team_Name__c,Team_Manager__c,Region_Name__c,
                Division_Territory_Id__c,Primary_Rep_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,
                Region_Manager__c,Division_Name__c,Division_Manager__c from Opportunity where id=:opp.id];
    }
    
    public PageReference save(){
        
        String divMgr,regMgr,TeamMgr,Psr,divName,regName,teamName,soffc,psrTerr,RegTerrId,DivTerrId,TeamTerrId,PsrTerrId,psrname;
        
        divMgr = opp.Division_Manager__c;
        regMgr = opp.Region_Manager__c;
        TeamMgr = opp.Team_Manager__c;
        Psr = opp.Primary_Sales_Rep__c;
        
        divName = opp.Division_Name__c;
        regName = opp.Region_Name__c;
        teamName = opp.Team_Name__c;
        psrname = opp.Primary_Reps_Territory__c;
        
        soffc = opp.Account_Sales_Office_Text__c;
        psrTerr = opp.Primary_Reps_Territory__c;
        RegTerrId = opp.Region_Territory_Id__c;
        DivTerrId = opp.Division_Territory_Id__c;
        TeamTerrId = opp.Team_Territory_Id__c;
        PsrTerrId = opp.Primary_Rep_Territory_Id__c;
        
        Set<String> usernameset = new Set<String>();
        Set<String> Terrnameset = new Set<String>();
        
        //Map of 15 digit id and 18 digit id from user query
        Map<String,String> queryuseridmap = new Map<String,String>();
        Map<String,Territory2> queryTerridmap = new Map<String,Territory2>();
        
        if(divMgr <> null)
            usernameset.add(divMgr);
        if(TeamMgr <> null)
            usernameset.add(TeamMgr);
        if(Psr <> null)
            usernameset.add(Psr );
        if(regMgr <> null)
            usernameset.add(regMgr );
        
        if(divName<> null)
            Terrnameset.add(divName);
        if(RegName<> null)
            Terrnameset.add(RegName);
        if(teamName<> null)
            Terrnameset.add(TeamName);
        if(psrName<> null)
            Terrnameset.add(psrName);
        
        List<User> lstusr = new List<user>();
        List<Territory2> lstTerr = new List<Territory2>();
         
        lstusr = [select id,username from user where username in: usernameset];
        lstTerr = [select id,name from Territory2 where name in: Terrnameset]; 
       
        if(lstusr.size() >0 ){
            for(user u:lstusr){
                queryuseridmap.put(u.username,u.id);
            }
        }
        
        if(lstTerr.size() >0 ){
            for(Territory2 t : lstTerr){
                queryTerridmap.put(t.name,t);
            }
        }
        
        boolean invalidDivuser = false;
        boolean invalidReguser = false;
        boolean invalidPsruser = false;
        boolean invalidTeamuser = false;
        
        boolean invalidDivterr = false;
        boolean invalidRegterr = false;
        boolean invalidPsrterr = false;
        boolean invalidTeamterr = false;
        
        String errorPlace='';
            
            if(divMgr<>null && (!queryuseridmap.containsKey(divMgr) && divMgr != originalopp.Division_Manager__c)){
                invalidDivuser = true;
                errorPlace = 'Division Manager';
            }
            
            if(TeamMgr <>null && (!queryuseridmap.containsKey(TeamMgr) && TeamMgr != originalopp.Team_Manager__c)){
                invalidTeamuser = true;
                errorPlace = 'Team Manager';
            }
            
            if(Psr <>null && (!queryuseridmap.containsKey(Psr) && Psr  != originalopp.Primary_Sales_Rep__c)){
                invalidPsruser = true;
                errorPlace = 'Primary Sales Rep';
            }
            
            if(regMgr <>null && (!queryuseridmap.containsKey(regMgr) && regMgr != originalopp.Region_Manager__c)){
                invalidReguser = true;
                errorPlace = 'Region Manager';
            }
           
           
          if(divName<>null && (!queryTerridmap.containsKey(divName) && divName != originalopp.Division_Name__c)){
                invalidDivterr = true;
                errorPlace = 'Division Name';
            }
            
            if(RegName<>null && (!queryTerridmap.containsKey(regName) && regName != originalopp.Region_Name__c)){
                invalidRegterr = true;
                errorPlace = 'Region Name';
            }
            
            if(psrName <>null && (!queryTerridmap.containsKey(psrName ) && psrName != originalopp.Primary_Reps_Territory__c)){
                invalidPsrterr = true;
                errorPlace = 'Primary Reps Territory';
            }
            
            if(TeamName<>null && (!queryTerridmap.containsKey(TeamName) && TeamName != originalopp.Team_Name__c)){
                invalidTeamterr = true;
                errorPlace = 'Team Name';
            }           
        
        if(invalidDivuser || invalidTeamuser || invalidPsruser || invalidReguser || invalidTeamterr || invalidPsrterr || invalidRegterr || invalidDivterr ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid '+errorPlace)); 
            return null;
        }    
        else{
            opp.Division_Manager__c = queryuseridmap.get(opp.Division_Manager__c);
            opp.Region_Manager__c= queryuseridmap.get(opp.Region_Manager__c);
            opp.Team_Manager__c = queryuseridmap.get(opp.Team_Manager__c );
            opp.Primary_Sales_Rep__c= queryuseridmap.get(opp.Primary_Sales_Rep__c);
            
            if(queryTerridmap.containsKey(opp.Division_Name__c)){
                opp.Division_Name__c = queryTerridmap.get(opp.Division_Name__c).name;
                opp.Division_Territory_Id__c = queryTerridmap.get(opp.Division_Name__c).id;
            }
            if(queryTerridmap.containsKey(opp.Region_Name__c)){
                opp.Region_Name__c = queryTerridmap.get(opp.Region_Name__c).name;
                opp.Region_Territory_Id__c= queryTerridmap.get(opp.Region_Name__c).id;
            }   
            if(queryTerridmap.containsKey(opp.Team_Name__c)){
                opp.Team_Name__c = queryTerridmap.get(opp.Team_Name__c).name;
                opp.Team_Territory_Id__c = queryTerridmap.get(opp.Team_Name__c).id;
            }   
            if(queryTerridmap.containsKey(opp.Primary_Reps_Territory__c)){
                opp.Primary_Reps_Territory__c = queryTerridmap.get(opp.Primary_Reps_Territory__c).name;
                opp.Primary_rep_Territory_Id__c= queryTerridmap.get(opp.Primary_Reps_Territory__c).id;
            }
            update opp;
        }
        
        return new PageReference('/'+opp.id);    
    }
    
}