/**********************************************************************************************************************************************
Description: Batch class to make update the ownership during the staff Transfer          
Created on:  29 September 2015
Created by:  Kavitha

Version 1:   Batch class utilized in StaffTransfer Controller to transfer Accounts, Contacts,Opoortunities, Goals__c and their associated Tasks
             Email functioanlity to send an email to the new owners and their managers with the list of the Records categorized by each SObject
Version 2:   Updated the Opportunites that are retrieved. To allow closed won opportunites only when there Insert_Date__c or Cycle_Start__c is greater
             than the effective date. Which is the current date.
Version 3:   Jagan: Fixed duplicate goals issue
************************************************************************************************************************************************/
global class StaffTransferBatchClass implements Database.Batchable<sObject> {

  global final String Query;

  public Map<String, String> sObjectOwnerIDMap = new Map<String, String>();
  public list<Account> accountToUpdate = new list<Account>();
  public list<Opportunity> opportunitiesToUpdate = new list<Opportunity>();
  public list<Contact> contactsToUpdate = new list<Contact>();
  public list<Account> prospectsToUpdate = new list<Account>();
 
  public list<Task> tasks = new list<Task>();
  public list<Staff_Transfer_Request__c> updateSTR = new list<Staff_Transfer_Request__c>();
  
  // constructor
  global StaffTransferBatchClass(string query) {
    this.query = query;
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    system.debug(query);
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext BC, List<sObject> scope){
        list<Goal__c> goalsToUpdate = new list<Goal__c>();      
        set<string> whatIds = new set<string>();
        map<string,string> whatIdOwnerMap = new map<string,string>();
        map<string,string> prospectOwnerMap = new map<string,string>();
        map<string,string> accountOwnerMap = new map<string,string>();
        map<string,string> goalOwnerMap = new map<string,string>();
        
        list<Contact> tempContacts = new list<Contact>();
        list<Opportunity> tempOppty = new list<Opportunity>();
        list<Goal__c> tempGoals = new list<Goal__c>();
        list<Task> tempTask = new list<Task>();
        
        list<AccountTeamMember> memberToDelete = new list<AccountTeamMember>();
        list<OpportunityTeamMember> salesMembersToDelete = new list<OpportunityTeamMember>();
        set<string> previousAccountTeamMember = new set<string>();
        set<string> previousSalesTeamMember = new set<string>();
        map<string,string> ownerMap = new map<string,string>();
        
        Set<Id> goalidset = new Set<Id>();
        
        //SEM: have a map of the new owner
        map<string, User> newOwnerRecord = new map<string, User>();
       
        for(sobject s : scope) {
           Staff_Transfer_Request__c req = (Staff_Transfer_Request__c)s;
            req.Is_Processed__c = True;
            updateSTR.add(req);
           if(req.Account__c != Null) {
                if(!req.Access_through_Account_Team__c) {
                    ownerMap.put(req.Account__c,req.New_Owner__c);
                    // remove the new owner from account teams
                    previousAccountTeamMember.add(req.New_Owner__c);
                }
                accountOwnerMap.put(req.Account__c,req.Current_Owner__c);
                
           }
           else if(req.Prospect__c!=Null) {
            if(!req.Access_through_Account_Team__c) {
                   ownerMap.put(req.Prospect__c,req.New_Owner__c);
                    // remove the new owner from account teams
                    previousAccountTeamMember.add(req.New_Owner__c);
                }                
                accountOwnerMap.put(req.Prospect__c,req.Current_Owner__c); //add Prospect records to AccountOwnerMap

           }
           else if(req.Goal__c != Null) {
            goalOwnerMap.put(req.Goal__c, req.Current_Owner__c);
            OwnerMap.put(req.Goal__c,req.New_Owner__c);
               System.debug('@@@@@KR:OwnerMap::'+OwnerMap.keyset());
                system.debug('@@@@KR:req::'+req);               
           }
        }
        // query for Account information
        System.debug('Account Owner Map: '+accountOwnerMap);
        if(accountOwnerMap.size() > 0) {//Seth: Update where condition for Opportunities
            for(Account acc : [SELECT id,OwnerId, Name, RecordType.Name, (SELECT id,OwnerId,WhatId, Subject FROM Tasks WHERE IsClosed = false AND OwnerId IN :accountOwnerMap.values()),
                               (SELECT id,OwnerId,AccountId, Name FROM Account.Contacts WHERE OwnerId IN :accountOwnerMap.values()),
                               (SELECT id,OwnerId,AccountId, Name FROM Account.Opportunities WHERE (isClosed = false AND OwnerId IN :accountOwnerMap.values()) OR (isClosed = true AND OwnerId IN :accountOwnerMap.values() AND (Insert_Date__c > :System.today() OR Cycle_Start_Date__c > :System.today()))),
                               (SELECT id,Assigned_To__c,Account__c, Name FROM Account.Goals__r WHERE Assigned_To__c IN :accountOwnerMap.values())
                                FROM Account WHERE id in :accountOwnerMap.keyset() LIMIT :Limits.getLimitQueryRows()]) {
                
                System.debug('Current Account id...'+acc.id);
                System.debug('Account Child: ' + acc.Opportunities);
                
                if(ownerMap.get(acc.Id) != Null) {
                    acc.OwnerId = ownerMap.get(acc.Id);
                    acc.Transfer_Effective_Date__c = System.today(); //KR Updated
                    
                    if(acc.RecordType.Name == 'Prospect')
                        prospectsToUpdate.add(acc);
                    else
                        accountToUpdate.add(acc);
                    if(sObjectOwnerIDMap.containsKey(acc.Id) != true){
                        sObjectOwnerIDMap.put(acc.Id, ownerMap.get(acc.Id));
                    }

                    // process related contacts
                    for(Contact con : acc.Contacts) {
                        system.debug('con:'+con);
                        if(accountOwnerMap.get(con.AccountId) != Null && con.OwnerId == accountOwnerMap.get(con.AccountId)) {
                            con.Transfer_Effective_Date__c = System.today(); //KR
                            tempContacts.add(con);
                            whatIds.add(con.Id);
                            whatIdOwnerMap.put(con.Id,ownerMap.get(acc.Id));
                        }
                    }
                    // process related Opportunities
                    for(Opportunity opp : acc.Opportunities) {
                        system.debug('opp:'+opp);
                        if(accountOwnerMap.get(opp.AccountId) != Null && opp.OwnerId == accountOwnerMap.get(opp.AccountId)) {
                            opp.Transfer_Effective_Date__c = System.today(); //KR
                            tempOppty.add(opp);
                            whatIds.add(opp.Id);
                            whatIdOwnerMap.put(opp.Id,ownerMap.get(acc.Id));
                        }
                    }
                    // process related Goals__c
                    
                    for(Goal__c g: acc.Goals__r){
                        system.debug('Goals associated to Accounts:'+g);
                        if(accountOwnerMap.containsKey(g.Account__c) && accountOwnerMap.get(g.Account__c) != Null){
                            if( g.Assigned_To__c == accountOwnerMap.get(g.Account__c)) {
                                g.Assigned_To__c = ownerMap.get(acc.Id);
                                g.Transfer_Effective_Date__c = System.today();
                                tempGoals.add(g);
                                whatIds.add(g.Id);
                                whatIdOwnerMap.put(g.Id,ownerMap.get(acc.Id));
                            }
                        }
                    }
                    // process related Tasks
                    for(Task tsk : acc.Tasks) {
                        if(accountOwnerMap.get(tsk.whatId) != Null && tsk.OwnerId == accountOwnerMap.get(tsk.whatId)) {
                            tsk.Transfer_Effective_Date__c = System.today();//KR
                            tempTask.add(tsk);
                        }
                    }
                    
                    System.debug('tempContacts:'+tempContacts);
                    System.debug('tempOppty:'+tempOppty);
                    System.debug('tempgoals:'+tempgoals);
                    // reassign open tasks
                    tasks.addAll((list<Task>)reAssignSObject(tempTask,ownerMap.get(acc.Id)));
                    
                    // reassign contacts
                    contactsToUpdate.addAll((list<Contact>)reAssignSObject(tempContacts,ownerMap.get(acc.Id)));
                    
                    // reassign open Opportunities                    
                    opportunitiesToUpdate.addAll((list<Opportunity>)reAssignSObject(tempOppty,ownerMap.get(acc.Id)));
                    
                    //reassign Goals
                    goalsToUpdate.addAll((list<Goal__c>)reAssignSObject(tempGoals,ownerMap.get(acc.Id)));
                    
                    
                    for(Goal__c g:goalsToUpdate)
                        goalidset.add(g.id);
                }
                //clean out temp files
                tempContacts.clear();
                tempOppty.clear();
                tempTask.clear();
                tempGoals.clear();
            }
        }
        // reassign the tasks associated to contact and opportunity
        for(Task tsk : getTasksAssociated(whatIds)) {
            tsk.OwnerId = whatIdOwnerMap.get(tsk.whatId);
            tsk.Transfer_Effective_Date__c = System.today(); //KR
            tasks.add(tsk);
        }

       
       System.debug('ids in goal id set... '+goalidset);
       System.debug('...Goals udpate list before for loop... '+goalsToUpdate);
       //query for goal information
       if(goalOwnerMap.size()>0) {
            for(Goal__c g: [SELECT id,Name, Assigned_To__c, (SELECT OwnerId,WhatId, Transfer_Effective_Date__c FROm Tasks where isClosed = false AND OwnerId IN :goalOwnerMap.values()) FROM Goal__c WHERE id IN: goalOwnerMap.keyset()]) {
                if(!goalidset.contains(g.id)){
                    goalidset.add(g.id);
                    system.debug('Goal id...is...'+g.id);
                    if(ownerMap.get(g.Id) != ''){                       
                            g.Assigned_To__c = ownerMap.get(g.Id);
                            g.Transfer_Effective_Date__c = System.today();                                           
                            goalsToUpdate.add(g);
                            if(sObjectOwnerIDMap.containsKey(g.Id) != true){
                                sObjectOwnerIDMap.put(g.Id, ownerMap.get(g.Id));
                                System.debug('@@@KR:goalsToUpdate'+goalsToUpdate);
                            }
                        // process related Goal Tasks
                        tempTask.clear();
                        for(Task tsk : g.Tasks) {
                            if(goalOwnerMap.get(tsk.whatId) != Null && tsk.OwnerId == goalOwnerMap.get(tsk.whatId)) {
                                tsk.Transfer_Effective_Date__c = system.today(); 
                                tempTask.add(tsk);
                            }
                        }
                        tasks.addAll((list<Task>)reAssignSObject(g.Tasks,goalOwnerMap.get(g.Id)));
                    }
            
                }
            }
       }
       
       System.debug('...Goals udpate list after for loop... '+goalsToUpdate);
       OpportunityTriggerHelper.AccountValidationCheckfromBatch = TRUE;
        //update isProcessed=TRUE on Staff Transfer Requests
        if(updateSTR.size()>0)
            update updateSTR;
        //update prospects
        if(prospectsToUpdate.size() > 0)
            update prospectsToUpdate;
             System.debug('prospectsToUpdate:'+prospectsToUpdate); 
        // update accounts
        if(accountToUpdate.size() > 0)
            update accountToUpdate;
            System.debug('accountToUpdate:'+accountToUpdate); 
        if(contactsToUpdate.size() > 0)        
            update contactsToUpdate;
            System.debug('contactsToUpdate:'+contactsToUpdate);
        if(opportunitiesToUpdate.size() > 0)
            update opportunitiesToUpdate;
        if(goalsToUpdate.size()> 0){
            for(Goal__c g:goalsToUpdate)
                g.BatchApexContext__c = TRUE;
            update goalsToUpdate;
            System.debug('goalsToUpdate:'+goalsToUpdate); 
        }
        if(tasks.size() > 0)
            update tasks;
        
        //Call email method
        SendEmail(accountToUpdate, opportunitiesToUpdate, contactsToUpdate, prospectsToUpdate, goalsToUpdate, tasks, sObjectOwnerIDMap);
        
  }

    private void SendEmail(List<Account> accountToUpdate, List<Opportunity> opportunitiesToUpdate, List<Contact> contactsToUpdate, List<Account> prospectsToUpdate, List<Goal__c> goalsToUpdate, List<Task> tasks, Map<String, String> sObjectOwnerIDMap){
        Map<String, List<String>> usertoAcctMap = new Map<String, List<String>>();
        Map<String, List<String>> usertoConMap = new Map<String, List<String>>();
        Map<String, List<String>> usertoOppMap = new Map<String, List<String>>();
        Map<String, List<String>> usertoProspectMap = new Map<String, List<String>>(); 
        Map<String, List<String>> usertoGoalMap = new Map<String, List<String>>(); 
        Map<String, List<String>> usertoTaskMap = new Map<String, List<String>>();

        //one batch of emails to not go over email send limits
        Messaging.SingleEmailMessage[] theEmails = new List<Messaging.SingleEmailMessage>();
        
        //Manager Name and Manager Email are queried
        System.debug('Execute Users: ' + sObjectOwnerIDMap);
        System.debug('Accounts: ' + accountToUpdate);
        for(User u: [select Id, Name, Email, Manager.Name, Manager.Email from user where Id IN: sObjectOwnerIDMap.values()]){
            System.debug('User ID: ' + u.Id);
            for(Account a: accountToUpdate){
                System.debug('Account ID: ' + a.Id);

                if(usertoAcctMap.containsKey(u.Id))
                    usertoAcctMap.get(u.Id).add(a.Name);
                else                
                    usertoAcctMap.put(u.Id, new List<string>{a.Name});
            }

        
            for(Contact c: contactsToUpdate){

                if(usertoConMap.containsKey(u.Id))
                    usertoConMap.get(u.Id).add(c.Name);
                else                
                    usertoConMap.put(u.Id, new List<string>{c.Name});
            }
        
            for(Opportunity opp: opportunitiesToUpdate){

                if(usertoOppMap.containsKey(u.Id))
                    usertoOppMap.get(u.Id).add(opp.Name);
                else                
                    usertoOppMap.put(u.Id, new List<string>{opp.Name});
            }
        
            for(Account p: prospectsToUpdate){

                if(usertoProspectMap.containsKey(u.Id))
                    usertoProspectMap.get(u.Id).add(p.Name);
                else                
                    usertoProspectMap.put(u.Id, new List<string>{p.Name});
            }
            
            for(Goal__c g: goalsToUpdate) {
                if(usertoGoalMap.containsKey(u.Id))
                    userToGoalMap.get(u.Id).add(g.Name);
                else
                    userToGoalMap.put(u.Id, new List<String>{g.Name});
            }
                

            for(Task t: Tasks){

                if(usertoTaskMap.containsKey(u.Id))
                    usertoTaskMap.get(u.Id).add(t.Subject);
                else                
                    usertoTaskMap.put(u.Id, new List<string>{t.Subject});
            }

            //create the email
            String[] toAddress = new List<String>();
            toAddress.add(u.Email);
            if(u.Manager.Email != null){
                toAddress.add(u.Manager.Email);
            }
            String subjectTag = 'Staff Transfer Summary for ' + u.Name;

            String msgBody = 'The following records have been transferred to ' + u.Name + ': <br><br>';

            //the user has an account that is changed
            if(usertoAcctMap.containsKey(u.Id)){
                msgBody = msgBody + '<br>Accounts: <br>';
                //loop over list of strings
                if(usertoAcctMap.containsKey(u.Id)){
                    msgBody = msgBody + '<ul>';
                    for(String accountName: usertoAcctMap.get(u.Id)){
                        msgBody = msgBody + '<li>  ' + accountName +'</li>';
                    }
                    msgBody = msgBody + '</ul>';
                }
            }

            //the user has an account that is changed
            if(usertoConMap.containsKey(u.Id)){
                //loop over list of strings
                msgBody = msgBody + '<br>Contacts: <br>';
                if(usertoConMap.containsKey(u.Id)){
                    msgBody = msgBody + '<ul>';
                    for(String contactName: usertoConMap.get(u.Id)){
                        msgBody = msgBody + '<li>  ' + contactName +'</li>';
                    }
                    msgBody = msgBody + '</ul>';
                }
            }
            
            //the user has a Prospect that is changed
            if(usertoProspectMap.containsKey(u.Id)){
                msgBody = msgBody + '<br>Prospects: <br>';
                //loop over list of strings
                if(usertoProspectMap.containsKey(u.Id)){
                    msgBody = msgBody + '<ul>';
                    for(String prospectName: usertoProspectMap.get(u.Id)){
                        msgBody = msgBody + '<li>  ' + prospectName +'</li>';
                    }
                    msgBody = msgBody + '</ul>';
                }
            }
            
            //the user has an Opportunity that is changed
            if(usertoOppMap.containsKey(u.Id)){
                msgBody = msgBody + '<br>Opportunities: <br>';
                //loop over list of strings
                if(usertoOppMap.containsKey(u.Id)){
                    msgBody = msgBody + '<ul>';
                    for(String oppName: usertoOppMap.get(u.Id)){
                        msgBody = msgBody + '<li>  ' + oppName +'</li>';
                    }
                     msgBody = msgBody + '</ul>';
                }
            }
            //the user has a Goal that is changed
            if(userToGoalMap.containsKey(u.Id)) {
                msgBody = msgBody + '<br>Goals: <br>';
                //loop over list of strings
                if(userToGoalMap.containsKey(u.Id)){
                    msgBody = msgBody + '<ul>';
                    for(String goalName: userToGoalMap.get(u.Id)){
                        msgBody = msgBody + '<li>  ' + goalName +'</li>';
                    }
                     msgBody = msgBody + '</ul>';
                }
            
            }
            
            //the user has a Task that is changed
            if(usertoTaskMap.containsKey(u.Id)){
                msgBody = msgBody + '<br>Task: <br>';
                //loop over list of strings
                if(usertoTaskMap.containsKey(u.Id)){
                    msgBody = msgBody + '<ul>';
                    for(String taskName: usertoTaskMap.get(u.Id)){
                        msgBody = msgBody + '<li>  ' + taskName +'</li>';
                    }
                    msgBody = msgBody + '</ul>';
                }
            }

            msgBody = msgBody + '<br><br>Thank you';

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddress);
            mail.setReplyTo('no-reply@nam.com');
            mail.setSubject(subjectTag);
            mail.setHtmlBody(msgBody);

            //add email to list of all emails
            theEmails.add(mail);
            System.debug('Message Body: ' + msgBody);

        }

        System.debug('Emails that were created: ' + theEmails);
        if(theEmails.size() > 0){
            Messaging.sendEmail(theEmails);
            //System.debug('Emails sent status: ' + results);
        }
    }
    /********************************************************************************************************
    Utility Method to reassign Tasks 
    *********************************************************************************************************/
    private list<SObject> reAssignSObject(list<SObject> recs,string newOwner) {
        list<SObject> reassignedRecs = new list<SObject>();
        
        for(SObject obj : recs) {            
            obj.put(StaffTransferCntl.OwnerId,newOwner);
            if(sObjectOwnerIDMap.containsKey(obj.Id) != true){
                sObjectOwnerIDMap.put(obj.Id, newOwner);
            }
            reassignedRecs.add(obj);
        }
        return reassignedRecs;
    }
    /********************************************************************************************************
    Method to retrieve Tasks associated to an SObject
    *********************************************************************************************************/
    private list<Task> getTasksAssociated(set<string> whatIds) {
        list<Task> tsk = new list<Task>();
        if(whatIds.size() > 0)
            tsk = [SELECT OwnerId,WhatId FROM Task WHERE isClosed = false AND WhatId in :whatIds];
        return tsk;
    }
    /********************************************************************************************************
    Method to Create Account Team Members
    *********************************************************************************************************/
    public static AccountTeamMember createAccountTeamMember(Id acctId,Id ownrId) {
        AccountTeamMember mem = new AccountTeamMember();
        mem.AccountId = acctId;
        mem.UserId = ownrId;
        return mem;
    }
    /********************************************************************************************************
    Method to Create Opportunity Team Members
    *********************************************************************************************************/
    public static OpportunityTeamMember createOpportunityTeamMember(Id opptId,Id ownrId) {
        OpportunityTeamMember mem = new OpportunityTeamMember();
        mem.OpportunityId = opptId;
        mem.UserId = ownrId;
        return mem;
    }
    
  
    global void finish(Database.BatchableContext BC){
  
    
    }
  
 
}