@isTest

public class QuoteLineItemTriggerHelperTest{

    static TestMethod void QuoteLineItemTriggerHelperTestMethod(){
    
          //Create a Corp Account record
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.title = 'Account Director (AD)';
        //secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        //insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            //div1.Finance_Verified__c = true;
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = corp1.Id);
            insert accTeamMember;
            
            QuoteAndProductLineItemFieldMap__c fldmap = new QuoteAndProductLineItemFieldMap__c();
            fldmap.name = 'Component__c';
            fldmap.Product_line_item_field_name__c = 'Component__c';
            Insert fldmap;
         
            QuoteAndProductLineItemFieldMap__c fldmap2 = new QuoteAndProductLineItemFieldMap__c();
            fldmap2.name = 'Charge_Type__c';
            fldmap2.Product_line_item_field_name__c = 'Charge_Type__c';
            Insert fldmap2;
            
            
            Cycle__c c = new Cycle__c();
            c.name = 'Test Cycle';
            c.Begin_Date__c = system.today();
            c.Week__c = 1;
            c.End_Date__c = system.today()+30;
            insert c;
            
              Cycle__c c2 = new Cycle__c();
            c2.name = 'Test Cycle';
            c2.Begin_Date__c = system.today();
            c2.Week__c = 2;
            c2.End_Date__c = system.today()+30;
            insert c2;
            TitlesToUpdateOppOwner__c ti = new TitlesToUpdateOppOwner__c();
            ti.name = 'Account Director (AD)';
            insert ti;
            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
            testOppty.CurrencyISOCode = 'USD';
            testOppty.instore_cycle__c= c2.id;
            testOppty.StageName = 'Client Engagement';
            insert testOppty;
            
            
            Product_Line__c FSIProductLine = TestDataUtils.createProductLine('FSI');
            insert FSIProductLine;
            
            Product_Line__c InsProductLine = TestDataUtils.createProductLine('InStore');
            insert InsProductLine;
            
            Product2 prod = TestDataUtils.createProduct2('Test Prod');
            insert prod;
            
            Pricebook2 pbk1 = new Pricebook2 (Name='Test Pricebook Entry 1',Description='Test Pricebook Entry 1', isActive=true);
            insert pbk1;
            
            //CREATE PRICEBOOK ENTRY RECORD
            Id stdpricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = stdpricebookId, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
            insert standardPrice;
              
            PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
            priceEntry.pricebook2id = pbk1.id;
            priceEntry.usestandardprice = false;
            priceEntry.UnitPrice=50;
            insert priceEntry;
            
            Charge_Type__c ct = new Charge_Type__c();
            ct.Commissionable__c = true;
            ct.Line_of_Business__c = FSIProductLine.id;
            ct.Charge_Type_Categorty__c = 'Space';
            insert ct;
            Charge_Type__c ct2 = new Charge_Type__c();
            ct2.Commissionable__c = true;
            ct2.Line_of_Business__c = InsProductLine.id;
            ct2.Charge_Type_Categorty__c = 'Production';
            insert ct2;
            OpportunityTeamMember ot = new OpportunityTeamMember();
            ot.userid = user1.id;
            ot.OpportunityId = testOppty.id;
            ot.Commission_Percentage__c = 25;
            insert ot;
            
           
            OpportunityLineItem oli = new OpportunityLineItem();
            oli.TotalPrice =  1200;
            oli.Charge_Type__c  = ct.id;
            oli.Commissionable__c = true;
            oli.pricebookentryid = standardPrice.id;
            oli.opportunityid = testOppty.id;
            oli.Quantity = 1;
            insert oli;
            
            Quote q= new Quote();
            q.opportunityid = testoppty.id;
            q.name = 'Test method quote';
            q.Pricebook2Id = pbk1.id;
            insert q;
            
            QuotelineItem qli = new quotelineitem();
            qli.quoteid = q.id;
            qli.pricebookentryid = priceEntry.id;
            qli.Quantity = 1;
            qli.UnitPrice = 1200;
            insert qli;

            Quote testQuote = [Select Id, Name From Quote Where Id =:q.Id];
            System.assertEquals('Test method quote', testQuote.Name);
          }

    }
    
}