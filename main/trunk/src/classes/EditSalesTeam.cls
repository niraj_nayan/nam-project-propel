/**********************************************************************************************************************************************
Description: Controller for EditSalesTeam page          
Created on:  25 August 2015
Created by:  Seth

Version 1:   Created Controller for editing sales team
***********************************************************************************************************************************************/
public with sharing class EditSalesTeam {

    public String opprtyTeamMemberId = ApexPages.currentpage().getparameters().get('teamMember');
    public OpportunityTeamMember editTeamMember {get;set;}
    public User oppOwner {get;set;}
    public OpportunityShare memberShareRule {get;set;}
    public List<SelectOption> optns{get;set;}
    public Boolean sameUser {get;set;}
    public Boolean editUser {get;set;}
    public Boolean isOwner {get;set;}

    public EditSalesTeam() {

        System.debug('Team Member ID: ' + opprtyTeamMemberId);
        sameUser = false;
        isOwner = false;
        editUser = editAccess();
        sameUser = viewAccess();

        editTeamMember = getTeamMember(opprtyTeamMemberId);
        memberShareRule = editShareRule(opprtyTeamMemberId);
        if(memberShareRule <>null && memberShareRule.RowCause == 'Owner'){
            isOwner = true;
        }

        optns = getOpptyAccessOptions();

        if(UserInfo.getUserId() == editTeamMember.UserId){
            sameUser = true;
        }

        oppOwner = getOppOwner();
        
    }

    public User getOppOwner(){
        //select Opportunity
        Id oppOwnerId = [Select Id, Name, OwnerId
                            FROM Opportunity
                            WHERE Id =:editTeamMember.OpportunityId].OwnerId;

        User tempOwner = [Select Id, isActive, Name, Profile.Name
                            FROM User
                            WHERE Id =:oppOwnerId];

        return tempOwner;
    }


    public List<SelectOption> getOpptyAccessOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = OpportunityTeamMember.OpportunityAccessLevel.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple){
            if((p.getValue() != 'None') && (p.getValue() != 'All')){ 
                if(p.getValue() == 'Read'){
                    options.add(new SelectOption(p.getValue(), p.getValue()));
                }
                else{
                    options.add(new SelectOption(p.getValue(), p.getLabel()));
                }
            }
        }        
        return options;
    }

    public Boolean editAccess(){
        Set<String> profilesAccess = new Set<String>();
        User currentUser=[Select Id, Name, Profile.Name From User Where Id=:UserInfo.getUserId()];
        for(Sales_Teams_Profiles__c profile: Sales_Teams_Profiles__c.getAll().Values()){
            profilesAccess.add(profile.Name);
        }
        if(profilesAccess.contains(currentUser.Profile.Name)){
            return true;
        }
        else{
            return false;
        }
    }

    public Boolean viewAccess(){
        Set<String> profilesAccess = new Set<String>();
        User currentUser=[Select Id, Name, Profile.Name From User Where Id=:UserInfo.getUserId()];
        for(Sales_Team_View_Profile__c profile: Sales_Team_View_Profile__c.getAll().Values()){
            profilesAccess.add(profile.Name);
        }
        if(profilesAccess.contains(currentUser.Profile.Name)){
            return true;
        }
        else{
            return false;
        }
    }

    public OpportunityTeamMember getTeamMember(Id opprtyTeamMemberId){
        list<OpportunityTeamMember> otm = new list<OpportunityTeamMember>();
        otm = [Select Id, Opportunity.Name, OpportunityId, UserId, User.Name, User.isActive, TeamMemberRole, OpportunityAccessLevel, Commission_Percentage__c, Commission_Amount__c 
                    From OpportunityTeamMember
                    Where Id =:opprtyTeamMemberId];
        if(otm.size()>0)
            return otm[0];
        else
            return null;
    }

    public OpportunityShare editShareRule(Id opprtyTeamMemberId){
        List<OpportunityShare> osh = new List<OpportunityShare>();
        osh = [Select Id, OpportunityAccessLevel, RowCause, UserOrGroupId, OpportunityId From OpportunityShare
                                                                Where OpportunityId =:editTeamMember.OpportunityId AND UserOrGroupId =:editTeamMember.UserId AND (RowCause = 'Team' OR RowCause = 'Owner') limit 1];
        if(osh.size()>0)
        return osh[0];
        else
        return null;
    }

    public PageReference saveButton(){
        if(editTeamMember.Commission_Percentage__c < 0){
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, Label.SalesTeamEditError);
            ApexPages.addmessage(myMsg2);
            return null;
        }

        if(editTeamMember.User.isActive == FALSE){
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, Label.EditSalesTeamInactiveTeamMember);
            ApexPages.addmessage(myMsg2);
            return null;
        }

        if(oppOwner.isActive == FALSE){
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, Label.EditSalesTeamInactiveOwner);
            ApexPages.addmessage(myMsg2);
            return null;
        }


        if((editTeamMember.TeamMemberRole != null)){
            insert editTeamMember;
            
            if(memberShareRule.RowCause =='Team'){//the member is the not owner of the opportunity
                update memberShareRule;
            }
            PageReference opptyPage = new PageReference('/' + editTeamMember.OpportunityId);
            return opptyPage;
        }
        return null;
    }

    public PageReference cancelButton(){
        PageReference opptyPage = new PageReference('/' + editTeamMember.OpportunityId);
        return opptyPage;
    }
}