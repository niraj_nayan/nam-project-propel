@isTest
private class OppAcctRollUpRevenueSchedularTest {

	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	
	@isTest static void testOppAcctRollUpSchedular() {
		Test.startTest();

		//schedule the job
		String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new OpportunityAccountRollUpRevenueSchedular());

		//Get information from the CronTrigger API object
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime From CronTrigger Where Id =:jobId];

		//Verify the experssions are the same
		System.assertEquals(CRON_EXP, ct.CronExpression);

		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'tLastName';
        insert user1;

        User user2 = TestDataUtils.createUser('Test Last Name2', 'no-reply2@email.com', '12312312342');
        user2.FederationIdentifier = 'tLastName2';
        insert user2;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'Integration' Limit 1].Id;
        secondUser.FederationIdentifier = 'sUser';
        insert secondUser;


        Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
        insert corp1;

        //Create Division Account record
        Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
        //div1.Finance_Verified__c = true;
        insert div1;

        Account div2 = TestDataUtils.createDivAccount('nestle Div2', corp1.Id);
        //div1.Finance_Verified__c = true;
        insert div2;

        ///CREATE ACCOUNT SALES TEAM MEMBERS
        //AccountTeamMember accTeamMember = new AccountTeamMember(
        //    UserId = user1.Id,
        //    AccountId = corp1.Id);
        //insert accTeamMember;

        //AccountTeamMember accTeamMemberDiv = new AccountTeamMember(
        //    UserId = secondUser.Id,
        //    AccountId = div1.Id);
        //insert accTeamMemberDiv;

        //AccountTeamMember accTeamMemberDiv2 = new AccountTeamMember(
        //    UserId = user2.Id,
        //    TeamMemberRole = 'Account Director',
        //    AccountId = div1.Id);
        //insert accTeamMemberDiv2;

        Product_Line__c testProductLine = TestDataUtils.createProductLine('FSI');
        insert testProductLine;

        Period currentFiscalYear = [SELECT FiscalYearSettings.Name, Type, StartDate, EndDate  
                                FROM Period 
                                WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY];

        Calendar__c testCal = new Calendar__c();
        testCal.Fiscal_Year__c = currentFiscalYear.FiscalYearSettings.Name;
        testCal.Calendar_Date__c = System.today();
        testCal.Product_Line__c = testProductLine.Id;
        insert testCal;

        //System.runAs(secondUser){

            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
            testOppty.CurrencyISOCode = 'USD';
            testOppty.Amount = 100;
            testOppty.NEP__c = 10;
            testOppty.Insert_Date__c = System.today();
            testOppty.Calendar_Lookup__c = testCal.Id;
            insert testOppty;
            testOppty.Amount = 150;
            testOppty.Nep__c = 5;

            update testOppty;

            Opportunity testOppty2 = TestDataUtils.createOppty('Test Oppty2', 'Contract', div2.Id, 'FSI');
            testOppty2.CurrencyISOCode = 'USD';
            testOppty2.Amount = 100;
            insert testOppty2;

		//}

		Test.stopTest();

        //System Assert to check if the Roll Up Amount on Division account
        Account testAccount = [Select Id, Name, Roll_Up_Revenue__c FROM Account Where Id =:div1.Id];
        System.debug('Test Account: ' + testAccount);
        System.assertEquals(150, testAccount.Roll_Up_Revenue__c);

	}
	
}