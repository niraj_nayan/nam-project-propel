public class ParentOpportunityCreator {

    Static boolean createParentOpportunitycheck = FALSE;
    Static boolean updateParentOpportunitycheck = FALSE;
    public static void createParentOpportunity(List<Opportunity> oppors) {
    
        if(!createParentOpportunitycheck){
            createParentOpportunitycheck = TRUE;
            List<Id> accountIds = new List<Id>();
            List<Opportunity> parentOppyList = new List<Opportunity>();
            Map<String, Opportunity> parentChildIdMap = new Map<String, Opportunity>();
            Map<Id, Id> oppRecordTypeMapID = new Map<Id, Id>();
            //Map<Id, String> recordTypeIdNameMap = new Map<Id, String>();
    
            //Need to get the Opportunity ID and RecordType ID, then query for RecordType from the ID
            for(Opportunity oppor: oppors){
                oppRecordTypeMapID.put(oppor.Id, oppor.RecordTypeId);
                accountIds.add(oppor.AccountId);
            }
            //Get accounts of opps for account currency correction
            Map<Id, Account> accountsById = new Map<Id, Account>();
            accountsById.putAll([SELECT Id, CurrencyIsoCode FROM Account WHERE Id IN :accountIds]);

            for(Opportunity oppor : oppors){
                oppor.CurrencyIsoCode = accountsById.get(oppor.AccountId).CurrencyIsoCode;
            }
            //Query for all of the record types and create a map of record type Id and record Type name
            //Jagan: Commented the below code as we do not need query to get record types
             
            Map<Id,Schema.RecordTypeInfo> rtMapByID = Schema.Sobjecttype.Opportunity.getRecordTypeInfosById();

            //List<Opportunity> oppsToUpdateParents = new List<Opportunity>();

            for(Opportunity oppor : oppors) {
                //System.debug('Oppor Record Type: ' +recordTypeIdNameMap.get(oppor.RecordTypeId));
                if(oppor.Legacy_Source__c == null && rtMapByID.get(oppor.RecordTypeId).getName() == Label.OppRecordTypeInStore && !(oppor.CurrencyIsoCode == 'CAD' && oppor.Type == 'Standard') && String.isBlank(oppor.Parent_Opportunity__c)) {
                    Opportunity parentOpportunity = new Opportunity();
                    parentOpportunity = oppor.clone();
                    //Jagan: source key should not be cloned from child.
                    parentOpportunity.source_key__c = null;
                    parentOpportunity.Id = null;
                    parentOpportunity.Name = parentOpportunity.Name + '-Parent';
                    parentOppyList.add(parentOpportunity);
                    //parentChildIdMap.put(oppor, parentOpportunity);
                    //Insert parentOpportunity;
                    //oppor.Parent_Opportunity__c = parentOpportunity.Id;
                    System.debug('The parent opportunity is '+oppor.Parent_Opportunity__c);
                }
                //else if(String.isNotBlank(oppor.Parent_Opportunity__c)){
                //    oppsToUpdateParents.add(oppor);
                //}
            }
    
            if(parentOppyList.size() > 0){
                System.debug('Size is greater than 0: ' + parentOppyList.size());
                System.debug('');
                System.debug('Parent Opportunities: ' + parentOppyList);
                System.debug('Parent Opportunities: ' + parentOppyList.size());
                System.debug('Opportunity List: ' + oppors[0].Name);
                //System.debug('Map: ' + parentChildIdMap.get(oppors[0]).Name + ' &ID ' +parentChildIdMap.get(oppors[0]));
    
                try{
                    insert parentOppyList;
    
                    for(Opportunity parentOppy: parentOppyList){
                        String testTrim = null;
                        testTrim = parentOppy.Name;
                        testTrim = testTrim.removeEnd('-Parent');
                        System.debug('Trimmed String: ' + testTrim);
    
                        parentChildIdMap.put(testTrim, parentOppy);
                    }
    
                    for(Opportunity oppor: oppors){
                        if(parentChildIdMap.containsKey(oppor.Name) == TRUE){
                            System.debug('Parent Opp ID: ' + parentChildIdMap.get(oppor.Name).Id);
                            oppor.Parent_Opportunity__c = parentChildIdMap.get(oppor.Name).Id;
                        }
    
                        System.debug('The parent opportunity is '+oppor.Parent_Opportunity__c);
                    }
                }
                catch(DmlException e){
                    System.debug('Error Message: ' + e);
                    for(Opportunity oppty: oppors){
                        if(rtMapByID.get(oppty.RecordTypeId).getName() == Label.OppRecordTypeInStore){
                            oppty.addError(e.getDmlMessage(0));
                        }
                    }
                }
            }
            //if(!oppsToUpdateParents.isEmpty()){
            //    updateParentOpportunity(oppsToUpdateParents);
            //}
        }
    }

    //Update parent opportunity fields for opportunities that have an existing opp selected as a parent; currently unused
    //identical to createParentOpportunity in function except using a currently existing parent opp instead of making new one
    public static void updateParentOpportunity(List<Opportunity> oppors){
        
        if(!updateParentOpportunitycheck){
            updateParentOpportunitycheck = TRUE;

            List<Id> accountIds = new List<Id>();
            List<Opportunity> parentOpptyList = new List<Opportunity>();
            List<Id> parentOpptyIdList = new List<Id>();
            Map<String, Opportunity> parentOppChildNameMap = new Map<String, Opportunity>();
            Map<Id, Opportunity> parentOppMap = new Map<Id, Opportunity>();
            Map<Id, OpportunityLineItem> childLineItems = new Map<Id, OpportunityLineItem>();
            Map<String, Id> parentIdChildNameMap = new Map<String, Id>();
            Map<Id, Id> oppRecordTypeMapID = new Map<Id, Id>();
    
            for(Opportunity oppor: oppors){
                oppRecordTypeMapID.put(oppor.Id, oppor.RecordTypeId);
                accountIds.add(oppor.AccountId);
            }
            //Get accounts of opps for account currency correction
            Map<Id, Account> accountsById = new Map<Id, Account>();
            accountsById.putAll([SELECT Id, CurrencyIsoCode FROM Account WHERE Id IN :accountIds]);

            for(Opportunity oppor : oppors){
                oppor.CurrencyIsoCode = accountsById.get(oppor.AccountId).CurrencyIsoCode;
            }
            
            Map<Id,Schema.RecordTypeInfo> rtMapByID = Schema.Sobjecttype.Opportunity.getRecordTypeInfosById();
            
            for(Opportunity oppor : oppors) {
                if(oppor.Legacy_Source__c == null && rtMapByID.get(oppor.RecordTypeId).getName() == Label.OppRecordTypeInStore && !(oppor.CurrencyIsoCode == 'CAD' && oppor.Type == 'Standard') && !String.isBlank(oppor.Parent_Opportunity__c)) {
                    parentIdChildNameMap.put(oppor.Name, oppor.Parent_Opportunity__c);
                }
            }
            System.debug('UPDATE PARENT OPP' + parentIdChildNameMap);
            parentOpptyIdList = parentIdChildNameMap.values();
            parentOppMap.putAll([SELECT Id, Name, StageName, AccountId, CloseDate, Type, Primary_Product__c, 
                                                            InStore_Cycle__c, Estimated_Store_Count__c, Estimated_Average_CPS__c, Estimated_Production_Charges__c
                                                        FROM Opportunity
                                                        WHERE Id IN :parentOpptyIdList]);
            Opportunity parentOpp = new Opportunity();
            for(Opportunity childOpp: oppors){
                parentOpp = parentOppMap.get(parentIdChildNameMap.get(childOpp.Name));
                if(parentOpp != null){
                    parentOpp.StageName = childOpp.StageName;
                    parentOpp.AccountId = childOpp.AccountId;
                    parentOpp.CloseDate = childOpp.CloseDate;
                    parentOpp.Type = childOpp.Type;
                    parentOpp.Primary_Product__c = childOpp.Primary_Product__c;
                    parentOpp.InStore_Cycle__c = childOpp.InStore_Cycle__c;
                    parentOpp.Estimated_Average_CPS__c = childOpp.Estimated_Average_CPS__c;
                    parentOpp.Estimated_Store_Count__c = childOpp.Estimated_Store_Count__c;
                    parentOpp.Estimated_Production_Charges__c = childOpp.Estimated_Production_Charges__c;
                    parentOpptyList.add(parentOpp);
                }
            }
            System.debug(parentOpptyList);
            try{
                set<Opportunity> dupremoval = new Set<Opportunity>();
                dupremoval.addAll(parentOpptyList);
                parentOpptyList.clear();
                parentOpptyList.addAll(dupremoval);
                update parentOpptyList;
            }
            catch (DMLException e){
                System.debug(e);
            }
        }
    }
}