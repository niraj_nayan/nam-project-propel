/**********************************************************************************************************************************************
Description: Helper class for user trigger
             
Created on:  23nd October 2015
Created by:  Jagan
Version 1:   Created helper class to update territory fields in user when the user is assigned to territory.
version 2:   Hierarchy assumed earlier was wrong. Hence, updated class to restructure according to correct hierarchy model
version 3:   Requirement is changed and now the trigger is on UserTerritory2Assignment object as there is no concept of Primary Territory now
Version 4:   Added new methoed to add users to Account Team when a user is assigned from territory
Version 5:   Added new methoed to remove users from Account Team when a user is removed from territory
*********************************************************************************************************************************************/

public class userAssociationTriggerHelper{
    
    public static List<UserTerritory2Association> triggernew;
    public static void assignUsertoTerritory(List<UserTerritory2Association> lstUser){//, Map<id,UserTerritory2Association> oldusermap){
    
        List<user> lstusertoupdate = new List<user>();
        Set<Id> useridset = new Set<Id>();
        triggernew = lstUser;
        for(UserTerritory2Association u:lstUser){
            useridset.add(u.userid);
        }
        
        lstusertoupdate = updateusers(useridset);
        system.debug('User list to update...'+lstusertoupdate);
        if(lstusertoupdate.size()>0){
                //update lstusertoupdate;
                Database.SaveResult[] lsr = database.update(lstusertoupdate,false);
                for(Database.SaveResult sr : lsr)
                    if (!sr.isSuccess()) 
                        system.debug('Error occured during update....'+sr);
        }
    }

    
    public static List<user> updateUsers(Set<id> useridset){
        
        List<user> lstusertoupdate = new List<user>();
        Set<Id> terridset = new Set<Id>();
        Map<id,set<UserTerritory2Association>> allterrusermap = new Map<id,set<UserTerritory2Association>>();
        Set<Id> allprimSalesRepset = new Set<Id>();
        Map<Id,Integer> userPrimaryCount = new Map<Id,Integer>();
        Map<Id,Integer> TerrPrimaryCount = new Map<Id,Integer>();
        if(useridset.size()>0) {
            
            //Query all userTerritory Associations in the system
            List<UserTerritory2Association> userTerritoryList = [SELECT Id, IsActive, UserId, user.Name, user.Username, user.IsActive, Territory2Id,Territory2.Name,RoleInTerritory2 FROM UserTerritory2Association];
            
            //Based on the above query, frame map to have territory id and respective users of the territory
            for(UserTerritory2Association terr : userTerritoryList) {
                
                terridset.add(terr.Territory2Id);
                if(allterrusermap.containsKey(terr.Territory2Id))
                    allterrusermap.get(terr.Territory2Id).add(terr);
                else
                    allterrusermap.put(terr.Territory2Id,new set<UserTerritory2Association>{terr});
                
                if(terr.RoleInTerritory2<>null && terr.RoleInTerritory2.equalsIgnoreCase(Label.userAssociationTriggerPrimaryRole)){
                    allprimSalesRepset.add(terr.userid);
                    
                }
                //Count the number of Primary roles per user and per territory
                if(terr.RoleInTerritory2<>null && (terr.RoleInTerritory2.equalsIgnoreCase(Label.userAssociationTriggerPrimaryRole) || terr.RoleInTerritory2.equalsIgnoreCase(Label.PrimarySplitRole))){
                    
                    if(terr.RoleInTerritory2.equalsIgnoreCase(Label.userAssociationTriggerPrimaryRole)){
                        if(userPrimaryCount.containsKey(terr.userId)){
                            Integer tmp = userPrimaryCount.get(terr.userId);
                            tmp++;userPrimaryCount.put(terr.userId,tmp);
                        }
                        else
                            userPrimaryCount.put(terr.userId,1);
                    }
                    if(TerrPrimaryCount.containsKey(terr.Territory2Id)){
                        Integer tmp = TerrPrimaryCount.get(terr.Territory2Id);
                        tmp++;TerrPrimaryCount.put(terr.Territory2Id,tmp);
                    }
                    else
                        TerrPrimaryCount.put(terr.Territory2Id,1);
                }
                
                
            }    
            
            if(triggernew != null){
                
                for(UserTerritory2Association uta : triggernew){
                    //A Territory cannot have more than one user as primary
                    if(TerrPrimaryCount.containsKey(uta.Territory2Id) && TerrPrimaryCount.get(uta.Territory2Id) > 1)
                        uta.addError(Label.TerritoryPrimaryUserError);
                    //A user cannot have more than one user as primary
                    if(userPrimaryCount.containsKey(uta.userid) && userPrimaryCount.get(uta.userid) > 1)
                        uta.addError(Label.UserPrimaryTerritoryError);
                }
                
            }
            
            
            Map<Id,Territory2> TerrIdTerrMap = new Map<Id,Territory2>();
            
            //Query all territories and put in map with territory id as key and territory as value
            for( Territory2 terr: [SELECT Id, Name, ParentTerritory2Id,ParentTerritory2.Name, Territory2Type.MasterLabel,Territory2ModelId, Territory2TypeId FROM Territory2]){
                TerrIdTerrMap.put(terr.id,terr);
            }
            
            
            Set<id> tempusrid = new set<id>();
            
            //Update parent territory field values in to Division/Region/Team fields
            
            for(UserTerritory2Association  u : userTerritoryList){
                
               if(useridset.contains(u.userid) && u.RoleInTerritory2<>null && u.RoleInTerritory2.equalsIgnoreCase(Label.userAssociationTriggerPrimaryRole)){
               
                   //Get current primary territory details from user and based on that, traveserse through territory hierarchy until it reaches top.
                   //In the traversing process, update Division/Region/Team fields of user based on the current primary territory value choosed.
                   
                   Id userOrigPrimTerrId  = u.Territory2Id;
                   
                  
                   User objusr = new User(id=u.userid, username=u.user.username);
                   objusr = setHiearchyBasedonPrimary(userOrigPrimTerrId,u.Territory2.Name,objusr,TerrIdTerrMap,allterrusermap);
                   
                   //If Region Manager is null copy Division manager 
                   if(objusr.Region_Manager__c == null)
                       objusr.Region_Manager__c  = objusr.Division_Manager__c;
                       
                   //If Team Manager is null copy Region manager
                   if(objusr.Team_Manager__c == null)
                       objusr.Team_Manager__c  = objusr.Region_Manager__c;
                                       
                   system.debug('user id.. '+objusr.id+ 'username.. '+objusr.username+ '...TM/RM/DM..'+objusr.Team_Manager__c+'...'+objusr.Region_Manager__c+'....'+objusr.Division_Manager__c);
                   if(!tempusrid.contains(objusr.id)){
                        lstusertoupdate.add(objusr);
                        tempusrid.add(objusr.id);
                   }
               }
               
               
               //system.debug('All primary reps...'+allprimSalesRepset+'...this user...'+u.userid);  
               
            }
            
            //this is to check if the user does not have any primary sales rep associations (mainly when territory association is removed)
            for(Id userid: useridset){
            
                if(useridset.contains(userid) && !allprimSalesRepset.contains(userid)){
                    User objusr = new User(id=userid);
                    objusr.Region_Manager__c = null;
                    objusr.Team_Manager__c = null;
                    objusr.Division_Manager__c = null;
                    objusr.Region_Territory_Id__c=null;
                    objusr.Division_Territory_Id__c=null;
                    objusr.Team_Territory_Id__c=null;
                    objusr.Assigned_Primary_TerritoryId__c = null;
                    objusr.Division_Name__c = null;
                    objusr.Region_Name__c = null;
                    objusr.Team_Name__c = null;
                    objusr.Division_Manager_Name__c = null;
                    objusr.Region_Manager_Name__c = null;
                    objusr.Team_Manager_Name__c = null;
                    if(!tempusrid.contains(objusr.id)){
                        lstusertoupdate.add(objusr);
                        tempusrid.add(objusr.id);
                    }
               }
              
            }
        }
        return lstusertoupdate;
    }
    
    //Generic Method which is useful to traverse hierarchy for a given primary Territory and user
    public static User setHiearchyBasedonPrimary(Id userOrigPrimTerrId,String OrigUserTerrName,User objusr,Map<Id,Territory2> TerrIdTerrMap,Map<id,set<UserTerritory2Association>> allterrusermap){
        
        Id parentTerrid;
        String TerrType;
        String primaryTerrName;
        String primaryTerrId;                
        Id userprimterrid = userOrigPrimTerrId;
        objusr.Assigned_Primary_TerritoryId__c = userOrigPrimTerrId;
        objusr.Assigned_Primary_Territory_Name__c = OrigUserTerrName;
        do{
            if(TerrIdTerrMap.containsKey(userprimterrid)){
                
                parentTerrId = TerrIdTerrMap.get(userprimterrid).ParentTerritory2Id;
                TerrType = TerrIdTerrMap.get(userprimterrid).Territory2Type.MasterLabel;
                primaryTerrName = TerrIdTerrMap.get(userprimterrid).Name;
                primaryTerrId = TerrIdTerrMap.get(userprimterrid).Id;
                Set<UserTerritory2Association> allusersinthisterr = new set<UserTerritory2Association>();
                allusersinthisterr = allterrusermap.get(userprimterrid);
                
                if(TerrType <> null && TerrType.contains(' ')){
                    TerrType = TerrType.split(' ')[1];
                }
                //system.debug('...TERR TYPE....'+TerrType);
                //system.debug('...division label contains terr type....'+Label.userAssociationTriggerDivision.containsIgnoreCase(TerrType));
                //system.debug('...region label contains terr type....'+Label.userAssociationTriggerRegion.containsIgnoreCase(TerrType));
                //system.debug('...team label contains terr type....'+Label.userAssociationTriggerTeam.containsIgnoreCase(TerrType));
                //system.debug('Territory name....'+primaryTerrName);
                          
                if(TerrType<>null && Label.userAssociationTriggerDivision.containsIgnoreCase(TerrType)){
                    objusr.Division_Name__c = primaryTerrName;
                    objusr.Division_Territory_Id__c = primaryTerrId;
                    
                    String origuserterrtype = TerrIdTerrMap.get(userOrigPrimTerrId).Territory2Type.MasterLabel;
                    if(origuserterrtype <> null && origuserterrtype.contains(' ')){
                        origuserterrtype = origuserterrtype.split(' ')[1];
                    }
                    
                    if(allusersinthisterr <> null){
                        for(UserTerritory2Association u2a:allusersinthisterr){
                            if(u2a.RoleInTerritory2 <> null && u2a.RoleInTerritory2.containsIgnoreCase(Label.userAssociationTriggerPrimaryRole)){
                                objusr.Division_Manager__c = u2a.userid; 
                                objusr.Division_Manager_Name__c = u2a.user.Name;
                            }   
                        }
                    }
                    
                    if(Label.userAssociationTriggerDivision.containsIgnoreCase(origuserterrtype)){
                        objusr.Team_Manager__c = null; 
                        objusr.Region_Manager__c = null;
                        objusr.Team_Manager_Name__c = null;
                        objusr.Region_Manager_Name__c = null;
                        objusr.Region_Name__c = null;
                        objusr.Team_Name__c = null;
                    }
                }
                
                else if(TerrType<>null && Label.userAssociationTriggerRegion.containsIgnoreCase(TerrType)){
                    objusr.Region_Name__c = primaryTerrName;
                    objusr.Region_Territory_Id__c = primaryTerrId;
                    
                    String origuserterrtype = TerrIdTerrMap.get(userOrigPrimTerrId).Territory2Type.MasterLabel;
                    if(origuserterrtype <> null && origuserterrtype.contains(' ')){
                        origuserterrtype = origuserterrtype.split(' ')[1];
                    }
                    
                    if(allusersinthisterr <> null){
                        for(UserTerritory2Association u2a:allusersinthisterr){
                            // && u2a.userid <> objusr.id
                            if(u2a.RoleInTerritory2 <> null && u2a.RoleInTerritory2.containsIgnoreCase(Label.userAssociationTriggerPrimaryRole)){
                                objusr.Region_Manager__c = u2a.userid;  
                                objusr.Region_Manager_Name__c = u2a.user.Name;
                            }
                        }  
                    }
                    if(Label.userAssociationTriggerRegion.containsIgnoreCase(origuserterrtype)){
                        objusr.Team_Manager__c = null; 
                        objusr.Team_Manager_Name__c = null;
                    }
                }
                
                else if(TerrType<>null && Label.userAssociationTriggerTeam.containsIgnoreCase(TerrType)){
                    objusr.Team_Name__c = primaryTerrName;  
                    objusr.Team_Territory_Id__c  = primaryTerrId;
                    if(allusersinthisterr <> null){
                        for(UserTerritory2Association u2a:allusersinthisterr){
                            if(u2a.RoleInTerritory2 <> null && u2a.RoleInTerritory2.containsIgnoreCase(Label.userAssociationTriggerPrimaryRole)){
                                objusr.Team_Manager__c = u2a.userid;  
                                objusr.Team_Manager_Name__c = u2a.user.Name;
                            }
                        }
                    }
                }
                           
                userprimterrid = parentTerrId;
                           
            }
            else{
                parentTerrId = null;
            }
                                           
        
        }while(parentTerrId != null);
                    
        return objusr;   
        
    }
    
    
    //Method to add users to Account Team
    
    public static void addUsertoAccountTeam(List<UserTerritory2Association> lstUser){
    
        Set<Id> useridset = new Set<Id>();
        Set<Id> terrasidset = new Set<Id>();
        set<Id> terridset = new set<Id>();
        Map<id,String> userTerrRole = new Map<Id,String>();
        for(UserTerritory2Association u:lstUser)
            terrasidset.add(u.id);
        lstUser = [select id,userid,Territory2Id,Territory2.Territory2Type.MasterLabel,RoleInTerritory2 from UserTerritory2Association where id in:terrasidset];
        for(UserTerritory2Association u:lstUser){
            useridset.add(u.userid);
            terridset.add(u.Territory2Id);
            userTerrRole.put(u.userid,u.Territory2.Territory2Type.MasterLabel+'-'+u.RoleInTerritory2);
        }
        system.debug('CURRENT SYSTEM CONTEXT IS FUTURE???....'+System.isFuture()); 
        
        //Call respective method based on the current execution context
        if(!System.isFuture())
            userAssociationTriggerHelper.addorRemoveAccountTeam(terridset,useridset,userTerrRole,true);
        else
            userAssociationTriggerHelper.nonFutureAddorRemoveAccountTeam(terridset,useridset,userTerrRole,true);
    }
    
    //Method to remove users from Account Team
    public static void removeUserfromAccountTeam(List<UserTerritory2Association> lstUser){
    
        Set<Id> useridset = new Set<Id>();
        set<Id> terridset = new set<Id>();
        Map<id,String> userTerrRole = new Map<Id,String>();
        
        for(UserTerritory2Association u:lstUser){
            useridset.add(u.userid);
            terridset.add(u.Territory2Id);
            userTerrRole.put(u.userid,u.RoleInTerritory2);
        }
        
        system.debug('CURRENT SYSTEM CONTEXT IS FUTURE???....'+System.isFuture()); 
        
        //Call respective method based on the current execution context
        if(!System.isFuture())
            userAssociationTriggerHelper.addorRemoveAccountTeam(terridset,useridset,userTerrRole,false);
        else
            userAssociationTriggerHelper.nonFutureAddorRemoveAccountTeam(terridset,useridset,userTerrRole,false);
    }
    
    @future
    static void addorRemoveAccountTeam(Set<Id> terridset,Set<Id> useridset,Map<id,String> userTerrRole,Boolean addUsers){
        
        list<Group> lstgroup = new List<Group>();
        List<AccountTeamMember> lstacctteam = new List<AccountTeamMember>();
        List<AccountShare> lstAccountShare = new List<AccountShare>();
        Set<id> accidset = new Set<Id>();
        LIst<ObjectTerritory2Association> lstObjasso = new LIst<ObjectTerritory2Association>();
        system.debug('Territory ids...'+terridset);
        lstobjasso = [select id,Objectid,Territory2Id,SobjectType from ObjectTerritory2Association where Territory2Id in :terridset];
        
        //lstgroup = [Select Id, RelatedId from Group where (Type='Territory' OR Type='TerritoryAndSubordinates') AND RelatedId IN : terridset];
        //lstAccountShare =[Select AccountId,UserOrGroupId from AccountShare where UserOrGroupId IN : lstgroup AND RowCause IN ('Territory', 'TerritoryManual', 'TerritoryRule')];
        
        for(ObjectTerritory2Association ot2 : lstobjasso){
            if(ot2.SobjectType == 'Account')
            accidset.add(ot2.ObjectId);
        }
        
        //For each User associated to territory, create those users as team members in corresponding Accounts in that territory
        //Here we are adding only Account Team but not Account share because this is only for respresentational purpose. 
        //Usually users in Territory will have access to the Account by default.
        if(addUsers){
            for(Id accid:accidset){
             
                for(Id usrid:useridset){ 
                
                    AccountTeamMember atm = new AccountTeamMember();
                    atm.accountid = accid;
                    atm.userid = usrid;
                    atm.TeamMemberRole = userTerrRole.get(usrid);                
                    lstacctteam.add(atm);
                }   
            }
            if(lstacctteam.size()>0){
                database.insert(lstacctteam,false);
            }
        }
        else{
            List<AccountTeamMember> deleteList = new List<AccountTeamMember>();
            deleteList = [select id,userid,accountid from AccountTeamMember where Userid in:useridset and AccountId in:accidset];
            //for(AccountTeamMember atm: [select id,userid,accountid from AccountTeamMember where Userid in:useridset and AccountId in:accidset]){
               
            //}
            if(deleteList.size()>0)
                database.delete(deleteList,false);
        }
    }
    
   
   
   //THis is non-future add or Remove Account team method - especially for stafftransferprocess
    static void nonFutureAddorRemoveAccountTeam(Set<Id> terridset,Set<Id> useridset,Map<id,String> userTerrRole,Boolean addUsers){
        
        list<Group> lstgroup = new List<Group>();
        List<AccountTeamMember> lstacctteam = new List<AccountTeamMember>();
        List<AccountShare> lstAccountShare = new List<AccountShare>();
        Set<id> accidset = new Set<Id>();
        LIst<ObjectTerritory2Association> lstObjasso = new LIst<ObjectTerritory2Association>();
        system.debug('Territory ids...'+terridset);
        lstobjasso = [select id,Objectid,Territory2Id,SobjectType from ObjectTerritory2Association where Territory2Id in :terridset];
        
        //lstgroup = [Select Id, RelatedId from Group where (Type='Territory' OR Type='TerritoryAndSubordinates') AND RelatedId IN : terridset];
        //lstAccountShare =[Select AccountId,UserOrGroupId from AccountShare where UserOrGroupId IN : lstgroup AND RowCause IN ('Territory', 'TerritoryManual', 'TerritoryRule')];
        
        for(ObjectTerritory2Association ot2 : lstobjasso){
            if(ot2.SobjectType == 'Account')
            accidset.add(ot2.ObjectId);
        }
        
        //For each User associated to territory, create those users as team members in corresponding Accounts in that territory
        //Here we are adding only Account Team but not Account share because this is only for respresentational purpose. 
        //Usually users in Territory will have access to the Account by default.
        if(addUsers){
            for(Id accid:accidset){
             
                for(Id usrid:useridset){ 
                
                    AccountTeamMember atm = new AccountTeamMember();
                    atm.accountid = accid;
                    atm.userid = usrid;
                    atm.TeamMemberRole = userTerrRole.get(usrid);                
                    lstacctteam.add(atm);
                }   
            }
            if(lstacctteam.size()>0){
                database.insert(lstacctteam,false);
            }
        }
        else{
            List<AccountTeamMember> deleteList = new List<AccountTeamMember>();
            deleteList = [select id,userid,accountid from AccountTeamMember where Userid in:useridset and AccountId in:accidset];
            //for(AccountTeamMember atm: [select id,userid,accountid from AccountTeamMember where Userid in:useridset and AccountId in:accidset]){
               
            //}
            if(deleteList.size()>0)
                database.delete(deleteList,false);
        }
    }
    
    
}