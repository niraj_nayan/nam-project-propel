/***************************************************************************************************************************
Description: Controller for OpptyNewProposal VF page 
Created on:  10/7/2015
Created by:  Seth

Version 1:   Controller for OpptyNewProposal to check if Account is finance verified. If not, error will be displayed
Version 2:   Change of the verification to use Site ID instead of Finance_Verified
********************************************************************************************************************************/
public with sharing class OpptyNewProposal {

    public string OpptyID = ApexPages.currentpage().getparameters().get('addTo');

    public Opportunity currentOppty {get;set;}

    public OpptyNewProposal() {
        currentOppty = [Select Id, Name, Account.Site From Opportunity Where Id =:OpptyID];
    }

    public PageReference redirectOnLoad(){
        if(String.isBlank(currentOppty.Account.Site) == false){
            //redirect to new opportunity screen
            PageReference newOpptyPage = new PageReference('/0Q0/e?retURL=%2F' + OpptyID + '&oppid=' + OpptyID);
            return newOpptyPage;
        }
        else{
            //PageReference accountPage = new PageReference('/' + AccID);
            //System.debug('redirect page: ' + accountPage);
            return null;
        }
    }

    public PageReference redirectFromErrorPage(){
        PageReference opptyPage = new PageReference('/' + OpptyID);
        return opptyPage;
    }
}