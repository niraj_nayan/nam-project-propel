global class OpportunityAccountRollUpRevenueSchedular implements Schedulable {
	global void execute(SchedulableContext sc) {
		String query = 'SELECT Id, Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, RecordType.Name, ParentId FROM Account WHERE RecordType.Name Like \'%Division%\'';
		OpportunityAccountRollUpRevenueBatch batch = new OpportunityAccountRollUpRevenueBatch(query);
		//
		database.executebatch(batch);
		//database.executebatch(b);
	}
}