/*****************************************************************************************************************
Description: This Trigger is used to update the Revenue Roll Up and NEP Roll Up fields on Accounts. The revenue roll up
             is all of the Opportunity Amounts related to a Division Account roll up, added together. Then the Corp Account gets a sum of
             all of the Division Accounts Roll Up to get a Roll Up Revenue at the Corporate Level.
             NEP Roll up goes through the same process of rolling up Opportunity NEP values to the division level and then to
             Corp level

Created on:  10/23/2014
Created by:  Seth Martin
Version 1:   1
********************************************************************************************************************/


Public class AccountTriggerHelper {
    //STATIC variable to prevent recursive updates
    public Static boolean afterUpdateChangeCorpOwnercheck = FALSE;
    public static boolean beforeinsertupdateCopyTerritoryFieldsCheck = FALSE;
    public static boolean UpdateFromBatch = FALSE;
    
    //Method to generate source key
    public static void beforeInsertSourceKeyGenerate(List<Account> acct){
    
      integer counter = 0;
        for(Account acctChange: acct){
          acctChange.LOC__c = acctChange.Site;
          if(acctChange.Source_Key__c == null){
            DateTime current = System.now();
            counter += 10;
            acctChange.Source_Key__c = sourceKeyGenerationUtility.generateSourceKey('A', current.getTime() + counter);
          }
        }
    }

    /***********************************************************************
    TRIGGER BEFORE UPDATE: method to update the roll up values,  Revenue Roll Up
      and NEP roll up
    ************************************************************************/
    public static void beforeUpdateRollUpRevenue(List<Account> changedAccount, Map<Id, Account> oldMap){
        Set<String> accountTypeCorp = new Set<String>();
        Set<String> accountTypeDiv = new Set<String>();
            List<Account> corpAccount = new List<Account>();
        
        //Only get accounts that have changed parentId
        for(Account accountRollUp: changedAccount){
          System.debug('Current Account Record Type: ' + accountRollUp.RecordType);
          accountRollUp.LOC__c = accountRollUp.Site;
          System.debug('Old Account Record Type: ' + oldMap.get(accountRollUp.Id).RecordType);
            if((accountRollUp.ParentId != oldMap.get(accountRollUp.Id).ParentId) || ((accountRollUp.RecordType.Name == Label.AccRecordTypeDiv) && (oldMap.get(accountRollUp.Id).RecordType.Name) == Label.AccRecordTypeProspect)){
                accountTypeDiv.add(accountRollUp.Id);
                accountTypeCorp.add(accountRollUp.ParentId);//update new parent Id
                accountTypeCorp.add(oldMap.get(accountRollUp.Id).ParentId);//update old parent account roll up revenue amount
            }               
        }
            //If the size of the division accounts that have changed Parent Accounts(Corp Accounts) and Corp Accounts
            //is greater than 0 means there are Division and Corp Accounts with changes
        if(accountTypeDiv.size() > 0 && accountTypeCorp.size() > 0){
            Period currentFiscalYear = [SELECT FiscalYearSettings.Name, Type, StartDate, EndDate  
                                    FROM Period 
                                    WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY];
            List<Opportunity> divOpp = [SELECT Id, Amount, NEP__c, AccountId, RecordType.Name, Insert_Date__c, Cycle_Start_Date__c 
                                            From Opportunity 
                                            WHERE AccountId in :accountTypeDiv and Calendar_Fiscal_Year__c =: currentFiscalYear.FiscalYearSettings.Name];


            //AggregateResult[] groupedResults = [Select AccountId, SUM(Amount) From Opportunity Where AccountId in:accountTypeDiv GROUP BY AccountId];
            //System.debug('Sum results: ' + groupedResults);

            //Get all of the accounts, Division and Corp Accounts
            List<Account> allAcc = [Select Id, Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, Parent.Id From Account Where (Parent.Id in :accountTypeCorp OR Id in :accountTypeCorp)];
            List<Account> divAcc = new List<Account>();
            List<Account> corpAcc = new List<Account>();
            for(Account allAccounts: allAcc){
                  //if the account is a division account, its ID is in the division account set
                  if(accountTypeDiv.contains(allAccounts.Id)){
                        divAcc.add(allAccounts);
                  }
                  else if(accountTypeCorp.contains(allAccounts.Id)){
                        corpAcc.add(allAccounts);
                  }
            }

            //WE ARE LOOPING THROUGH THE PASSED IN ACCOUNTS, WHICH COULD BE CORP ACOUNTS OR DIVISION ACCOUNTS
                  //WE WANT TO UPDATE THE DIVISION ACCOUNTS ONLY
            for(Account acc: changedAccount){
                acc.Roll_Up_Revenue__c = 0;
                acc.NEP_Roll_Up__c = 0;
                if(accountTypeDiv.contains(acc.Id)){//IF THE ACCOUNT WE ARE LOOKING AT IS A DIVISION ACCOUNT ONLY WANT DIVISION ACCOUNTS
                    for(Opportunity opp: divOpp){
                        if((opp.AccountId == acc.Id)){
                            if(opp.Amount != null)
                                acc.Roll_Up_Revenue__c = acc.Roll_Up_Revenue__c + opp.Amount;
                            if(opp.NEP__c != null)
                                acc.NEP_Roll_Up__c = acc.NEP_Roll_Up__c + opp.NEP__c;
                        }   
                    }
                }
            }
                  //BECAUSE CORP ACCOUNTS ROLL UP IS A SUM OF THE DIVISION ACCOUNTS WE NEED TO UPDATE ONLY THE DIVISION ACCOUNTS FIRST
            //THEN WE CAN UPDATE THE CORP ACCOUNTS
            for(Account corp: corpAcc){
                corp.Roll_Up_Revenue__c = 0;
                corp.NEP_Roll_Up__c = 0;
                for(Account div: divAcc){
                    if((corp.Id == div.ParentId)){
                        if(div.Roll_Up_Revenue__c != null)
                            corp.Roll_Up_Revenue__c = corp.Roll_Up_Revenue__c + div.Roll_Up_Revenue__c;
                        if(div.NEP_Roll_Up__c != null)
                            corp.NEP_Roll_Up__c = corp.NEP_Roll_Up__c + div.NEP_Roll_Up__c;
                    }

                }
                corpAccount.add(corp);
            }

            try{
              Database.update(corpAccount, false);

            }catch(DmlException e){
              //going to check and see if the error message contains
              System.debug('Failed to Update Account: ' + e.getDmlType(0));
              System.debug('Failed to Update Account: ' + e.getCause());
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, ''+e.getDmlMessage(0));
              ApexPages.addmessage(myMsg);
            }
        }
        afterUpdateChangeCorpOwnercheck = FALSE;
    }
    
    /* Check if parent account is updated for a division account and if parent account has integration user as owner*/
    
    public static void afterUpdateChangeCorpOwner(List<Account> triggernew, Map<Id, Account> oldMap){
        
        if(!afterUpdateChangeCorpOwnercheck){
            afterUpdateChangeCorpOwnercheck = TRUE;
            set<id> corpaccidset = new Set<id>();
            Map<id,Account> childownermap = new Map<id,Account>();
            List<Account> parAcclst = new List<Account>();
            List<Account> childAcclst = new List<Account>();
            
            for(Account acc:triggernew){
                system.debug('acc parent...'+acc.parentid+'...'+oldMap.get(acc.id).parentid);
                if(acc.parentid<>null && oldMap.get(acc.id).parentid == null){
                    corpaccidset.add(acc.parentid);
                    
                }
            }
            
            if(corpaccidset.size()>0){
                parAcclst = [select id,ownerid,owner.name,recordtype.name,recordtypeid from Account where id=:corpaccidset ];
                childAcclst = [select id,ownerid,owner.name,createddate,parentid,recordtype.name,recordtypeid from Account where parentid<>null and parentid=:corpaccidset order by createddate];
                if(childAcclst.size()>0){
                    for(Account acc:childAcclst){
                        if(!childownermap.containsKey(acc.parentid))
                            childownermap.put(acc.parentid,acc);
                    }
                }
                
                system.debug('Child owner map...'+childownermap);
                List<Account> accsToUpdate = new List<Account>();    
                for(Account acc:parAcclst){
                    system.debug('Parent account..'+acc);
                    if(acc.recordtype.name == Label.AccRecordTypeCorp && acc.owner.name == Label.IntegrationUser){
                        if(childownermap.containsKey(acc.id) && childownermap.get(acc.id).owner.name != Label.IntegrationUser){
                            acc.ownerid = childownermap.get(acc.id).ownerid; 
                            accsToUpdate.add(acc);
                        }
                    }
                }
                if(accsToUpdate.size()>0){
                    Database.SaveResult[] lsr = database.update(accsToUpdate,false);
                    for(Database.SaveResult sr : lsr)
                        if (!sr.isSuccess()) system.debug('Error occured during parent account update....'+sr);
                }
            
            }
        }
        
    }
    
    /***********************************************************************************************
    Method to update territory hierarchy fields of opp whenever the opportunity is inserted or updated
    ************************************************************************************************/
    public static void beforeinsertupdateCopyTerritoryFields(List<Account> triggernew,Map<Id,Account> triggernewmap,Map<Id,Account> triggeroldmap){
        
        if(!beforeinsertupdateCopyTerritoryFieldsCheck){
            beforeinsertupdateCopyTerritoryFieldsCheck = TRUE;
            Set<Id> owneridset = new Set<id>();
            Set<Id> oppidset = new set<Id>();
            set<string> fedidset = new set<String>();
            
            Set<id> accidset = new Set<Id>();
            //Map to check if the Account owner is part of users in the territories related to Account
            Map<Id,boolean> accTerrCheckMap = new Map<Id,boolean>();
            Map<Id,Id> accIdOwnerIdMap = new Map<Id,Id>();
            for(Account acc: triggernew){
                
                if(Label.ForceUpdateHierarchy == Label.TrueValue){
                    accTerrCheckMap.put(acc.id,false);
                    accIdOwnerIdMap.put(acc.id,acc.ownerid);
                    owneridset.add(acc.ownerid);
                    accidset.add(acc.id);
                    //For data migration activity, federation ids are given in hierarchy fields
                    if(Label.DataMigration == Label.TrueValue){
                        fedidset.add(acc.Division_Manager__c); 
                        fedidset.add(acc.Region_Manager__c); 
                        fedidset.add(acc.Team_Manager__c );
                        fedidset.add(acc.Primary_Sales_Rep__c); 
                    }
                }
                    
                //if(triggeroldmap == null || UpdateFromBatch || (triggeroldmap<>null && triggernewmap<>null && triggernewmap.get(acc.id).ownerid <> triggeroldmap.get(acc.id).ownerid)){
                if(acc.Frozen_Hierarchy__c == FALSE){
                        accTerrCheckMap.put(acc.id,false);
                        owneridset.add(acc.ownerid);
                        accIdOwnerIdMap.put(acc.id,acc.ownerid);
                        accidset.add(acc.id);
                        if(Label.DataMigration == Label.TrueValue){
                            fedidset.add(acc.Division_Manager__c); 
                            fedidset.add(acc.Region_Manager__c); 
                            fedidset.add(acc.Team_Manager__c );
                            fedidset.add(acc.Primary_Sales_Rep__c); 
                        }
                }    
                //}
                
            }
            if(owneridset.size()>0){
                
                List<ObjectTerritory2Association> lstobta = new List<ObjectTerritory2Association>();
                set<Id> terridset = new Set<Id>();
                //get territories associate to accounts
                lstobta = [select id,ObjectId, Territory2Id from ObjectTerritory2Association where ObjectId in :accidset];
                Map<Id,Id> userPrimaryTerrIdMap = new Map<Id,Id>();
                Map<Id,String> userPrimaryTerrNameMap = new Map<Id,String>();
                    
                if(lstobta.size()>0){
                    
                    for(ObjectTerritory2Association o2a: lstobta)
                        terridset.add(o2a.Territory2Id);
                    
                    List<UserTerritory2Association> lstuta = new List<UserTerritory2Association>();
                    lstuta = [select id,userid,RoleInTerritory2,Territory2Id,Territory2.Name from UserTerritory2Association where Territory2Id in : terridset and userid in:owneridset and RoleInTerritory2 in (:Label.userAssociationTriggerPrimaryRole,:Label.PrimarySplitRole)];
                    
                    //Loop over all users in those territories 
                    for(UserTerritory2Association u2a: lstuta){
                        if(u2a.RoleInTerritory2 == Label.PrimarySplitRole && !userPrimaryTerrIdMap.containsKey(u2a.userid)){
                            userPrimaryTerrIdMap.put(u2a.userid,u2a.Territory2Id);
                            userPrimaryTerrNameMap.put(u2a.userid,u2a.Territory2.Name);
                        }
                        if(u2a.RoleInTerritory2 == Label.userAssociationTriggerPrimaryRole){
                            userPrimaryTerrIdMap.put(u2a.userid,u2a.Territory2Id);
                            userPrimaryTerrNameMap.put(u2a.userid,u2a.Territory2.Name);
                        }
                    }
                
                }
                for(Id accid:accIdOwnerIdMap.keyset()){
                    if(userPrimaryTerrIdMap.containsKey(accIdOwnerIdMap.get(accid)))
                        accTerrCheckMap.put(accid,true);
                }
                
                system.debug('acc Territory check map...'+accTerrCheckMap);
                
                Map<Id,user> usermap = new Map<Id,User>([select id,FederationIdentifier,Assigned_Primary_Territory_Name__c,Assigned_Primary_TerritoryId__c,Division_Manager__c,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,
                                        Team_Territory_Id__c,Region_Name__c,Region_Manager__c,Team_Manager__c,Team_Name__c from user where id in:owneridset or federationIdentifier =:fedidset]);
                Map<String,Id> fedUserIdMap = new Map<String,Id>();
                for(User u:usermap.values())
                    fedUserIdMap.put(u.FederationIdentifier, u.id);
                
                //Query all territories and put in map with territory id as key and territory as value
                Map<Id,Territory2> TerrIdTerrMap = new Map<Id,Territory2>([SELECT Id, Name, ParentTerritory2Id,ParentTerritory2.Name, Territory2Type.MasterLabel,Territory2ModelId, Territory2TypeId FROM Territory2]);
                
                
                Map<id,set<UserTerritory2Association>> allterrusermap = new Map<id,set<UserTerritory2Association>>();
                
                List<UserTerritory2Association> userTerritoryList = [SELECT Id, IsActive, UserId, user.Name, user.Username, user.IsActive, Territory2Id,Territory2.Name,RoleInTerritory2 FROM UserTerritory2Association];
            
                //Based on the above query, frame map to have territory id and respective users of the territory
                for(UserTerritory2Association terr : userTerritoryList) {
                    if(allterrusermap.containsKey(terr.Territory2Id))
                        allterrusermap.get(terr.Territory2Id).add(terr);
                    else
                        allterrusermap.put(terr.Territory2Id,new set<UserTerritory2Association>{terr});
                }
                
                for(Account acc: triggernew){
                    if(accTerrCheckMap.containsKey(acc.id) && accTerrCheckMap.get(acc.id) == false){
                        if(usermap.containsKey(acc.ownerid)){
                            //skip if the account is being migrated(as we will have the historical data during load)
                            if(Label.DataMigration != Label.TrueValue)
                                assignHierarchyFields(acc,usermap);
                            else
                                assignHierarchyFields(acc,fedUserIdMap);
                        }
                    }
                    else if(accTerrCheckMap.containsKey(acc.id) && accTerrCheckMap.get(acc.id) == true){
                        String terrIdforTraverse = userPrimaryTerrIdMap.get(acc.ownerid);
                        String terrNameforTraverse = userPrimaryTerrNameMap.get(acc.ownerid);
                        User accOwner = new User(id=acc.ownerid);
                        //Traverse hierarchy and get corresponding fields
                        accOwner = userAssociationTriggerHelper.setHiearchyBasedonPrimary(terrIdforTraverse,terrNameforTraverse,accOwner,TerrIdTerrMap,allterrusermap);
                        system.debug('User hierarchy.....'+accOwner);
                        acc.Primary_Reps_Territory__c = accOwner.Assigned_Primary_Territory_Name__c;
                        acc.Division_Manager__c = accOwner.Division_Manager__c;
                        acc.Division_Name__c = accOwner.Division_Name__c;
                        acc.Primary_Rep_Territory_Id__c = accOwner.Assigned_Primary_TerritoryId__c;
                        acc.Region_Manager__c = accOwner.Region_Manager__c;
                        acc.Region_Name_Frozen__c = accOwner.Region_Name__c;
                        acc.Account_Sales_Office_Text__c = acc.Account_Sales_Office__c;
                        acc.Team_Manager__c = accOwner.Team_Manager__c;
                        acc.Team_Name__c = accOwner.Team_Name__c;
                        acc.Primary_Sales_Rep__c = acc.ownerid;
                        acc.Division_Territory_Id__c = accOwner.Division_Territory_Id__c;
                        acc.Region_Territory_Id__c = accOwner.Region_Territory_Id__c;
                        acc.Team_Territory_Id__c = accOwner.Team_Territory_Id__c;
                        
                    }
                }
            }
        }
    }
    
    /* Update flat hierarchy fields */
    
     public static void assignHierarchyFields(Account acc,Map<id,User> usermap){
        acc.Primary_Reps_Territory__c = usermap.get(acc.ownerid).Assigned_Primary_Territory_Name__c;
        acc.Division_Manager__c = usermap.get(acc.ownerid).Division_Manager__c;
        acc.Division_Name__c = usermap.get(acc.ownerid).Division_Name__c;
        acc.Primary_Rep_Territory_Id__c = usermap.get(acc.ownerid).Assigned_Primary_TerritoryId__c;
        acc.Region_Manager__c = usermap.get(acc.ownerid).Region_Manager__c;
        acc.Region_Name_Frozen__c = usermap.get(acc.ownerid).Region_Name__c;
        acc.Account_Sales_Office_Text__c = acc.Account_Sales_Office__c;
        acc.Team_Manager__c = usermap.get(acc.ownerid).Team_Manager__c;
        acc.Team_Name__c = usermap.get(acc.ownerid).Team_Name__c;
        acc.Primary_Sales_Rep__c = acc.ownerid;
        acc.Division_Territory_Id__c =usermap.get(acc.ownerid).Division_Territory_Id__c;
        acc.Region_Territory_Id__c = usermap.get(acc.ownerid).Region_Territory_Id__c;
        acc.Team_Territory_Id__c = usermap.get(acc.ownerid).Team_Territory_Id__c;
    }
    //FOR DATA MIGRATION PURPOSE
    public static void assignHierarchyFields(Account acc,Map<String,Id> fedUserIdMap){
        
        if(fedUserIdMap.containsKey(acc.Division_Manager__c))
            acc.Division_Manager__c = fedUserIdMap.get(acc.Division_Manager__c);
            
        if(fedUserIdMap.containsKey(acc.Region_Manager__c))
            acc.Region_Manager__c = fedUserIdMap.get(acc.Region_Manager__c);
            
        if(fedUserIdMap.containsKey(acc.Team_Manager__c))
            acc.Team_Manager__c = fedUserIdMap.get(acc.Team_Manager__c);
            
        if(fedUserIdMap.containsKey(acc.Primary_Sales_Rep__c))
            acc.Primary_Sales_Rep__c = fedUserIdMap.get(acc.Primary_Sales_Rep__c);
        
    }
    
    
    
    
}