public with sharing class CreateTieInOpportunity {

	public string OpportId = ApexPages.currentpage().getparameters().get('addTo');

	public Linked_Opportunity__c newLinkedOpportunity {get;set;}
	public List<Linked_Opportunity__c> allTieInOpportuntiy {get;set;}

	public CreateTieInOpportunity() {
		newLinkedOpportunity = new Linked_Opportunity__c();
		RecordType linkedTieInType = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Linked_Opportunity__c' and Name ='Tie-In Opportunity'];
		newLinkedOpportunity.Tie_In_Opportunity__c = OpportId;
		newLinkedOpportunity.RecordTypeId = linkedTieInType.Id;

		//grab all linked Opportunities where the related_Opportunity is the current Opportunity ID
		allTieInOpportuntiy = [Select Id, Tie_In_Opportunity__c, Linked_Opportunity__c From Linked_Opportunity__c Where Tie_In_Opportunity__c =:OpportId];
		System.debug('all related opp: ' + allTieInOpportuntiy);
	}

	public PageReference saveLinkedOpportunity(){

		if(newLinkedOpportunity.Tie_In_Opportunity__c == newLinkedOpportunity.Linked_Opportunity__c){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, Label.LinkedOpportunitySameOpptyValidation);
			ApexPages.addmessage(myMsg);
			return null;
		}

		if(allTieInOpportuntiy.size() > 0){
			for(Linked_Opportunity__c relatedOpp: allTieInOpportuntiy){
				if(relatedOpp.Linked_Opportunity__c == newLinkedOpportunity.Linked_Opportunity__c){
					ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, Label.LinkedOpportunityDuplicateValidation);
					ApexPages.addmessage(myMsg);
					return null;
				}
			}
		}
		
		insert newLinkedOpportunity;
		PageReference opptyPage = new PageReference('/' + OpportId);

        System.debug('Page Refernce Cancel: ' + opptyPage);
        return opptyPage;
	}

	/***********************************************************************
    Method to return user to opportunity detail page
    ************************************************************************/
    public PageReference cancelButton(){
    	newLinkedOpportunity.Linked_Opportunity__c = null;
        PageReference opptyPage = new PageReference('/' + OpportId);

        System.debug('Page Refernce Cancel: ' + opptyPage);
        return opptyPage;
    }
}