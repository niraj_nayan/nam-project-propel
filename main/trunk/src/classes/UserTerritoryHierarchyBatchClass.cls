/**********************************************************************************************************************************************
Description: Batch class to adjust territory hierarchy for all users       
Created on:  5 Jan 2016
Created by:  Jagan

Version 1:   Created the class
************************************************************************************************************************************************/

Global class UserTerritoryHierarchyBatchClass implements Database.Batchable<sObject> {
    
    //Get the query from custom label
    Global final String Query= Label.TerritoryHierarchyBatchQuery;
    //This is the query in custom label: Select id,name,Division_Manager__c,Division_Name__c,Division_Territory_Id,Region_Name__c,Region_Manager__c,Region_Territory_Id,Team_Manager__c,Team_Name__c,Team_Territory_Id from user where isActive = TRUE  
    
    //Query all active users in the system
    Global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug(query);
        return Database.getQueryLocator(query);
    }
  
    /********************************************************************************************************************************  
    * Loop over the current batch of users to 
    * 1. Update Territory hierarchy of that user.
    * 2. Get the opportunities where this user is owner and update the territory hierarchy fields based on insert_date__c
    * 3. Get the opportunity team memembers for that user and update the hierarchy fields based on opportunitie's insert_date__c
    * 4. Get the goals where the user is owner and update the hierarchy fields based on effective date of the goal.
    *********************************************************************************************************************************/
    
    Global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        Set<Id> useridset = new set<Id>();
        List<User> lstusertoupdate = new List<User>();
        Map<Id,User> originalusrmap = new Map<Id,User>();
        Map<Id,User> finalusermap = new Map<Id,User>();
        
        for(sobject s : scope) {
            User usr = (User) s;
            useridset.add(usr.id);
            originalusrmap.put(usr.id,usr);
        }          
        
        //update territory hierarchy for all those users
        lstusertoupdate = userAssociationTriggerHelper.updateUsers(useridset);
        
        if(lstusertoupdate.size()>0){
            
            //List to be updated will be compared with the original list of users
            //and only those users where the data is changed will be updated.
            for(User u:lstusertoupdate){
                if(originalusrmap.containsKey(u.id)){
                    User origUsr = originalusrmap.get(u.id);
                    
                    System.debug('Original user ...'+origUsr+'....Modified user...'+u);
                    
                    //add the user to final list only if any of RM/DM/TM is changed
                    if( u.Region_Manager__c <> origUsr.Region_Manager__c || 
                        u.Division_Manager__c <> origUsr.Division_Manager__c  ||  
                        u.Team_Manager__c <> origUsr.Team_Manager__c ) {
                        
                        finalusermap.put(u.id,u);
                    }
                }
            }
            
            if(finalusermap.size()>0){
                Database.SaveResult[] lsr = database.update(finalusermap.values(),false);
                for(Database.SaveResult sr : lsr)
                    if (!sr.isSuccess()) system.debug('Error occured during user update....'+sr);
                updateRecords(finalusermap);
                
            }
        }
           
    }
    
    //Get the opportunites/opportunity team members/goals owned by users in final user set
    public static void updateRecords(Map<id,user> finalusermap){
        
        List<Account> acclst = new List<Account>();
        List<Account> accUpdlst = new List<Account>();
        
        acclst = [select  id,Frozen_Hierarchy__c,Account_Sales_Office_Text__c,Account_Sales_Office__c,Primary_Reps_Territory__c,Division_Manager__c,Primary_Rep_Territory_Id__c,Ownerid,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,Region_Name_Frozen__c,Region_Manager__c,Team_Manager__c,Team_Name__c,Primary_Sales_Rep__c from Account where ownerid in :finalusermap.keySet()];
        AccountTriggerHelper.UpdateFromBatch = TRUE;
        if(acclst.size()>0){
            Database.SaveResult[] lsr = database.update(acclst,false);
            for(Database.SaveResult sr : lsr)
                if (!sr.isSuccess()) system.debug('Error occured during account update....'+sr);
        }
        
        List<Opportunity> opplst = new List<Opportunity>();
        List<Opportunity> oppUpdlst = new List<Opportunity>();
             
        opplst = [select id,Insert_date__c,Frozen_Hierarchy__c,Account_Sales_Office_Text__c,Account_Sales_Office__c,Primary_Reps_Territory__c,Primary_Rep_Territory_Id__c,Division_Manager__c,Cycle_Start_Date__c,Ownerid,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,Region_Name__c,Region_Manager__c,Team_Manager__c,Team_Name__c,Primary_Sales_Rep__c from Opportunity where ownerid in :finalusermap.keySet()];
            
            if(opplst.size()>0){
                
                /* for(Opportunity opp : opplst){
                   if(opp.Frozen_Hierarchy__c == false && finalusermap.containsKey(opp.ownerid) &&  ((opp.Insert_Date__c<>null && system.today() <= opp.Insert_Date__c) || (opp.Cycle_Start_Date__c<>null && system.today() <= opp.Cycle_Start_Date__c))){
                        opp.Primary_Reps_Territory__c= finalusermap.get(opp.ownerid).Assigned_Primary_Territory_Name__c;
                        opp.Primary_Rep_territory_Id__c = finalusermap.get(opp.ownerid).Assigned_Primary_TerritoryId__c;
                        opp.Division_Manager__c = finalusermap.get(opp.ownerid).Division_Manager__c;
                        opp.Division_Name__c = finalusermap.get(opp.ownerid).Division_Name__c;
                        opp.Region_Manager__c = finalusermap.get(opp.ownerid).Region_Manager__c;
                        opp.Region_Name__c = finalusermap.get(opp.ownerid).Region_Name__c;
                        opp.Team_Manager__c = finalusermap.get(opp.ownerid).Team_Manager__c;
                        opp.Team_Name__c = finalusermap.get(opp.ownerid).Team_Name__c;
                        opp.Primary_Sales_Rep__c = opp.ownerid;
                        opp.Division_Territory_Id__c = finalusermap.get(opp.ownerid).Division_Territory_Id__c;
                        opp.Region_Territory_Id__c = finalusermap.get(opp.ownerid).Region_Territory_Id__c;
                        opp.Team_Territory_Id__c = finalusermap.get(opp.ownerid).Team_Territory_Id__c; 
                        oppUpdlst.add(opp);
                    } 
                }*/
                if(opplst.size()>0){
                    Database.SaveResult[] lsr = database.update(opplst,false);
                    for(Database.SaveResult sr : lsr)
                        if (!sr.isSuccess()) system.debug('Error occured during opp update....'+sr);
                }
            }
        
        List<OpportunityTeamMember> oppteamlst = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> oppteamupdlst = new List<OpportunityTeamMember>();
            
        oppteamlst = [select id,userid,Frozen_Hierarchy__c,Primary_Reps_Territory__c,Primary_Rep_Territory_Id__c,Account_Sales_Office_Text__c,Account_Sales_Office__c,Opportunity.Insert_Date__c,Opportunity.Cycle_Start_Date__c,Division_Manager__c,Division_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,Division_Name__c,Region_Name__c,Region_Manager__c,Team_Manager__c,Team_Name__c,Primary_Sales_Rep__c from OpportunityTeamMember where userid in :finalusermap.keySet()];
            if(oppteamlst.size()>0){
                
                /*for(OpportunityTeamMember otm: oppteamlst){
                
                    if(otm.Frozen_Hierarchy__c == false && finalusermap.containsKey(otm.userid) &&  ((otm.Opportunity.Insert_Date__c<>null && system.today() <= otm.Opportunity.Insert_Date__c) || (otm.Opportunity.Cycle_Start_Date__c<>null && system.today() <= otm.Opportunity.Cycle_Start_Date__c)) ){
                        otm.Primary_Reps_Territory__c= finalusermap.get(otm.userid).Assigned_Primary_Territory_Name__c;
                        otm.Primary_Rep_territory_Id__c = finalusermap.get(otm.userid).Assigned_Primary_TerritoryId__c;
                        otm.Division_Manager__c = finalusermap.get(otm.userid).Division_Manager__c;
                        otm.Division_Name__c = finalusermap.get(otm.userid).Division_Name__c;
                        otm.Region_Manager__c = finalusermap.get(otm.userid).Region_Manager__c;
                        otm.Region_Name__c = finalusermap.get(otm.userid).Region_Name__c;
                        otm.Team_Manager__c = finalusermap.get(otm.userid).Team_Manager__c;
                        otm.Team_Name__c = finalusermap.get(otm.userid).Team_Name__c;
                        otm.Primary_Sales_Rep__c = otm.userid;
                        otm.Division_Territory_Id__c = finalusermap.get(otm.userid).Division_Territory_Id__c;
                        otm.Region_Territory_Id__c = finalusermap.get(otm.userid).Region_Territory_Id__c;
                        otm.Team_Territory_Id__c = finalusermap.get(otm.userid).Team_Territory_Id__c; 
                        oppteamupdlst.add(otm);
                    }
                }*/
                
                if(oppteamlst.size()>0){
                    Database.SaveResult[] lsr = database.update(oppteamlst,false);
                    for(Database.SaveResult sr : lsr)
                        if (!sr.isSuccess()) system.debug('Error occured during opp team update....'+sr);
                }
            }
       
        List<Goal__c> goallst = new List<Goal__c>();
        List<Goal__c> goalupdlst = new List<Goal__c>();
        
        Goal_record_share_helper.batchUpdate = TRUE;
        Goal_record_share_helper objG = new Goal_record_share_helper();
        goallst = [select id,CreatedbyId,Ownerid,Effective_date__c,Frozen_Hierarchy__c,Primary_Reps_Territory__c,Primary_Rep_Territory_Id__c,Account_Sales_Office_Text__c,Account_Sales_Office__c,Cycle_Begin_Date__c,Division_Territory_Id__c,Division_Manager__c,Region_Territory_Id__c,Team_Territory_Id__c,Division_Name__c,Region_Name__c,Region_Manager__c,Team_Manager__c,Team_Name__c,Primary_Sales_Rep__c from Goal__c where ownerid in :finalusermap.keySet()];
        
        if(goallst.size()>0){
            Map<Id,Goal__c> goalMap = new Map<Id,Goal__c>();
            for(Goal__c g: goallst){
                goalMap.put(g.id,g);
            }
            
              /*  
                for(Goal__c g: goallst){
                    if(g.Frozen_Hierarchy__c == false && finalusermap.containsKey(g.ownerid) && ((g.effective_Date__c<>null && system.today() <= g.effective_Date__c) || (g.Cycle_Begin_Date__c<>null && system.today() <= g.Cycle_Begin_Date__c))){
                        g.Primary_Reps_Territory__c = finalusermap.get(g.ownerid).Assigned_Primary_Territory_Name__c;
                        g.Primary_Rep_territory_Id__c = finalusermap.get(g.ownerid).Assigned_Primary_TerritoryId__c;
                        g.Division_Manager__c = finalusermap.get(g.ownerid).Division_Manager__c;
                        g.Division_Name__c = finalusermap.get(g.ownerid).Division_Name__c;
                        g.Region_Manager__c = finalusermap.get(g.ownerid).Region_Manager__c;
                        g.Region_Name__c = finalusermap.get(g.ownerid).Region_Name__c;
                        g.Team_Manager__c = finalusermap.get(g.ownerid).Team_Manager__c;
                        g.Team_Name__c = finalusermap.get(g.ownerid).Team_Name__c;
                        g.Primary_Sales_Rep__c = g.ownerid;
                        g.Division_Territory_Id__c = finalusermap.get(g.ownerid).Division_Territory_Id__c;
                        g.Region_Territory_Id__c = finalusermap.get(g.ownerid).Region_Territory_Id__c;
                        g.Team_Territory_Id__c = finalusermap.get(g.ownerid).Team_Territory_Id__c;  
                        goalupdlst.add(g);
                    }   
                } 
                */
            if(goallst.size()>0){
                Database.SaveResult[] lsr = database.update(goallst,false);
                for(Database.SaveResult sr : lsr)
                    if (!sr.isSuccess()) system.debug('Error occured during goal update....'+sr);
                
                objG.shareGoals(goalMap,goalMap);
            }
        }   
        
        
        List<Form_Header__c> FormHdrlst = new List<Form_Header__c>();
        
        FormHdrlst = [select id,ownerid,createdbyid from Form_Header__c where ownerid = :finalusermap.keySet() or createdbyid = :finalusermap.keySet()];
        Map<Id,Form_Header__c> formHdrMap = new Map<Id,Form_Header__c>();
        
        for(Form_Header__c fh: FormHdrlst)
            formHdrMap.put(fh.id,fh);
        FormHeaderTriggerHelper.isBatchUpdate = TRUE;
        FormHeaderTriggerHelper.shareFormHeader(formHdrMap,formHdrMap);
    }
    
    
    //finsih method - Actions TBD once the batch is finished
    Global void finish(Database.BatchableContext BC){
  
    
    }
  
 
}