@isTest
private class AddSalesTeamTest {
	static Account nestleCorp;
	static Account nestleDivision;
	static Opportunity testOppty;

	@isTest static void addSalesTeamInsertSalesTeamMember() {
		// Implement test code

		User newAddUser = TestDataUtils.createUser('Test Last Name1', 'no-reply@email.com', '1231231234');
		User newAddUser2 = TestDataUtils.createUser('Test Last Name2', 'no-reply@email1.com', '1231231234');
		User newAddUser3 = TestDataUtils.createUser('Test Last Name3', 'no-reply@email2.com', '1231231234');
		User newAddUser4 = TestDataUtils.createUser('Test Last Name4', 'no-reply@email3.com', '1231231234');
		User newAddUser5 = TestDataUtils.createUser('Test Last Name5', 'no-reply@email4.com', '1231231234');
		User newAddUser6 = TestDataUtils.createUser('Test Last Name6', 'no-reply@email5.com', '1231231234');
		insert newAddUser;
		insert newAddUser2;
		insert newAddUser3;
		insert newAddUser4;
		insert newAddUser5;
		insert newAddUser6;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		insert secondUser;

		System.runAs(secondUser){

			nestleCorp = TestDataUtils.createCorpAccount('Nestle Corp');
			insert nestleCorp;

			//Create Division Account record
			nestleDivision = TestDataUtils.createDivAccount('nestle Div', nestleCorp.Id);
			insert nestleDivision;

			//then an opportunity with lookup to the account
			testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', nestleDivision.Id, 'FSI');
			insert testOppty;

			OpportunityTeamMember testMember1 = TestDataUtils.createTeamMember(secondUser.Id, 'Temp Role', testOppty.Id, 100);
			insert testMember1;

			delete testMember1;

			ApexPages.currentPage().getParameters().put('addTo', testOppty.Id);
			AddSalesTeam controller = new AddSalesTeam();
			controller.redirectOnLoad();
			//set all of the other sales team objects to null
			for(AddSalesTeam.SalesTeam testTeam: controller.opptyTeamMember){
	  			testTeam.selectedTeamRole = '-- None --';
	  		}

			//create sales team member object
	  		AddSalesTeam.SalesTeam newMember = new AddSalesTeam.SalesTeam();
	  		newMember.selectedOpptyAccess = 'Read';
	  		newMember.selectedTeamRole = 'Manager';
	  		newMember.teamMember.UserId = newAddUser.Id;
	  		newMember.teamMember.Commission_Percentage__c = 10;

	  		controller.saveSalesTeam();

	  		controller.opptyTeamMember[0] = newMember;
	  		controller.opptyTeamMember[1] = newMember;

	  		//add new sales team member
	  		//controller.opptyTeamMember.add(newMember);
	  		//call the save function
	  		controller.saveSalesTeam();
	  		Integer salesTeamCnt = [select count() from OpportunityTeamMember where opportunityid=:testOppty.id];
	  		system.assert(salesTeamCnt > 0);
	  		
	  		System.assertEquals([Select Id, OpportunityAccessLevel, OpportunityId From OpportunityShare Where OpportunityId =:testOppty.Id and UserOrGroupId =:newAddUser.Id].OpportunityAccessLevel, 'Read');

	  		//create sales team member object
	  		AddSalesTeam.SalesTeam secondMember = new AddSalesTeam.SalesTeam();
	  		secondMember.selectedOpptyAccess = 'Read';
	  		secondMember.selectedTeamRole = 'Temp Access';
	  		secondMember.teamMember.UserId = secondUser.Id;
	  		secondMember.teamMember.Commission_Percentage__c = 10;

	  		controller.opptyTeamMember[0] = secondMember;
	  		controller.opptyTeamMember[1] = newMember;
	  		controller.opptyTeamMember[2] = newMember;

	  		controller.saveSalesTeamAndMore();

	  		controller.cancelButton();

	  		delete testOppty;

  		}
	}
	
}