/**********************************************************************************************************************************************
Description: Batch class to adjust the Account Team based on Territory       
Created on:  5 Jan 2016
Created by:  Jagan

Version 1:   Created the class
************************************************************************************************************************************************/

Global class AccountTeamAdjustBatchClass implements Database.Batchable<sObject> {
    
    //Query to get all the accounts from Object territory association
    Global final String Query= Label.AccountTeamAdjustBatchQuery;
    
    
    //Query all active users in the system
    Global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug(query);
        return Database.getQueryLocator(query);
    }
  
    /********************************************************************************************************************************  
    * Loop over the current batch of object territory associations to 
    * 1. Get the Account Id .
    * 2. Get the territory(ies) of the Account.
    * 3. Gather users for each territory and add them as Account Team.
    * 4. If users in territory is not part of Account team then they are inserted.
    * 5. If Account team member is not part of the users in territory then they are deleted.
    *********************************************************************************************************************************/
    
    Global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        Map<Id,set<Id>> teamAccountMap = new Map<Id,set<Id>>();
        //Map to insert team members back if the deleted team member is part of the territory that account is assinged.
        Map<Id,set<UserTerritory2Association>> FinalteamAccountMap = new Map<Id,set<UserTerritory2Association>>();
        Set<Id> Accidset = new Set<Id>();
        Map<Id,Set<Id>> accTerrMap = new Map<id,Set<Id>>();
        Map<Id,Set<UserTerritory2Association>> accUserMap = new Map<id,Set<UserTerritory2Association>>();
        Set<Id> terridset = new Set<Id>();
        List<AccountTeamMember> lstatm = new List<AccountTeamMember>();
        Map<Id,Set<AccountTeamMember>> AccIdTeamMemMap = new Map<Id,set<AccountTeamMember>>();
        Map<Id,set<Id>> AccIdTerrMemMap = new Map<Id,set<Id>>();
        List<UserTerritory2Association> userTerritoryList = new List<UserTerritory2Association>();
        
        Map<Id,DateTime> accTerrModDateMap = new Map<Id,DateTime>();
        //list to insert
        List<AccountTeamMember> lstAcctTeam = new List<AccountTeamMember>();
        
        for(sobject s : scope) {
            ObjectTerritory2Association ota = (ObjectTerritory2Association) s;
            if(ota.SobjectType == 'Account'){
                Accidset.add(ota.ObjectId);
                terridset.add(ota.territory2id); 
                //Map to store all account ids per territory
                if(accTerrMap.containsKey(ota.Territory2Id))
                    accTerrMap.get(ota.Territory2Id).add(ota.ObjectId);
                else
                    accTerrMap.put(ota.Territory2Id, new set<Id>{ota.ObjectId});
                
                accTerrModDateMap.put(ota.ObjectId, ota.LastModifiedDate); 
            }
        }          
        
        
        
        if(terridset.size()>0){
        
            lstatm = [select id,UserId,AccountId,CreatedDate from AccountTeamMember where AccountId in:accidset];
            
            for(AccountTeamMember atm:lstatm){
                if(!AccIdTeamMemMap.containsKey(atm.AccountId))
                    AccIdTeamMemMap.put(atm.AccountId, new Set<AccountTeamMember>{atm});
                else
                    AccIdTeamMemMap.get(atm.AccountId).add(atm);
            }
        
            
            userTerritoryList = [SELECT Id, IsActive,Territory2.Territory2Type.MasterLabel, UserId, Territory2Id,Territory2.Name,RoleInTerritory2 FROM UserTerritory2Association where Territory2Id in :terridset];
            
            
            for(UserTerritory2Association  uta :userTerritoryList){
                Set<id> tempaccset = new set<Id>();
                tempaccset =  accTerrMap.get(uta.Territory2Id);
                for(id accid:tempaccset){
                    if(!AccIdTerrMemMap.containsKey(accid))
                        AccIdTerrMemMap.put(accid, new Set<Id>{uta.UserId});
                    else
                        AccIdTerrMemMap.get(accid).add(uta.UserId);
                }
            }
            
            List<AccountTeamMember> lstTeamtoDelete = new List<AccountTeamMember>();
            //Find the account team members to be deleted
            for(Id acid: AccIdTeamMemMap.keySet()){
                
                if(AccIdTerrMemMap.containsKey(acid)){
                    for(AccountTeamMember atm:AccIdTeamMemMap.get(acid)){
                        //if account team member is not part of users in the territories related to account, then that user should be deleted from the Account
                        if(!AccIdTerrMemMap.get(acid).contains(atm.UserId) && accTerrModDateMap.containsKey(atm.AccountId) && atm.createdDate < accTerrModDateMap.get(atm.AccountId)){
                            lstTeamtoDelete.add(atm); 
                        }
                    }
                    
                }
                
            }
            
            Set<String> lstTeamtoInsert = new Set<String>();
            //Find the account team members to be inserted
            for(Id acid: AccIdTerrMemMap.keySet()){
                
                if(AccIdTeamMemMap.containsKey(acid)){
                    Set<id> tempset = new Set<Id>();
                
                    for(AccountTeamMember atm:AccIdTeamMemMap.get(acid))
                            tempset.add(atm.UserId);
                    
                    for(Id usrid: AccIdTerrMemMap.get(acid)){
                        //if user in territory is not part of users in account team, then that user should be inserted as team member
                        if(!tempset.contains(usrid)){
                            lstTeamtoInsert.add(''+acid+usrid); 
                        }
                    }
                    
                }
                else{
                    for(Id usrid: AccIdTerrMemMap.get(acid)){
                        lstTeamtoInsert.add(''+acid+usrid); 
                    }
                }
                
            }
            
            if(lstTeamtoDelete.size() > 0){
                Database.DeleteResult[] lsr = database.delete(lstTeamtoDelete,false);
                for(Database.DeleteResult sr : lsr)
                    if (!sr.isSuccess()) system.debug('Error occured during user update....'+sr);
            }
            
            //For each user associated to the territory, create an account team member if not already existed
            set<String> dupcheck = new Set<String>();
            for(UserTerritory2Association  uta :userTerritoryList){
                    for(id accid : accTerrMap.get(uta.Territory2Id)){
                        if(!dupcheck.contains(''+accid+uta.userid) && lstTeamtoInsert.contains(''+accid+uta.userid)){
                             dupcheck.add(''+accid+uta.userid);
                             AccountTeamMember atm = new AccountTeamMember();
                             atm.userid = uta.userid;
                             atm.TeammemberRole = uta.Territory2.Territory2Type.MasterLabel+' - '+uta.RoleInTerritory2;
                             atm.accountid = accid;
                             lstAcctTeam.add(atm);  
                        }
                    }
                
            }
            
            if(lstAcctTeam.size()>0){
                Database.SaveResult[] lsr = database.insert(lstAcctTeam,false);
                for(Database.SaveResult sr : lsr)
                    if (!sr.isSuccess()) system.debug('Error occured during user update....'+sr);
            }  
            
        }
    }
    
    //finsih method - Actions TBD once the batch is finished
    Global void finish(Database.BatchableContext BC){
      
    
    }
  
 
}