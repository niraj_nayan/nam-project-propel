public with sharing class Utility {
	
	/**
	 * @param  sObjectName  API name for an sObject
	 * @param  apiFieldName API name for a field on sObject
	 * @return              the next available unique integer
	 */
	public static String createUniqueNumericStringKey(String sObjectName, String apiFieldName){
		if(String.isBlank(sObjectName) || String.isBlank(apiFieldName)){
			return null;
		}

		List<SObject> sObjects;
		String maxValue;

		try{
			String query = 'SELECT MAX(' + apiFieldName + ') From ' + sObjectName;
			sObjects = Database.Query(query);
		}catch(QueryException e){
			System.debug('The following error occured when attempting to query for MAX('+apiFieldName+') on'+sObjectName+'s: '+e.getMessage());
			return null;
		}

		if(!sObjects.isEmpty()){
			maxValue = (String)sObjects.get(0).get(apiFieldName);
			if(maxValue != null && Integer.valueOf(maxValue) >= 0){
				return String.valueOf(Integer.valueOf(maxValue)+1);
			}
		}
		return '1';
	}
}