/**********************************************************************************************************************************************
Description: Utility class to hold the structure to the Credit Profile JSON string          
Created on:  29 September 2015
Created by:  Kavitha

Version 1:   Utility class with Credit Profile JSON string and is being called from AcctCreditProfileExtn Controller
***********************************************************************************************************************************************/
public class CreditProfileUtility{

    public static profileDetails parseProfileFeed(string jsonString) {
        
        JSONParser parser = JSON.createParser(jsonString);
        profileDetails acctProfile ;                   
        while (parser.nextToken() != null) 
        {
            System.debug('parser:'+parser.getText() );
            acctProfile = (profileDetails)parser.readValueAs(profileDetails.class);
        }
        return(acctProfile) ;
    }
    
    public class profileDetails{
        public String id {get; set;}
        public order order {get;set;}
        public List<results> results {get;set;}
        
        public profileDetails(String profileId, order ord, List<results> res)         
        {
            id = profileId;
            If(ord != NULL) {order = ord.clone(); } 
            If(res != NULL) {results = res.clone(); }           
        }
    
    }
    public class order {
        Public String orderId {get;set;} 
        public String orderSystemName {get;set;}
        public Boolean notPersisted {get;set;}
        public String accountId {get;set;}
        public Decimal grandTotalOverride {get;set;}
        public Decimal grandTotal {get;set;}
        public Boolean checkAllReasons {get;set;}
        public Boolean ignoreRepeatingFailures {get;set;}               
    }
    
    public class results{
        Public String resultId {get;set;}
        Public String accountLOC {get;set;}
        public Boolean clientOnHold {get;set;}
        public Boolean prePayRequired {get;set;}
        public Boolean checkFail {get;set;}
        Public Boolean checkRequired {get;set;}
        public List<errors> errors {get;set;}
        public accountLimit accountLimit {get;set;}
        Public Decimal overdueAmount {get;set;}
        Public Decimal totalReceivedCash {get;set;}
        Public Decimal totalNotInvoicedFSIOrders {get;set;}
        Public Decimal totalNotInvoicedAndInVoicedFSIOrders {get;set;}
        Public Decimal totalNotInvoicedInstoreOrders {get;set;}
        Public Decimal totalInvoiceOutstandingAmount {get;set;}
        Public Decimal totalAvailableCredit {get;set;}
        Public Decimal totalAvailableCreditRaw {get;set;}
        Public Boolean checkAllReasons {get;set;}
        Public List<notInvoicedOrders> notInvoicedOrders {get;set;}
        Public List<notInvoicedAndInVoicedOrders> notInvoicedAndInVoicedOrders {get;set;}
        public Map<String, Decimal> dateAgingBuckets {get;set;}       
        
    }
    
    public class errors {
        Public String errorCode {get;set;} 
        public String errorMsg {get;set;}              
    }
    public class accountLimit {
        public Boolean creditCheckRequired {get;set;}
        public Boolean onHold {get;set;}
        Public Decimal orderLimit {get;set;} 
        Public Decimal orderTolerance {get;set;}
        public Decimal overallLimit {get;set;}
        Public Decimal overallTolerance {get;set;}
        public Boolean prePayOnly {get;set;}     
        Public Decimal accountNumber {get;set;}
        Public Decimal accountLocation {get;set;}
        Public Integer overdueGracePeriod {get;set;}
    }
    public class notInvoicedOrders{
        public invoiceOrder order {get;set;}
        public Decimal notInvoicedAmount {get;set;}
        
    }
    public class notInvoicedAndInVoicedOrders{
        public invoiceOrder order {get;set;}
        public Decimal notInvoicedAmount {get;set;}        
    }
    public class invoiceOrder {
        public Integer orderNum {get;set;}
        public Integer orderId {get;set;}
        public String orderSystemName {get;set;}
        public grantedStatus grantedStatus{get;set;}
        public requestedStatus requestedStatus {get;set;}
        public status status {get;set;}
        public String systemStatusCode {get;set;}
        public Boolean notPersisted {get;set;}
        public String accountId {get;set;}
        public Decimal grandTotalOverride {get;set;}
        public Decimal grandTotal {get;set;}
        public Boolean checkAllReasons {get;set;}
        public Boolean ignoreRepeatingFailures {get;set;}
    }
    public class grantedStatus{
        public String id {get;set;}
        public String name {get;set;}
        public string code {get;set;}
    }
    public class requestedStatus{
        public String id {get;set;}
        public String name {get;set;}
        public string code {get;set;}        
    }
    public class status{
        public String id {get;set;}
        public String name {get;set;}
        public string code {get;set;}        
    }    
}