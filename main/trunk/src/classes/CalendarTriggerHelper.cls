/* CalendarTriggerHelper
 * Class to update opportunities related to calendars after update for fiscal year calculation.
 * Author: Stephen Lane
 */
public class CalendarTriggerHelper {
    Static boolean updateRelatedOpportunitiesCheck = FALSE;
    
	public static void updateRelatedOpportunities(List<Calendar__c> calendars){
        if(!updateRelatedOpportunitiesCheck){
            updateRelatedOpportunitiesCheck = TRUE;
            List<Id> calendarIds = new List<Id>();
            for(Calendar__c calendar: calendars){
                calendarIds.add(calendar.Id);
            }
            if(calendarIds <> null){
                List<Opportunity> relatedOpps = [SELECT Id
                                                 FROM Opportunity
                                                 WHERE Calendar_Lookup__c IN :calendarIds];
                update relatedOpps;
            }
        }
    }
}