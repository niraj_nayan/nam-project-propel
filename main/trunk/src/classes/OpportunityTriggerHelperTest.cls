@isTest
private class OpportunityTriggerHelperTest {
    
    @isTest static void TestInsertOpportunity() {
        // Implement test code
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'U1';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        secondUser.FederationIdentifier = 'U2';
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = 'Merch Sales Rep');
            insert accTeamMember;

            AccountTeamMember accTeamMember2 = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = 'SSD Sales Rep');
            insert accTeamMember2;

            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, Label.OppRecordTypeMerchandising);
            testOppty.Amount = 100;
            insert testOppty;

            //Opportunity testOppty2 = TestDataUtils.createOppty('Test Oppty2', 'Contract', div1.Id, 'SmartSource Direct');
            //testOppty2.Amount = 100;
            //insert testOppty2;


            OpportunityTeamMember testMember = TestDataUtils.createTeamMember(user1.Id, 'Manager', testOppty.Id, 10);
            insert testMember;

            Product2 prod = TestDataUtils.createProduct2('Test Prod');
            insert prod;

            //CREATE PRICEBOOK ENTRY RECORD
            PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
            insert priceEntry;

            //ADD OPPORTUNITY LINE ITEM
            OpportunityLineItem testProduct = TestDataUtils.createOpportunityLineItem('Test ProdLine', 'Test Pricing Detail', 150, 10, testOppty.Id, priceEntry.Id);
            insert testProduct;

            //testOppty.Owner = user1;
            //update testProduct;

            //delete testOppty;

            Opportunity testAssertOpp = [Select Id, Name From Opportunity Where Id =:testOppty.Id];
            System.assertEquals('Test Oppty', testAssertOpp.Name);

        }
    }
    
    @isTest static void TestOwnerChange() {
        // Implement test code

        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'U1';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        secondUser.FederationIdentifier = 'U2';
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = Label.userAssociationTriggerRegionalManager+'- Digital');
            insert accTeamMember;
            //Want to insert digital opportunity with sales rep
            Opportunity testOpptyWith = TestDataUtils.createOppty('Test Oppty3', 'Contract', div1.Id, Label.OppRecordTypeDigital);
            testOpptyWith.Amount = 100;
            testOpptyWith.ownerid = user1.id;
            insert testOpptyWith;

            testOpptyWith.ownerid = seconduser.id;
            update testOpptyWith;

            Opportunity testAssertOpp = [Select Id, Name, Owner.Id From Opportunity Where Id =:testOpptyWith.Id];
            System.assertEquals(secondUser.Id, testAssertOpp.Owner.Id);
        }
    }



    @isTest static void TestInsertSSDOpportunity() {
        // Implement test code

        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'U1';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        secondUser.FederationIdentifier = 'U2';
        insert secondUser;
        User ThirdUser = TestDataUtils.createUser('third User', 'no-reply@gmail.com', '123123456');
        thirdUser.FederationIdentifier = 'U3';
        insert thirduser;
        
        Id divisiontypeid;
        Id regiontypeid;
        id teamtypeid;
        
        List<Territory2Type> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerrTypeTestData');
        //[SELECT id, DeveloperName,MasterLabel from Territory2Type]
        for (Territory2Type te: listTerritoryType){
            if(te.MasterLabel == 'TestDivision')
                divisiontypeid = te.id;
            else if(te.MasterLabel == 'TestRegion')
                Regiontypeid = te.id;
            else if(te.MasterLabel == 'TestTeam')
                Teamtypeid = te.id;
        }
        
        //id modelid = [select id from Territory2Model where name='NAM Sales Hierarchy'].id;
        Territory2Model terrModel = new Territory2Model();
        terrModel .DeveloperName='TestNAMModelName'; // required field
        terrModel.Name = 'Name'; // required field
        insert terrModel ;
    
        Territory2 terr = new Territory2();
        terr.DeveloperName = 'Division';
        terr.Name ='Division';
        terr.Territory2TypeId = divisiontypeid;
        terr.Territory2ModelId = terrModel.id;
        insert terr;
        
        Territory2 terr2 = new Territory2();
        terr2.DeveloperName = 'Regional';
        terr2.Name ='Regional';
        terr2.Territory2TypeId = Regiontypeid;
        terr2.Territory2ModelId = terrModel.id;
        terr2.ParentTerritory2Id =terr.id;
        insert terr2;
        
        Territory2 terr3 = new Territory2();
        terr3.DeveloperName = 'Team';
        terr3.Name ='Team';
        terr3.Territory2TypeId = Teamtypeid;
        terr3.Territory2ModelId = terrModel.id;
        terr3.ParentTerritory2Id = terr2.id;
        insert terr3;
        
        UserTerritory2Association u2a = new UserTerritory2Association();
        u2a.userid = user1.id;
        u2a.territory2id = terr.id;
        u2a.RoleInTerritory2 = Label.userAssociationTriggerDivisionManager;
        insert u2a;
        
        UserTerritory2Association u2a2 = new UserTerritory2Association();
        u2a2.userid = seconduser.id;
        u2a2.territory2id = terr2.id;
        u2a2.RoleInTerritory2 = Label.userAssociationTriggerRegionalManager+'-'+Label.OppRecordTypeSSD;
        insert u2a2;
        
        UserTerritory2Association u2a3 = new UserTerritory2Association();
        u2a3.userid = thirduser.id;
        u2a3.territory2id = terr3.id;
        u2a3.RoleInTerritory2 = Label.PrimaryRoletoShareGoal;
        insert u2a3;
        
        
        
        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;
            
            SpecialtyTeamRoles__c stp = new SpecialtyTeamRoles__c();
            stp.name = '1';
            stp.ProductLine__c = Label.OppRecordTypeSSD;
            stp.TeamMemberRole__c = 'Account Director-'+Label.OppRecordTypeSSD;
            insert stp;
            
            SpecialtyTeamRoles__c stp2 = new SpecialtyTeamRoles__c();
            stp2.name = '2';
            stp2.ProductLine__c = Label.OppRecordTypeSSD;
            stp2.TeamMemberRole__c = 'Regional Manager-'+Label.OppRecordTypeSSD;
            insert stp2;
            
            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div1;
            
            Account div2 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div2;
            
            ObjectTerritory2Association ot = new ObjectTerritory2Association();
            ot.ObjectId = div1.id;
            ot.AssociationCause = 'Territory2Manual';
            ot.Territory2Id = terr3.id;
            //ot.SobjectType = 'Account';
            insert ot;
            
            ObjectTerritory2Association ot2 = new ObjectTerritory2Association();
            ot2.ObjectId = div2.id;
            ot2.Territory2Id = terr3.id;
            ot2.AssociationCause = 'Territory2Manual';
            //ot2.SobjectType = 'Account';
            insert ot2;
            
            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole ='Regional Manager-'+Label.OppRecordTypeSSD);
            insert accTeamMember;
            
            AccountTeamMember accTeamMember2 = new AccountTeamMember(
                UserId = seconduser.Id,
                AccountId = div2.Id,
                TeamMemberRole = 'Account Director-'+Label.OppRecordTypeSSD);
            insert accTeamMember2;
            //Want to insert digital opportunity with sales rep
            Opportunity testOpptyWith = TestDataUtils.createOppty('Test Oppty3', 'Contract', div1.Id, Label.OppRecordTypeSSD);
            testOpptyWith.Amount = 100;
            testOpptyWith.ownerid = user1.id;
            insert testOpptyWith;

            Opportunity testAssertOpp = [Select Id, Name, Owner.Id From Opportunity Where Id =:testOpptyWith.Id];
            System.assertEquals(user1.Id, testAssertOpp.Owner.Id);
            delete testOpptyWith;
        }
    }
 
    static testMethod void testAddNewProduct() {
       //Create a Corp Account record
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'U1';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        secondUser.FederationIdentifier = 'U2';
        insert secondUser;

        LockOpportunityFields__c lof = new LockOpportunityFields__c();
        lof.name='Testlof';
        lof.field_name__c = 'Amount';
        lof.record_Type_Name__c = 'FSI';
        insert lof;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            //div1.Finance_Verified__c = true;
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = corp1.Id);
            insert accTeamMember;
            
            Cycle__c c = new Cycle__c();
            c.name = 'Test Cycle';
            c.Begin_Date__c = system.today();
            c.Week__c = 1;
            c.End_Date__c = system.today()+30;
            insert c;
            
            Cycle__c c2 = new Cycle__c();
            c2.name = 'Test Cycle';
            c2.Begin_Date__c = system.today();
            c2.Week__c = 2;
            c2.End_Date__c = system.today()+30;
            insert c2;
            
            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, Label.OppRecordTypeFSI);
            testOppty.CurrencyISOCode = 'USD';
            testOppty.insert_date__c = system.today();
            testOppty.StageName = 'Client Engagement';
            insert testOppty;
            
            //Opportunity testOppty2 = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'SmartSource Direct');
            //testOppty2.CurrencyISOCode = 'USD';
            //insert testOppty2;
            
            Product_Line__c testProductLine = TestDataUtils.createProductLine('FSI');
            insert testProductLine;
            
            Product_Line__c testProductLine2 = TestDataUtils.createProductLine('Instore');
            insert testProductLine2;
            
            Product2 prod = TestDataUtils.createProduct2('Test Prod');
            insert prod;

            //CREATE PRICEBOOK ENTRY RECORD
            PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
            insert priceEntry;

            //Create product junction object
            //Product_Junction__c testProdJunc = TestDataUtils.createProductJunction(prod.Id, testProductLine.Id);
            //insert testProdJunc;
            
            
            Product2 prod2 = TestDataUtils.createProduct2('Test SSD Prod');
            insert prod2;

            //CREATE PRICEBOOK ENTRY RECORD
            PricebookEntry priceEntry2 = TestDataUtils.createPriceBookEntry(10000, prod2.Id);
            insert priceEntry2;

            //Create product junction object
           // Product_Junction__c testProdJunc2 = TestDataUtils.createProductJunction(prod2.Id, testProductLine2.Id);
            //insert testProdJunc2;
            
            //Create NAM rate card
            
            
           Charge_Type__c ct = new Charge_Type__c();
            ct.Commissionable__c = true;
            ct.Line_of_Business__c = testProductLine.id;
            ct.Charge_Type_Categorty__c = 'Space';
            insert ct;
            
             Charge_Type__c ct2 = new Charge_Type__c();
            ct2.Commissionable__c = true;
            ct2.Line_of_Business__c = testProductLine2.id;
            ct2.Charge_Type_Categorty__c = 'Production';
            insert ct2;
            
            OpportunityTeamMember ot = new OpportunityTeamMember();
            ot.userid = user1.id;
            ot.OpportunityId = testOppty.id;
            ot.Commission_Percentage__c = 25;
            insert ot;
            
           OpportunityLineItem oli = new OpportunityLineItem();
            oli.TotalPrice =  1200;
            oli.Charge_Type__c  = ct.id;
            oli.Commissionable__c = true;
            oli.pricebookentryid = priceEntry.id;
            oli.opportunityid = testOppty.id;
            oli.Quantity = 2;
            insert oli;
           
           Opportunity testAssertOpp = [Select Id, Name, Owner.Id From Opportunity Where Id =:testOppty.Id];
            System.assertEquals('Test Oppty', testAssertOpp.Name);
             
        }

        
    }
    
    @isTest static void testNotLostStatus() {
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'U1';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        secondUser.FederationIdentifier = 'U2';
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = 'Merch Sales Rep');
            insert accTeamMember;

            AccountTeamMember accTeamMember2 = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = 'SSD Sales Rep');
            insert accTeamMember2;

            OpportunityStagesPerStatus__c setting = new OpportunityStagesPerStatus__c();
            setting.Name = 'Sold';
            setting.Corresponding_Stage__c = 'Closed Won';
            setting.Is_Lost__c = false;
            insert setting;

            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, Label.OppRecordTypeMerchandising);
            testOppty.Amount = 100;
            testOppty.Status__c = 'Sold';
            insert testOppty;
            testOppty = [SELECT Name, StageName, Lost_Reason__c FROM Opportunity WHERE Name = 'Test Oppty'];
            System.assertEquals('Closed Won', testOppty.StageName);
            System.assertEquals(true, String.isEmpty(testOppty.Lost_Reason__c));
        }
    }

    @isTest static void testLostStatus() {
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'U1';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        secondUser.FederationIdentifier = 'U2';
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = 'Merch Sales Rep');
            insert accTeamMember;

            AccountTeamMember accTeamMember2 = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = 'SSD Sales Rep');
            insert accTeamMember2;

            OpportunityStagesPerStatus__c setting = new OpportunityStagesPerStatus__c();
            setting.Name = 'Cancelled Reserved';
            setting.Corresponding_Stage__c = 'Closed Lost';
            setting.Is_Lost__c = true;
            insert setting;

            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, Label.OppRecordTypeMerchandising);
            testOppty.Amount = 100;
            testOppty.Status__c = 'Cancelled Reserved';
            insert testOppty;
            testOppty = [SELECT Name, StageName, Lost_Reason__c FROM Opportunity WHERE Name = 'Test Oppty'];
            System.assertEquals('Closed Lost', testOppty.StageName);
            System.assertEquals(false, String.isEmpty(testOppty.Lost_Reason__c));
        }
    }

    @isTest static void testExpiredStatusUpdate() {
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'U1';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
        secondUser.FederationIdentifier = 'U2';
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = 'Merch Sales Rep');
            insert accTeamMember;

            AccountTeamMember accTeamMember2 = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = div1.Id,
                TeamMemberRole = 'SSD Sales Rep');
            insert accTeamMember2;

            OpportunityStagesPerStatus__c setting = new OpportunityStagesPerStatus__c();
            setting.Name = 'Expired';
            setting.Corresponding_Stage__c = 'Closed Lost';
            setting.Is_Lost__c = true;
            insert setting;

            //then an opportunity with lookup to the account
            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, Label.OppRecordTypeMerchandising);
            testOppty.Amount = 100;
            testOppty.Status__c = 'Expired';
            insert testOppty;
            testOppty = [SELECT Name, StageName, Lost_Reason__c FROM Opportunity WHERE Name = 'Test Oppty'];
            System.assertEquals('Closed Lost', testOppty.StageName);
            System.assertEquals(false, String.isEmpty(testOppty.Lost_Reason__c));
            testOppty.Status__c = 'Reserved - RS1';
            update testOppty;
            testOppty = [SELECT Name, StageName, Status__c, Lost_Reason__c, Amount FROM Opportunity WHERE Name = 'Test Oppty'];
            System.assertEquals('Reserved - RS1', testOppty.Status__c);
            System.assertEquals('Closed Lost', testOppty.StageName, 'Not correct; Opp stage should be Client Engagement and works properly in org testing');
        }
    }
}