@isTest
private class ParentOpportunityCreatorTest {
	
	@isTest static void test_method_one() {
		// Implement test code
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		user1.FederationIdentifier = 'tLastName';
		insert user1;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		user1.FederationIdentifier = 'secondUser2';
		insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            corp1.Site = '-42';
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            //div1.Finance_Verified__c = true;
            //div1.Site = '-42';
            div1.CurrencyIsoCode = 'CAD';
            insert div1;
            System.debug('Account Currency Code: ' + div1.CurrencyIsoCode);

            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'InStore');
            testOppty.CurrencyISOCode = 'CAD';
            insert testOppty;

            List<Opportunity> testOppList = [Select Id, Name From Opportunity];
            System.assertEquals(testOppList.size(), 2);
        }
	}
	
}