/****************************************************************************************************************************
Description: Helper class for Goal Share Trigger. 
             Whenever a goal record is inserted/update, get the territories of the goal owner and their parent territories
             and share the goal with all the users belonging to those territories. 
             
Created on:  12th October 2015
Created by:  Kavitha
Version 1:   Created class to share the goal record.
Version 2:   Jagan: Refactored the code and completed the requirement
Version 3:   Jagan: Updated code to use "Ownerid" instead of "Assigned To" of Goal record
Version 4:   Jagan: Added new method to map assigned to field to owner.
Version 5:   Jagan: added new method to update territory hierarchy of owner
*******************************************************************************************************************************/

public class Goal_record_share_helper {

    Set<Id> useridset = new Set<Id>();
    Set<Id> updateduseridset = new Set<Id>();
    Map<Id,Id> userterrmap = new Map<Id,Id>();
    Set<Id> userstoshare = new Set<Id>();
    Map<id,set<UserTerritory2Association>> allterrusermap = new Map<id,set<UserTerritory2Association>>();
    Map<Id,Set<Id>> supervisormap = new Map<Id,Set<Id>>();
    List<Goal__Share> goal_Shares = new List<Goal__Share>();
    Map<Id,Id> terrGroupMap = new Map<Id,Id>();
    public boolean isFormHeaderShare = FALSE;
    public static boolean shareGoalsCheck = TRUE;
    public static boolean batchUpdate = FALSE;
    public void beforeInsertSourceKeyGenerate(List<Goal__c> lstGoal){

        integer counter = 0;
        for(Goal__c insertGoal: lstGoal){
            if(insertGoal.Source_Key__c == null){
                DateTime current = System.now();
                counter += 10;
                insertGoal.Source_Key__c = sourceKeyGenerationUtility.generateSourceKey('G', current.getTime() + counter);  
            }
        }
    }
    
    /***********************************************************************************************
    Method to update territory hierarchy fields of opp whenever the opportunity is inserted or updated
    ************************************************************************************************/
    public void beforeinsertupdateCopyTerritoryFields(List<Goal__c> triggernew,Map<Id,Goal__c> triggernewmap,Map<Id,Goal__c> triggeroldmap){
        
        Set<Id> owneridset = new Set<id>();
        set<string> fedidset = new set<String>();
        Set<Id> accidSet = new Set<Id>();
        for(Goal__c g: triggernew){
            
             if(Label.ForceUpdateHierarchy == Label.TrueValue){
                    owneridset.add(g.ownerid);
                    if(g.Account__c <> null)
                        accidSet.add(g.Account__c);
                    if(Label.DataMigration == Label.TrueValue){
                        fedidset.add(g.Division_Manager__c); 
                        fedidset.add(g.Region_Manager__c); 
                        fedidset.add(g.Team_Manager__c );
                        fedidset.add(g.Primary_Sales_Rep__c); 
                    }
             }
        
            if(triggeroldmap == null || batchUpdate || (g.Frozen_Hierarchy__c == null && triggeroldmap<>null && triggernewmap<>null && triggernewmap.get(g.id).ownerid <> triggeroldmap.get(g.id).ownerid)){
                if(triggeroldmap == null || ((g.Frozen_Hierarchy__c == null && g.effective_Date__c<>null && system.today() <= g.effective_Date__c) || (g.Cycle_Begin_Date__c<>null && system.today() <= g.Cycle_Begin_Date__c))){
                    owneridset.add(g.ownerid);
                    if(g.Account__c <> null)
                        accidSet.add(g.Account__c);
                    if(Label.DataMigration == Label.TrueValue){
                        fedidset.add(g.Division_Manager__c); 
                        fedidset.add(g.Region_Manager__c); 
                        fedidset.add(g.Team_Manager__c );
                        fedidset.add(g.Primary_Sales_Rep__c); 
                    }
                    
                }
            }
            if(triggeroldmap != null && (triggernewmap.get(g.id).account__c <> triggeroldmap.get(g.id).account__c) && ((g.effective_Date__c<>null && system.today() <= g.effective_Date__c) || (g.Cycle_Begin_Date__c<>null && system.today() <= g.Cycle_Begin_Date__c))){
                g.Account_Sales_Office_Text__c = g.Account_Sales_Office__c;
            }
            
        }
        if(owneridset.size()>0){
            
            Map<Id,Account> accidMap = new Map<Id,Account>();
            if(accidSet.size()>0)
                accidMap = new Map<Id,Account>([select id,name,ownerid,Account_Sales_Office__c,Primary_Reps_Territory__c,Primary_Rep_Territory_Id__c,Division_Manager__c,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,
                                        Team_Territory_Id__c,Region_Name_Frozen__c,Region_Manager__c,Team_Manager__c,Team_Name__c from Account where id in :accidset]);
                                        
            Map<Id,user> usermap = new Map<Id,User>([select id,FederationIdentifier,Assigned_Primary_Territory_Name__c,Assigned_Primary_TerritoryId__c,Division_Manager__c,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,Region_Name__c,Region_Manager__c,Team_Manager__c,Team_Name__c from user where id in:owneridset  or federationIdentifier in:fedidset]);
            Map<String,Id> fedUserIdMap = new Map<String,Id>();
            for(User u:usermap.values())
                fedUserIdMap.put(u.FederationIdentifier, u.id);              
            
            for(Goal__c g: triggernew){
                if(usermap.containsKey(g.ownerid) && (g.Account__c == null || (g.Account__c <> null && accidMap.containsKey(g.Account__c) && accidMap.get(g.Account__c).ownerid != g.ownerid))){
                    if(Label.DataMigration != Label.TrueValue)
                        assignHierarchyFields(g,usermap);
                    else
                        assignHierarchyFields(g,fedUserIdMap);
                } 
                else if(usermap.containsKey(g.ownerid) && g.Account__c <> null && accidMap.containsKey(g.Account__c) && accidMap.get(g.Account__c).ownerid == g.ownerid){
                    if(Label.DataMigration != Label.TrueValue)
                        assignHierarchyFields(g,accidMap.get(g.Account__c));
                    else
                        assignHierarchyFields(g,fedUserIdMap);
                }    
                                           
            }
        }
    }
    
     public static void assignHierarchyFields(Goal__c g,Account acc){
        g.Primary_Reps_Territory__c = acc.Primary_Reps_Territory__c;
        g.Primary_Rep_Territory_Id__c = acc.Primary_Rep_Territory_Id__c;
        g.Division_Manager__c = acc.Division_Manager__c;
        g.Division_Name__c = acc.Division_Name__c;
        
        if(acc.Region_Manager__c != null)
            g.Region_Manager__c = acc.Region_Manager__c;
        else
            g.Region_Manager__c = acc.Division_Manager__c;
            
        g.Region_Name__c = acc.Region_Name_Frozen__c;
        g.Account_Sales_Office_Text__c = acc.Account_Sales_Office__c;
        
        if(acc.Team_Manager__c != null)
            g.Team_Manager__c = acc.Team_Manager__c;
        else
            g.Team_Manager__c = g.Region_Manager__c;
            
        g.Team_Name__c = acc.Team_Name__c;
        g.Primary_Sales_Rep__c = g.ownerid;
        g.Division_Territory_Id__c =acc.Division_Territory_Id__c;
        g.Region_Territory_Id__c = acc.Region_Territory_Id__c;
        g.Team_Territory_Id__c = acc.Team_Territory_Id__c;
    }
     public static void assignHierarchyFields(Goal__c g,Map<id,User> usermap){
         g.Primary_Reps_Territory__c = usermap.get(g.ownerid).Assigned_Primary_Territory_Name__c;
         g.Primary_Rep_Territory_Id__c = usermap.get(g.ownerid).Assigned_Primary_TerritoryId__c; 
         g.Division_Manager__c = usermap.get(g.ownerid).Division_Manager__c;
         g.Division_Name__c = usermap.get(g.ownerid).Division_Name__c;
         
         if(usermap.get(g.ownerid).Region_Manager__c != null)
             g.Region_Manager__c = usermap.get(g.ownerid).Region_Manager__c;
         else
             g.Region_Manager__c = usermap.get(g.ownerid).Division_Manager__c;
             
         g.Region_Name__c = usermap.get(g.ownerid).Region_Name__c;
         g.Account_Sales_Office_Text__c = g.Account_Sales_Office__c;
         
         if(usermap.get(g.ownerid).Team_Manager__c != null)
             g.Team_Manager__c = usermap.get(g.ownerid).Team_Manager__c;
         else
             g.Team_Manager__c = g.Region_Manager__c;
             
         g.Team_Name__c = usermap.get(g.ownerid).Team_Name__c;
         g.Primary_Sales_Rep__c = g.ownerid;
         g.Division_Territory_Id__c = usermap.get(g.ownerid).Division_Territory_Id__c;
         g.Region_Territory_Id__c = usermap.get(g.ownerid).Region_Territory_Id__c;
         g.Team_Territory_Id__c = usermap.get(g.ownerid).Team_Territory_Id__c;
    }    
    //FOR DATA MIGRATION PURPOSE
    public static void assignHierarchyFields(Goal__c g,Map<String,Id> fedUserIdMap){
        
        if(fedUserIdMap.containsKey(g.Division_Manager__c))
            g.Division_Manager__c = fedUserIdMap.get(g.Division_Manager__c);
            
        if(fedUserIdMap.containsKey(g.Region_Manager__c))
            g.Region_Manager__c = fedUserIdMap.get(g.Region_Manager__c);
            
        if(fedUserIdMap.containsKey(g.Team_Manager__c))
            g.Team_Manager__c = fedUserIdMap.get(g.Team_Manager__c);
            
        if(fedUserIdMap.containsKey(g.Primary_Sales_Rep__c))
            g.Primary_Sales_Rep__c = fedUserIdMap.get(g.Primary_Sales_Rep__c);
        
    }
    
    //map the assigned to to owner of the goal   
    public void beforeinsertupdategoalowner(List<Goal__c> lstGoal){
        for(Goal__c objGoal: lstGoal){
            if(objGoal.assigned_to__c <>null)
                objGoal.ownerid = objGoal.assigned_to__c;
        }
    }
    public void shareGoals(Map<Id,Goal__c> TriggerNewMap,Map<Id,Goal__c> TriggerOldMap ){
        if(Goal_record_share_helper.shareGoalsCheck == FALSE){
            Goal_record_share_helper.shareGoalsCheck = true;
        for(id g:TriggerNewMap.keyset()){
            if(TriggerOldMap == null)
                useridset.add(TriggerNewMap.get(g).ownerid);
            else{
                //if the owner is changed for a goal
                if(batchUpdate || TriggerNewMap.get(g).ownerid <> TriggerOldMap.get(g).ownerid)
                    updateduseridset.add(TriggerNewMap.get(g).ownerid);
            }
        }
        
        //share goals for new members
        if(useridset.size()>0)
            shareGoals(useridset,TriggerNewMap);
        else if(updateduseridset.size()>0){
            //clear the sharerecord of old user
            clearSharerecords(TriggerOldMap.keySet());
            shareGoals(updateduseridset,TriggerNewMap);
        }
        }
        
    }
    
    //code to delete previous shares
    public void clearSharerecords(Set<Id> deletegoalsset){
    
        List<Goal__share> lstshare = new LIst<Goal__share>();
        if(deletegoalsset.size()>0){
            lstshare = [select id,parentid from Goal__share where parentid in: deletegoalsset];
            if(lstshare.size()>0)
                database.delete(lstshare,false);
        }
    }
    
    public void shareGoals(Set<id> useridset,Map<Id,Goal__c> TriggerNewMap){
        
        if(useridset.size()>0){
            
            //Query all groups where type is territory. This is used while creating share record
            for(Group gr: [Select id,DeveloperName,RelatedId,Type from Group where Type='Territory']){
                terrGroupMap.put(gr.relatedid, gr.id);
                //system.debug('Territory id...'+gr.relatedid +'...Group id...'+gr.id);
            }
            
            //query all user territory association record wherever the user is part of
            List<UserTerritory2Association> userTerritoryList = new List<UserTerritory2Association>();
            userTerritoryList = [SELECT Id, IsActive, UserId, Territory2Id,Territory2.ParentTerritory2Id, Territory2.Name,RoleInTerritory2 FROM UserTerritory2Association where UserId in :useridset];
            
            if(userTerritoryList.size()>0){
                for(UserTerritory2Association u:userTerritoryList){
                    if(useridset.contains(u.userid) && u.RoleInTerritory2 == Label.PrimaryRoletoShareGoal)//If user is in multiple territories then filter only if he/she is role "Primary"
                        userterrmap.put(u.userid,u.Territory2Id);
                }
                
                //Get the list of parent territory names for each corresponding user assigned to Goal
                //Removed allterrusermap as a parameter
                supervisormap  = GetAllParentElements(userterrmap);
                
                //system.debug('users with their supervisors...'+supervisormap);
    
                for(id gid:TriggerNewMap.keyset()){
                
                    id assignedTo = TriggerNewMap.get(gid).ownerid;
                    system.debug('Owner of the goal...'+assignedTo);
                    if(!supervisormap.isEmpty() && assignedTo<>null && supervisormap.containsKey(assignedTo)){
                        for(Id parid:supervisormap.get(assignedTo)){
                            //system.debug('parent id...'+parid+'..is it in main map...'+terrGroupMap.containsKey(parid));
                            //if(terrGroupMap.containsKey(parid)){
                                Goal__Share objGs = new Goal__Share();
                                objGs.ParentId = gid;
                                objGs.AccessLevel = 'Read';
                                objGs.RowCause = 'Manual';
                                //get the corresponding group id from territory and group map
                                //objGs.UserOrGroupId = terrGroupMap.get(parid);
                                objGs.UserOrGroupId = parid;
                                goal_Shares.add(objGs);
                            //}
                        }
                        
                    }
                }
                system.debug('Goals tobe shared..'+goal_Shares);
                if(goal_Shares.size()>0){
                    Database.SaveResult[] lsr = database.Insert(goal_Shares,false);
                    for(Database.SaveResult sr : lsr)
                        if (!sr.isSuccess()) system.debug('Error occured during Form header share records insert....'+sr);
                }        
            }
        }
    }
    
    //In the below method, the first parameter can be removed when the goal is shared with territory. This was kept when initially it was decided to share with users in territory
    //Map<id,set<UserTerritory2Association>> allterrusermap,
    public Map<Id,Set<Id>> GetAllParentElements(Map<Id,Id> userterrmap){
        
        Map<Id,Set<Id>> allusermap = new Map<Id,Set<Id>>();
        Map<Id,Territory2> TerrIdTerrMap = new Map<Id,Territory2>();
        
        Map<id,set<UserTerritory2Association>> allterrusermap = new Map<id,set<UserTerritory2Association>>();
        
        //Query all userTerritory Associations in the system
        List<UserTerritory2Association> userTerritoryList = [SELECT Id, IsActive, UserId, user.Name, user.Username, user.IsActive, Territory2Id,Territory2.Name,RoleInTerritory2 FROM UserTerritory2Association];
            
        //Based on the above query, frame map to have territory id and respective users of the territory
        for(UserTerritory2Association terr : userTerritoryList) {
            if(allterrusermap.containsKey(terr.Territory2Id))
                allterrusermap.get(terr.Territory2Id).add(terr);
            else
                allterrusermap.put(terr.Territory2Id,new set<UserTerritory2Association>{terr});
        }    
        
        //Query all territories and put in map with territory name as key and territory as value
        for( Territory2 terr: [SELECT Id, Name, ParentTerritory2Id,ParentTerritory2.Name, Territory2Type.MasterLabel,Territory2ModelId, Territory2TypeId FROM Territory2 ]){
            TerrIdTerrMap.put(terr.id,terr);
        }
        
        for(Id uid:userterrmap.keyset()){
            
            Id terrid = userterrmap.get(uid);
            Id Parterrid;
            Set<Id> parterrset = new Set<Id>();
            do{
                
                //add the parent territory to the set
                //if(Parterrid<>null)
                //    parterrset.add(Parterrid);
                //Get the parent territory id from the territory id map which has all territories
                Parterrid = TerrIdTerrMap.get(terrid).ParentTerritory2Id;
                Set<UserTerritory2Association> allusersinthisterr = new set<UserTerritory2Association>();
                allusersinthisterr = allterrusermap.get(Parterrid);    
                if(allusersinthisterr <> null){
                    for(UserTerritory2Association u2a:allusersinthisterr){
                        //For goals, share only with Primary user
                        if(!isFormHeaderShare && u2a.RoleInTerritory2 <> null && (u2a.RoleInTerritory2.containsIgnoreCase(Label.userAssociationTriggerPrimaryRole) || u2a.RoleInTerritory2.containsIgnoreCase(Label.PrimarySplitRole))){
                            parterrset.add(u2a.userid);
                        }
                        //For form header, share with all users
                        else   
                            parterrset.add(u2a.userid);
                    }
                }
                
                terrid = Parterrid;
            
            }while(Parterrid<>null);
            
            //add the set of parent territories to map
            if(parterrset.size()>0)
                allusermap.put(uid,parterrset); 
        }
        //return map which has list of user ids and corresponding parent territory ids
        return allusermap;
    }

}