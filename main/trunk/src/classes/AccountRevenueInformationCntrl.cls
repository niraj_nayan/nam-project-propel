/*
SFIT-787 :- Non owner can see All financial details of Account

*/

public class AccountRevenueInformationCntrl {
    
    List<AccountTeamMember> lstAcctTeam = new List<AccountTeamMember>();
    public Account acc {get;set;}
    public revenueInfo objRi {get;set;}
    public boolean isVisible {get;set;}
    ApexPages.StandardController stdCtrl {get;set;}
    public boolean showError {Get;set;}
    
    public AccountRevenueInformationCntrl(ApexPages.StandardController controller) {
        stdCtrl=controller;
        acc = (Account)controller.getRecord();
        objRi = new revenueInfo();
        isVisible = FALSE;
        isEdit = FALSE;
        showError = FALSE;
        acc = [Select Id,Name,Roll_Up_Revenue__c,Proposed_Full_Page_Rate__c,Proposed_Half_Page_Rate__c,Annual_NAM_NEPs__c,NEP_Roll_Up__c,Full_Page_Rate__c,Half_Page_Rate__c,
               Annual_VNEPs__c,Annual_Corporate_NAM_NEPs__c,NEP_Roll_UP_from_Corporate__c,Full_Page_Rate_from_Corporate__c,Half_Page_Rate_from_Corporate__c,
               Proposed_Rate_from_Corporate__c,Annual_V_NEPs_from_Corporate__c from Account where id=:acc.id];
        getRevenueDetails();
    }
    public void getRevenueDetails(){
    
        Set<Id> useridset = new Set<Id>();
        lstAcctTeam = [select userid from accountteammember where accountid =:acc.id];
        
        for(AccountTeamMember atm:lstAcctTeam)
            useridset.add(atm.userid);
        
        //query all user territory association record wherever the user is part of
        List<UserTerritory2Association> userTerritoryList = new List<UserTerritory2Association>();
        Map<Id,Set<Id>> supervisormap = new Map<Id,Set<Id>>();
        Map<Id,Id> userterrmap = new Map<Id,Id>();
        Set<Id> allusersinhierarchy = new Set<Id>();
        allusersinhierarchy.addAll(useridset); 
        
        if(useridset.size() >0 ) {
            
            userTerritoryList = [SELECT Id, IsActive, UserId, Territory2Id,Territory2.ParentTerritory2Id, Territory2.Name,RoleInTerritory2 FROM UserTerritory2Association where UserId in :useridset];
            for(UserTerritory2Association u:userTerritoryList){
                if(useridset.contains(u.userid) && u.RoleInTerritory2 == Label.PrimaryRoletoShareGoal)
                    userterrmap.put(u.userid,u.Territory2Id);
            }
            
            //Get the list of parent territory names for each corresponding user in Account team member
            Goal_record_share_helper objGrecsh = new Goal_record_share_helper();
            objGrecsh.isFormHeaderShare = TRUE;
            supervisormap  = objGrecsh.GetAllParentElements(userterrmap);
            
            
            if(supervisormap != null) {
                for(id uid:supervisormap.keyset())
                allusersinhierarchy.addAll(supervisormap.get(uid));
            }
            
            String profName =[select profile.name from user where id=:userinfo.getUserId()].profile.name;
          
            if((allusersinhierarchy.size() > 0 && allusersinhierarchy.contains(userinfo.getUserId())) || (profName == 'Finance' || profName == 'Sales Ops' || profName == 'Integration' || profName == 'System Administrator') ){    
                isVisible = TRUE;
                objRi.RollUpRevenue = acc.Roll_Up_Revenue__c;
                objRi.AnnualNAMNEPs = acc.Annual_NAM_NEPs__c;
                objRi.NEPRollUp = acc.NEP_Roll_Up__c;
                objRi.FullPageRate = acc.Full_Page_Rate__c;
                objRi.HalfPageRate = acc.Half_Page_Rate__c;
                objRi.ProposedFullPageRate = acc.Proposed_Full_Page_Rate__c;
                objRi.ProposedHalfPageRate = acc.Proposed_Half_Page_Rate__c;
                objRi.AnnualVNEPs = acc.Annual_VNEPs__c;
                objRi.AnnualCorporateNAMNEPs = acc.Annual_Corporate_NAM_NEPs__c;//formula
                objRi.NEPRollUpfromCorporate = acc.NEP_Roll_UP_from_Corporate__c;//formula
                objRi.FullPageRatefromCorporate = acc.Full_Page_Rate_from_Corporate__c;//formula
                objRi.HalfPageRatefromCorporate = acc.Half_Page_Rate_from_Corporate__c;//formula
                objRi.ProposedRatefromCorporate = acc.Proposed_Rate_from_Corporate__c;//formual
                objRi.AnnualVNEPsfromCorporate = acc.Annual_V_NEPs_from_Corporate__c;//formula
            }
            else{
                showError = TRUE;
                
            }    
        
        } 
        
    }
    public Boolean refreshPage {get; set;}
    public boolean isEdit {get;set;}
    
    public void setEditFlag(){
        isEdit = TRUE;
    }
    public PageReference saveAccount(){
         refreshPage=true;
         isEdit = FALSE;
         acc.Roll_Up_Revenue__c = objRi.RollUpRevenue;
         acc.Annual_NAM_NEPs__c = objRi.AnnualNAMNEPs;
         acc.NEP_Roll_Up__c = objRi.NEPRollUp;
         acc.Full_Page_Rate__c = objRi.FullPageRate;
         acc.Half_Page_Rate__c = objRi.HalfPageRate;
         acc.Proposed_Full_Page_Rate__c = objRi.ProposedFullPageRate;
         acc.Proposed_Half_Page_Rate__c = objRi.ProposedHalfPageRate;
         acc.Annual_VNEPs__c = objRi.AnnualVNEPs ;
         update acc;
         return null;
    }
    
    public class revenueInfo{
        
        public Decimal RollUpRevenue {get;set;}
        public Decimal AnnualNAMNEPs {get;set;}
        public Decimal NEPRollUp {get;set;}
        public Decimal FullPageRate {get;set;}
        public Decimal HalfPageRate {get;set;}
        public Decimal ProposedFullPageRate {get;set;}
        public Decimal ProposedHalfPageRate {get;set;}
        public Decimal AnnualVNEPs {get;set;}
        public Decimal AnnualCorporateNAMNEPs {get;set;}
        public Decimal NEPRollUpfromCorporate {get;set;}
        public Decimal FullPageRatefromCorporate {get;set;}
        public Decimal HalfPageRatefromCorporate {get;set;}
        public Decimal ProposedRatefromCorporate {get;set;}
        public Decimal AnnualVNEPsfromCorporate {get;set;}
        
    }

}