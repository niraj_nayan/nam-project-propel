/***************************************************************************************************************************
Description: Scheduler Class for UserTerritoryHierarchyBatchClass
Created on:  5 Jan 2016
Created by:  Jagan

Version 1:   Created the class
********************************************************************************************************************************/
Global class UserTerritoryHierarchySchedular implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        //Execute the territory hierarchy batch class
        UserTerritoryHierarchyBatchClass batch = new UserTerritoryHierarchyBatchClass();
        database.executebatch(batch,1);
    }

}