@isTest
private class LinkedOpportunityTriggerHelperTest {
	
	@isTest static void LinkedOpportunityTriggerInsertTest() {
		// Implement test code
		//Create Corp Account
		Account corp1 = TestDataUtils.createCorpAccount('TestCorp');
		insert corp1;

		//Create Div Account
		Account div1 = TestDataUtils.createDivAccount('Test Div', corp1.Id);
		insert div1;

		//Create first Opportunity
		Opportunity opp1 = TestDataUtils.createOppty('Oppty 1', 'Client Engagement', div1.Id, 'FSI');
		insert opp1;

		//Create Second Opportunity
		Opportunity opp2 = TestDataUtils.createOppty('Oppty 2', 'Client Engagement', div1.Id, 'FSI');
		insert opp2;

		//Create a related opportunity link
		Linked_Opportunity__c testLink = new Linked_Opportunity__c(
			Related_Opportunity__c = opp2.Id,
			Linked_Opportunity__c = opp1.Id);
		//Linking opp1 to opp2
		//So Opp2 is the related opportunity to Opp1
		System.debug('Opp2 Id: ' + opp2.Id);
		insert testLink;

		//Linked_Opportunity__c assertLink = [Select Id, Related_Opportunity__r.Name, Linked_Opportunity__r.Name From Linked_Opportunity__c
												//Where Id =:testLink.Id];

		System.assertEquals(opp2.Id, testLink.Related_Opportunity__c);
		//System.assertEquals('Oppty 1', testLink.Linked_Opportunity__r.Name);
		update testlink;
		delete testlink;

		//testLink.Related_Opportunity__c = opp3.Id;
		//update testLink;
	}	
	
}