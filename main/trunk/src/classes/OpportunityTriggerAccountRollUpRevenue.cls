public class OpportunityTriggerAccountRollUpRevenue {
    Static Boolean afterUpdateRollUpRevenueCheck = FALSE;
    Static Boolean afterDeleteRollUpRevenueCheck = FALSE;

    public static void afterUpdateRollUpRevenue(List<Opportunity> oppty, Map<Id, Opportunity>oldMap){
        
        if(!afterUpdateRollUpRevenueCheck){
            afterUpdateRollUpRevenueCheck = TRUE;
            List<Account> accDiv = new List<Account>();
            List<Account> accCorp = new List<Account>();
            Set<Id> filteredAccount = new Set<Id>();
            Set<String> filteredCorpAccounts = new Set<String>();
            Map<Id,List<Opportunity>> accIdOppMap = new Map<Id,LIst<Opportunity>>();

            Period currentFiscalYear = [SELECT FiscalYearSettings.Name, Type, StartDate, EndDate  
                                            FROM Period 
                                            WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY];
            System.debug('Fiscal Year: ' + currentFiscalYear);
            //get all DIVISION accounts related to opportunities that have a different amount
            for(Opportunity opp : oppty) {
                //ROLL UP REVENUE PART WILL NOT BE EXECUTED FOR THE MIGRATED OPPORTUNTIIES
                if(opp.Legacy_Source__c == null){
                    if(accIdOppMap.containsKey(opp.Accountid)){
                        accIdOppMap.get(opp.Accountid).add(opp);
                    }
                    else{    
                        accIdOppMap.put(opp.accountid, new List<Opportunity>{opp});
                    }
                }
            }
            filteredAccount = accIdOppMap.keySet();
            
            //Update Division Accounts
            if(filteredAccount.size() > 0) {
                //Pass in filtered Accounts, empty set of Corp accounts to be set, map
                accDiv = divisionAccountRollUp(currentFiscalYear.FiscalYearSettings.Name, filteredCorpAccounts, accIdOppMap, oldMap);
                if(accDiv.size() > 0){
                    update accDiv;
                }
                System.debug('Corp Accounts: ' + filteredCorpAccounts);
            }
            //Update Corporate Accounts
            if(filteredCorpAccounts.size() > 0){
                accCorp = corpAccountRollUp(accIdOppMap.keySet(), filteredCorpAccounts);
                if(accCorp.size() > 0){
                    update accCorp;
                }
            }
        }
    }

    public static List<Account> divisionAccountRollUp(String currentFiscalYear, Set<String> filteredCorpAccounts, Map<Id, List<Opportunity>> accIdOppMap, Map<Id, Opportunity>oldMap){

        //***************************************************
        //DIVISION ACCOUNTS
        //WE GET ALL DIVISION ACCOUNTS THAT HAVE AN OPPORTUNITY WHAT A CHANGED AMOUNT
        List<Account> accDiv = new List<Account>();
        List<Account> acc = new List<Account>();
        Set<Id> filteredAccount = accIdOppMap.keySet();
        acc = [SELECT Id, Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, ParentId, Type
                FROM Account
                WHERE Id in :filteredAccount];
        System.debug('Accounts to update: ' + acc);

        //want to loop through the Accounts that are related to the Opportunities
        for(Account accType: acc) {
            //get opportunities related to curernt account
            List<Opportunity> tempOppList = new List<Opportunity>();
            if(accIdOppMap.containsKey(accType.id)){
                tempOppList = accIdOppMap.get(accType.id);
            }
            for(Opportunity opp : tempOppList){
                System.debug('DEBUG Opportunity: ' + opp);
                System.debug('Opportunity Fiscal Year: ' + opp.Calendar_Fiscal_Year__c);
                System.debug('Current Fiscal Year: ' + currentFiscalYear);
                //check if opp is in current fiscal year
                if(opp.Calendar_Fiscal_Year__c == currentFiscalYear){
                    //if old opp was not in current fiscal year we are adding it for the first time
                    if(oldMap == NULL || oldMap.containsKey(opp.Id) == FALSE){//meaning no old opportunity. Being entered for the first time
                        if(accType.Roll_Up_Revenue__c != NULL && opp.Amount != NULL){
                            accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c + opp.Amount;
                        }
                        else if(accType.Roll_Up_Revenue__c == NULL && opp.Amount != NULL){
                            accType.Roll_Up_Revenue__c = opp.Amount;
                        }
                        //if opp.Amount is NULL and accType.Roll_Up_Revenue__c = NULL we dont do anything
                    }
                    //If there is an old map(not null) and the old opportunity was not on the fiscal year, need to add it
                    if(oldMap != NULL && oldMap.get(opp.Id).Calendar_Fiscal_Year__c != currentFiscalYear){
                        //check if account roll up is not null
                        if(accType.Roll_Up_Revenue__c != NULL && opp.Amount != NULL){
                            accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c + opp.Amount;
                        }
                        else if(accType.Roll_Up_Revenue__c == NULL && opp.Amount != NULL){
                            accType.Roll_Up_Revenue__c = opp.Amount;
                        }
                    }
                    //the old fiscal year is also in the current fiscal year need to update roll up amoung
                    else if(oldMap != NULL && oldMap.get(opp.Id).Calendar_Fiscal_Year__c == currentFiscalYear){
                        //current and past in same fiscal year
                        if(opp.Amount != oldMap.get(opp.Id).Amount && opp.Amount != NULL && oldMap.get(opp.Id).Amount != NULL){
                            accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c + opp.Amount;
                            accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c - oldMap.get(opp.Id).Amount;
                        }
                        //if new amount is null then we need to remove the old amount from the roll up
                        if(opp.Amount != oldMap.get(opp.Id).Amount && opp.Amount == NULL && oldMap.get(opp.Id).Amount != NULL){
                            accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c - oldMap.get(opp.Id).Amount;
                        }
                    }
                }
                //if current opp is not in fiscal year but old was in fiscal year
                else if(opp.Calendar_Fiscal_Year__c != currentFiscalYear 
                    && (oldMap!= NULL && oldMap.get(opp.Id).Calendar_Fiscal_Year__c == currentFiscalYear)){
                        System.debug('if a change happens and opp year does not equal current but old did');
                        if(accType.Roll_Up_Revenue__c != NULL){
                            accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c - oldMap.get(opp.Id).Amount;
                        }
                }

                //checking roll up for NEP
                //Added to this process cause just need to update the NEP Roll Up
                if(oldMap!=null && (opp.NEP__c != oldMap.get(opp.Id).NEP__c)){
                    if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c != null) && (oldMap.get(opp.Id) != null)){
                        accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c + opp.NEP__c;//Add in the new amount
                        accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c - oldMap.get(opp.Id).NEP__c;//take the old amount out of the account roll up revenue
                    }
                    else if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c != null) && (oldMap.get(opp.Id).NEP__c == null)){
                        accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c + opp.NEP__c;
                    }
                    //if it is a new Account with no Roll Up Revenue
                    else if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c == null)){
                        accType.NEP_Roll_Up__c = opp.NEP__c;
                    }
                }
                else if(oldMap == NULL || oldMap.containsKey(opp.Id) == FALSE){
                    if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c != null)){
                        accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c + opp.NEP__c;
                    }
                    //if it is a new Account with no NEP Roll Up
                    else if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c == null)){
                        accType.NEP_Roll_Up__c = opp.NEP__c;
                    }
                }
                
            }
            accDiv.add(accType);//Get a list of all Division accounts
            filteredCorpAccounts.add(accType.ParentId);//getting all of the Ids of the corp accounts related to division accounts of changed Opportunities
        }
        return accDiv;
    }
    public static List<Account> corpAccountRollUp(Set<Id> filteredAccount, Set<String> filteredCorpAccounts){
        List<Account> accCorp = new List<Account>();
        //*****************************************************
        //CORP ACCOUNTS RELATED TO DIVISION ACCOUNTS
        //GET ALL CORP ACCOUNTS RELATED TO DIVISION ACCOUNTS TO ROLL UP THE AMOUNT TO THE CORP ACCOUNT
        if(filteredCorpAccounts.size() > 0) {
            //need to get all Division accounts, even ones I have not updated
            List<Account> divAcc = [Select Id, Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, Parent.Id 
                                        From Account 
                                        Where (ParentId in :filteredCorpAccounts AND Id NOT IN :filteredCorpAccounts)];
            List<Account> corpAcc = [Select Id, Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, Parent.Id
                                        From Account
                                        Where (Id IN:filteredCorpAccounts)];
            Map<Id, List<Account>> corpDivMap = new Map<Id, List<Account>>();

            //loop through div accounts, if parent id is in the corpDivMap add div Account to map
            //if map does not have 
            for(Account div: divAcc){
                if(corpDivMap.containsKey(div.ParentId)){
                    corpDivMap.get(div.ParentId).add(div);
                }
                else{
                    corpDivMap.put(div.ParentId, new List<Account>{div});
                }
            }

            for(Account corp : corpAcc){
                corp.Roll_Up_Revenue__c = 0;
                corp.NEP_Roll_Up__c = 0;

                List<Account> tempAccList = new List<Account>();
                if(corpDivMap.containsKey(corp.Id)){
                    tempAccList = corpDivMap.get(corp.Id);
                }

                for(Account div : tempAccList){
                    if((div.ParentId == corp.Id) && (div.Roll_Up_Revenue__c != null)){//The division account is related to the corp account
                        corp.Roll_Up_Revenue__c = corp.Roll_Up_Revenue__c + div.Roll_Up_Revenue__c;
                    }

                    //checking for NEP
                    if((div.ParentId == corp.Id) && (div.NEP_Roll_Up__c != null)){
                        corp.NEP_Roll_Up__c = corp.NEP_Roll_Up__c + div.NEP_Roll_Up__c;
                    }
                }
                System.debug('Corp Roll up: ' + corp.Roll_Up_Revenue__c);
                accCorp.add(corp);//get list of corp accounts that are updated
            }
        }

        return accCorp;   
    }
    //IS AFTER AND IS DELETE**************
    //Method for deleting the roll up revenue amount from the account only if the Opportunity is in the current fiscal year
    public static void afterDeleteRollUpRevenue(List<Opportunity> oppty){
    
        if(!afterDeleteRollUpRevenueCheck){
            afterDeleteRollUpRevenueCheck  = TRUE;
            List<Account> accDiv = new List<Account>();
            List<Account> accCorp = new List<Account>();
            Set<Id> filteredAccount = new Set<Id>();
            Set<String> filteredCorpAccounts = new Set<String>();
            Map<Id, Opportunity> opptyMap = new Map<Id,Opportunity>();

            Period currentFiscalYear = [SELECT FiscalYearSettings.Name, Type, StartDate, EndDate  
                                            FROM Period 
                                            WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY];
            System.debug('Fiscal Year: ' + currentFiscalYear);
            //get all DIVISION accounts related to opportunities that are in the current fiscal year
            for(Opportunity opp : oppty) {
                if(opp.Calendar_Fiscal_Year__c == currentFiscalYear.FiscalYearSettings.Name){
                    System.debug('Delete Old Amount: '+ opp.Amount);
                    filteredAccount.add(opp.AccountId);             
                    opptyMap.put(opp.Id, opp);
                }
            }
            
            //Update Division Accounts
            if(filteredAccount.size() > 0) {
                //Pass in filtered Accounts, empty set of Corp accounts to be set, map
                accDiv = deleteOppFromDivisionRollUp(filteredAccount, filteredCorpAccounts, opptyMap);
                if(accDiv.size() > 0){
                    update accDiv;
                }
                System.debug('Corp Accounts: ' + filteredCorpAccounts);
            }

            //Update Corporate Accounts
            if(filteredCorpAccounts.size() > 0){
                accCorp = corpAccountRollUp(filteredAccount, filteredCorpAccounts);
                if(accCorp.size() > 0){
                    update accCorp;
                }
            }
        
        }
        
    }

    public static List<Account> deleteOppFromDivisionRollUp(Set<Id> filteredAccount, Set<String> filteredCorpAccounts, Map<Id, Opportunity> opptyMap){
        List<Account> accDiv = new List<Account>();

        List<Account> acc = [SELECT Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, ParentId, Type FROM Account WHERE Id in :filteredAccount];
        System.debug('Delete Filtered Account: ' + filteredAccount);
        System.debug('Delete Opportunities: ' + opptyMap.values());
        System.debug('Delete Account(s): ' + acc);
        if(filteredAccount.size() > 0) {
            for(Account accType: acc) {
                //loop through oppty where oppty Account = accType
                for(Opportunity opp : opptyMap.values()){
                    if(opp.AccountId == accType.Id){
                        //Checking roll up for revenue
                        System.debug('Roll up amount: ' + accType.Roll_Up_Revenue__c);
                        if((opp.Amount != null) && (accType.Roll_Up_Revenue__c != null) && (opptyMap.get(opp.Id).Amount != null)){
                            accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c - opptyMap.get(opp.Id).Amount;//take the old amount out of the account roll up revenue
                        }

                        //checking roll up for NEP
                        if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c != null) && (opptyMap.get(opp.Id).NEP__c != null)){
                            //accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c + opp.NEP__c;//Add in the new amount
                            accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c - opptyMap.get(opp.Id).NEP__c;//take the old amount out of the account roll up revenue
                        }
                    }
                }
                accDiv.add(accType);//Get a list of all Division accounts
                filteredCorpAccounts.add(accType.ParentId);//getting all of the Ids of the corp accounts related to division accounts of changed Opportunities
            }
        }

        return accDiv;
    }

}

//old roll up logic
//check if todays date is greater than the Cycle Start Date or Insert Date
    //if((opp.RecordType.Name == Label.OppRecordTypeInStore && date.today() > opp.Cycle_Start_Date__c)
    //    || (opp.RecordType.Name != Label.OppRecordTypeInStore && date.today() > opp.Insert_Date__c)){
    //    //Just to check that the Insert Date or Cycle Date is in the Fiscal Year of the current Opportunity
    //    if((opp.Cycle_Start_Date__c >= currentFiscalYear.StartDate && opp.Cycle_Start_Date__c <= currentFiscalYear.EndDate && opp.RecordType.Name == Label.OppRecordTypeInStore) 
    //        || (opp.Insert_Date__c >= currentFiscalYear.StartDate && opp.Insert_Date__c <= currentFiscalYear.EndDate && opp.RecordType.Name != Label.OppRecordTypeInStore)){
    //        //CHECK: If Opportunity is won status and the amount value has change OR isWon is now true
    //        if(oldMap!=null && (((opp.Amount != oldMap.get(opp.Id).Amount) && (opp.isWon == TRUE))  || ((oldMap.get(opp.Id).isWon == FALSE) && (opp.isWon == TRUE)))){
    //            System.debug('Either isWon changed or amounts have changed while in isWon');
    //            //If opportunity changed 
    //            if(oldMap.get(opp.Id).isWon != null && ((oldMap.get(opp.Id).isWon == FALSE) && (opp.isWon == TRUE) && (accType.Roll_Up_Revenue__c != null))){
    //                accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c + opp.Amount;
    //            }
    //            else if(oldMap.get(opp.Id).isWon != null && ((oldMap.get(opp.Id).isWon == FALSE) && (opp.isWon == TRUE) && (accType.Roll_Up_Revenue__c == null))){
    //                accType.Roll_Up_Revenue__c = opp.Amount;
    //            }
    //            else if((opp.Amount != null) && (accType.Roll_Up_Revenue__c != null) && (opptyOldAmount.get(opp.Id) != null)){
    //                accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c + opp.Amount;//Add in the new amount
    //                accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c - opptyOldAmount.get(opp.Id);//take the old amount out of the account roll up revenue
    //            }
    //            else if((opp.Amount != null) && (accType.Roll_Up_Revenue__c != null) && (opptyOldAmount.get(opp.Id) == null)){
    //                accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c + opp.Amount;
    //            }
    //            //if it is a new Account with no Roll Up Revenue
    //            else if((opp.Amount != null) && (accType.Roll_Up_Revenue__c == null)){
    //                accType.Roll_Up_Revenue__c = opp.Amount;
    //            }
    //        }
    //        //CHECK: ELSE IF isWon changed from TRUE to FALSE, have to remove Opp amount from roll
    //        else if(oldMap!= null && oldMap.get(opp.Id).isWon != null && (oldMap.get(opp.Id).isWon == TRUE) && (opp.isWon == FALSE)){
    //            System.debug('isWon change from TRUE to FALSE');
    //            if((opp.Amount != null) && (accType.Roll_Up_Revenue__c != null)){
    //                accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c - opp.Amount;
    //            }
    //        }
    //        //check if oldmap is null, means its a completely new amount being inserted
    //        if(oldMap == null && opp.isWon == TRUE){
    //            if(accType.Roll_Up_Revenue__c == null && opp.Amount != null){
    //                accType.Roll_Up_Revenue__c = opp.Amount;
    //            }
    //            else if(accType.Roll_Up_Revenue__c != null && opp.Amount != null){
    //                accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c + opp.Amount;
    //            } 
    //        }
    //    }
    //    //if today date is greater than Cycle or Insert Date but current Opportunity is not in fiscal year but old opportunity was in fiscal year
    //    else if((oldMap!=null && oldMap.get(opp.Id).Cycle_Start_Date__c >= currentFiscalYear.StartDate && oldMap.get(opp.Id).Cycle_Start_Date__c <= currentFiscalYear.EndDate && oldMap.get(opp.Id).RecordType.Name == Label.OppRecordTypeInStore) 
    //        || (oldMap!=null && oldMap.get(opp.Id).Insert_Date__c >= currentFiscalYear.StartDate && oldMap.get(opp.Id).Insert_Date__c <= currentFiscalYear.EndDate && oldMap.get(opp.Id).RecordType.Name != Label.OppRecordTypeInStore)){
    //        //if old amount is not null then we want to subtract old amount from roll up to remove the value
    //        if((oldMap.get(opp.Id).Amount != null) && oldMap.get(opp.Id).isWon == TRUE){
    //            accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c - oldMap.get(opp.Id).Amount;
    //        }
    //    }
    //}
    ////Check if todays date is less than or equal to Cycle Start Date pr Insert Date
    //else if((opp.RecordType.Name == Label.OppRecordTypeInStore && date.today() <= opp.Cycle_Start_Date__c)
    //    || (opp.RecordType.Name != Label.OppRecordTypeInStore && date.today() <= opp.Insert_Date__c) 
    //    && ((oldMap!=null && oldMap.get(opp.Id).RecordType.Name == Label.OppRecordTypeInStore && date.today() > oldMap.get(opp.Id).Cycle_Start_Date__c)
    //        || (oldMap!=null && oldMap.get(opp.Id).RecordType.Name != Label.OppRecordTypeInStore && date.today() > oldMap.get(opp.Id).Insert_Date__c))){
    //    accType.Roll_Up_Revenue__c = accType.Roll_Up_Revenue__c - oldMap.get(opp.Id).Amount;
    //}

    ////checking roll up for NEP
    //if(oldMap!=null && (opp.NEP__c != oldMap.get(opp.Id).NEP__c)){
    //    if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c != null) && (opptyOldNEPvalue.get(opp.Id) != null)){
    //        accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c + opp.NEP__c;//Add in the new amount
    //        accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c - opptyOldNEPvalue.get(opp.Id);//take the old amount out of the account roll up revenue
    //    }
    //    else if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c != null) && (opptyOldNEPvalue.get(opp.Id) == null)){
    //        accType.NEP_Roll_Up__c = accType.NEP_Roll_Up__c + opp.NEP__c;
    //    }
    //    //if it is a new Account with no Roll Up Revenue
    //    else if((opp.NEP__c != null) && (accType.NEP_Roll_Up__c == null)){
    //        accType.NEP_Roll_Up__c = opp.NEP__c;
    //    }
    //}