/* OpportunityFiscalYearCalculator
 * Calculates opportunities' Fiscal Year based on the relevant Calender record.
 * Author: Stephen Lane
*/

public class OpportunityFiscalYearCalculator {
    Static boolean calculateFiscalYearCalenderCheck = FALSE;
    
    public static void calculateFiscalYearCalendar(List<Opportunity> oppty){
        if(!calculateFiscalYearCalenderCheck){
            calculateFiscalYearCalenderCheck = TRUE;
            List<Date> startDates = new List<Date>();
            List<Opportunity> inStoreOpps = new List<Opportunity>();
            List<Id> cycles = new List<Id>();
            List<String> productLines = new List<String>();
            //Get start dates and record types of opportunities for query matching on calendars
            for(Opportunity opp: oppty){
                if(opp.InStore_Cycle__c <> null){
                    inStoreOpps.add(opp);
                    cycles.add(opp.InStore_Cycle__c);
                }
                else{
                    startDates.add(opp.Insert_Date__c);
                }
                productLines.add(opp.RecordTypeId);
            }
            //Retrieve and map the cycles of any opps that have them (in store opps)
            Map<Id, Cycle__c> inStoreOppsByCycles = new Map<Id, Cycle__c>();
            if(!cycles.isEmpty()){
                Map<Id, Cycle__c> inStoreCycles = new Map<Id, Cycle__c>(
                                                   [SELECT Begin_Date__c 
                                                   FROM Cycle__c 
                                                   WHERE Id IN :cycles]);
                for(Opportunity opp: inStoreOpps){
                    startDates.add(inStoreCycles.get(opp.InStore_Cycle__c).Begin_Date__c);
                    inStoreOppsByCycles.put(opp.Id, inStoreCycles.get(opp.InStore_Cycle__c));
                }
            }
            //Retrieve and map the opps' record type names for calender Product Line matching
            Map<Id, RecordType> recordTypesByName = new Map<Id, RecordType>(
                [SELECT Name 
                FROM RecordType 
                WHERE Id IN :productLines]);
            Map<Id, String> oppsByRecordTypeName = new Map<Id, String>();
            for(Opportunity opp: oppty){
                oppsByRecordTypeName.put(opp.Id, recordTypesByName.get(opp.RecordTypeId).name);
            }
            List<String> recordTypeNames = new List<String>(oppsByRecordTypeName.values());
            //Get calendars matching the product lines and start dates
            List<Calendar__c> calendars = [SELECT Id, Calendar_Date__c, Product_Line__r.Name
                                           FROM Calendar__c
                                           WHERE Calendar_Date__c IN :startDates
                                           AND Product_Line__r.Name IN :recordTypeNames
                                           AND Calendar_Date__c != null AND Product_Line__r.Name != null AND Product_Line__r.Name != ''];
            Boolean hasCalendar;
            //Set opps' calendar lookup based on start date and product line; use cycle's start date if there is one
            if(!calendars.isEmpty()){
                for(Opportunity opp: oppty){
                    hasCalendar = false;
                    for(Calendar__c calendar: calendars){
                        if(opp.InStore_Cycle__c <> null &&
                           inStoreOppsByCycles.get(opp.Id).Begin_Date__c == calendar.Calendar_Date__c &&
                           oppsByRecordTypeName.get(opp.Id) == calendar.Product_Line__r.Name){
                                opp.Calendar_Lookup__c = calendar.Id;
                                hasCalendar = true;
                        }
                        else if(opp.Insert_Date__c == calendar.Calendar_Date__c &&
                            oppsByRecordTypeName.get(opp.Id) == calendar.Product_Line__r.Name){
                                opp.Calendar_Lookup__c = calendar.Id;
                                hasCalendar = true;
                        }
                        else{
                            System.debug('calendar not matched');
                            System.debug(inStoreOppsByCycles.get(opp.Id).Begin_Date__c);
                            System.debug(calendar.Calendar_Date__c);
                            System.debug(oppsByRecordTypeName.get(opp.Id));
                            System.debug(calendar.Product_Line__r.Name);
                        }
                    }
                    //If a calendar wasn't found, reset the calendar lookup to null for this opp
                    if(!hasCalendar){
                        opp.Calendar_Lookup__c = null;
                    }
                }
            }
        }
    }
}