public with sharing class CreateRelatedOpportunity {

	public string OpportId = ApexPages.currentpage().getparameters().get('addTo');

	public Linked_Opportunity__c newLinkedOpportunity {get;set;}
	public List<Linked_Opportunity__c> allRelatedOpportunity {get;set;}

	public CreateRelatedOpportunity() {
		RecordType linkedRelatedType = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Linked_Opportunity__c' and Name ='Related Opportunity'];
		newLinkedOpportunity = new Linked_Opportunity__c();
		newLinkedOpportunity.Related_Opportunity__c = OpportId;
		newLinkedOpportunity.RecordTypeId = linkedRelatedType.Id;

		//grab all linked Opportunities where the related_Opportunity is the current Opportunity ID
		allRelatedOpportunity = [Select Id, Related_Opportunity__c, Linked_Opportunity__c From Linked_Opportunity__c Where Related_Opportunity__c =:OpportId];
		System.debug('all related opp: ' + allRelatedOpportunity);

	}

	public PageReference saveLinkedOpportunity(){


		if(newLinkedOpportunity.Related_Opportunity__c == newLinkedOpportunity.Linked_Opportunity__c){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, Label.LinkedOpportunitySameOpptyValidation);
			ApexPages.addmessage(myMsg);
			return null;
		}

		if(allRelatedOpportunity.size() > 0){
			for(Linked_Opportunity__c relatedOpp: allRelatedOpportunity){
				if(relatedOpp.Linked_Opportunity__c == newLinkedOpportunity.Linked_Opportunity__c){
					ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, Label.LinkedOpportunityDuplicateValidation);
					ApexPages.addmessage(myMsg);
					return null;
				}
			}
		}
		System.debug('Related to: ' + newLinkedOpportunity.Related_Opportunity__c);
		System.debug('Linked to: ' + newLinkedOpportunity.Linked_Opportunity__c);

		insert newLinkedOpportunity;
		PageReference opptyPage = new PageReference('/' + OpportId);

        System.debug('Page Refernce Cancel: ' + opptyPage);
        return opptyPage;
	}

	/***********************************************************************
    Method to return user to opportunity detail page
    ************************************************************************/
    public PageReference cancelButton(){
        PageReference opptyPage = new PageReference('/' + OpportId);

        System.debug('Page Refernce Cancel: ' + opptyPage);
        return opptyPage;
    }
}