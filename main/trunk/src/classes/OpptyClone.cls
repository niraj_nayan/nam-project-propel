/**********************************************************************************************************************************************
Description: Controller for Oppty_clone page          
Created on:  9/17/2015
Created by:  Seth

Version 1:   Created Controller for cloning Opportunity
***********************************************************************************************************************************************/
public with sharing class OpptyClone {

    public Id currentRecordId = ApexPages.currentPage().getParameters().get('id');
    public String currentOpptyID {get;set;}
    public Opportunity currentOppty {get;set;}
    public Opportunity cloneOppty{get;set;}
    public Boolean cloneTypeWithProduct {get;set;}
    public Boolean isInstoreRecordType {get;set;}
    public Boolean isLoading {get;set;}
    public String currRecordType;
    public Boolean isRendable {get;set;}
    /***********************************************************************
    Constructor
    ************************************************************************/
    public OpptyClone() {
        currentOppty = [Select Id, Name, OwnerId, Owner.Name, AccountId, StageName, Amount, Late_Buy__c, Agency_Account__c, CloseDate, Type,
                                Trade_Account__c, CurrencyIsoCode, Commission_Percentage__c, Probability__c, Parent_Opportunity__c, Insert_Date__c,
                                Rejection_Reasons__c, Submission_Reason__c, Stage_Started_On__c, Status__c, Lost_Reason__c, Contract_Date__c, 
                                Business_Type__c, Bonus_Type__c, Canada_Execution__c,RecordTypeId, RecordType.Name, Instore_Cycle__c,
                                Artwork_Due_Date__c, Brand__c, Pricing_Request_Description__c 
                            From Opportunity Where Id =:currentRecordId];
        currentOpptyID = currentOppty.Id;
        currRecordType = currentOppty.RecordType.Name;
        isRendable = false;
        if(currRecordType.equals('InStore')){
            isRendable = true;
        }
        String cloneValue = ApexPages.currentPage().getParameters().get('cloneWithProd');
        
        System.debug('Instore Cycle: ' + currentOppty.InStore_Cycle__c);
        if(cloneValue.equalsIgnoreCase('true')){
            cloneTypeWithProduct = true;
        }
        else{
            cloneTypeWithProduct = false;
        }
        //checking to see if the opportunity is an instore opportunity to display the cycle field
        if(currentOppty.RecordType.Name == Label.OppRecordTypeInStore){
            isInstoreRecordType = true;
        }
        else{
            isInstoreRecordType = false;
        }
        
        String opptyQuery = createQuery();
        opptyQuery+=' Where id=\''+currentRecordId+'\'';
        currentOppty = database.query(opptyQuery);
        
        currentOppty.StageName = Label.OpptyCloneStageName;
        //currentOppty.Business_Type__c = 'Existing Business';
        currentOppty.Lost_Reason__c = '';
        currentOppty.Rejection_Reasons__c = '';
        //currentOppty.Status__c = '';
        isLoading = false;

        System.debug('Clone Value String: ' + cloneValue);
        System.debug('Clone Type: ' + cloneTypeWithProduct);
        System.debug('Current Oppty: ' + currentOpptyID);
    }
    
    public String createQuery(){
        
        String commaSepratedFields = '';
        
        Map<String,ExcludedFieldsforOpptyClone__c> exlcudedFieldmap = ExcludedFieldsforOpptyClone__c.getAll();
        
        Schema.DescribeSobjectResult result = Opportunity.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = result.fields.getMap();
        Set<String> exlcudedFieldset = new Set<String>();
        
        for(String s: exlcudedFieldmap.Keyset()){
            ExcludedFieldsforOpptyClone__c lof = exlcudedFieldmap.get(s); 
            if(lof.Record_Type_Name__c == currRecordType){
                exlcudedFieldset.add(lof.field_name__c);
            }
            
        }
        
        for(String fieldName : fieldMap.keyset()){
            if(exlcudedFieldset==null || (exlcudedFieldset<>null && !exlcudedFieldset.contains(fieldName))){
                if(commaSepratedFields == null || commaSepratedFields == ''){
                    commaSepratedFields = fieldName;
                }else{
                    commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                }
            }
        }
 
        String query = 'select ' + commaSepratedFields + ' from Opportunity ';
 
        return query;
    }
    public PageReference saveButton(){
        //Create new Opportunity
        isLoading = true;
        System.debug('Current Oppty: ' + currentOppty.AccountId);
        Boolean isUserHaveEditAccess = [Select HasEditAccess, RecordId From UserRecordAccess Where UserId =:UserInfo.getUserId() AND RecordId =:currentOppty.Id].HasEditAccess;
        if(isUserHaveEditAccess == false){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, Label.OpptyCloneInsufficientPrivilegeError);
            ApexPages.addmessage(myMsg);
            return null;
        }

        if(currentOppty.AccountId == null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, Label.OpptyCloneError);
            ApexPages.addmessage(myMsg);
            return null;
        }
        System.debug('Current Oppty Id: ' + currentOpptyID);
        Savepoint sp = Database.setSavepoint();

        cloneOppty = currentOppty.clone();
        cloneOppty.Id = null;
        //cloneOppty.Business_Type__c = Label.OpptyCloneBusinessType;
        cloneOppty.Lost_Reason__c = '';
        cloneOppty.Rejection_Reasons__c = '';
        cloneOppty.Status__c = '';
        cloneOppty.Source_Key__c = null;
        cloneOppty.OwnerId = UserInfo.getUserId();
        if(cloneTypeWithProduct != true){
            cloneOppty.Amount = 0;
        }

        try{
            //insert clone oppty so we can get an ID
            insert cloneOppty;
            
            List<OpportunityTeamMember> lstteam = new List<OpportunityTeamMember>();
            List<OpportunityTeamMember> newlstteam = new List<OpportunityTeamMember>();
            List<OpportunityShare> lstshare = new List<OpportunityShare>();
            lstteam = [SELECT userid, user.isActive, opportunityid,opportunityaccesslevel,teammemberrole FROM OpportunityTeamMember 
                            where opportunityid=:currentOppty.id AND teammemberrole != :Label.Insert_Opportunity_Owner_Role AND userid != :UserInfo.getUserId() AND user.isActive = TRUE ];
            for(OpportunityTeamMember otm:lstteam){
                OpportunityTeamMember newotm = new OpportunityTeamMember();
                newotm.userid=otm.userid;
                newotm.opportunityid=cloneOppty.id;
                //newotm.opportunityaccesslevel = otm.opportunityaccesslevel;
                newotm.teammemberrole = otm.teammemberrole;
                newlstteam.add(newotm);
                system.debug('Opportunit team member.....'+otm);
                if(otm.userid <> cloneOppty.ownerid){
                    OpportunityShare osh= new OpportunityShare();
                    osh.opportunityid = cloneOppty.id;
                    osh.userorgroupid = otm.userid;
                    
                    if(otm.OpportunityAccessLevel<>'All')
                        osh.OpportunityAccessLevel = otm.OpportunityAccessLevel;
                    else
                        osh.OpportunityAccessLevel = 'Edit';
                    
                    lstshare.add(osh);
                }
            }
            
            if(newlstteam.size()>0){
                insert newlstteam;
                insert lstshare;
            }
            //only clone with products if they select the option
            if(cloneTypeWithProduct == true){
                //Get all products related to the old opportunity
                List<OpportunityLineItem> allProducts = [Select Id, Quantity, Pricing_Detail__c, TotalPrice, Sales_Price__c, Beginning_Range__c, End_Range__c,
                                                                 ServiceDate, PricebookEntryId, OpportunityId, Commissionable__c,Charge_Type_Category_text__c,
                                                                 Charge_Type__c, Circulation__c, Component__c, PricebookEntry.CurrencyIsoCode, PricebookEntry.isActive, Product2.isActive
                                                            From OpportunityLineItem
                                                            Where OpportunityId =:currentOpptyID];
                Account oppAccount = [SELECT Id, Name, CurrencyIsoCode
                                        FROM Account
                                        WHERE Id =:cloneOppty.AccountId];
                //Create list of new products
                List<OpportunityLineItem> cloneProducts = new List<OpportunityLineItem>();
                OpportunityLineItem tempProduct = new OpportunityLineItem();

                //setting all of the values to the new product
                for(OpportunityLineItem product: allProducts){
                    System.debug('Product Currency: ' + product.PriceBookEntry.CurrencyIsoCode);
                    System.debug('Opp Currency Code: ' + cloneOppty.CurrencyIsoCode);
                    System.debug('Account Opp Currency: ' + oppAccount.CurrencyIsoCode);
                    if(product.PriceBookEntry.CurrencyIsoCode != oppAccount.CurrencyIsoCode 
                        ||product.Product2.isActive == false
                        || product.PricebookEntry.isActive == false){
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, Label.OpptyCloneInActiveProducts);
                        ApexPages.addmessage(myMsg);
                        return null;
                    }
                    //first check if Pricebook Entry Currency is the same as Opportuntiy
                    tempProduct.Quantity = product.Quantity;
                    tempProduct.Pricing_Detail__c = product.Pricing_Detail__c;
                    tempProduct.TotalPrice = product.TotalPrice;
                    tempProduct.Sales_Price__c = product.Sales_Price__c;
                    tempProduct.ServiceDate = product.ServiceDate;
                    tempProduct.PricebookEntryId = product.PricebookEntryId;
                    tempProduct.OpportunityId = cloneOppty.Id;
                    //tempProduct.Product_Line__c = product.Product_Line__c;
                    tempProduct.Commissionable__c = product.Commissionable__c;
                    tempProduct.Charge_Type_Category_text__c = product.Charge_Type_Category_text__c;
                    tempProduct.Charge_Type__c = product.Charge_Type__c;
                    tempProduct.Circulation__c = product.Circulation__c;
                    tempProduct.Component__c = product.Component__c;
                    tempProduct.Beginning_Range__c = product.Beginning_Range__c;
                    tempProduct.End_Range__c = product.End_Range__c;
                    cloneProducts.add(tempProduct);
                    tempProduct = new OpportunityLineItem();
                }

                System.debug('Products: ' + cloneProducts);

                insert cloneProducts;
            }

            PageReference opptyPage = new PageReference('/' + cloneOppty.Id);
            System.debug('redirect page: ' + opptyPage);
            return opptyPage;
        }
        catch(DmlException e){
            System.debug('Save Failed: ' + e.getMessage());
            Database.rollback(sp);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Exception occured: '+e.getDmlMessage(0));
            ApexPages.addmessage(myMsg);
            return null;
        }
        catch(Exception e){
            System.debug('Save Failed: ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPAges.Severity.Error, 'An error has occured. Please contact your adminstrator');
            ApexPages.addmessage(myMsg);
            return null;
        }
    }

    public PageReference cancelButton(){
        PageReference opptyPage = new PageReference('/' + currentOpptyID);
        return opptyPage;
    }
}