@isTest
private class AccountNewOpportunityTest {
    
    @isTest static void AccountNewOpportunityRedirect() {
        // Implement test code
        Account acc1 = TestDataUtils.createCorpAccount('Nestle Corp');
        insert acc1;

        //Create Division Account record
        Account acc2 = TestDataUtils.createDivAccount('nestle Div', acc1.Id);
        acc2.Bill_To_Only__c = false;
        insert acc2;

        Test.startTest();
        ApexPages.currentPage().getParameters().put('addTo', acc2.Id);
        AccountNewOpportunity controller = new AccountNewOpportunity();
        Pagereference pagRf = controller.redirectOnLoad();
        PageReference testPageRf = new Pagereference('/setup/ui/recordtypeselect.jsp?ent=Opportunity&retURL=%' + acc2.Id + '&save_new_url=%2F006%2Fe%3FretURL%3D%252F' +acc2.Id+'%26accid%3D'+acc2.Id);
        system.debug(pagRf+'....page reference test...'+testPageRf);
        system.assert(pagRf.getUrl() == testPageRf.getUrl() );

        acc2.Bill_To_Only__c = true;
        update acc2;
        ApexPages.currentPage().getParameters().put('addTo', acc2.Id);
        AccountNewOpportunity controller2 = new AccountNewOpportunity();
        PageReference pagRef = controller2.redirectOnLoad();
        system.assert(pagRef == null);
        controller2.redirectFromErrorPage();
        Test.stopTest();
    }
    
    
    
}