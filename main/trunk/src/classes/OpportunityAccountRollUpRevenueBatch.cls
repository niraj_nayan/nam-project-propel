global class OpportunityAccountRollUpRevenueBatch implements Database.Batchable<sObject> {
	
	global final String query;
	
	//constructor
	global OpportunityAccountRollUpRevenueBatch(String query) {
		this.query = query;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		//scope will be all of the accounts
		List<Id> divAccountIDs = new List<Id>();
		Map<Id,List<Opportunity>> accIdOppMap = new Map<Id,List<Opportunity>>();
		Map<Id,List<Account>> corpAcctIdDivAccMap = new Map<Id,List<Account>>();
		Map<Id,Account> divAccountsToUpdate = new Map<Id,Account>();
		List<Id> corpAccountID = new List<Id>();

		for(sobject s: scope){
			Account acct = (Account)s;
			acct.Roll_Up_Revenue__c = 0;

			divAccountIDs.add(acct.Id);
			divAccountsToUpdate.put(acct.Id, acct);
			corpAccountID.add(acct.ParentId);
		}

		System.debug('Account Size: '+divAccountIDs.size());

		Period currentFiscalYear = [SELECT FiscalYearSettings.Name, Type, StartDate, EndDate  
									FROM Period 
									WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY];
		List<Opportunity> divOpp = [SELECT Id, Name, Amount, NEP__c, AccountId, RecordType.Name, Insert_Date__c, Cycle_Start_Date__c,
											Calendar_Fiscal_Year__c
                                        From Opportunity 
                                        WHERE AccountId in :divAccountIDs and Calendar_Fiscal_Year__c =: currentFiscalYear.FiscalYearSettings.Name];
		List<Account> corpAccounts = [Select Id, Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, ParentId
											From Account
												Where Id =:corpAccountId];

		System.debug('Opportunity Size: ' + divOpp.size());
		for(Opportunity opp: divOpp){
			System.debug('Opportunity Name: ' + opp.Name);

                if(accIdOppMap.containsKey(opp.Accountid)){
                    accIdOppMap.get(opp.Accountid).add(opp);
                }
                else{    
                    accIdOppMap.put(opp.accountid, new List<Opportunity>{opp});
                }

		}

		//update the Division Accounts
		//loop through the Map of Division Accounts
		for(Account divAccount: divAccountsToUpdate.values()){
			divAccount.Roll_Up_Revenue__c = 0;

			List<Opportunity> tempopplist = new List<Opportunity>();

            if(accIdOppMap.containsKey(divAccount.Id)){
                tempopplist = accIdOppMap.get(divAccount.Id);
            }
            for(Opportunity opp : tempopplist){
            	if(opp.Amount != null){
                	divAccount.Roll_Up_Revenue__c = divAccount.Roll_Up_Revenue__c + opp.Amount;
              	}
            }

            //Map for Corp Account and Div Account
            if(corpAcctIdDivAccMap.containsKey(divAccount.ParentId)){
                    corpAcctIdDivAccMap.get(divAccount.ParentId).add(divAccount);
            }
            else{    
                corpAcctIdDivAccMap.put(divAccount.ParentId, new List<Account>{divAccount});
            }
		}
		update divAccountsToUpdate.values();

		System.debug('Corp Account: ' + corpAcctIdDivAccMap);

		for(Account corp: corpAccounts){
			corp.Roll_Up_Revenue__c = 0;
			List<Account> tempCorpAcc = new List<Account>();

			if(corpAcctIdDivAccMap.containsKey(corp.Id)){
                tempCorpAcc = corpAcctIdDivAccMap.get(corp.Id);
            }
            for(Account div : tempCorpAcc){
            	if(div.Roll_Up_Revenue__c != null){
                	corp.Roll_Up_Revenue__c = corp.Roll_Up_Revenue__c + div.Roll_Up_Revenue__c;
              	}
            }
		}

		update corpAccounts; 
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}