@isTest
private class OpptyNewProposalTest {
	
	@isTest static void OpptyNewProposalRedirectTest() {
		// Implement test code
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		user1.FederationIdentifier = 'tLastName';
		insert user1;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		user1.FederationIdentifier = 'secondUser2';
		insert secondUser;

		System.runAs(secondUser){

			Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
			insert corp1;

			//Create Division Account record
			Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
			div1.Site = '100324-42';
			insert div1;

			//then an opportunity with lookup to the account
			Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
			insert testOppty;

			ApexPages.currentPage().getParameters().put('addTo', testOppty.Id);
			OpptyNewProposal controller = new OpptyNewProposal();

			PageReference newOpptyPage = new PageReference('/0Q0/e?retURL=%2F' + testOppty.id + '&oppid=' + testOppty.id);
			PageReference pagRef = controller.redirectOnLoad();
			 system.assert(pagRef.getUrl() == newOpptyPage.getUrl() );

			PageReference pageRef = controller.redirectFromErrorPage();
			system.assert(pageRef.getUrl() == '/'+testOppty.Id );

		}
	}
	
}