@isTest
private class ManageProductInStoreTest {
	
	@isTest static void testInStoreInsertProductNoPreviousProductUSD() {
        // Implement test code
        Id rateCardNAMRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'NAM Rate Card'].Id;
        //Id rateCardClientRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'Client Rate Card'].Id;
        //Id oppFSIRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name = 'FSI'].Id;
        //Id oppSSDRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name = 'SmartSource Direct'].Id;
        
        //Create a Corp Account record
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'tLastName';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'Integration' Limit 1].Id;
        secondUser.FederationIdentifier = 'sUser';
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            //div1.Finance_Verified__c = true;
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = corp1.Id);
            insert accTeamMember;

            
            Product_Line__c testProductLine = TestDataUtils.createProductLine('InStore');
            insert testProductLine;
            
            
            Product2 prod = TestDataUtils.createProduct2('Test Prod');
            prod.isActive = TRUE;
            prod.CurrencyISOCode = 'USD';
            insert prod;

            Id standardPBId = Test.getStandardPricebookId();
            Pricebook2 standardPB = new Pricebook2(Name='Standard Price Book', isActive=true);
            insert standardPB; 

            //CREATE PRICEBOOK ENTRY RECORD FOR STANDARD PRICE BOOK
            PricebookEntry priceEntryStandard = TestDataUtils.createPriceBookEntry(1000, prod.Id);
            priceEntryStandard.CurrencyISOCode = 'USD';
            priceEntryStandard.pricebook2id = standardPBId;
            priceEntryStandard.UseStandardPrice = false;
            insert priceEntryStandard;

            //CREATE PRICEBOOK ENTRY RECORD FOR CUSTOM PRICE BOOK
            PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
            priceEntry.CurrencyISOCode = 'USD';
            priceEntry.pricebook2id = standardPB.id;
            priceEntry.UseStandardPrice = false;
            insert priceEntry;

            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'InStore');
            testOppty.CurrencyISOCode = 'USD';
            insert testOppty;
            
            testOppty.pricebook2id = standardPB.id;
            update testOppty;

            //Create product junction object
            Product_Junction__c testProdJunc = TestDataUtils.createProductJunction(prod.Id, testProductLine.Id);
            insert testProdJunc;
                       
            
            //Create NAM rate card
            Rate_Card__c rateCardNAM = new Rate_Card__c(
                RecordTypeId = rateCardNAMRecordId,
                Product__c = prod.Id,
                Product_Line__c = testProductLine.Id);
            rateCardNAM.CurrencyIsoCode = 'USD';
            rateCardNAM.Account__c = div1.id;
            insert rateCardNAM;
            
            Charge_Type__c tstChargeType = new Charge_Type__c();
            tstChargeType.Line_of_Business__c = testProductLine.id;
            insert tstChargeType;

            Pricing_Detail__c pd = new Pricing_Detail__c();
            pd.name = 'test PricingDetail';
            pd.Rate_Card__c = rateCardNAM.id;
            pd.Price__c = 120;
            pd.Charge_Type_2__c = tstChargeType.id;
            insert pd;

            //create rate card

            ApexPages.currentPage().getParameters().put('addTo', testOppty.Id);
            ManageProducts controller = new ManageProducts();
            System.debug('Product List: ' + controller.productList);
            
            controller.getImageName();
            controller.productList[0].isSelected = true;
            controller.selectedProductID = controller.productList[0].proJunc.Id;
            controller.productList[0].customProdPrice = '100';
            controller.saveISOPOpportunityProduct();

            Integer olicount = [select count() from OpportunityLineItem where opportunityid=:testOppty.id ]; 
            system.assert(olicount > 0 );

            controller.backButton();
            controller.cancelButton();
               
        }

    }

    @isTest static void testDigitalInsertProductCAD() {
        // Implement test code
        Id rateCardNAMRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'NAM Rate Card'].Id;
        //Id rateCardClientRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'Client Rate Card'].Id;
        //Id oppFSIRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name = 'FSI'].Id;
        //Id oppSSDRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name = 'SmartSource Direct'].Id;
        
        //Create a Corp Account record
        User user1 = TestDataUtils.createUser('Test Last Name New', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'tLastName';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'Integration' Limit 1].Id;
        secondUser.FederationIdentifier = 'sUser';
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            corp1.Site = '-42';
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            //div1.Finance_Verified__c = true;
            //div1.Site = '-42';
            div1.CurrencyIsoCode = 'CAD';
            insert div1;
            System.debug('Account Currency Code: ' + div1.CurrencyIsoCode);

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = corp1.Id);
            insert accTeamMember;

            
            Product_Line__c testProductLine = TestDataUtils.createProductLine('InStore');
            insert testProductLine;
            
            
            Product2 prod = TestDataUtils.createProduct2('Test Prod');
            prod.isActive = TRUE;
            prod.CurrencyISOCode = 'CAD';
            insert prod;

            Product2 prod2 = TestDataUtils.createProduct2('Test Prod');
            prod2.isActive = TRUE;
            prod2.CurrencyISOCode = 'CAD';
            insert prod2;

            Id standardPBId = Test.getStandardPricebookId();
            Pricebook2 standardPB = new Pricebook2(Name='Standard Price Book', isActive=true);
            insert standardPB; 

            //CREATE PRICEBOOK ENTRY RECORD FOR STANDARD PRICE BOOK
            PricebookEntry priceEntryStandard = TestDataUtils.createPriceBookEntry(1000, prod.Id);
            priceEntryStandard.CurrencyISOCode = 'CAD';
            priceEntryStandard.pricebook2id = standardPBId;
            priceEntryStandard.UseStandardPrice = false;
            insert priceEntryStandard;

            PricebookEntry priceEntryStandard2 = TestDataUtils.createPriceBookEntry(1000, prod2.Id);
            priceEntryStandard2.CurrencyISOCode = 'CAD';
            priceEntryStandard2.pricebook2id = standardPBId;
            priceEntryStandard2.UseStandardPrice = false;
            insert priceEntryStandard2;

            //CREATE PRICEBOOK ENTRY RECORD FOR CUSTOM PRICE BOOK
            PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
            priceEntry.CurrencyISOCode = 'CAD';
            priceEntry.pricebook2id = standardPB.id;
            priceEntry.UseStandardPrice = false;
            insert priceEntry;

            PricebookEntry priceEntry2 = TestDataUtils.createPriceBookEntry(10000, prod2.Id);
            priceEntry2.CurrencyISOCode = 'CAD';
            priceEntry2.pricebook2id = standardPB.id;
            priceEntry2.UseStandardPrice = false;
            insert priceEntry2;

            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'InStore');
            testOppty.CurrencyISOCode = 'CAD';
            insert testOppty;
            
            testOppty.pricebook2id = standardPB.id;
            update testOppty;

            //Create product junction object
            Product_Junction__c testProdJunc = TestDataUtils.createProductJunction(prod.Id, testProductLine.Id);
            insert testProdJunc;

            Product_Junction__c testProdJunc2 = TestDataUtils.createProductJunction(prod2.Id, testProductLine.Id);
            insert testProdJunc2;
                       
            
            //Create NAM rate card
            Rate_Card__c rateCardNAM = new Rate_Card__c(
                RecordTypeId = rateCardNAMRecordId,
                Product__c = prod.Id,
                Product_Line__c = testProductLine.Id);
            rateCardNAM.CurrencyIsoCode = 'CAD';
            rateCardNAM.Account__c = div1.id;
            insert rateCardNAM;

            Rate_Card__c rateCardNAM2 = new Rate_Card__c(
                RecordTypeId = rateCardNAMRecordId,
                Product__c = prod2.Id,
                Product_Line__c = testProductLine.Id);
            rateCardNAM2.CurrencyIsoCode = 'CAD';
            rateCardNAM2.Account__c = div1.id;
            insert rateCardNAM2;
            
            Charge_Type__c tstChargeType = new Charge_Type__c();
            tstChargeType.Line_of_Business__c = testProductLine.id;
            insert tstChargeType;

            Pricing_Detail__c pd = new Pricing_Detail__c();
            pd.name = 'test PricingDetail';
            pd.Rate_Card__c = rateCardNAM.id;
            pd.Price__c = 120;
            pd.Charge_Type_2__c = tstChargeType.id;
            insert pd;

            OpportunityLineItem testProduct = new OpportunityLineItem();
            testProduct.Beginning_Range__c = '1';
            testProduct.End_Range__c = '100';
            testProduct.PricebookEntryId = priceEntry.Id;
            testProduct.Pricing_Detail__c = 'test PricingDetail';
            testProduct.Sales_Price__c = 100;
            testProduct.Quantity = 1;
            testProduct.TotalPrice = testProduct.Sales_Price__c * testProduct.Quantity;
            testProduct.OpportunityId = testOppty.Id;
            insert testProduct;

            //create rate card

            ApexPages.currentPage().getParameters().put('addTo', testOppty.Id);
            ManageProducts controller = new ManageProducts();
            System.debug('Product List: ' + controller.productList);
            
            controller.getImageName();
            controller.productList[0].isSelected = true;
            controller.processProductTables();
            controller.productList[0].addPricingSelection();
            controller.productList[0].deletePricingSelectionIndex = controller.productList[0].pricingOptions[1].index;
            controller.productList[0].deletePricingSelection();

            controller.productList[0].pricingOptions[0].selectedProductType = pd.Name;
            controller.productList[0].pricingOptions[0].customPrice = '100';
            controller.saveCustomInstoreCADOpportunity();

            controller.productList[0].deletePricingSelectionIndex = controller.productList[0].pricingOptions[0].index;
            controller.productList[0].deletePricingSelection();

            System.debug('Pricing Options Size: ' + controller.productList[0].pricingOptions.size());
            

            //Integer olicount = [select count() from OpportunityLineItem where opportunityid=:testOppty.id ]; 
            //system.assert(olicount > 0 );

            controller.backButton();
            controller.cancelButton();
               
        }

    }
	
    @isTest static void testInStoreInsertProduct() {
        // Implement test code
        Id rateCardNAMRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'NAM Rate Card'].Id;
        //Id rateCardClientRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'Client Rate Card'].Id;
        //Id oppFSIRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name = 'FSI'].Id;
        //Id oppSSDRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name = 'SmartSource Direct'].Id;
        
        //Create a Corp Account record
        User user1 = TestDataUtils.createUser('Test Last Name New', 'no-reply@email.com', '1231231234');
        user1.FederationIdentifier = 'tLastName';
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'Integration' Limit 1].Id;
        secondUser.FederationIdentifier = 'sUser';
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            //div1.Finance_Verified__c = true;
            //div1.Site = '-42';
            div1.CurrencyIsoCode = 'USD';
            insert div1;
            System.debug('Account Currency Code: ' + div1.CurrencyIsoCode);

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = corp1.Id);
            insert accTeamMember;

            
            Product_Line__c testProductLine = TestDataUtils.createProductLine('Digital');
            insert testProductLine;
            
            
            Product2 prod = TestDataUtils.createProduct2('Test Prod');
            prod.isActive = TRUE;
            prod.CurrencyISOCode = 'USD';
            insert prod;

            Product2 prod2 = TestDataUtils.createProduct2('Test Prod2');
            prod2.isActive = TRUE;
            prod2.CurrencyISOCode = 'USD';
            insert prod2;

            Id standardPBId = Test.getStandardPricebookId();
            Pricebook2 standardPB = new Pricebook2(Name='Standard Price Book', isActive=true);
            insert standardPB; 

            //CREATE PRICEBOOK ENTRY RECORD FOR STANDARD PRICE BOOK
            PricebookEntry priceEntryStandard = TestDataUtils.createPriceBookEntry(1000, prod.Id);
            priceEntryStandard.CurrencyISOCode = 'USD';
            priceEntryStandard.pricebook2id = standardPBId;
            priceEntryStandard.UseStandardPrice = false;
            insert priceEntryStandard;

            PricebookEntry priceEntryStandard2 = TestDataUtils.createPriceBookEntry(1000, prod2.Id);
            priceEntryStandard2.CurrencyISOCode = 'USD';
            priceEntryStandard2.pricebook2id = standardPBId;
            priceEntryStandard2.UseStandardPrice = false;
            insert priceEntryStandard2;

            //CREATE PRICEBOOK ENTRY RECORD FOR CUSTOM PRICE BOOK
            PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod.Id);
            priceEntry.CurrencyISOCode = 'USD';
            priceEntry.pricebook2id = standardPB.id;
            priceEntry.UseStandardPrice = false;
            insert priceEntry;

            PricebookEntry priceEntry2 = TestDataUtils.createPriceBookEntry(10000, prod2.Id);
            priceEntry2.CurrencyISOCode = 'USD';
            priceEntry2.pricebook2id = standardPB.id;
            priceEntry2.UseStandardPrice = false;
            insert priceEntry2;

            Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'Digital');
            testOppty.CurrencyISOCode = 'USD';
            insert testOppty;
            
            testOppty.pricebook2id = standardPB.id;
            update testOppty;

            //Create product junction object
            Product_Junction__c testProdJunc = TestDataUtils.createProductJunction(prod.Id, testProductLine.Id);
            insert testProdJunc;

            Product_Junction__c testProdJunc2 = TestDataUtils.createProductJunction(prod2.Id, testProductLine.Id);
            insert testProdJunc2;
                       
            
            //Create NAM rate card
            Rate_Card__c rateCardNAM = new Rate_Card__c(
                RecordTypeId = rateCardNAMRecordId,
                Product__c = prod.Id,
                Product_Line__c = testProductLine.Id);
            rateCardNAM.CurrencyIsoCode = 'USD';
            rateCardNAM.Account__c = div1.id;
            insert rateCardNAM;

            Rate_Card__c rateCardNAM2 = new Rate_Card__c(
                RecordTypeId = rateCardNAMRecordId,
                Product__c = prod2.Id,
                Product_Line__c = testProductLine.Id);
            rateCardNAM2.CurrencyIsoCode = 'USD';
            rateCardNAM2.Account__c = div1.id;
            insert rateCardNAM2;
            
            Charge_Type__c tstChargeType = new Charge_Type__c();
            tstChargeType.Line_of_Business__c = testProductLine.id;
            insert tstChargeType;

            Pricing_Detail__c pd = new Pricing_Detail__c();
            pd.name = 'test PricingDetail';
            pd.Rate_Card__c = rateCardNAM.id;
            pd.Price__c = 120;
            pd.Charge_Type_2__c = tstChargeType.id;
            insert pd;

            //OpportunityLineItem testProduct = new OpportunityLineItem();
            //testProduct.Beginning_Range__c = '1';
            //testProduct.End_Range__c = '100';
            //testProduct.PricebookEntryId = priceEntry.Id;
            //testProduct.Pricing_Detail__c = 'test PricingDetail';
            //testProduct.Sales_Price__c = 100;
            //testProduct.Quantity = 1;
            //testProduct.TotalPrice = testProduct.Sales_Price__c * testProduct.Quantity;
            //testProduct.OpportunityId = testOppty.Id;
            //insert testProduct;

            //create rate card

            ApexPages.currentPage().getParameters().put('addTo', testOppty.Id);
            ManageProducts controller = new ManageProducts();
            System.debug('Product List: ' + controller.productList);
            
            controller.getImageName();
            controller.productList[0].isSelected = true;
            controller.processProductTables();
            controller.productList[0].addPricingSelection();
            controller.productList[0].deletePricingSelectionIndex = controller.productList[0].pricingOptions[1].index;
            controller.productList[0].deletePricingSelection();
            //controller.productList[0].deletePricingSelectionIndex = controller.productList[0].pricingOptions[0].index;
            //controller.productList[0].deletePricingSelection();

            controller.productList[0].pricingOptions[0].selectedProductType = pd.Name;
            controller.productList[0].pricingOptions[0].customPrice = '100';
            controller.saveCustomInstoreCADOpportunity();

            controller.productList[0].deletePricingSelectionIndex = controller.productList[0].pricingOptions[0].index;
            controller.productList[0].deletePricingSelection();

            System.debug('Pricing Options Size: ' + controller.productList[0].pricingOptions.size());
            

            //Integer olicount = [select count() from OpportunityLineItem where opportunityid=:testOppty.id ]; 
            //system.assert(olicount > 0 );

            controller.backButton();
            controller.cancelButton();
               
        }

    }
}