/****************************************************************************************************************************************************
Description : Utility class to generate salesforce source key.           
Created on  : 3rd November 2015
Created by  : Pavan
Version 1   : The salesforce id is 18 character long and the down stream applications cannot accept this value
              and so, we need to use the below method to generate source key.
******************************************************************************************************************************************************/
public class sourceKeyGenerationUtility {
  public static string generateSourceKey(string prefix,long input) {
     string sourceKey = '';
     if(input != null) {
         string v_AnyNo = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
         // Variables for conversion
         integer n_Base = 36;
         long n_DecNo = Math.abs(input);
         integer n_Cnt = 0;
         integer n_Val = 0;
         n_Cnt    = 0 ;
         //loop and convert
         While (n_DecNo != 0) {
              n_Val    = (integer) (Math.mod(n_DecNo,(long)Math.pow(n_Base,n_Cnt + 1))/Math.pow(n_Base,n_Cnt));
              n_DecNo  = (long) (n_DecNo - (n_Val * Math.pow(n_Base,n_Cnt)));
              sourceKey = sourceKey + v_AnyNo.substring(n_Val,n_Val + 1);
              n_Cnt++;
         }
         if(!String.isEmpty(prefix) && !String.isEmpty(sourcekey))
             sourcekey = prefix + sourcekey;
         if(input < 0 )
          sourceKey = '-' + sourcekey;
          
     }
     return sourceKey;
  }

}