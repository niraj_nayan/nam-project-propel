@isTest
private class EditSalesTeamTest {
	
	@isTest static void EditSalesTeamTest() {
		// Implement test code
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		user1.FederationIdentifier = 'firstUser';
		insert user1;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		secondUser.FederationIdentifier = 'firstUser2';
		insert secondUser;


		Account acc1 = TestDataUtils.createCorpAccount('Nestle Corp');
		insert acc1;

		//Create Division Account record
		Account div1 = TestDataUtils.createDivAccount('nestle Div', acc1.Id);
		insert div1;

		//then an opportunity with lookup to the account
		Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
		insert testOppty;

		OpportunityTeamMember testMember = TestDataUtils.createTeamMember(user1.Id, 'Manager', testOppty.Id, 10);
		insert testMember;

		ApexPages.currentPage().getParameters().put('teamMember', testMember.Id);
		EditSalesTeam controller = new EditSalesTeam();

		controller.saveButton();
		System.assertEquals(10, controller.editTeamMember.Commission_Percentage__c);
		controller.cancelButton();
	}
	
	
}