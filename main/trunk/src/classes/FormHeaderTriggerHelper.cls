/***
Helper class for FormHeaderTrigger
Version 1: SFIT- 719 :- Share the Form Header with the Primary territory hierarchy of the Owner
**/
public class FormHeaderTriggerHelper{
    
    public static boolean isupdate = FALSE;
    public static boolean isBatchUpdate = FALSE;
    public static void shareFormHeader(Map<Id,Form_Header__c> triggernewmap,Map<Id,Form_Header__c> triggeroldmap){
        
        Set<Id> useridset = new Set<Id>();    
        Set<Id> updateduseridset = new Set<Id>();    
        for(id g:TriggerNewMap.keyset()){
            if(TriggerOldMap == null)
                useridset.add(TriggerNewMap.get(g).createdbyid);
            else{
                //if the owner is changed for a FORM HEADER
                isupdate = TRUE;
                if(isBatchUpdate || TriggerNewMap.get(g).ownerid <> TriggerOldMap.get(g).ownerid){
                    updateduseridset.add(TriggerNewMap.get(g).ownerid);
                }
            }
        }
        
        //share goals for new members
        if(useridset.size()>0)
            shareFormHeaderHelper(useridset,TriggerNewMap);
        else if(updateduseridset.size()>0){
            //clear the sharerecord of old user
            clearSharerecords(TriggerOldMap.keySet());
            shareFormHeaderHelper(updateduseridset,TriggerNewMap);
        }
        
    }
    
    //Helper method for sharing records
    public static void shareFormHeaderHelper(Set<id> useridset,Map<Id,Form_Header__c> TriggerNewMap){
        
        if(useridset.size()>0){
            List<Form_Header__share> Form_Header_Shares = new List<Form_Header__share>();   
            Map<Id,Set<Id>> supervisormap = new Map<Id,Set<Id>>();
            Map<Id,Id> userterrmap = new Map<Id,Id>();
            Map<Id,Id> terrGroupMap = new Map<Id,Id>();
            
            
            //Query all groups where type is territory. This is used while creating share record
            for(Group gr: [Select id,DeveloperName,RelatedId,Type from Group where Type='Territory']){
                terrGroupMap.put(gr.relatedid, gr.id);
                
            }
            
            //query all user territory association record wherever the user is part of
            List<UserTerritory2Association> userTerritoryList = new List<UserTerritory2Association>();
            userTerritoryList = [SELECT Id, IsActive, UserId, Territory2Id,Territory2.ParentTerritory2Id, Territory2.Name,RoleInTerritory2 FROM UserTerritory2Association where UserId in :useridset];
            
            if(userTerritoryList.size()>0){
                for(UserTerritory2Association u:userTerritoryList){
                    if(useridset.contains(u.userid) && u.RoleInTerritory2 == Label.PrimaryRoletoShareGoal)//If user is in multiple territories then filter only if he/she is role "Primary"
                        userterrmap.put(u.userid,u.Territory2Id);
                }
                
                //Get the list of parent territory names for each corresponding user assigned to Form Header
                Goal_record_share_helper objGrecsh = new Goal_record_share_helper();
                objGrecsh.isFormHeaderShare = TRUE;
                supervisormap  = objGrecsh.GetAllParentElements(userterrmap);
                
                //system.debug('users with their supervisors...'+supervisormap);
    
                for(id gid:TriggerNewMap.keyset()){
                    
                    id createdbyUserId;
                    if(! isupdate)
                        createdbyUserId = TriggerNewMap.get(gid).createdbyid;
                    else
                        createdbyUserId = TriggerNewMap.get(gid).ownerid;
                    
                    if(! isupdate && createdbyUserId <> TriggerNewMap.get(gid).ownerID ){
                        Form_Header__share objGs1 = new Form_Header__share();
                        objGs1.ParentId = gid;
                        objGs1.AccessLevel = 'Edit';
                        objGs1.RowCause = 'Manual';
                        objGs1.UserOrGroupId = createdbyUserId;
                        form_Header_Shares.add(objGs1);  
                    }
                    /*else{
                        Form_Header__share objGs2 = new Form_Header__share();
                        objGs2.ParentId = gid;
                        objGs2.AccessLevel = 'Edit';
                        objGs2.RowCause = 'Manual';
                        objGs2.UserOrGroupId = TriggerNewMap.get(gid).ownerid;
                        form_Header_Shares.add(objGs2); 
                    }*/
                    system.debug('Created user/owner of the form header...'+createdbyUserId);
                    
                    if(!supervisormap.isEmpty() && createdbyUserId<>null && supervisormap.containsKey(createdbyUserId)){
                        for(Id parid:supervisormap.get(createdbyUserId)){
                            //system.debug('parent id...'+parid+'..is it in main map...'+terrGroupMap.containsKey(parid));
                            //if(terrGroupMap.containsKey(parid)){
                                Form_Header__share objGs = new Form_Header__share();
                                objGs.ParentId = gid;
                                objGs.AccessLevel = 'Read';
                                objGs.RowCause = 'Manual';
                                //get the corresponding group id from territory and group map
                                //objGs.UserOrGroupId = terrGroupMap.get(parid);
                                objGs.UserOrGroupId = parid;
                                Form_Header_Shares.add(objGs);
                            //}
                        }
                    }
                }
                system.debug('Form Headers tobe shared..'+Form_Header_Shares);
                if(Form_Header_Shares.size()>0){
                   
                    Database.SaveResult[] lsr = database.Insert(Form_Header_Shares,false);
                    for(Database.SaveResult sr : lsr)
                        if (!sr.isSuccess()) system.debug('Error occured during Form header share records insert....'+sr);
                }
            }
        }
    }
    
    //code to delete previous shares
    public static void clearSharerecords(Set<Id> deleteformheaderssset){
    
        List<Form_Header__share> lstshare = new LIst<Form_Header__share>();
        if(deleteformheaderssset.size()>0){
            lstshare = [select id,parentid from Form_Header__share where parentid in: deleteformheaderssset];
            if(lstshare.size()>0)
                database.delete(lstshare,false);
        }
    }
    
}