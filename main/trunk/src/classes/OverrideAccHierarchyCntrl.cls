public with sharing class OverrideAccHierarchyCntrl {
    
     /*Sunil Taruvu
    * Date : 9/15/16
    * Commented the below code as a part of JIRA [PP-72] fix
    * As the "Change Hierarchy" button/functionaly is no more required in the current flow/scope, this controller and related Visualforce page is commented,
    * the "Change Hierarchy" button is still availble for use but is removed from all the 3 account page layouts.
    */
    
    /*public Account acc {get;set;}
    public Account originalAcc {get;set;}
    public OverrideAccHierarchyCntrl(ApexPages.StandardController controller) {
        acc = (Account) controller.getRecord();
        if(acc == null || acc.id == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Account selected')); 
            return;
        }    
        acc = [select id,name,Frozen_Hierarchy__c,Account_Sales_Office_Text__c,Primary_Reps_Territory__c,Primary_Sales_Rep__c,Team_Name__c,Team_Manager__c,
                Division_Territory_Id__c,Primary_Rep_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,
                Region_Name_Frozen__c,Region_Manager__c,Division_Name__c,Division_Manager__c from Account where id=:acc.id];
        originalAcc =  [select id,name,Frozen_Hierarchy__c,Account_Sales_Office_Text__c,Primary_Reps_Territory__c,Primary_Sales_Rep__c,Team_Name__c,Team_Manager__c,
                Division_Territory_Id__c,Primary_Rep_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,
                Region_Name_Frozen__c,Region_Manager__c,Division_Name__c,Division_Manager__c from Account where id=:acc.id];
        
    }
    
    public PageReference save(){
        
        String divMgr,regMgr,TeamMgr,Psr,divName,regName,teamName,soffc,psrTerr,RegTerrId,DivTerrId,TeamTerrId,PsrTerrId,psrname;
        
        divMgr = acc.Division_Manager__c;
        regMgr = acc.Region_Manager__c;
        TeamMgr = acc.Team_Manager__c;
        Psr = acc.Primary_Sales_Rep__c;
        
        divName = acc.Division_Name__c;
        regName = acc.Region_Name_Frozen__c;
        teamName = acc.Team_Name__c;
        psrname = acc.Primary_Reps_Territory__c;
        
        soffc = acc.Account_Sales_Office_Text__c;
        psrTerr = acc.Primary_Reps_Territory__c;
        RegTerrId = acc.Region_Territory_Id__c;
        DivTerrId = acc.Division_Territory_Id__c;
        TeamTerrId = acc.Team_Territory_Id__c;
        PsrTerrId = acc.Primary_Rep_Territory_Id__c;
        
        Set<String> usernameset = new Set<String>();
        Set<String> Terrnameset = new Set<String>();
        
        //Map of 15 digit id and 18 digit id from user query
        Map<String,String> queryuseridmap = new Map<String,String>();
        Map<String,Territory2> queryTerridmap = new Map<String,Territory2>();
        
        if(divMgr <> null)
            usernameset.add(divMgr);
        if(TeamMgr <> null)
            usernameset.add(TeamMgr);
        if(Psr <> null)
            usernameset.add(Psr );
        if(regMgr <> null)
            usernameset.add(regMgr );
        
        if(divName<> null)
            Terrnameset.add(divName);
        if(RegName<> null)
            Terrnameset.add(RegName);
        if(teamName<> null)
            Terrnameset.add(TeamName);
        if(psrName<> null)
            Terrnameset.add(psrName);
        
        List<User> lstusr = new List<user>();
        List<Territory2> lstTerr = new List<Territory2>();
         
        lstusr = [select id,username from user where username in: usernameset];
        lstTerr = [select id,name from Territory2 where name in: Terrnameset]; 
       
        if(lstusr.size() >0 ){
            for(user u:lstusr){
                queryuseridmap.put(u.username,u.id);
            }
        }
        
        if(lstTerr.size() >0 ){
            for(Territory2 t : lstTerr){
                queryTerridmap.put(t.name,t);
            }
        }
        
        boolean invalidDivuser = false;
        boolean invalidReguser = false;
        boolean invalidPsruser = false;
        boolean invalidTeamuser = false;
        
        boolean invalidDivterr = false;
        boolean invalidRegterr = false;
        boolean invalidPsrterr = false;
        boolean invalidTeamterr = false;
        
        String errorPlace='';
            
            if(divMgr<>null && (!queryuseridmap.containsKey(divMgr) && divMgr != originalacc.Division_Manager__c)){
                invalidDivuser = true;
                errorPlace = 'Division Manager';
            }
            
            if(TeamMgr <>null && (!queryuseridmap.containsKey(TeamMgr) && TeamMgr != originalacc.Team_Manager__c)){
                invalidTeamuser = true;
                errorPlace = 'Team Manager';
            }
            
            if(Psr <>null && (!queryuseridmap.containsKey(Psr) && Psr  != originalacc.Primary_Sales_Rep__c)){
                invalidPsruser = true;
                errorPlace = 'Primary Sales Rep';
            }
            
            if(regMgr <>null && (!queryuseridmap.containsKey(regMgr) && regMgr != originalacc.Region_Manager__c)){
                invalidReguser = true;
                errorPlace = 'Region Manager';
            }
           
           
          if(divName<>null && (!queryTerridmap.containsKey(divName) && divName != originalacc.Division_Name__c)){
                invalidDivterr = true;
                errorPlace = 'Division Name';
            }
            
            if(RegName<>null && (!queryTerridmap.containsKey(regName) && regName != originalacc.Region_Name_Frozen__c)){
                invalidRegterr = true;
                errorPlace = 'Region Name';
            }
            
            if(psrName <>null && (!queryTerridmap.containsKey(psrName ) && psrName != originalacc.Primary_Reps_Territory__c)){
                invalidPsrterr = true;
                errorPlace = 'Primary Reps Territory';
            }
            
            if(TeamName<>null && (!queryTerridmap.containsKey(TeamName) && TeamName != originalacc.Team_Name__c)){
                invalidTeamterr = true;
                errorPlace = 'Team Name';
            }           
        
        if(invalidDivuser || invalidTeamuser || invalidPsruser || invalidReguser || invalidTeamterr || invalidPsrterr || invalidRegterr || invalidDivterr ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid '+errorPlace)); 
            return null;
        }    
        else{
            acc.Division_Manager__c = queryuseridmap.get(acc.Division_Manager__c);
            acc.Region_Manager__c= queryuseridmap.get(acc.Region_Manager__c);
            acc.Team_Manager__c = queryuseridmap.get(acc.Team_Manager__c );
            acc.Primary_Sales_Rep__c= queryuseridmap.get(acc.Primary_Sales_Rep__c);
            
            if(queryTerridmap.containsKey(acc.Division_Name__c)){
                acc.Division_Name__c = queryTerridmap.get(acc.Division_Name__c).name;
                acc.Division_Territory_Id__c = queryTerridmap.get(acc.Division_Name__c).id;
            }
            if(queryTerridmap.containsKey(acc.Region_Name_Frozen__c)){
                acc.Region_Name_Frozen__c = queryTerridmap.get(acc.Region_Name_Frozen__c).name;
                acc.Region_Territory_Id__c= queryTerridmap.get(acc.Region_Name_Frozen__c).id;
            }   
            if(queryTerridmap.containsKey(acc.Team_Name__c)){
                acc.Team_Name__c = queryTerridmap.get(acc.Team_Name__c).name;
                acc.Team_Territory_Id__c = queryTerridmap.get(acc.Team_Name__c).id;
            }   
            if(queryTerridmap.containsKey(acc.Primary_Reps_Territory__c)){
                acc.Primary_Reps_Territory__c = queryTerridmap.get(acc.Primary_Reps_Territory__c).name;
                acc.Primary_rep_Territory_Id__c= queryTerridmap.get(acc.Primary_Reps_Territory__c).id;
            }
            update acc;
        }
        
        return new PageReference('/'+acc.id);    
    }*/
    
}