/*****************************************************************************************************************
Description: This class is used to update the commission amount for the team member as well as update the Opportunity
             Commission Percentage when a team member is added or when a sales team member commission amount is updated
             
Created on:  10/23/2015
Created by:  Seth Martin
Version 1:   Created the helper class
Version 2:   Updated class to add new after update method to update goals when commission percentage is changed
********************************************************************************************************************/

Public class OpportunityTeamMemberTriggerHelper {

    /*******************************************************************************************
    Method to set the team member commission amount before update based on the commission percentage
    and the opportunity commission roll up value
    ********************************************************************************************/
    //This variable is used to check if the current operation is opportunity create or not
    //If it is opportunity create then afterOpportunityCommissionPercentage will not be executed
    public static boolean isopportunityCreate = FALSE;
    
    public static void beforeUpdateCommissionValue(List<OpportunityTeamMember> salesTeam){

        Set<Id> opptyIds = new Set<Id>();
        for (OpportunityTeamMember otm: salesTeam){
            opptyIds.add(otm.OpportunityId);
        }
        
        Map<Id, Opportunity> opptyMap = new Map<Id, Opportunity>([Select Id, Name, Amount, Commission_Roll_Up__c From Opportunity Where Id IN: opptyIds]);
        System.debug('opptyMap: ' + opptyMap);
        System.debug('Team Member' + salesTeam);
        for(OpportunityTeamMember teamMember: salesTeam){
            //Need to check to see if commission percentage is null cause if it is then a regular user is adding a team member so we will not calculate the commission amount  
            //System.debug('Opp Commission Percentage: ' + opptyMap.get(teamMember.OpportunityId).Commission_Roll_Up__c);
            if(opptyMap!=null && opptyMap.containsKey(teamMember.OpportunityId) && (opptyMap.get(teamMember.OpportunityId).Commission_Roll_Up__c != null) && (teamMember.Commission_Percentage__c != null)){
                System.debug('Commission Amount: ' + teamMember.Commission_Amount__c);
                System.debug('Commission Percentage: ' + teamMember.Commission_Percentage__c);
                System.debug('Opp Name: ' + opptyMap.get(teamMember.OpportunityId).Name);
                System.debug('Commission Roll Up: ' + opptyMap.get(teamMember.OpportunityId).Commission_Roll_Up__c);
                System.debug('');
                teamMember.Commission_Amount__c = 0;
                teamMember.Commission_Amount__c = (teamMember.Commission_Percentage__c *.01) * (opptyMap.get(teamMember.OpportunityId).Commission_Roll_Up__c);
               
            }
        }
    }
    
    /*******************************************************************************************
    Method to update the sales team member to copy territory hierarchy from the user
    ********************************************************************************************/
    public static void beforeinsertUpdateHierarchy(List<OpportunityTeamMember> triggernew){
        
        set<Id> useridset = new set<Id>();
        set<string> fedidset = new set<String>();
        Set<Id> oppidset = new Set<Id>();
        for(OpportunityTeamMember otm: triggernew){
            oppidset.add(otm.OpportunityId);
            if(otm.Frozen_Hierarchy__c == null)
                useridset.add(otm.userid);
            if(Label.DataMigration == Label.TrueValue){
                fedidset.add(otm.Division_Manager__c); 
                fedidset.add(otm.Region_Manager__c); 
                fedidset.add(otm.Team_Manager__c );
                fedidset.add(otm.Primary_Sales_Rep__c); 
            }
        }
        
        if(useridset.size()>0){
            Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([select id,name,ownerid,Account.Ownerid,Account.Primary_Reps_Territory__c,Account.Primary_Rep_Territory_Id__c,Account.Division_Manager__c,
                                                            Account.Division_Name__c,Account.Division_Territory_Id__c,Account.Region_Territory_Id__c,
                                                            Account.Team_Territory_Id__c,Account.Region_Name_Frozen__c,Account.Region_Manager__c,Account.Team_Manager__c,
                                                            Account.Team_Name__c from Opportunity where id in :oppidset]);
                
            Map<Id,user> usermap = new Map<Id,User>([select id,FederationIdentifier,Assigned_Primary_Territory_Name__c,Assigned_Primary_TerritoryId__c,Division_Manager__c,Division_Name__c,Division_Territory_Id__c,Region_Territory_Id__c,Team_Territory_Id__c,Region_Name__c,Region_Manager__c,Team_Manager__c,Team_Name__c from user where id in :useridset  or federationIdentifier in:fedidset]);
            Map<String,Id> fedUserIdMap = new Map<String,Id>();
            for(User u:usermap.values())
                fedUserIdMap.put(u.FederationIdentifier, u.id);            
            for(OpportunityTeamMember otm: triggernew){
                if(usermap.containsKey(otm.userid) && oppMap.containsKey(otm.OpportunityId) && otm.userid != oppMap.get(otm.OpportunityId).Account.OwnerId ){
                    if(Label.DataMigration != Label.TrueValue)
                        assignHierarchyFields(otm,usermap);
                    else
                        assignHierarchyFields(otm,fedUserIdMap);
                }
                else if(usermap.containsKey(otm.userid) && oppMap.containsKey(otm.OpportunityId) && otm.userid == oppMap.get(otm.OpportunityId).Account.OwnerId ){
                    if(Label.DataMigration != Label.TrueValue)
                        assignHierarchyFields(otm,oppMap.get(otm.OpportunityId));
                    else
                        assignHierarchyFields(otm,fedUserIdMap);
                }
            }
        }
    }
    
     public static void assignHierarchyFields(OpportunityTeamMember otm,Map<id,User> usermap){
        otm.Primary_Reps_Territory__c  = usermap.get(otm.userid).Assigned_Primary_Territory_Name__c;
        otm.Primary_Rep_Territory_Id__c = usermap.get(otm.userid).Assigned_Primary_TerritoryId__c;
        otm.Division_Manager__c = usermap.get(otm.userid).Division_Manager__c;
        otm.Division_Name__c = usermap.get(otm.userid).Division_Name__c;
        
        if(usermap.get(otm.userid).Region_Manager__c != null)
            otm.Region_Manager__c = usermap.get(otm.userid).Region_Manager__c;
        else
            otm.Region_Manager__c = usermap.get(otm.userid).Division_Manager__c;
            
        otm.Region_Name__c = usermap.get(otm.userid).Region_Name__c;
        
        if(usermap.get(otm.userid).Team_Manager__c != null)
            otm.Team_Manager__c = usermap.get(otm.userid).Team_Manager__c;
        else
            otm.Team_Manager__c = otm.Region_Manager__c;
            
        otm.Account_Sales_Office_Text__c = otm.Account_Sales_Office__c;
        otm.Team_Name__c = usermap.get(otm.userid).Team_Name__c;
        otm.Primary_Sales_Rep__c = otm.userid;
        otm.Division_Territory_Id__c = usermap.get(otm.userid).Division_Territory_Id__c;
        otm.Region_Territory_Id__c = usermap.get(otm.userid).Region_Territory_Id__c;
        otm.Team_Territory_Id__c = usermap.get(otm.userid).Team_Territory_Id__c;
    }
    public static void assignHierarchyFields(OpportunityTeamMember otm,Opportunity opp){
        otm.Primary_Reps_Territory__c = opp.Account.Primary_Reps_Territory__c;
        otm.Primary_Rep_Territory_Id__c = opp.Account.Primary_Rep_Territory_Id__c;
        otm.Division_Manager__c = opp.Account.Division_Manager__c;
        otm.Division_Name__c = opp.Account.Division_Name__c;
        
        if(opp.Account.Region_Manager__c != null)
            otm.Region_Manager__c = opp.Account.Region_Manager__c;
        else
            otm.Region_Manager__c = opp.Account.Division_Manager__c;
            
        otm.Region_Name__c = opp.Account.Region_Name_Frozen__c;
        otm.Account_Sales_Office_Text__c = opp.Account.Account_Sales_Office__c;
        
        if(opp.Account.Team_Manager__c != null)
            otm.Team_Manager__c = opp.Account.Team_Manager__c;
        else
            otm.Team_Manager__c = otm.Region_Manager__c;
            
        otm.Team_Name__c = opp.Account.Team_Name__c;
        otm.Primary_Sales_Rep__c = otm.userid;
        otm.Division_Territory_Id__c =opp.Account.Division_Territory_Id__c;
        otm.Region_Territory_Id__c = opp.Account.Region_Territory_Id__c;
        otm.Team_Territory_Id__c = opp.Account.Team_Territory_Id__c;
    }
    
    //FOR DATA MIGRATION PURPOSE
    public static void assignHierarchyFields(OpportunityTeamMember otm,Map<String,Id> fedUserIdMap){
        
        if(fedUserIdMap.containsKey(otm.Division_Manager__c))
            otm.Division_Manager__c = fedUserIdMap.get(otm.Division_Manager__c);
            
        if(fedUserIdMap.containsKey(otm.Region_Manager__c))
            otm.Region_Manager__c = fedUserIdMap.get(otm.Region_Manager__c);
            
        if(fedUserIdMap.containsKey(otm.Team_Manager__c))
            otm.Team_Manager__c = fedUserIdMap.get(otm.Team_Manager__c);
            
        if(fedUserIdMap.containsKey(otm.Primary_Sales_Rep__c))
            otm.Primary_Sales_Rep__c = fedUserIdMap.get(otm.Primary_Sales_Rep__c);
        
    }
    
    /*******************************************************************************************
    Method to update the Opportunity Commission Percentage, which is the sum of all sales team members
    commission percentage only when a sales team member percentage has changed
    ********************************************************************************************/
    public static void afterUpdateOpportunityCommissionPercentage(List<OpportunityTeamMember> salesTeam, Map<Id,OpportunityTeamMember> oldMap){
        System.debug('Opp Team Update!!!');
        List<Opportunity> changeOppty = new List<Opportunity>();
        
        //take list of opportunity team members and gets all opportunites that are different and
        //that opportunity team members have changed their commission percentage
        for(OpportunityTeamMember mem: salesTeam){
            //check to see if OpportunityTeamMember was updated
            if(mem.Commission_Percentage__c != oldMap.get(mem.Id).Commission_Percentage__c){
                //opptyId.add(mem.OpportunityId);
                Opportunity updateOppy = new Opportunity();
                updateOppy.Id = mem.OpportunityId;
                changeOppty.add(updateOppy);
            }
        }

        //create list of opportunities based on set of strings
        Database.update(changeOppty, false);
    }
    
   
    
    /*******************************************************************************************
    Method to update the goals related to opportunity team when a sales team member is deleted.
    ********************************************************************************************/
    /*public static void afterDeleteUpdateGoals(List<OpportunityTeamMember> salesTeam){
        
        Set<Id> oppidset = new Set<Id>();
        Set<Id> usridset = new Set<Id>();
        List<goal__c> lstgoals = new List<goal__c>();
        List<Opportunity_Goals__c> deloppgoals = new List<Opportunity_Goals__c>();
        for(OpportunityTeamMember mem: salesTeam){
                oppidset.add(mem.opportunityid);
                usridset.add(mem.userid);
        }
        if(oppidset.size()>0){
            List<Opportunity_Goals__c> lstoppgoal = new List<Opportunity_Goals__c>();
            //get all opportuntity goal records related to current opportunities
            lstoppgoal = [select id,opportunity__c,Other_Revenue__c,Participation_Revenue__c,Production_Revenue__c,Space_Revenue__c, goal__c,goal__r.ownerid,
                                goal__r.Paced_Space_Revenue__c, goal__r.Paced_Participation_Revenue__c,goal__r.Paced_Other_Revenue__c,goal__r.Paced_Production_Revenue__c 
                                from Opportunity_Goals__c where opportunity__c in:oppidset];
            
            for(Opportunity_Goals__c og:lstoppgoal){
                //If the goal is related to the team member being deleted, then update the goal with difference amount
                if(usridset.contains(og.goal__r.ownerid)){
                    deloppgoals.add(og);
                }
            }
            if(deloppgoals.size()>0)
                database.delete(deloppgoals,false);
        }

    }*/
    /*******************************************************************************************
    AFTER INSERT TRIGGER: Method to update the Opportunity Commission Percentage, which is the sum of all sales team members
    commission percentage only when a sales team member percentage has changed
    ********************************************************************************************/
    public static void afterOpportunityCommissionPercentage(List<OpportunityTeamMember> salesTeam){
        
        if(!isopportunityCreate){
            Set<Opportunity> updateOppty = new Set<Opportunity>();
            List<Opportunity> oppUpdate = new List<Opportunity>();
            //Set<String> opptyId = new Set<String>();
            //get a set of all opportunities that are being inserted
            for(OpportunityTeamMember mem: salesTeam){
                Opportunity tempOppy = new Opportunity();
                tempOppy.Id = mem.OpportunityId;
                updateOppty.add(tempOppy);
                
            }
    
            //turn that set of opportunities into a list to be updated
            System.debug('Oppty to Update: ' + updateOppty);
    
            for(Opportunity opp: updateOppty){
                Opportunity tempOppy = new Opportunity();
                tempOppy.Id = opp.Id;
                oppUpdate.add(tempOppy);
            }
    
            Database.update(oppUpdate, false);
        }
    }
}