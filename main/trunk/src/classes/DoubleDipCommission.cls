/**********************************************************************************************************************************************
Description: Controller for Double_Dip_Commissions page          
Created on:  26 October 2015
Created by:  Seth

Version 1:   Created Controller
***********************************************************************************************************************************************/
public with sharing class DoubleDipCommission {
    public string OpportId = ApexPages.currentpage().getparameters().get('addTo');
    public Opportunity currentOppty {get;set;}
    public List<DoubleDipUsers> teamMembers{get;set;}
    public List<Form_Detail__c> submitDetail{get;set;}
    public Form_Header__c submitHeader{get;set;}

    public DoubleDipCommission() {
        teamMembers = new List<DoubleDipUsers>();
        //Need to create a list of submitted users for form detail
        submitDetail = new List<Form_Detail__c>();
        submitHeader = new Form_Header__c();
        //This will add the first 5 users   
        currentOppty = [Select Id, Name From Opportunity Where Id =:OpportId];
        System.debug('Before getting team members');
        getTeamMembers();
        //System.debug('Opport Name: ' + currentOppty.Name);
        
    }
    public void getTeamMembers(){
        //get list of team members then set them to class of team members
        List<OpportunityTeamMember> tempList = [Select Id, UserId, User.Name, TeamMemberRole, Commission_Percentage__c, OpportunityId From OpportunityTeamMember 
                                                    Where OpportunityId =:OpportId];
        for(OpportunityTeamMember temp: tempList){
            DoubleDipUsers member = new DoubleDipUsers(temp);
            teamMembers.add(member);
        }
    }

    public PageReference submitButton(){
        System.debug('inside submit button');
        //Loop through all sales team members
        //submitHeader.RecordTypeId = [select Id, Name From RecordType Where Name=:Label.DoubleDipCommRecType].Id;
        submitHeader.RecordTypeId = Schema.Sobjecttype.Form_Header__c.getRecordTypeInfosByName().get(Label.DoubleDipCommRecType).getRecordTypeId();
        System.debug('RecordType');
        submitHeader.Status__c = 'Submitted';
        submitHeader.Opportunity__c = currentOppty.Id;
        System.debug('Header Id: ' + submitHeader.Id);
        System.debug('Header: ' + submitHeader);
        System.debug('RecordType: ' + submitHeader.RecordType);

        for(DoubleDipUsers dipUser: teamMembers){
            if(dipUser.dipCommission > 0.0){
                Form_Detail__c tempDetail = new Form_Detail__c();
                tempDetail.Sales_Person__c = dipUser.salesTeamMember.UserId;
                tempDetail.Opportunity__c = dipUser.salesTeamMember.OpportunityId;
                tempDetail.Commission__c = dipUser.dipCommission;
                submitDetail.add(tempDetail);
            }
        }
        
        if(submitDetail.size() > 0){
            submitHeader.OwnerId = [Select Id, SobjectType, QueueId, Queue.Name from QueueSobject Where Queue.Name = :Label.FinanceQueueName Limit 1].QueueId;
            System.debug('OwnerId: '+submitHeader.OwnerId);
            System.debug('Owner Name: ' + submitHeader.Owner.Name);
            insert submitHeader;
            //insert q;
            for(Form_Detail__c dipUser: submitDetail){
                dipUser.Form_Header__c = submitHeader.Id;
            }
            insert submitDetail;
        }
        else{
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Error, Label.DoubleDipCommError);
            ApexPages.addmessage(myMsg2);
            //init();
            return null;
        }

        submitDetail = new List<Form_Detail__c>();
        PageReference opptyPage = new PageReference('/' + OpportId);
        return opptyPage;
    }

    public PageReference cancelButton(){
        PageReference opptyPage = new PageReference('/' + OpportId);
        return opptyPage;
    }

    public class DoubleDipUsers{
        public OpportunityTeamMember salesTeamMember{get;set;}
        public Boolean selectedUser {get;set;}
        public Double dipCommission {get;set;}

        public DoubleDipUsers(OpportunityTeamMember teamMembers){
            salesTeamMember = teamMembers;
            selectedUser = false;
            dipCommission = 0;
        }
    }


}