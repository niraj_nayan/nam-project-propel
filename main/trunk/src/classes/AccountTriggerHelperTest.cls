@isTest
private class AccountTriggerHelperTest {
    
    @isTest static void AccountTriggerUpdateRollUpTest() {
        // Implement test code
        //First Corp Account

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'Integration' Limit 1].Id;
        secondUser.FederationIdentifier = 'firstUser2';
        insert secondUser;

        System.runAs(secondUser){
            Account corp1 = TestDataUtils.createCorpAccount('Corp 1');
            insert corp1;

            //Division Related to first corp
            Account div1 = TestDataUtils.createDivAccount('Div 1', corp1.Id);
            insert div1;
            
            //Account seconddiv1 = TestDataUtils.createDivAccount('Div 1', corp1.Id);
            //insert seconddiv1;
            
            
            //Second Corp Account
            Account corp2 = TestDataUtils.createCorpAccount('Corp 2');
            insert corp2;

            //Division Related to second corp
            Account div2 = TestDataUtils.createDivAccount('Div 2', corp2.Id);
            insert div2;
            
            //Account seconddiv2 = TestDataUtils.createDivAccount('Div 2', corp2.Id);
            //insert seconddiv2;

            Account corp3 = TestDataUtils.createCorpAccount('Corp 1');
            insert corp3;

        
            test.startTest();
                //switching Corp Accounts for Division Accounts
                
                Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Closed Won', div1.Id, 'Merchandising');
                testOppty.Amount = 100;
                testOppty.NEP__c = 1000;
                testOppty.stagename = 'Closed Won';
                testOppty.Insert_Date__c = Date.today().addDays(-2);
                insert testOppty;
                
                Opportunity testOppty2 = TestDataUtils.createOppty('Test Oppty2', 'Closed Won', div2.Id, 'Merchandising');
                testOppty2.Amount = 1001;
                testOppty2.NEP__c = 1000;
                testOppty2.stagename = 'Closed Won';
                testOppty2.Insert_Date__c = Date.today().addDays(-2);
                insert testOppty2;

                div1.ParentId = corp2.Id;
                div2.ParentId = corp1.Id;
                
                update div1;
                update div2;

                corp3.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.AccRecordTypeDiv).getRecordTypeId();
                corp3.ParentId = corp1.Id;
                update corp3;

                Account testdiv1 = [Select Id, Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, Parent.Id From Account where id=:div1.id];
                Opportunity testOpptyCheck = [Select Id, Name, Amount, Account.Name From Opportunity Where Account.Name Like '%Div 1%'];
                System.debug('Opp: ' + testOpptyCheck);
                System.debug('Opp Account Name: ' + testOpptyCheck.Account.Name);
                System.debug('Account: ' + testdiv1);
                system.assertEquals(testdiv1.Roll_Up_Revenue__c, 100);
                //system.assertEquals(testdiv1.NEP_Roll_Up__c, 1000);
                
                update div2;
                Account testdiv2 = [Select Id, Name, Roll_Up_Revenue__c, NEP_Roll_Up__c, Parent.Id From Account where id=:div2.id];
                System.debug('Account: ' + testdiv2);
                system.assertEquals(testdiv2.Roll_Up_Revenue__c, 1001);
                //system.assertEquals(testdiv2.NEP_Roll_Up__c, 1000);
            test.stopTest();
        }
    }   
}