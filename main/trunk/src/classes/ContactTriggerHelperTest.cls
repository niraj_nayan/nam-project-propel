@isTest
private class ContactTriggerHelperTest{

    private static TestMethod Void contactTriggerTest(){
        
        Account testacct = TestDataUtils.createCorpAccount('Corp 1');
        insert testacct;
        
        Contact c = new Contact();
        c.AccountId = testacct.id;
        c.firstname ='test first';
        c.lastname = 'test last';
        insert c;

        Contact con = [select Source_Key__c from Contact where id=:c.id];
        system.assert(con.Source_Key__c <> null);
    
    }
    
}