@isTest
private class ManageProductSSDTest {
	
	@isTest static void getProductLineDropdownSSD() {
        // Implement test code
        Id rateCardNAMRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'NAM Rate Card'].Id;
        Id rateCardClientRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Rate_Card__c' and Name = 'Client Rate Card'].Id;
        //Id oppFSIRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name = 'FSI'].Id;
        //Id oppSSDRecordId = [Select Id, Name, SObjectType From RecordType Where SObjectType = 'Opportunity' and Name = 'SmartSource Direct'].Id;
        
        //Create a Corp Account record
        User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
        insert user1;

        User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
        secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'Integration' Limit 1].Id;
        insert secondUser;

        System.runAs(secondUser){

            Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
            insert corp1;

            //Create Division Account record
            Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
            //div1.Finance_Verified__c = true;
            insert div1;

            ///CREATE ACCOUNT SALES TEAM MEMBERS
            AccountTeamMember accTeamMember = new AccountTeamMember(
                UserId = user1.Id,
                AccountId = corp1.Id);
            insert accTeamMember;

           

            Product_Line__c testProductLine2 = TestDataUtils.createProductLine('SmartSource Direct');
            insert testProductLine2;

            Product2 prod2 = TestDataUtils.createProduct2('Test SSD Prod');
            prod2.isActive = TRUE;
            prod2.CurrencyISOCode = 'USD';
            insert prod2;

            Id standardPBId = Test.getStandardPricebookId();
            Pricebook2 standardPB = new Pricebook2(Name='Standard Price Book', isActive=true);
            insert standardPB; 

            //CREATE PRICEBOOK ENTRY RECORD
            PricebookEntry priceEntry2 = TestDataUtils.createPriceBookEntry(1000, prod2.Id);
            priceEntry2.CurrencyISOCode = 'USD';
            priceEntry2.pricebook2id = standardPBid;
            priceEntry2.UseStandardPrice = false;
            insert priceEntry2;

            PricebookEntry priceEntry = TestDataUtils.createPriceBookEntry(10000, prod2.Id);
            priceEntry.CurrencyISOCode = 'USD';
            priceEntry.pricebook2id = standardPB.id;
            priceEntry.UseStandardPrice = false;
            insert priceEntry;


            Opportunity testOppty2 = TestDataUtils.createOppty('Test Oppty', 'Client Engagement', div1.Id, 'SmartSource Direct');
            testOppty2.CurrencyISOCode = 'USD';
            insert testOppty2;
            
            testOppty2.pricebook2id = standardPB.id;
            update testOppty2;

            //Create product junction object
            Product_Junction__c testProdJunc2 = TestDataUtils.createProductJunction(prod2.Id, testProductLine2.Id);
            insert testProdJunc2;

             Rate_Card__c rateCardNAM2 = new Rate_Card__c(
                RecordTypeId = rateCardNAMRecordId,
                Product__c = prod2.Id,
                Product_Line__c = testProductLine2.Id);
            rateCardNAM2.CurrencyIsoCode = 'USD';
            insert rateCardNAM2;

            Pricing_Detail__c pd2 = new Pricing_Detail__c();
            pd2.name = 'test SSD PricingDetail';
            pd2.Rate_Card__c = rateCardNAM2.id;
            pd2.Price__c = 120;
            insert pd2;

            ApexPages.currentPage().getParameters().put('addTo', testOppty2.Id);
            ManageProducts controller2 = new ManageProducts();
            
            controller2.productList[0].isSelected = true;
            controller2.productList[0].proJunc = testProdJunc2;
            //controller.getRateCards();
            controller2.processedSelectedTable();
            System.debug('');
            controller2.selectedProducts[0].Component = 'Test Prod';
            controller2.selectedProducts[0].Circulation = '12';

            controller2.selectedProducts[0].pricingOptions[0].quantityValue = 5;
            controller2.selectedProducts[0].pricingOptions[0].customPrice = '1000';
            controller2.selectedProducts[0].pricingOptions[0].selectedProductType = 'test SSD PricingDetail';
            controller2.selectedProducts[0].pricingOptions[0].GetInformation();
            ApexPages.currentPage().getParameters().put('thisPro',testProdJunc2.id);
            controller2.addAdditionalProductSection();
            controller2.selectedProducts[1].pricingOptions[0].customPrice = '100';
            controller2.selectedProducts[1].Component = 'Test Prod';
            controller2.selectedProducts[1].Circulation = '10';
            controller2.selectedProducts[1].pricingOptions[0].quantityValue = 5;
            controller2.selectedProducts[1].pricingOptions[0].selectedProductType = 'test SSD PricingDetail';
            //controller2.productList[1].pricingOptions[0].GetInformation();
            
            controller2.saveOpportunityProduct();
            system.debug('**********TEST METHOD CHECKPOINT: AFTER SAVE OPPORTUNITY PRODUCT **********');
            Integer olicount = [select count() from OpportunityLineItem where opportunityid=:testOppty2.id ];
            System.debug('Integer count: ' + olicount); 
            system.assert(olicount > 0 );
            controller2.selectedProducts[0].Component = '--None--';
            controller2.selectedProducts[0].pricingOptions[0].GetInformation();
            controller2.saveOpportunityProduct();

        }
    }
}