@isTest
private class CreateRelatedOpportunityTest {
	
	@isTest static void CreateRelatedOppSaveTest() {
		// Implement test code
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		user1.FederationIdentifier = 'tLastName';
		insert user1;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		user1.FederationIdentifier = 'secondUser2';
		insert secondUser;

		System.runAs(secondUser){

			Account corp1 = TestDataUtils.createCorpAccount('Nestle Corp');
			insert corp1;

			//Create Division Account record
			Account div1 = TestDataUtils.createDivAccount('nestle Div', corp1.Id);
			insert div1;

			//then an opportunity with lookup to the account
			Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', div1.Id, 'FSI');
			insert testOppty;

			Opportunity testOppty2 = TestDataUtils.createOppty('Test Oppty2', 'Contract', div1.Id, 'FSI');
			insert testOppty2;

			ApexPages.currentPage().getParameters().put('addTo', testOppty.Id);
            CreateRelatedOpportunity controller = new CreateRelatedOpportunity();

            controller.newLinkedOpportunity.Linked_Opportunity__c = testOppty2.Id;
            controller.saveLinkedOpportunity();

            Linked_Opportunity__c testLink = [Select Id, Related_Opportunity__r.Name, Linked_Opportunity__r.Name From Linked_Opportunity__c 
            										Where Related_Opportunity__r.Name like 'Test Oppty'];
            										
            System.assertEquals('Test Oppty', testLink.Related_Opportunity__r.Name);
            controller.cancelButton();
		}
	}
	
	
}