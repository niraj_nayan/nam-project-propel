@isTest
private class DoubleDipCommissionTest {
	
	@isTest static void DoubleDipCommissionSubmitButton() {
		// Implement test code
		User user1 = TestDataUtils.createUser('Test Last Name', 'no-reply@email.com', '1231231234');
		user1.FederationIdentifier = 'tLastName';
		insert user1;

		User secondUser = TestDataUtils.createUser('Second User', 'no-reply@reply.com', '1231234');
		secondUser.ProfileId = [Select Id, Name From Profile Where Name = 'System Administrator' Limit 1].Id;
		user1.FederationIdentifier = 'secondUser2';
		insert secondUser;

		System.runAs(secondUser){

			Account nestleCorp = TestDataUtils.createCorpAccount('Nestle Corp');
			insert nestleCorp;

			//Create Division Account record
			Account nestleDivision = TestDataUtils.createDivAccount('nestle Div', nestleCorp.Id);
			insert nestleDivision;

			//then an opportunity with lookup to the account
			Opportunity testOppty = TestDataUtils.createOppty('Test Oppty', 'Contract', nestleDivision.Id, 'FSI');
			insert testOppty;

			//add in the Opportunity Team Member to the Opportunity
			OpportunityTeamMember testTeamMember = TestDataUtils.createTeamMember(user1.Id, 'Manager', testOppty.Id, 10);
			insert testTeamMember;

			update testTeamMember;
			ApexPages.currentPage().getParameters().put('addTo', testOppty.Id);
			DoubleDipCommission controller = new DoubleDipCommission();


			//Making a double dip commission with value greater than 0
			for(DoubleDipCommission.DoubleDipUsers testUser: controller.teamMembers){
				testUser.dipCommission = 10;
			}

			controller.submitButton();

			System.assertEquals([Select Id, Sales_Person__r.Id, Opportunity__c, Commission__c From Form_Detail__c where Sales_Person__r.Id =:user1.Id].Commission__c, 10);

			controller.cancelButton();
		}
	}
	
}